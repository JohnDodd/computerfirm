package tusur.kkv.data.base

interface TimestampProvider {

    fun currentTimeMillis(): Long

}