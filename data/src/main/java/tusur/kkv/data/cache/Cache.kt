package tusur.kkv.data.cache

import com.annimon.stream.Optional
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Scheduler
import tusur.kkv.data.base.TimestampProvider
import tusur.kkv.data.injection.qualifier.ForComputation
import tusur.kkv.data.store.Store
import java.util.concurrent.ConcurrentHashMap
import javax.inject.Inject

class Cache<Key, Value>
@Inject constructor(private val timestampProvider: TimestampProvider,
                    private val extractKeyFromModel: (Value) -> Key,
                    private val itemLifespanMs: Optional<Long> = Optional.empty(),
                    @ForComputation
                    private val schedulerComputation: Scheduler) : Store.MemoryStore<Key, Value> {

    private val cache = ConcurrentHashMap<Key, CacheEntry<Value>>()

    override fun all(): Maybe<List<Value>> {
        return Observable.fromIterable(cache.values)
                .subscribeOn(schedulerComputation)
                .filter { notExpired(it) }
                .map { it.value }
                .toList()
                .filter { it.isNotEmpty() }
    }

    override fun several(keys: List<Key>): Maybe<List<Value>> {
        return Observable.fromIterable(keys.asSequence().map { cache[it] }.filter { it != null }.toList())
                .subscribeOn(schedulerComputation)
                .filter { notExpired(it) }
                .map { it.value }
                .toList()
                .filter { it.isNotEmpty() }
    }

    override fun singular(key: Key): Maybe<Value> {
        return Maybe.fromCallable { cache.containsKey(key) }
                .subscribeOn(schedulerComputation)
                .filter { isPresent -> isPresent }
                .map { cache[key] }
                .filter { notExpired(it) }
                .map { it.value }
    }

    override fun putAll(values: List<Value>) {
        cache.putAll(values.map {
            Pair(extractKeyFromModel(it), createCacheEntry(it))
        }.toMap())
    }

    override fun putSingular(value: Value) {
        cache[extractKeyFromModel(value)] = createCacheEntry(value)
    }

    override fun deleteSingular(key: Key) {
        cache.remove(key)
    }

    override fun clear() {
        cache.clear()
    }

    private fun createCacheEntry(value: Value): CacheEntry<Value> {
        return CacheEntry(value, timestampProvider.currentTimeMillis())
    }

    private fun notExpired(cacheEntry: CacheEntry<Value>): Boolean {
        return itemLifespanMs
                .map { lifespanMs -> cacheEntry.creationTimestamp + lifespanMs > timestampProvider.currentTimeMillis() }
                .orElseGet { true }
    }

}