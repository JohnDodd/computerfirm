package tusur.kkv.data.cache

data class CacheEntry<T>(val value: T, val creationTimestamp: Long)