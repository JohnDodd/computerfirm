package tusur.kkv.data.database.base

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import dagger.Lazy
import tusur.kkv.data.injection.qualifier.ForApplication
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DatabaseGateway
@Inject constructor(@ForApplication context: Context)
    : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {
        const val DATABASE_VERSION = 1
        const val DATABASE_NAME = "Firm.db"
    }

    @Inject
    lateinit var tables: Lazy<Tables>

    override fun onCreate(sqLiteDatabase: SQLiteDatabase) {
        tables.get().value.forEach { it.onCreate(sqLiteDatabase) }
    }

    override fun onUpgrade(sqLiteDatabase: SQLiteDatabase, i: Int, i1: Int) {
        tables.get().value.forEach { it.onUpgrade(sqLiteDatabase, i, i1) }
        onCreate(sqLiteDatabase)
    }

    class QueryBuilder(private val mTableName: String, private val gateway: DatabaseGateway)
        : IQueryBuilder {

        private val mContentValues = ContentValues()
        private var mColumns: Array<String>? = null
        private var mSelection: String? = null
        private var mSelectionArgs: Array<String>? = null
        private var mGroupBy: String? = null
        private var mHaving: String? = null
        private var mOrderBy: String? = null
        private val dateFormat by lazy { SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()) }

        private val writableDatabase get() = gateway.writableDatabase
        private val readableDatabase get() = gateway.readableDatabase

        override fun put(key: String, value: String?): QueryBuilder {
            mContentValues.put(key, value)
            return this
        }

        override fun put(key: String, value: Long?): QueryBuilder {
            mContentValues.put(key, value)
            return this
        }

        override fun put(key: String, date: Date?): QueryBuilder {
            return put(key, dateFormat.format(date))
        }

        override fun update(): Long {
            return update(mSelection, mSelectionArgs)
        }

        override fun update(whereClause: String?, vararg whereArgs: Any?): Long {
            return writableDatabase.update(
                    mTableName,
                    mContentValues,
                    whereClause,
                    whereArgs.map { it.toString() }.toTypedArray()
            ).toLong()
        }

        override fun delete(): Long {
            return writableDatabase.delete(
                    mTableName,
                    mSelection,
                    mSelectionArgs
            ).toLong()
        }

        override fun delete(whereClause: String?, vararg whereArgs: Any?): Long {
            TODO("Not implemented")
            return writableDatabase.delete(
                    mTableName,
                    whereClause,
                    whereArgs.map { it.toString() }.toTypedArray()
            ).toLong()
        }

        override fun insert(): Long {
            return writableDatabase.insert(mTableName, null, mContentValues)
        }

        override fun columns(vararg columns: Any): QueryBuilder {
            mColumns = columns.map { it.toString() }.toTypedArray()
            return this
        }

        override fun selection(selection: String): QueryBuilder {
            mSelection = selection
            return this
        }

        override fun selection(selection: String, selectionArgs: List<Any>?): QueryBuilder {
            mSelection = selection
            selectionArgs(selectionArgs)
            return this
        }

        override fun selectionArgs(selectionArgs: List<Any>?): QueryBuilder {
            mSelectionArgs = selectionArgs?.map { it.toString() }?.toTypedArray()
            return this
        }

        override fun groupBy(groupBy: String): QueryBuilder {
            mGroupBy = groupBy
            return this
        }

        override fun having(having: String): QueryBuilder {
            mHaving = having
            return this
        }

        override fun orderBy(orderBy: String): QueryBuilder {
            mOrderBy = orderBy
            return this
        }

        override fun get(): Cursor {
            return readableDatabase.query(mTableName,
                    mColumns,
                    mSelection,
                    mSelectionArgs,
                    mGroupBy,
                    mHaving,
                    mOrderBy)
        }

        override fun rawQuery(sql: String): Cursor {
            return readableDatabase.rawQuery(sql, null)
        }
    }

}