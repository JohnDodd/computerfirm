package tusur.kkv.data.database.base

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DateFormatter @Inject constructor() {
    private val regex = Regex.fromLiteral(BIRTHDATE_DB_FORMAT)
    private val clientDateFormat = SimpleDateFormat(BIRTHDATE_CLIENT_FORMAT, Locale.getDefault())
    private val serverDateFormat = SimpleDateFormat(BIRTHDATE_DB_FORMAT, Locale.getDefault())

    fun matchesServer(serverDate: String) =
            try {
                serverDateFormat.parse(serverDate)
                true
            } catch (e: ParseException) {
                false
            }


    fun parseFromViewDate(viewDate: String?) =
            viewDate?.let {
                val parse = clientDateFormat.parse(it)
                serverDateFormat.format(parse)
            }

    fun formatToViewDate(date: Date): String =
            clientDateFormat.format(date)

    fun parseFromServerDate(serverDate: String?): String? =
            serverDate?.let {
                val parse = serverDateFormat.parse(it)
                clientDateFormat.format(parse)
            }

    fun formatToServerDate(date: Date): String =
            serverDateFormat.format(date)

    companion object {
        const val BIRTHDATE_DB_FORMAT = "yyyy-MM-dd HH:mm:ss"
        const val BIRTHDATE_CLIENT_FORMAT = "dd MMM yyyy"
    }

}