package tusur.kkv.data.database.base

import android.database.Cursor
import java.util.*

interface IQueryBuilder {
    fun put(key: String, value: String?): IQueryBuilder

    fun put(key: String, value: Long?): IQueryBuilder

    fun put(key: String, date: Date?): IQueryBuilder

    fun update(): Long

    fun update(whereClause: String?, vararg whereArgs: Any?): Long

    fun delete(): Long

    fun delete(whereClause: String?, vararg whereArgs: Any?): Long

    fun insert(): Long

    fun columns(vararg columns: Any): IQueryBuilder

    fun selection(selection: String): IQueryBuilder

    fun selection(selection: String, selectionArgs: List<Any>?): IQueryBuilder

    fun selectionArgs(selectionArgs: List<Any>?): IQueryBuilder

    fun groupBy(groupBy: String): IQueryBuilder

    fun having(having: String): IQueryBuilder

    fun orderBy(orderBy: String): IQueryBuilder

    fun get(): Cursor

    fun rawQuery(sql: String): Cursor
}