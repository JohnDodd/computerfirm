package tusur.kkv.data.database.base

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Scheduler
import io.reactivex.Single
import tusur.kkv.data.database.mapper.CursorListMapper
import tusur.kkv.data.database.mapper.RawFactory
import tusur.kkv.data.injection.qualifier.ForComputation
import tusur.kkv.data.store.JoinDiskService

abstract class JoinTableDiskService<Key1, Key2, Raw>(queryable: Queryable,
                                                     @ForComputation
                                                     protected val schedulerComputation: Scheduler,
                                                     rawFactory: RawFactory<Raw>) :
        Table(queryable),
        JoinDiskService<Key1, Key2, Raw> {

    protected abstract val key1id: String
    protected abstract val key2id: String

    private val cursorListMapper = CursorListMapper(rawFactory)

    override fun allByFirst(key: Key1): Maybe<List<Raw>> {
        return Single.fromCallable {
            builder()
                    .selection("$key1id = ?", listOf<Any>(key!!))
                    .get()
        }
                .subscribeOn(schedulerComputation)
                .flatMapMaybe(cursorListMapper)
    }

    override fun allBySecond(key: Key2): Maybe<List<Raw>> {
        return Single.fromCallable {
            builder()
                    .selection("$key2id = ?", listOf<Any>(key!!))
                    .get()
        }
                .subscribeOn(schedulerComputation)
                .flatMapMaybe(cursorListMapper)
    }

    override fun putSingular(key1: Key1, key2: Key2): Single<Long> {
        return Single.fromCallable {
            putSingularBuilder(key1, key2).insert()
        }
                .subscribeOn(schedulerComputation)
    }

    override fun deleteByFirst(vararg keys: Key1): Completable {
        return Completable.fromAction {
            builder()
                    .selection("$key1id in " +
                               keys.joinToString(prefix = "(", postfix = ")"))
                    .delete()
        }
                .subscribeOn(schedulerComputation)
    }

    override fun deleteBySecond(vararg keys: Key2): Completable {
        return Completable.fromAction {
            builder()
                    .selection("$key2id in ?",
                            listOf(keys.joinToString(prefix = "(", postfix = ")")))
                    .delete()
        }
                .subscribeOn(schedulerComputation)
    }

    override fun deleteSingular(key1: Key1, key2: Key2): Completable {
        return Completable.fromAction {
            builder()
                    .selection("$key1id = ? and $key2id = ?", listOf<Any>(key1!!, key2!!))
                    .delete()
        }
                .subscribeOn(schedulerComputation)
    }


    protected fun putSingularBuilder(key1: Key1, key2: Key2) =
            builder()
                    .put(key1id, key1.toString())
                    .put(key2id, key2.toString())
}