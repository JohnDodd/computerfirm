package tusur.kkv.data.database.base

interface Queryable {
    fun create(tableName: String): IQueryBuilder
}