package tusur.kkv.data.database.base

import io.reactivex.Scheduler
import tusur.kkv.data.database.base.Table.TableBuilder.SimpleInstruction.Type.INTEGER
import tusur.kkv.data.database.mapper.RawFactory
import tusur.kkv.data.injection.qualifier.ForComputation
import tusur.kkv.data.model.JoinRaw

abstract class SimpleJoinTableDiskService(queryable: Queryable,
                                          @ForComputation schedulerComputation: Scheduler,
                                          rawFactory: RawFactory<JoinRaw>) :
        JoinTableDiskService<Long, Long, JoinRaw>(queryable, schedulerComputation, rawFactory) {

    override val createTableBuilder
        get() = tableBuilder {
            property(key1id) { simple(INTEGER) }
            property(key2id) { simple(INTEGER) }
            property("") { primaryKey(key1id, key2id) }
        }
}