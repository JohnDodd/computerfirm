package tusur.kkv.data.database.base

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Scheduler
import io.reactivex.Single
import tusur.kkv.data.database.mapper.CursorListMapper
import tusur.kkv.data.database.mapper.CursorMapper
import tusur.kkv.data.database.mapper.RawFactory
import tusur.kkv.data.injection.qualifier.ForComputation

abstract class SimpleTableDiskService<Raw, Params>(queryable: Queryable,
                                                   @ForComputation schedulerComputation: Scheduler,
                                                   rawFactory: RawFactory<Raw>) :
        TableDiskService<Long, Raw, Params>(queryable, schedulerComputation, rawFactory) {

    protected abstract val keyId: String

    override fun several(keys: List<Long>): Maybe<List<Raw>> {
        return Single.fromCallable {
            builder()
                    .selection("$keyId in ${keys.toString().replace("[", "(").replace("]", ")")}")
                    .get()
        }
                .subscribeOn(schedulerComputation)
                .flatMapMaybe(CursorListMapper(rawFactory))
    }

    override fun singular(key: Long): Maybe<Raw> {
        return Single.fromCallable {
            builder()
                    .selection("$keyId = ?", listOf<Any>(key))
                    .get()
        }
                .subscribeOn(schedulerComputation)
                .flatMapMaybe(CursorMapper(rawFactory))
    }


    override fun putSingular(params: Params): Single<Long> {
        return Single.fromCallable {
            putSingularBuilder(params).insert()
        }
                .subscribeOn(schedulerComputation)
    }

    override fun replaceSingular(key: Long, params: Params): Completable {
        return Completable.fromAction {
            replaceSingularBuilder(key, params).update("$keyId = ?", key)
        }
                .subscribeOn(schedulerComputation)
    }

    override fun deleteSingular(key: Long): Completable {
        return Completable.fromAction {
            builder()
                    .selection("$keyId = ?", listOf<Any>(key))
                    .delete()
        }
                .subscribeOn(schedulerComputation)
    }

    override fun replaceSingularBuilder(key: Long, params: Params) =
            putSingularBuilder(params).put(keyId, key)

}