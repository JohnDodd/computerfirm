package tusur.kkv.data.database.base

import android.database.sqlite.SQLiteDatabase

abstract class Table(private val queryable: Queryable) {

    fun onCreate(sqLiteDatabase: SQLiteDatabase) {
        sqLiteDatabase.execSQL(createTableBuilder.build())
    }

    fun onUpgrade(sqLiteDatabase: SQLiteDatabase, i: Int, i1: Int) {
        sqLiteDatabase.execSQL("drop table if exists $tableName")
    }

    protected abstract val tableName: String

    protected abstract val createTableBuilder: TableBuilder

    protected fun builder() = queryable.create(tableName)

    protected fun tableBuilder(init: TableBuilder.() -> Unit): TableBuilder {
        val builder = TableBuilder(tableName)
        builder.init()
        return builder
    }

    class TableBuilder(private val tableName: String) {
        private val properties = mutableListOf<Property>()

        fun property(key: String, inst: Property.() -> Unit): Property {
            val property = Property(key)
            property.inst()
            properties.add(property)
            return property
        }

        fun foreignKey(key: String, foreignTable: String, foreignKey: String): Property {
            val property = ForeignKeyProperty(key, foreignTable, foreignKey)
            properties.add(property)
            return property
        }

        fun build(): String {
            val builder = StringBuilder()
            builder.append("create table $tableName(")
            properties.forEach {
                builder.append("${it.key} ")
                it.instructions.forEach { inst ->
                    builder.append(inst.value)
                            .append(" ")
                }
                if (builder.endsWith(" "))
                    builder.deleteCharAt(builder.length - 1)
                builder.append(", ")
            }
            if (builder.endsWith(", ")) {
                builder.deleteCharAt(builder.length - 1)
                builder.deleteCharAt(builder.length - 1)
            }
            builder.append(")")
            return builder.toString()
        }

        open class Property(val key: String?) {
            val instructions = mutableListOf<Instruction>()

            fun simple(vararg types: SimpleInstruction.Type) {
                instructions.add(SimpleInstruction(*types))
            }

            fun primaryKey(vararg keys: String) {
                instructions.add(PrimaryKeyInstruction(*keys))
            }
        }

        private class ForeignKeyProperty(key: String,
                                         foreignTable: String,
                                         foreignKey: String)
            : Property("FOREIGN KEY ($key) REFERENCES $foreignTable($foreignKey)")

        abstract class Instruction(val value: String)
        class SimpleInstruction(vararg types: Type) : Instruction(StringBuilder()
                .let { builder ->
                    types.forEach { builder.append("${it.type} ") }
                    builder.deleteCharAt(builder.length - 1)
                    builder.toString()
                }) {
            enum class Type(val type: String) {
                TEXT("text"),
                INTEGER("integer"),
                PRIMARY_KEY("primary key"),
                AUTOINCREMENT("autoincrement"),
                DATETIME("datetime"),
                DEFAULT("default"),
                CURRENT_TIME("current_timestamp")
            }
        }

        private class PrimaryKeyInstruction(vararg keys: String) : Instruction(StringBuilder()
                .let { builder ->
                    builder.append("primary key (")
                    keys.forEach { builder.append(it).append(", ") }
                    builder.deleteCharAt(builder.length - 1)
                    builder.deleteCharAt(builder.length - 1)
                    builder.append(")")
                    builder.toString()
                })
    }

}