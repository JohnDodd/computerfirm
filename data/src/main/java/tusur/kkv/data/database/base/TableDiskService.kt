package tusur.kkv.data.database.base

import android.database.Cursor
import io.reactivex.*
import tusur.kkv.data.database.mapper.CursorListMapper
import tusur.kkv.data.database.mapper.RawFactory
import tusur.kkv.data.injection.qualifier.ForComputation
import tusur.kkv.data.store.DiskService

abstract class TableDiskService<Key, Raw, Params>(queryable: Queryable,
                                                  @ForComputation
                                                  protected val schedulerComputation: Scheduler,
                                                  protected val rawFactory: RawFactory<Raw>) :
        Table(queryable),
        DiskService<Key, Raw, Params> {

    abstract fun putSingularBuilder(params: Params): IQueryBuilder

    abstract fun replaceSingularBuilder(key: Key, params: Params): IQueryBuilder

    override fun all(): Maybe<List<Raw>> {
        return Single.fromCallable { builder().get() }
                .subscribeOn(schedulerComputation)
                .flatMapMaybe(CursorListMapper(rawFactory))
    }

    override fun putAll(paramsList: List<Params>): Single<List<Raw>> {
        return Observable.fromIterable(paramsList)
                .flatMapSingle { putSingular(it) }
                .toList()
                .flatMapMaybe { several(it) }
                .toSingle()
    }

    override fun clear(): Completable {
        return Completable.fromAction { builder().delete() }
                .subscribeOn(schedulerComputation)
    }

    override fun search(rawQuery: String): Maybe<List<Raw>> {
        return Single.fromCallable {
            builder().rawQuery(rawQuery)
        }
                .subscribeOn(schedulerComputation)
                .flatMapMaybe(CursorListMapper(rawFactory))
    }

    override fun searchCursor(rawQuery: String): Single<Cursor> {
        return Single.fromCallable {
            builder().rawQuery(rawQuery)
        }
    }

}