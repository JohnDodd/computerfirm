package tusur.kkv.data.database.base

import javax.inject.Inject

data class Tables @Inject constructor(val value: List<Table>)