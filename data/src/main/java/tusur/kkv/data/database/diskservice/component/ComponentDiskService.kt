package tusur.kkv.data.database.diskservice.component

import io.reactivex.Scheduler
import tusur.kkv.data.database.base.Queryable
import tusur.kkv.data.database.base.SimpleTableDiskService
import tusur.kkv.data.database.base.Table.TableBuilder.SimpleInstruction.Type.*
import tusur.kkv.data.database.diskservice.componenttype.ComponentTypeDiskService
import tusur.kkv.data.database.diskservice.purveyor.PurveyorDiskService
import tusur.kkv.data.database.mapper.RawFactory
import tusur.kkv.data.injection.qualifier.ForComputation
import tusur.kkv.data.model.ComponentRaw
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ComponentDiskService
@Inject constructor(queryable: Queryable,
                    @ForComputation schedulerComputation: Scheduler,
                    rawFactory: RawFactory<ComponentRaw>)
    : SimpleTableDiskService<ComponentRaw, ComponentParams>(queryable,
        schedulerComputation,
        rawFactory) {

    override val tableName = TABLE_NAME
    override val keyId = KEY_ID

    override fun putSingularBuilder(params: ComponentParams) =
            builder()
                    .put(KEY_COST, params.cost)
                    .put(KEY_NAME, params.name)
                    .put(KEY_GUARANTEE_PERIOD, params.guaranteePeriod)
                    .put(KEY_PURVEYOR_ID, params.purveyorId)
                    .put(KEY_COMPONENT_TYPE_ID, params.typeId)

    override val createTableBuilder = tableBuilder {
        property(KEY_ID) { simple(INTEGER, PRIMARY_KEY, AUTOINCREMENT) }
        property(KEY_GUARANTEE_PERIOD) { simple(INTEGER) }
        property(KEY_NAME) { simple(TEXT) }
        property(KEY_COST) { simple(INTEGER) }
        property(KEY_COMPONENT_TYPE_ID) { simple(INTEGER) }
        property(KEY_PURVEYOR_ID) { simple(INTEGER) }
        foreignKey(KEY_COMPONENT_TYPE_ID,
                ComponentTypeDiskService.TABLE_NAME,
                ComponentTypeDiskService.KEY_ID)
        foreignKey(KEY_PURVEYOR_ID, PurveyorDiskService.TABLE_NAME, PurveyorDiskService.KEY_ID)
    }

    companion object {
        const val TABLE_NAME = "Components"
        const val KEY_ID = "_id"
        const val KEY_COMPONENT_TYPE_ID = "componentTypeId"
        const val KEY_PURVEYOR_ID = "purveyorId"
        const val KEY_GUARANTEE_PERIOD = "guaranteePeriod"
        const val KEY_NAME = "name"
        const val KEY_COST = "cost"
    }

}