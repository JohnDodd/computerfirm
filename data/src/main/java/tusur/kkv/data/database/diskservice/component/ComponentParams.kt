package tusur.kkv.data.database.diskservice.component

data class ComponentParams(val typeId: Long,
                           val guaranteePeriod: Long?,
                           val name: String,
                           val cost: Long,
                           val purveyorId: Long)