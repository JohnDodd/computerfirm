package tusur.kkv.data.database.diskservice.componenttype

import io.reactivex.Scheduler
import tusur.kkv.data.database.base.Queryable
import tusur.kkv.data.database.base.SimpleTableDiskService
import tusur.kkv.data.database.base.Table.TableBuilder.SimpleInstruction.Type.*
import tusur.kkv.data.database.mapper.RawFactory
import tusur.kkv.data.injection.qualifier.ForComputation
import tusur.kkv.data.model.ComponentTypeRaw
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ComponentTypeDiskService
@Inject constructor(queryable: Queryable,
                    @ForComputation schedulerComputation: Scheduler,
                    rawFactory: RawFactory<ComponentTypeRaw>)
    : SimpleTableDiskService<ComponentTypeRaw, ComponentTypeParams>(queryable,
        schedulerComputation,
        rawFactory) {

    override val keyId = KEY_ID

    override val tableName = TABLE_NAME

    override fun putSingularBuilder(params: ComponentTypeParams) =
            builder()
                    .put(KEY_NAME, params.name)
                    .put(KEY_SPECIFICATIONS, params.specifications)
                    .put(KEY_MANUFACTURER, params.manufacturer)

    override val createTableBuilder = tableBuilder {
        property(KEY_ID) { simple(INTEGER, PRIMARY_KEY, AUTOINCREMENT) }
        property(KEY_NAME) { simple(TEXT) }
        property(KEY_SPECIFICATIONS) { simple(TEXT) }
        property(KEY_MANUFACTURER) { simple(TEXT) }
    }


    companion object {
        const val TABLE_NAME = "ComponentTypes"
        const val KEY_ID = "_id"
        const val KEY_NAME = "name"
        const val KEY_SPECIFICATIONS = "specifications"
        const val KEY_MANUFACTURER = "manufacturer"
    }

}