package tusur.kkv.data.database.diskservice.componenttype

data class ComponentTypeParams(val name: String,
                               val specifications: String,
                               val manufacturer: String)