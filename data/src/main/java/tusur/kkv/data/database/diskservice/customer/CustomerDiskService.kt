package tusur.kkv.data.database.diskservice.customer

import io.reactivex.Scheduler
import tusur.kkv.data.database.base.Queryable
import tusur.kkv.data.database.base.SimpleTableDiskService
import tusur.kkv.data.database.base.Table.TableBuilder.SimpleInstruction.Type.*
import tusur.kkv.data.database.mapper.RawFactory
import tusur.kkv.data.injection.qualifier.ForComputation
import tusur.kkv.data.model.CustomerRaw
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CustomerDiskService
@Inject constructor(queryable: Queryable,
                    @ForComputation schedulerComputation: Scheduler,
                    rawFactory: RawFactory<CustomerRaw>) :
        SimpleTableDiskService<CustomerRaw, CustomerParams>(queryable,
                schedulerComputation,
                rawFactory) {

    override val tableName = TABLE_NAME
    override val keyId = KEY_ID

    override val createTableBuilder = tableBuilder {
        property(KEY_ID) { simple(INTEGER, PRIMARY_KEY, AUTOINCREMENT) }
        property(KEY_NAME) { simple(TEXT) }
        property(KEY_EMAIL) { simple(TEXT) }
        property(KEY_PHONE_NUMBER) { simple(TEXT) }
    }

    override fun putSingularBuilder(params: CustomerParams) =
            builder()
                    .put(KEY_NAME, params.fullName)
                    .put(KEY_EMAIL, params.email)
                    .put(KEY_PHONE_NUMBER, params.phoneNumber)

    companion object {
        const val TABLE_NAME = "Customers"
        const val KEY_ID = "_id"
        const val KEY_NAME = "name"
        const val KEY_EMAIL = "email"
        const val KEY_PHONE_NUMBER = "phoneNumber"
    }
}