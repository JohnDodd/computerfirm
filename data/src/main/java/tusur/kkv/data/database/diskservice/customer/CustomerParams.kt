package tusur.kkv.data.database.diskservice.customer

data class CustomerParams(val fullName: String,
                          val email: String?,
                          val phoneNumber: String)