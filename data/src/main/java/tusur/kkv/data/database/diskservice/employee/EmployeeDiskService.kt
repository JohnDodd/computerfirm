package tusur.kkv.data.database.diskservice.employee

import io.reactivex.Scheduler
import tusur.kkv.data.database.base.Queryable
import tusur.kkv.data.database.base.SimpleTableDiskService
import tusur.kkv.data.database.base.Table.TableBuilder.SimpleInstruction.Type.*
import tusur.kkv.data.database.diskservice.position.PositionDiskService
import tusur.kkv.data.database.mapper.RawFactory
import tusur.kkv.data.injection.qualifier.ForComputation
import tusur.kkv.data.model.EmployeeRaw
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class EmployeeDiskService
@Inject constructor(queryable: Queryable,
                    @ForComputation schedulerComputation: Scheduler,
                    rawFactory: RawFactory<EmployeeRaw>) :
        SimpleTableDiskService<EmployeeRaw, EmployeeParams>(queryable,
                schedulerComputation,
                rawFactory) {

    override val tableName = TABLE_NAME
    override val keyId = KEY_ID

    override fun putSingularBuilder(params: EmployeeParams) =
            builder()
                    .put(KEY_FULL_NAME, params.fullName)
                    .put(KEY_ADDRESS, params.address)
                    .put(KEY_BIRTHDATE, params.birthDate)
                    .put(KEY_PASSPORT_DATA, params.passportData)
                    .put(KEY_PHONE_NUMBER, params.phoneNumber)
                    .put(KEY_SEX, params.gender)
                    .put(KEY_POSITION_ID, params.positionId)

    override val createTableBuilder = tableBuilder {
        property(KEY_ID) { simple(INTEGER, PRIMARY_KEY, AUTOINCREMENT) }
        property(KEY_PASSPORT_DATA) { simple(TEXT) }
        property(KEY_FULL_NAME) { simple(TEXT) }
        property(KEY_BIRTHDATE) { simple(TEXT) }
        property(KEY_SEX) { simple(TEXT) }
        property(KEY_ADDRESS) { simple(TEXT) }
        property(KEY_PHONE_NUMBER) { simple(TEXT) }
        property(KEY_POSITION_ID) { simple(INTEGER) }
        foreignKey(KEY_POSITION_ID, PositionDiskService.TABLE_NAME, PositionDiskService.KEY_ID)
    }

    companion object {
        const val TABLE_NAME = "Employees"
        const val KEY_ID = "_id"
        const val KEY_FULL_NAME = "fullName"
        const val KEY_BIRTHDATE = "birthdate"
        const val KEY_SEX = "gender"
        const val KEY_ADDRESS = "address"
        const val KEY_PHONE_NUMBER = "phoneNumber"
        const val KEY_PASSPORT_DATA = "passportData"
        const val KEY_POSITION_ID = "positionId"

        fun allKeys() = listOf(
                KEY_ID,
                KEY_FULL_NAME,
                KEY_BIRTHDATE,
                KEY_SEX,
                KEY_ADDRESS,
                KEY_PHONE_NUMBER,
                KEY_PASSPORT_DATA,
                KEY_POSITION_ID
        )
    }
}