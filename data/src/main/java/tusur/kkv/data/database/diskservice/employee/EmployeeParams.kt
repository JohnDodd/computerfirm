package tusur.kkv.data.database.diskservice.employee

data class EmployeeParams(val fullName: String,
                          val birthDate: String,
                          val gender: String?,
                          val address: String,
                          val phoneNumber: String,
                          val passportData: String,
                          val positionId: Long)