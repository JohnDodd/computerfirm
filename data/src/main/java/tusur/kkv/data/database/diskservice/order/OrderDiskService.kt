package tusur.kkv.data.database.diskservice.order

import io.reactivex.Scheduler
import tusur.kkv.data.database.base.Queryable
import tusur.kkv.data.database.base.SimpleTableDiskService
import tusur.kkv.data.database.base.Table.TableBuilder.SimpleInstruction.Type.*
import tusur.kkv.data.database.diskservice.customer.CustomerDiskService
import tusur.kkv.data.database.diskservice.employee.EmployeeDiskService
import tusur.kkv.data.database.mapper.RawFactory
import tusur.kkv.data.injection.qualifier.ForComputation
import tusur.kkv.data.model.OrderRaw
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class OrderDiskService
@Inject constructor(queryable: Queryable,
                    @ForComputation schedulerComputation: Scheduler,
                    rawFactory: RawFactory<OrderRaw>) :
        SimpleTableDiskService<OrderRaw, OrderParams>(queryable, schedulerComputation, rawFactory) {

    override val tableName = TABLE_NAME
    override val keyId = KEY_ID

    override fun putSingularBuilder(params: OrderParams) =
            builder()
                    .put(KEY_CUSTOMER_ID, params.customerId)
                    .put(KEY_EMPLOYEE_ID, params.employeeId)
                    .put(KEY_EXECUTION_DATE, params.executionDate)
                    .put(KEY_REGISTRATION_DATE, params.registrationDate)

    override val createTableBuilder = tableBuilder {
        property(KEY_ID) { simple(INTEGER, PRIMARY_KEY, AUTOINCREMENT) }
        property(KEY_REGISTRATION_DATE) { simple(DATETIME, DEFAULT, CURRENT_TIME) }
        property(KEY_EXECUTION_DATE) { simple(DATETIME) }
        property(KEY_CUSTOMER_ID) { simple(INTEGER) }
        property(KEY_EMPLOYEE_ID) { simple(INTEGER) }
        foreignKey(KEY_CUSTOMER_ID, CustomerDiskService.TABLE_NAME, CustomerDiskService.KEY_ID)
        foreignKey(KEY_EMPLOYEE_ID, EmployeeDiskService.TABLE_NAME, EmployeeDiskService.KEY_ID)
    }

    companion object {
        const val TABLE_NAME = "Orders"
        const val KEY_ID = "_id"
        const val KEY_REGISTRATION_DATE = "registrationDate"
        const val KEY_EXECUTION_DATE = "executionDate"
        const val KEY_CUSTOMER_ID = "customerId"
        const val KEY_EMPLOYEE_ID = "employeeId"
    }

}