package tusur.kkv.data.database.diskservice.order

data class OrderParams(val customerId: Long,
                       val employeeId: Long,
                       val executionDate: String?,
                       val registrationDate: String?)