package tusur.kkv.data.database.diskservice.ordercomponentjoin

import io.reactivex.Scheduler
import tusur.kkv.data.database.base.Queryable
import tusur.kkv.data.database.base.SimpleJoinTableDiskService
import tusur.kkv.data.database.mapper.OrderComponentJoinRawFactory
import tusur.kkv.data.injection.qualifier.ForComputation
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class OrderComponentJoinDiskService
@Inject constructor(queryable: Queryable,
                    @ForComputation schedulerComputation: Scheduler,
                    rawFactory: OrderComponentJoinRawFactory)
    : SimpleJoinTableDiskService(queryable, schedulerComputation, rawFactory) {

    override val key1id = KEY_ORDER_ID
    override val key2id = KEY_COMPONENT_ID
    override val tableName = TABLE_NAME

    companion object {
        const val TABLE_NAME = "OrdersComponents"
        const val KEY_ORDER_ID = "orderId"
        const val KEY_COMPONENT_ID = "componentId"
    }
}