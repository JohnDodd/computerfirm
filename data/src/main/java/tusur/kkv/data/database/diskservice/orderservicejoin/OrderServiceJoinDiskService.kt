package tusur.kkv.data.database.diskservice.orderservicejoin

import io.reactivex.Scheduler
import tusur.kkv.data.database.base.Queryable
import tusur.kkv.data.database.base.SimpleJoinTableDiskService
import tusur.kkv.data.database.mapper.OrderServiceJoinRawFactory
import tusur.kkv.data.injection.qualifier.ForComputation
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class OrderServiceJoinDiskService
@Inject constructor(queryable: Queryable,
                    @ForComputation schedulerComputation: Scheduler,
                    rawFactory: OrderServiceJoinRawFactory) :
        SimpleJoinTableDiskService(queryable, schedulerComputation, rawFactory) {

    override val key1id = KEY_ORDER_ID
    override val key2id = KEY_SERVICE_ID
    override val tableName = TABLE_NAME

    companion object {
        const val TABLE_NAME = "OrdersServices"
        const val KEY_ORDER_ID = "orderId"
        const val KEY_SERVICE_ID = "serviceId"
    }

}