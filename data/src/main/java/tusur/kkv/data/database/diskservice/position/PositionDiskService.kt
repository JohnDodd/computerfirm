package tusur.kkv.data.database.diskservice.position

import io.reactivex.Scheduler
import tusur.kkv.data.database.base.Queryable
import tusur.kkv.data.database.base.SimpleTableDiskService
import tusur.kkv.data.database.base.Table.TableBuilder.SimpleInstruction.Type.*
import tusur.kkv.data.database.mapper.RawFactory
import tusur.kkv.data.injection.qualifier.ForComputation
import tusur.kkv.data.model.PositionRaw
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PositionDiskService
@Inject constructor(queryable: Queryable,
                    @ForComputation schedulerComputation: Scheduler,
                    rawFactory: RawFactory<PositionRaw>) :
        SimpleTableDiskService<PositionRaw, PositionParams>(queryable,
                schedulerComputation,
                rawFactory) {

    override val tableName = TABLE_NAME
    override val keyId = KEY_ID

    override fun putSingularBuilder(params: PositionParams) =
            builder()
                    .put(KEY_NAME, params.name)
                    .put(KEY_SALARY, params.salary)
                    .put(KEY_DUTIES, params.duties)

    override val createTableBuilder = tableBuilder {
        property(KEY_ID) { simple(INTEGER, PRIMARY_KEY, AUTOINCREMENT) }
        property(KEY_NAME) { simple(TEXT) }
        property(KEY_SALARY) { simple(INTEGER) }
        property(KEY_DUTIES) { simple(TEXT) }
    }

    companion object {
        const val TABLE_NAME = "Positions"
        const val KEY_ID = "_id"
        const val KEY_NAME = "name"
        const val KEY_SALARY = "salary"
        const val KEY_DUTIES = "duties"
    }
}