package tusur.kkv.data.database.diskservice.position

data class PositionParams(val name: String,
                          val salary: Long?,
                          val duties: String)