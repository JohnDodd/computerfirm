package tusur.kkv.data.database.diskservice.purveyor

import io.reactivex.Scheduler
import tusur.kkv.data.database.base.Queryable
import tusur.kkv.data.database.base.SimpleTableDiskService
import tusur.kkv.data.database.base.Table.TableBuilder.SimpleInstruction.Type.*
import tusur.kkv.data.database.mapper.RawFactory
import tusur.kkv.data.injection.qualifier.ForComputation
import tusur.kkv.data.model.PurveyorRaw
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PurveyorDiskService
@Inject constructor(queryable: Queryable,
                    @ForComputation schedulerComputation: Scheduler,
                    rawFactory: RawFactory<PurveyorRaw>)
    : SimpleTableDiskService<PurveyorRaw, PurveyorParams>(queryable,
        schedulerComputation,
        rawFactory) {

    override val tableName = TABLE_NAME
    override val keyId = KEY_ID

    override fun putSingularBuilder(params: PurveyorParams) =
            builder()
                    .put(KEY_NAME, params.name)
                    .put(KEY_PHONE_NUMBER, params.phoneNumber)
                    .put(KEY_ADDRESS, params.address)
                    .put(KEY_REPRESENTATIVE_NAME, params.representativeName)

    override fun replaceSingularBuilder(key: Long, params: PurveyorParams) =
            builder()
                    .put(KEY_ID, key)
                    .put(KEY_NAME, params.name)
                    .put(KEY_PHONE_NUMBER, params.phoneNumber)
                    .put(KEY_ADDRESS, params.address)
                    .put(KEY_REPRESENTATIVE_NAME, params.representativeName)

    override val createTableBuilder = tableBuilder {
        property(KEY_ID) { simple(INTEGER, PRIMARY_KEY, AUTOINCREMENT) }
        property(KEY_NAME) { simple(TEXT) }
        property(KEY_PHONE_NUMBER) { simple(TEXT) }
        property(KEY_ADDRESS) { simple(TEXT) }
        property(KEY_REPRESENTATIVE_NAME) { simple(TEXT) }
    }

    companion object {
        const val TABLE_NAME = "Purveyors"
        const val KEY_ID = "_id"
        const val KEY_NAME = "name"
        const val KEY_PHONE_NUMBER = "phoneNumber"
        const val KEY_ADDRESS = "address"
        const val KEY_REPRESENTATIVE_NAME = "representativeName"
    }

}