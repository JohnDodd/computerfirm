package tusur.kkv.data.database.diskservice.purveyor

data class PurveyorParams(val name: String,
                          val phoneNumber: String,
                          val address: String,
                          val representativeName: String?)