package tusur.kkv.data.database.diskservice.service

import io.reactivex.Scheduler
import tusur.kkv.data.database.base.Queryable
import tusur.kkv.data.database.base.SimpleTableDiskService
import tusur.kkv.data.database.base.Table.TableBuilder.SimpleInstruction.Type.*
import tusur.kkv.data.database.mapper.RawFactory
import tusur.kkv.data.injection.qualifier.ForComputation
import tusur.kkv.data.model.ServiceRaw
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ServiceDiskService
@Inject constructor(queryable: Queryable,
                    @ForComputation schedulerComputation: Scheduler,
                    rawFactory: RawFactory<ServiceRaw>) :
        SimpleTableDiskService<ServiceRaw, ServiceParams>(queryable,
                schedulerComputation,
                rawFactory) {

    override val tableName = TABLE_NAME
    override val keyId = KEY_ID

    override fun putSingularBuilder(params: ServiceParams) =
            builder()
                    .put(KEY_NAME, params.name)
                    .put(KEY_COST, params.cost)
                    .put(KEY_DESCRIPTION, params.description)

    override fun replaceSingularBuilder(key: Long, params: ServiceParams) =
            builder()
                    .put(KEY_ID, key)
                    .put(KEY_NAME, params.name)
                    .put(KEY_COST, params.cost)
                    .put(KEY_DESCRIPTION, params.description)

    override val createTableBuilder = tableBuilder {
        property(KEY_ID) { simple(INTEGER, PRIMARY_KEY, AUTOINCREMENT) }
        property(KEY_NAME) { simple(TEXT) }
        property(KEY_COST) { simple(INTEGER) }
        property(KEY_DESCRIPTION) { simple(TEXT) }
    }

    companion object {
        const val TABLE_NAME = "Services"
        const val KEY_ID = "_id"
        const val KEY_NAME = "name"
        const val KEY_COST = "cost"
        const val KEY_DESCRIPTION = "description"

        fun allKeys() = listOf(
                KEY_ID,
                KEY_NAME,
                KEY_COST,
                KEY_DESCRIPTION
        )
    }
}