package tusur.kkv.data.database.diskservice.service

data class ServiceParams(val name: String, val cost: Long, val description: String?)