package tusur.kkv.data.database.mapper

import android.database.Cursor
import tusur.kkv.data.model.BestEmployeeRaw
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BestEmployeeRawFactory
@Inject constructor(private val employeeRawFactory: EmployeeRawFactory) : RawFactory<BestEmployeeRaw> {

    override fun create(cursor: Cursor) = BestEmployeeRaw(
            employeeRawFactory.create(cursor),
            cursor.getLong(cursor.getColumnIndex(REVENUE)),
            cursor.getInt(cursor.getColumnIndex(COMPONENTS_COUNT)),
            cursor.getInt(cursor.getColumnIndex(SERVICES_COUNT))
    )

    companion object {
        const val REVENUE = "revenue"
        const val COMPONENTS_COUNT = "componentsCount"
        const val SERVICES_COUNT = "servicesCount"
    }
}