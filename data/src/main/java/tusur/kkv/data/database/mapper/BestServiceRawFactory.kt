package tusur.kkv.data.database.mapper

import android.database.Cursor
import tusur.kkv.data.model.BestServiceRaw
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BestServiceRawFactory
@Inject constructor(private val employeeRawFactory: ServiceRawFactory) : RawFactory<BestServiceRaw> {

    override fun create(cursor: Cursor) = BestServiceRaw(
            employeeRawFactory.create(cursor),
            cursor.getLong(cursor.getColumnIndex(REVENUE))
    )

    companion object {
        const val REVENUE = "revenue"
    }
}