package tusur.kkv.data.database.mapper

import android.database.Cursor
import tusur.kkv.data.database.diskservice.component.ComponentDiskService
import tusur.kkv.data.model.ComponentRaw
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ComponentRawFactory @Inject constructor() : RawFactory<ComponentRaw> {

    override fun create(cursor: Cursor) = ComponentRaw(
            cursor.getLong(cursor.getColumnIndex(ComponentDiskService.KEY_ID)),
            cursor.getLong(cursor.getColumnIndex(ComponentDiskService.KEY_COMPONENT_TYPE_ID)),
            cursor.getLong(cursor.getColumnIndex(ComponentDiskService.KEY_GUARANTEE_PERIOD)),
            cursor.getString(cursor.getColumnIndex(ComponentDiskService.KEY_NAME)),
            cursor.getLong(cursor.getColumnIndex(ComponentDiskService.KEY_COST)),
            cursor.getLong(cursor.getColumnIndex(ComponentDiskService.KEY_PURVEYOR_ID))
    )
}