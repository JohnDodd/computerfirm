package tusur.kkv.data.database.mapper

import android.database.Cursor
import tusur.kkv.data.database.diskservice.componenttype.ComponentTypeDiskService
import tusur.kkv.data.model.ComponentTypeRaw
import javax.inject.Inject

class ComponentTypeRawFactory @Inject constructor() : RawFactory<ComponentTypeRaw> {

    override fun create(cursor: Cursor) = ComponentTypeRaw(
            cursor.getLong(cursor.getColumnIndex(ComponentTypeDiskService.KEY_ID)),
            cursor.getString(cursor.getColumnIndex(ComponentTypeDiskService.KEY_NAME)),
            cursor.getString(cursor.getColumnIndex(ComponentTypeDiskService.KEY_SPECIFICATIONS)),
            cursor.getString(cursor.getColumnIndex(ComponentTypeDiskService.KEY_MANUFACTURER))
    )
}