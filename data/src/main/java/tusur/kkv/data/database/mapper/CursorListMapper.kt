package tusur.kkv.data.database.mapper

import android.database.Cursor
import io.reactivex.Maybe
import io.reactivex.MaybeSource
import io.reactivex.functions.Function

class CursorListMapper<Raw>(private val rawFactory: RawFactory<Raw>) : Function<Cursor, MaybeSource<List<Raw>>> {

    override fun apply(cursor: Cursor): MaybeSource<List<Raw>> {
        return when {
            cursor.moveToFirst() -> {
                val raws = mutableListOf<Raw>()
                do {
                    raws.add(rawFactory.create(cursor))
                } while (cursor.moveToNext())
                cursor.close()
                Maybe.just(raws)
            }
            else -> Maybe.empty<List<Raw>>()
        }
    }

}