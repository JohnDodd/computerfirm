package tusur.kkv.data.database.mapper

import android.database.Cursor
import io.reactivex.Maybe
import io.reactivex.MaybeSource
import io.reactivex.functions.Function

class CursorMapper<Raw>(private val rawFactory: RawFactory<Raw>) : Function<Cursor, MaybeSource<Raw>> {

    override fun apply(cursor: Cursor): MaybeSource<Raw> {
        return when {
            cursor.count > 1 -> Maybe.error(IllegalArgumentException("More than one element"))
            cursor.moveToFirst() -> {
                val raw = rawFactory.create(cursor)
                cursor.close()
                Maybe.just(raw)
            }
            else -> Maybe.empty<Raw>()
        }
    }

}