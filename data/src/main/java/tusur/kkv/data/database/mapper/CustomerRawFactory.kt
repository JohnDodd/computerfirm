package tusur.kkv.data.database.mapper

import android.database.Cursor
import tusur.kkv.data.database.diskservice.customer.CustomerDiskService
import tusur.kkv.data.model.CustomerRaw
import javax.inject.Inject

class CustomerRawFactory @Inject constructor() : RawFactory<CustomerRaw> {
    override fun create(cursor: Cursor) = CustomerRaw(
            cursor.getLong(cursor.getColumnIndex(CustomerDiskService.KEY_ID)),
            cursor.getString(cursor.getColumnIndex(CustomerDiskService.KEY_NAME)),
            cursor.getString(cursor.getColumnIndex(CustomerDiskService.KEY_EMAIL)),
            cursor.getString(cursor.getColumnIndex(CustomerDiskService.KEY_PHONE_NUMBER))
    )
}