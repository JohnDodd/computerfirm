package tusur.kkv.data.database.mapper

import android.database.Cursor
import tusur.kkv.data.database.diskservice.employee.EmployeeDiskService
import tusur.kkv.data.model.EmployeeRaw
import javax.inject.Inject

class EmployeeRawFactory @Inject constructor() : RawFactory<EmployeeRaw> {

    override fun create(cursor: Cursor) = EmployeeRaw(
            cursor.getLong(cursor.getColumnIndex(EmployeeDiskService.KEY_ID)),
            cursor.getString(cursor.getColumnIndex(EmployeeDiskService.KEY_FULL_NAME)),
            cursor.getString(cursor.getColumnIndex(EmployeeDiskService.KEY_BIRTHDATE)),
            cursor.getString(cursor.getColumnIndex(EmployeeDiskService.KEY_SEX)),
            cursor.getString(cursor.getColumnIndex(EmployeeDiskService.KEY_ADDRESS)),
            cursor.getString(cursor.getColumnIndex(EmployeeDiskService.KEY_PHONE_NUMBER)),
            cursor.getString(cursor.getColumnIndex(EmployeeDiskService.KEY_PASSPORT_DATA)),
            cursor.getLong(cursor.getColumnIndex(EmployeeDiskService.KEY_POSITION_ID))
    )
}