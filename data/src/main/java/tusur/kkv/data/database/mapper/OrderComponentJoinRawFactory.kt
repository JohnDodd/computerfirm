package tusur.kkv.data.database.mapper

import android.database.Cursor
import tusur.kkv.data.database.diskservice.ordercomponentjoin.OrderComponentJoinDiskService
import tusur.kkv.data.model.JoinRaw
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class OrderComponentJoinRawFactory @Inject constructor() : RawFactory<JoinRaw> {
    override fun create(cursor: Cursor) = JoinRaw(
            cursor.getLong(cursor.getColumnIndex(OrderComponentJoinDiskService.KEY_ORDER_ID)),
            cursor.getLong(cursor.getColumnIndex(OrderComponentJoinDiskService.KEY_COMPONENT_ID))
    )
}