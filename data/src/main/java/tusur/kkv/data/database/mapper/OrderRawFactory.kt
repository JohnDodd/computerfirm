package tusur.kkv.data.database.mapper

import android.database.Cursor
import tusur.kkv.data.database.diskservice.order.OrderDiskService
import tusur.kkv.data.model.OrderRaw
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class OrderRawFactory @Inject constructor() : RawFactory<OrderRaw> {
    override fun create(cursor: Cursor) = OrderRaw(
            cursor.getLong(cursor.getColumnIndex(OrderDiskService.KEY_ID)),
            cursor.getString(cursor.getColumnIndex(OrderDiskService.KEY_REGISTRATION_DATE)),
            cursor.getString(cursor.getColumnIndex(OrderDiskService.KEY_EXECUTION_DATE)),
            cursor.getLong(cursor.getColumnIndex(OrderDiskService.KEY_CUSTOMER_ID)),
            cursor.getLong(cursor.getColumnIndex(OrderDiskService.KEY_EMPLOYEE_ID))
    )
}