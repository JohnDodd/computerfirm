package tusur.kkv.data.database.mapper

import android.database.Cursor
import tusur.kkv.data.database.diskservice.orderservicejoin.OrderServiceJoinDiskService
import tusur.kkv.data.model.JoinRaw
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class OrderServiceJoinRawFactory @Inject constructor() : RawFactory<JoinRaw> {
    override fun create(cursor: Cursor) = JoinRaw(
            cursor.getLong(cursor.getColumnIndex(OrderServiceJoinDiskService.KEY_ORDER_ID)),
            cursor.getLong(cursor.getColumnIndex(OrderServiceJoinDiskService.KEY_SERVICE_ID))
    )
}