package tusur.kkv.data.database.mapper

import android.database.Cursor
import tusur.kkv.data.database.diskservice.position.PositionDiskService
import tusur.kkv.data.model.PositionRaw
import javax.inject.Inject

class PositionRawFactory @Inject constructor() : RawFactory<PositionRaw> {

    override fun create(cursor: Cursor) = PositionRaw(
            cursor.getLong(cursor.getColumnIndex(PositionDiskService.KEY_ID)),
            cursor.getString(cursor.getColumnIndex(PositionDiskService.KEY_NAME)),
            cursor.getLong(cursor.getColumnIndex(PositionDiskService.KEY_SALARY)),
            cursor.getString(cursor.getColumnIndex(PositionDiskService.KEY_DUTIES))

    )
}