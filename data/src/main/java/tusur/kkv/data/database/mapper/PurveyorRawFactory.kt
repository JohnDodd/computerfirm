package tusur.kkv.data.database.mapper

import android.database.Cursor
import tusur.kkv.data.database.diskservice.purveyor.PurveyorDiskService
import tusur.kkv.data.model.PurveyorRaw

class PurveyorRawFactory : RawFactory<PurveyorRaw> {

    override fun create(cursor: Cursor) = PurveyorRaw(
            cursor.getLong(cursor.getColumnIndex(PurveyorDiskService.KEY_ID)),
            cursor.getString(cursor.getColumnIndex(PurveyorDiskService.KEY_NAME)),
            cursor.getString(cursor.getColumnIndex(PurveyorDiskService.KEY_PHONE_NUMBER)),
            cursor.getString(cursor.getColumnIndex(PurveyorDiskService.KEY_ADDRESS)),
            cursor.getString(cursor.getColumnIndex(PurveyorDiskService.KEY_REPRESENTATIVE_NAME))
    )
}