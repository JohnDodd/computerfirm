package tusur.kkv.data.database.mapper

import android.database.Cursor

interface RawFactory<Raw> {
    fun create(cursor: Cursor): Raw

}