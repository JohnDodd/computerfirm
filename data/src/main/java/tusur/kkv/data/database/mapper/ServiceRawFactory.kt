package tusur.kkv.data.database.mapper

import android.database.Cursor
import tusur.kkv.data.database.diskservice.service.ServiceDiskService
import tusur.kkv.data.model.ServiceRaw
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ServiceRawFactory @Inject constructor() : RawFactory<ServiceRaw> {
    override fun create(cursor: Cursor) = ServiceRaw(
            cursor.getLong(cursor.getColumnIndex(ServiceDiskService.KEY_ID)),
            cursor.getString(cursor.getColumnIndex(ServiceDiskService.KEY_NAME)),
            cursor.getLong(cursor.getColumnIndex(ServiceDiskService.KEY_COST)),
            cursor.getString(cursor.getColumnIndex(ServiceDiskService.KEY_DESCRIPTION))
    )
}