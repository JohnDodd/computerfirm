package tusur.kkv.data.exception

data class IllegalParamsException(val id: Int, override val message: String)
    : RuntimeException(message)