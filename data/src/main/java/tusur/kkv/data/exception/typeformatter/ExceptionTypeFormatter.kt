package tusur.kkv.data.exception.typeformatter

import android.content.Context
import io.reactivex.functions.Function
import tusur.kkv.data.R
import tusur.kkv.data.injection.qualifier.ForApplication
import tusur.kkv.domain.repository.ParamsExceptionTypeId
import javax.inject.Inject

class ExceptionTypeFormatter
@Inject constructor(@ForApplication private val context: Context) : Function<Int, String> {

    override fun apply(type: Int): String = when (type) {
        ParamsExceptionTypeId.EMPTY -> context.getString(R.string.field_error_empty)
        ParamsExceptionTypeId.UNSATISFIED_FORMAT -> context.getString(R.string.field_error_unsatisfied_format)
        else -> context.getString(R.string.field_error_unknown)
    }
}