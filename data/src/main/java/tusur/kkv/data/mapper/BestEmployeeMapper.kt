package tusur.kkv.data.mapper

import io.reactivex.functions.Function
import tusur.kkv.data.model.BestEmployeeRaw
import tusur.kkv.domain.model.BestEmployee
import tusur.kkv.domain.model.Gender
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BestEmployeeMapper @Inject constructor(private val employeeRawMapper: EmployeeMapper)
    : Function<BestEmployeeRaw, BestEmployee> {

    override fun apply(raw: BestEmployeeRaw) = BestEmployee(
            raw.employeeRaw!!.id!!,
            raw.employeeRaw.fullName!!,
            raw.employeeRaw.birthDate!!,
            Gender.from(raw.employeeRaw.sex!!),
            raw.employeeRaw.address!!,
            raw.employeeRaw.phoneNumber!!,
            raw.employeeRaw.passportData!!,
            raw.revenue!!,
            raw.componentsCount!!,
            raw.servicesCount!!
    )
}