package tusur.kkv.data.mapper

import io.reactivex.functions.Function
import tusur.kkv.data.model.BestServiceRaw
import tusur.kkv.domain.model.BestService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BestServiceMapper @Inject constructor()
    : Function<BestServiceRaw, BestService> {

    override fun apply(raw: BestServiceRaw) = BestService(
            raw.serviceRaw!!.id!!,
            raw.serviceRaw.name!!,
            raw.serviceRaw.cost!!,
            raw.revenue!!
    )
}