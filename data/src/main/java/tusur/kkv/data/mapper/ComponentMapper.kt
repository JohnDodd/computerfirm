package tusur.kkv.data.mapper

import io.reactivex.functions.Function
import tusur.kkv.data.model.tuple.ComponentTuple
import tusur.kkv.domain.model.Component
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ComponentMapper @Inject constructor() : Function<ComponentTuple, Component> {

    override fun apply(tuple: ComponentTuple) =
            tuple.componentRaw.let { raw ->
                Component(
                        raw.id!!,
                        tuple.componentType!!,
                        raw.guaranteePeriod,
                        raw.name!!,
                        raw.cost!!,
                        tuple.purveyor
                )
            }

}