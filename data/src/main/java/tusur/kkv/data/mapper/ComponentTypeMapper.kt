package tusur.kkv.data.mapper

import io.reactivex.functions.Function
import tusur.kkv.data.model.ComponentTypeRaw
import tusur.kkv.domain.model.ComponentType
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ComponentTypeMapper @Inject constructor() : Function<ComponentTypeRaw, ComponentType> {
    override fun apply(raw: ComponentTypeRaw) = ComponentType(
            raw.id!!,
            raw.name!!,
            raw.specifications!!,
            raw.manufacturer!!
    )
}