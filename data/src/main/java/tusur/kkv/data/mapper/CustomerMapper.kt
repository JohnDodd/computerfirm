package tusur.kkv.data.mapper

import io.reactivex.functions.Function
import tusur.kkv.data.model.CustomerRaw
import tusur.kkv.domain.model.Customer
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CustomerMapper @Inject constructor() : Function<CustomerRaw, Customer> {
    override fun apply(raw: CustomerRaw) = Customer(
            raw.id!!,
            raw.fullName!!,
            raw.email,
            raw.phoneNumber!!
    )
}