package tusur.kkv.data.mapper

import io.reactivex.functions.Function
import tusur.kkv.data.model.tuple.EmployeeTuple
import tusur.kkv.domain.model.Employee
import tusur.kkv.domain.model.Gender
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class EmployeeMapper @Inject constructor() : Function<EmployeeTuple, Employee> {

    override fun apply(tuple: EmployeeTuple) =
            tuple.employeeRaw.let { raw ->
                Employee(raw.id!!,
                        raw.fullName!!,
                        raw.birthDate!!,
                        Gender.from(raw.sex!!),
                        raw.address!!,
                        raw.phoneNumber!!,
                        raw.passportData!!,
                        tuple.position)
            }
}