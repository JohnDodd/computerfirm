package tusur.kkv.data.mapper

import io.reactivex.functions.Function
import tusur.kkv.data.model.tuple.OrderTuple
import tusur.kkv.domain.model.Order
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class OrderMapper @Inject constructor() : Function<OrderTuple, Order> {
    override fun apply(tuple: OrderTuple) = Order(
            tuple.orderRaw.id!!,
            tuple.orderRaw.registrationDate!!,
            tuple.orderRaw.executionDate,
            tuple.customer,
            tuple.employee
    )
}