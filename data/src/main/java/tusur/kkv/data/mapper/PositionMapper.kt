package tusur.kkv.data.mapper

import io.reactivex.functions.Function
import tusur.kkv.data.model.PositionRaw
import tusur.kkv.domain.model.Position
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PositionMapper @Inject constructor() : Function<PositionRaw, Position> {
    override fun apply(raw: PositionRaw) =
            Position(
                    raw.id!!,
                    raw.name!!,
                    raw.salary!!,
                    raw.duties!!)
}