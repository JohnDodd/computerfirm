package tusur.kkv.data.mapper

import io.reactivex.functions.Function
import tusur.kkv.data.model.PurveyorRaw
import tusur.kkv.domain.model.Purveyor
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PurveyorMapper @Inject constructor() : Function<PurveyorRaw, Purveyor> {

    override fun apply(raw: PurveyorRaw): Purveyor {
        return Purveyor(
                raw.id!!,
                raw.name!!,
                raw.phoneNumber!!,
                raw.address!!,
                raw.representativeName)
    }
}