package tusur.kkv.data.mapper

import io.reactivex.functions.Function
import tusur.kkv.data.model.ServiceRaw
import tusur.kkv.domain.model.Service
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ServiceMapper @Inject constructor() : Function<ServiceRaw, Service> {
    override fun apply(raw: ServiceRaw): Service {
        return Service(
                raw.id!!,
                raw.name!!,
                raw.cost!!,
                raw.description)
    }
}