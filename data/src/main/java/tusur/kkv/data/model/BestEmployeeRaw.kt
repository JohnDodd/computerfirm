package tusur.kkv.data.model

data class BestEmployeeRaw(val employeeRaw: EmployeeRaw?,
                           val revenue: Long?,
                           val componentsCount: Int?,
                           val servicesCount: Int?)