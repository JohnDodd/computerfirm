package tusur.kkv.data.model

data class BestServiceRaw(val serviceRaw: ServiceRaw?,
                          val revenue: Long?)