package tusur.kkv.data.model

data class ComponentRaw(val id: Long?,
                        val componentTypeId: Long?,
                        val guaranteePeriod: Long?,
                        val name: String?,
                        val cost: Long?,
                        val purveyorId: Long?)