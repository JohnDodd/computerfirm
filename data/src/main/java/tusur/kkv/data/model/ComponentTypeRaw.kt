package tusur.kkv.data.model

data class ComponentTypeRaw(val id: Long?,
                            val name: String?,
                            val specifications: String?,
                            val manufacturer: String?)