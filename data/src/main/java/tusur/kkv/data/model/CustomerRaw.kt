package tusur.kkv.data.model

data class CustomerRaw(val id: Long?,
                       val fullName: String?,
                       val email: String?,
                       val phoneNumber: String?)