package tusur.kkv.data.model

data class EmployeeRaw(val id: Long?,
                       val fullName: String?,
                       val birthDate: String?,
                       val sex: String?,
                       val address: String?,
                       val phoneNumber: String?,
                       val passportData: String?,
                       val positionId: Long?)