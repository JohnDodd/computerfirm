package tusur.kkv.data.model

data class JoinRaw(val firstId: Long, val secondId: Long)