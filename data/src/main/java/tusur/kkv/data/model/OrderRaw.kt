package tusur.kkv.data.model

data class OrderRaw(val id: Long?,
                    val registrationDate: String?,
                    val executionDate: String?,
                    val customerId: Long?,
                    val employeeId: Long?)