package tusur.kkv.data.model

data class PositionRaw(val id: Long?,
                       val name: String?,
                       val salary: Long?,
                       val duties: String?)