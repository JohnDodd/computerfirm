package tusur.kkv.data.model

data class PurveyorRaw(val id: Long?,
                       val name: String?,
                       val phoneNumber: String?,
                       val address: String?,
                       val representativeName: String?)