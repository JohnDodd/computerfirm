package tusur.kkv.data.model

data class ServiceRaw(val id: Long?,
                      val name: String?,
                      val cost: Long?,
                      val description: String?)