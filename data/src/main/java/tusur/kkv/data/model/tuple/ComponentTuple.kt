package tusur.kkv.data.model.tuple

import tusur.kkv.data.model.ComponentRaw
import tusur.kkv.domain.model.ComponentType
import tusur.kkv.domain.model.Purveyor

data class ComponentTuple(val componentRaw: ComponentRaw,
                          val componentType: ComponentType?,
                          val purveyor: Purveyor)