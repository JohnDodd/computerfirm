package tusur.kkv.data.model.tuple

import tusur.kkv.data.model.EmployeeRaw
import tusur.kkv.domain.model.Position

data class EmployeeTuple(val employeeRaw: EmployeeRaw, val position: Position)