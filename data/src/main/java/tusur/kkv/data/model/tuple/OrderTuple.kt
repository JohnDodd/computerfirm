package tusur.kkv.data.model.tuple

import tusur.kkv.data.model.OrderRaw
import tusur.kkv.domain.model.Customer
import tusur.kkv.domain.model.Employee

data class OrderTuple(val orderRaw: OrderRaw,
                      val employee: Employee,
                      val customer: Customer)