package tusur.kkv.data.repository

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.Function
import tusur.kkv.data.database.diskservice.component.ComponentParams
import tusur.kkv.data.exception.IllegalParamsException
import tusur.kkv.data.exception.typeformatter.ExceptionTypeFormatter
import tusur.kkv.data.model.ComponentRaw
import tusur.kkv.data.model.tuple.ComponentTuple
import tusur.kkv.data.repository.base.ComplexRepository
import tusur.kkv.data.store.DiskService
import tusur.kkv.data.store.ReactiveStore
import tusur.kkv.domain.model.Component
import tusur.kkv.domain.repository.IComponentRepository
import tusur.kkv.domain.repository.IComponentRepository.ParamsExceptionId.Companion.COST
import tusur.kkv.domain.repository.IComponentRepository.ParamsExceptionId.Companion.NAME
import tusur.kkv.domain.repository.IComponentRepository.ParamsExceptionId.Companion.PURVEYOR_ID
import tusur.kkv.domain.repository.IComponentRepository.ParamsExceptionId.Companion.TYPE_ID
import tusur.kkv.domain.repository.IComponentTypeRepository
import tusur.kkv.domain.repository.IPurveyorRepository
import tusur.kkv.domain.repository.ParamsExceptionTypeId.Companion.EMPTY
import tusur.kkv.domain.repository.ParamsExceptionTypeId.Companion.UNSATISFIED_FORMAT
import tusur.kkv.domain.utils.listToMap
import tusur.kkv.domain.utils.mapToList
import javax.inject.Inject

class ComponentRepository
@Inject constructor(private val typeRepository: IComponentTypeRepository,
                    private val purveyorRepository: IPurveyorRepository,
                    private val exceptionTypeFormatter: ExceptionTypeFormatter,
                    mapper: Function<ComponentTuple, Component>,
                    diskService: DiskService<Long, ComponentRaw, ComponentParams>,
                    memoryStore: ReactiveStore<Long, Component>) :
        ComplexRepository<ComponentTuple, ComponentRaw, Component, ComponentParams>(
                mapper,
                diskService,
                memoryStore),
        IComponentRepository {

    override fun store(
            typeId: Long,
            guaranteePeriod: Long?,
            name: String,
            cost: Long,
            purveyorId: Long): Maybe<Component> {
        val params = ComponentParams(typeId, guaranteePeriod, name, cost, purveyorId)
        return assertParams(params)
                .andThen(diskService.putSingular(params))
                .flatMapMaybe { requestSingular(it) }
                .doOnSuccess { memoryStore.storeSingular(it) }
    }


    override fun replace(id: Long,
                         typeId: Long,
                         guaranteePeriod: Long?,
                         name: String,
                         cost: Long,
                         purveyorId: Long): Completable {
        val params = ComponentParams(typeId, guaranteePeriod, name, cost, purveyorId)
        return assertParams(params)
                .andThen(diskService.replaceSingular(id, params))
                .andThen(requestSingular(id))
                .doOnSuccess { memoryStore.storeSingular(it) }
                .ignoreElement()
    }

    private fun assertParams(params: ComponentParams): Completable =
            when {
                params.name.isBlank() ->
                    Completable.error(IllegalParamsException(NAME,
                            exceptionTypeFormatter.apply(EMPTY)))
                params.typeId < 0 ->
                    Completable.error(IllegalParamsException(TYPE_ID,
                            exceptionTypeFormatter.apply(EMPTY)))
                params.cost < 0 ->
                    Completable.error(IllegalParamsException(COST,
                            exceptionTypeFormatter.apply(UNSATISFIED_FORMAT)))
                params.purveyorId < 0 ->
                    Completable.error(IllegalParamsException(PURVEYOR_ID,
                            exceptionTypeFormatter.apply(EMPTY)))
                else -> Completable.complete()
            }


    override fun List<ComponentRaw>.toValues(): Single<List<Component>> {
        val types = typeRepository.requestSeveral(mapToList { componentTypeId }).listToMap { id }
        val purveyors = purveyorRepository.requestSeveral(mapToList { purveyorId }).listToMap { id }
        return types.flatMap { t ->
            purveyors.flatMap { p ->
                Observable.fromIterable(this)
                        .map { ComponentTuple(it, t[it.componentTypeId], p[it.purveyorId]!!) }
                        .map(mapper)
                        .toList()
            }
        }
    }

}