package tusur.kkv.data.repository

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.functions.Function
import tusur.kkv.data.database.diskservice.componenttype.ComponentTypeParams
import tusur.kkv.data.exception.IllegalParamsException
import tusur.kkv.data.exception.typeformatter.ExceptionTypeFormatter
import tusur.kkv.data.model.ComponentTypeRaw
import tusur.kkv.data.repository.base.SimpleRepository
import tusur.kkv.data.store.DiskService
import tusur.kkv.data.store.ReactiveStore
import tusur.kkv.domain.model.ComponentType
import tusur.kkv.domain.repository.IComponentTypeRepository
import tusur.kkv.domain.repository.ParamsExceptionTypeId

class ComponentTypeRepository(diskService: DiskService<Long, ComponentTypeRaw, ComponentTypeParams>,
                              memoryStore: ReactiveStore<Long, ComponentType>,
                              mapper: Function<ComponentTypeRaw, ComponentType>,
                              private val exceptionTypeFormatter: ExceptionTypeFormatter) :
        SimpleRepository<ComponentTypeRaw, ComponentType, ComponentTypeParams>(
                diskService,
                memoryStore,
                mapper),
        IComponentTypeRepository {

    override fun store(name: String,
                       specifications: String,
                       manufacturer: String): Maybe<ComponentType> {
        val params = ComponentTypeParams(name, specifications, manufacturer)
        return assertParams(params)
                .andThen(diskService.putSingular(params))
                .flatMapMaybe { diskService.singular(it) }
                .map(mapper)
                .doOnSuccess { memoryStore.storeSingular(it) }
    }

    override fun replace(id: Long,
                         name: String,
                         specifications: String,
                         manufacturer: String): Completable {
        val params = ComponentTypeParams(name, specifications, manufacturer)
        return assertParams(params)
                .andThen(diskService.replaceSingular(id, params))
                .andThen(requestSingular(id))
                .doOnSuccess { memoryStore.storeSingular(it) }
                .ignoreElement()
    }


    private fun assertParams(params: ComponentTypeParams): Completable =
            when {
                params.name.isBlank() ->
                    Completable.error(IllegalParamsException(
                            IComponentTypeRepository.ParamsExceptionId.NAME,
                            exceptionTypeFormatter.apply(ParamsExceptionTypeId.EMPTY)))
                params.specifications.isBlank() ->
                    Completable.error(IllegalParamsException(
                            IComponentTypeRepository.ParamsExceptionId.SPECIFICATIONS,
                            exceptionTypeFormatter.apply(ParamsExceptionTypeId.EMPTY)))
                params.manufacturer.isBlank() ->
                    Completable.error(IllegalParamsException(
                            IComponentTypeRepository.ParamsExceptionId.MANUFACTURER,
                            exceptionTypeFormatter.apply(ParamsExceptionTypeId.EMPTY)))

                else -> Completable.complete()
            }

}