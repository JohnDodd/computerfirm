package tusur.kkv.data.repository

import android.util.Patterns
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.functions.Function
import tusur.kkv.data.database.diskservice.customer.CustomerParams
import tusur.kkv.data.exception.IllegalParamsException
import tusur.kkv.data.exception.typeformatter.ExceptionTypeFormatter
import tusur.kkv.data.model.CustomerRaw
import tusur.kkv.data.repository.base.SimpleRepository
import tusur.kkv.data.store.DiskService
import tusur.kkv.data.store.ReactiveStore
import tusur.kkv.domain.model.Customer
import tusur.kkv.domain.repository.ICustomerRepository
import tusur.kkv.domain.repository.ICustomerRepository.ParamsExceptionId.Companion.EMAIL
import tusur.kkv.domain.repository.ICustomerRepository.ParamsExceptionId.Companion.NAME
import tusur.kkv.domain.repository.ICustomerRepository.ParamsExceptionId.Companion.PHONE
import tusur.kkv.domain.repository.ParamsExceptionTypeId.Companion.EMPTY
import tusur.kkv.domain.repository.ParamsExceptionTypeId.Companion.UNSATISFIED_FORMAT

class CustomerRepository(diskService: DiskService<Long, CustomerRaw, CustomerParams>,
                         memoryStore: ReactiveStore<Long, Customer>,
                         mapper: Function<CustomerRaw, Customer>,
                         private val exceptionTypeFormatter: ExceptionTypeFormatter) :
        SimpleRepository<CustomerRaw, Customer, CustomerParams>(diskService, memoryStore, mapper),
        ICustomerRepository {

    override fun store(fullName: String, email: String?, phoneNumber: String): Maybe<Customer> {
        val params = CustomerParams(fullName, email, phoneNumber)
        return assertParams(params)
                .andThen(diskService.putSingular(params))
                .flatMapMaybe { diskService.singular(it) }
                .map(mapper)
                .doOnSuccess { memoryStore.storeSingular(it) }
    }

    override fun replace(id: Long,
                         fullName: String,
                         email: String?,
                         phoneNumber: String): Completable {
        val params = CustomerParams(fullName, email, phoneNumber)
        return assertParams(params)
                .andThen(diskService.replaceSingular(id, params))
                .andThen(requestSingular(id))
                .doOnSuccess { memoryStore.storeSingular(it) }
                .ignoreElement()
    }

    private fun assertParams(params: CustomerParams): Completable =
            when {
                params.fullName.isBlank() ->
                    Completable.error(IllegalParamsException(NAME,
                            exceptionTypeFormatter.apply(EMPTY)))
                params.email != null && !Patterns.EMAIL_ADDRESS.matcher(params.email).matches() ->
                    Completable.error(IllegalParamsException(EMAIL,
                            exceptionTypeFormatter.apply(UNSATISFIED_FORMAT)))
                params.phoneNumber.isBlank() ->
                    Completable.error(IllegalParamsException(PHONE,
                            exceptionTypeFormatter.apply(EMPTY)))
                else -> Completable.complete()
            }


}