package tusur.kkv.data.repository

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.Function
import tusur.kkv.data.database.base.DateFormatter
import tusur.kkv.data.database.diskservice.component.ComponentDiskService
import tusur.kkv.data.database.diskservice.employee.EmployeeDiskService.Companion.KEY_FULL_NAME
import tusur.kkv.data.database.diskservice.employee.EmployeeDiskService.Companion.KEY_ID
import tusur.kkv.data.database.diskservice.employee.EmployeeDiskService.Companion.TABLE_NAME
import tusur.kkv.data.database.diskservice.employee.EmployeeDiskService.Companion.allKeys
import tusur.kkv.data.database.diskservice.employee.EmployeeParams
import tusur.kkv.data.database.diskservice.order.OrderDiskService
import tusur.kkv.data.database.diskservice.ordercomponentjoin.OrderComponentJoinDiskService
import tusur.kkv.data.database.diskservice.orderservicejoin.OrderServiceJoinDiskService
import tusur.kkv.data.database.diskservice.service.ServiceDiskService
import tusur.kkv.data.database.mapper.BestEmployeeRawFactory
import tusur.kkv.data.database.mapper.CursorListMapper
import tusur.kkv.data.exception.IllegalParamsException
import tusur.kkv.data.exception.typeformatter.ExceptionTypeFormatter
import tusur.kkv.data.mapper.BestEmployeeMapper
import tusur.kkv.data.model.EmployeeRaw
import tusur.kkv.data.model.tuple.EmployeeTuple
import tusur.kkv.data.repository.base.ComplexRepository
import tusur.kkv.data.store.DiskService
import tusur.kkv.data.store.ReactiveStore
import tusur.kkv.domain.model.BestEmployee
import tusur.kkv.domain.model.Employee
import tusur.kkv.domain.model.Gender
import tusur.kkv.domain.repository.IEmployeeRepository
import tusur.kkv.domain.repository.IEmployeeRepository.ParamsExceptionId.Companion.ADDRESS
import tusur.kkv.domain.repository.IEmployeeRepository.ParamsExceptionId.Companion.BIRTHDATE
import tusur.kkv.domain.repository.IEmployeeRepository.ParamsExceptionId.Companion.GENDER
import tusur.kkv.domain.repository.IEmployeeRepository.ParamsExceptionId.Companion.NAME
import tusur.kkv.domain.repository.IEmployeeRepository.ParamsExceptionId.Companion.PASSPORT_DATA
import tusur.kkv.domain.repository.IEmployeeRepository.ParamsExceptionId.Companion.PHONE_NUMBER
import tusur.kkv.domain.repository.IEmployeeRepository.ParamsExceptionId.Companion.POSITION
import tusur.kkv.domain.repository.IPositionRepository
import tusur.kkv.domain.repository.ParamsExceptionTypeId.Companion.EMPTY
import tusur.kkv.domain.repository.ParamsExceptionTypeId.Companion.UNSATISFIED_FORMAT
import tusur.kkv.domain.utils.listToMap
import tusur.kkv.domain.utils.mapToList
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class EmployeeRepository
@Inject constructor(private val positionRepository: IPositionRepository,
                    private val dateFormatter: DateFormatter,
                    private val exceptionTypeFormatter: ExceptionTypeFormatter,
                    private val bestEmployeeRawFactory: BestEmployeeRawFactory,
                    private val bestEmployeeMapper: BestEmployeeMapper,
                    mapper: Function<EmployeeTuple, Employee>,
                    diskService: DiskService<Long, EmployeeRaw, EmployeeParams>,
                    memoryStore: ReactiveStore<Long, Employee>) :
        ComplexRepository<EmployeeTuple, EmployeeRaw, Employee, EmployeeParams>(mapper,
                diskService,
                memoryStore),
        IEmployeeRepository {

    override fun store(name: String,
                       birthDate: String,
                       gender: Gender?,
                       address: String,
                       phoneNumber: String,
                       passportData: String,
                       positionId: Long): Maybe<Employee> {
        val params = EmployeeParams(name,
                birthDate,
                gender?.value,
                address,
                phoneNumber,
                passportData,
                positionId)

        return assertParams(params)
                .andThen(diskService.putSingular(params))
                .flatMapMaybe { requestSingular(it) }
                .doOnSuccess { memoryStore.storeSingular(it) }
    }

    override fun replace(id: Long,
                         name: String,
                         birthDate: String,
                         gender: Gender?,
                         address: String,
                         phoneNumber: String,
                         passportData: String,
                         positionId: Long): Completable {
        val params = EmployeeParams(name,
                birthDate,
                gender?.value,
                address,
                phoneNumber,
                passportData,
                positionId)

        return assertParams(params)
                .andThen(diskService.replaceSingular(id, params))
                .andThen(requestSingular(id))
                .doOnSuccess { memoryStore.storeSingular(it) }
                .ignoreElement()
    }

    override fun best(limit: Long): Maybe<List<BestEmployee>> {
        val columns = allKeys()
                              .joinToString(
                                      separator = ", $TABLE_NAME.",
                                      prefix = " $TABLE_NAME.") + ", " +
                      "sum (${ComponentDiskService.TABLE_NAME}.${ComponentDiskService.KEY_COST}) + " +
                      "sum (${ServiceDiskService.TABLE_NAME}.${ServiceDiskService.KEY_COST}) " +
                      "as ${BestEmployeeRawFactory.REVENUE}, " +

                      "count(${OrderComponentJoinDiskService.TABLE_NAME}.${OrderComponentJoinDiskService.KEY_COMPONENT_ID}) " +
                      "as ${BestEmployeeRawFactory.COMPONENTS_COUNT}, " +
                      "count(${OrderServiceJoinDiskService.TABLE_NAME}.${OrderServiceJoinDiskService.KEY_SERVICE_ID}) " +
                      "as ${BestEmployeeRawFactory.SERVICES_COUNT}"

        return diskService.searchCursor(
                "select $columns from $TABLE_NAME " +

                "left join ${OrderDiskService.TABLE_NAME} " +
                "on ${OrderDiskService.TABLE_NAME}.${OrderDiskService.KEY_EMPLOYEE_ID} = " +
                "$TABLE_NAME.$KEY_ID " +

                "left join ${OrderComponentJoinDiskService.TABLE_NAME} " +
                "on ${OrderComponentJoinDiskService.TABLE_NAME}.${OrderComponentJoinDiskService.KEY_ORDER_ID} = " +
                "${OrderDiskService.TABLE_NAME}.${OrderDiskService.KEY_ID} " +

                "left join ${ComponentDiskService.TABLE_NAME} " +
                "on ${OrderComponentJoinDiskService.TABLE_NAME}.${OrderComponentJoinDiskService.KEY_COMPONENT_ID} = " +
                "${ComponentDiskService.TABLE_NAME}.${ComponentDiskService.KEY_ID} " +

                "left join ${OrderServiceJoinDiskService.TABLE_NAME} " +
                "on ${OrderServiceJoinDiskService.TABLE_NAME}.${OrderServiceJoinDiskService.KEY_ORDER_ID} = " +
                "${OrderDiskService.TABLE_NAME}.${OrderDiskService.KEY_ID} " +

                "left join ${ServiceDiskService.TABLE_NAME} " +
                "on ${OrderServiceJoinDiskService.TABLE_NAME}.${OrderServiceJoinDiskService.KEY_SERVICE_ID} = " +
                "${ServiceDiskService.TABLE_NAME}.${ServiceDiskService.KEY_ID} " +

                "group by $TABLE_NAME.$KEY_ID " +
                "order by ${BestEmployeeRawFactory.REVENUE} desc, " +
                "$TABLE_NAME.$KEY_FULL_NAME asc"
        )
                .flatMapMaybe(CursorListMapper(bestEmployeeRawFactory))
                .flatMap { list ->
                    Observable.fromIterable(list)
                            .map(bestEmployeeMapper)
                            .toList()
                            .filter { it.isNotEmpty() }
                }
    }

    private fun assertParams(params: EmployeeParams): Completable =
            when {
                params.gender.isNullOrBlank() ->
                    Completable.error(IllegalParamsException(GENDER,
                            exceptionTypeFormatter.apply(EMPTY)))
                params.fullName.isBlank() ->
                    Completable.error(IllegalParamsException(NAME,
                            exceptionTypeFormatter.apply(EMPTY)))
                dateFormatter.matchesServer(params.birthDate) ->
                    Completable.error(IllegalParamsException(BIRTHDATE,
                            exceptionTypeFormatter.apply(UNSATISFIED_FORMAT)))
                params.address.isBlank() ->
                    Completable.error(IllegalParamsException(ADDRESS,
                            exceptionTypeFormatter.apply(EMPTY)))
                params.phoneNumber.isBlank() ->
                    Completable.error(IllegalParamsException(PHONE_NUMBER,
                            exceptionTypeFormatter.apply(EMPTY)))
                params.passportData.isBlank() ->
                    Completable.error(IllegalParamsException(PASSPORT_DATA,
                            exceptionTypeFormatter.apply(EMPTY)))
                params.positionId < 0L ->
                    Completable.error(IllegalParamsException(POSITION,
                            exceptionTypeFormatter.apply(EMPTY)))

                else -> Completable.complete()
            }


    override fun List<EmployeeRaw>.toValues(): Single<List<Employee>> =
            positionRepository.requestSeveral(mapToList { positionId })
                    .listToMap { id }
                    .flatMap { pos ->
                        Observable.fromIterable(this)
                                .map { EmployeeTuple(it, pos[it.positionId]!!) }
                                .map(mapper)
                                .toList()
                    }

}