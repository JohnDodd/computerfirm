package tusur.kkv.data.repository

import io.reactivex.Completable
import io.reactivex.Maybe
import tusur.kkv.data.database.base.SimpleJoinTableDiskService
import tusur.kkv.data.repository.base.JoinRepository
import tusur.kkv.domain.model.Component
import tusur.kkv.domain.model.Order
import tusur.kkv.domain.repository.IComponentRepository
import tusur.kkv.domain.repository.IOrderComponentRepository
import tusur.kkv.domain.repository.IOrderRepository
import javax.inject.Inject

class OrderComponentRepository
@Inject constructor(joinDiskService: SimpleJoinTableDiskService,
                    orderRepository: IOrderRepository,
                    componentRepository: IComponentRepository) :
        JoinRepository<Order, Component>(joinDiskService, orderRepository, componentRepository),
        IOrderComponentRepository {

    override fun deleteOrdersWithComponents(components: List<Long>): Completable =
            deleteFirstsWithSeconds(components)

    override fun deleteComponentsWithOrders(orders: List<Long>): Completable =
            deleteSecondsWithFirsts(orders)

    override fun storeOrderWithComponents(orderId: Long,
                                          components: List<Long>?): Maybe<Pair<Order, List<Component>>> =
            storeFirstWithSeconds(orderId, components)

    override fun replaceOrderWithComponents(orderId: Long,
                                            components: List<Long>?): Maybe<Pair<Order, List<Component>>> =
            replaceFirstWithSeconds(orderId, components)

    override fun storeComponentWithOrders(componentId: Long,
                                          orders: List<Long>?): Maybe<Pair<Component, List<Order>>> =
            storeSecondWithFirsts(componentId, orders)

    override fun replaceComponentWithOrders(componentId: Long,
                                            orders: List<Long>?): Maybe<Pair<Component, List<Order>>> =
            replaceSecondWithFirsts(componentId, orders)

    override fun requestOrdersByComponentId(componentId: Long): Maybe<List<Order>> =
            requestFirstsBySecondId(componentId)

    override fun requestComponentsByOrderId(orderId: Long): Maybe<List<Component>> =
            requestSecondsByFirstId(orderId)
}