package tusur.kkv.data.repository

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.Function
import tusur.kkv.data.database.base.DateFormatter
import tusur.kkv.data.database.diskservice.customer.CustomerDiskService
import tusur.kkv.data.database.diskservice.employee.EmployeeDiskService
import tusur.kkv.data.database.diskservice.order.OrderDiskService.Companion.KEY_CUSTOMER_ID
import tusur.kkv.data.database.diskservice.order.OrderDiskService.Companion.KEY_EMPLOYEE_ID
import tusur.kkv.data.database.diskservice.order.OrderDiskService.Companion.KEY_EXECUTION_DATE
import tusur.kkv.data.database.diskservice.order.OrderDiskService.Companion.KEY_ID
import tusur.kkv.data.database.diskservice.order.OrderDiskService.Companion.KEY_REGISTRATION_DATE
import tusur.kkv.data.database.diskservice.order.OrderDiskService.Companion.TABLE_NAME
import tusur.kkv.data.database.diskservice.order.OrderParams
import tusur.kkv.data.exception.IllegalParamsException
import tusur.kkv.data.exception.typeformatter.ExceptionTypeFormatter
import tusur.kkv.data.model.OrderRaw
import tusur.kkv.data.model.tuple.OrderTuple
import tusur.kkv.data.repository.base.ComplexRepository
import tusur.kkv.data.store.DiskService
import tusur.kkv.data.store.ReactiveStore
import tusur.kkv.domain.model.Order
import tusur.kkv.domain.repository.ICustomerRepository
import tusur.kkv.domain.repository.IEmployeeRepository
import tusur.kkv.domain.repository.IOrderRepository
import tusur.kkv.domain.repository.IOrderRepository.ParamsExceptionId.Companion.CUSTOMER
import tusur.kkv.domain.repository.IOrderRepository.ParamsExceptionId.Companion.EMPLOYEE
import tusur.kkv.domain.repository.IOrderRepository.ParamsExceptionId.Companion.EXECUTION_DATE
import tusur.kkv.domain.repository.IOrderRepository.ParamsExceptionId.Companion.REGISTRATION_DATE
import tusur.kkv.domain.repository.IOrderRepository.ParamsExceptionId.Companion.REGISTRATION_DATE_FROM
import tusur.kkv.domain.repository.IOrderRepository.ParamsExceptionId.Companion.REGISTRATION_DATE_TO
import tusur.kkv.domain.repository.ParamsExceptionTypeId.Companion.EMPTY
import tusur.kkv.domain.repository.ParamsExceptionTypeId.Companion.UNSATISFIED_FORMAT
import tusur.kkv.domain.utils.listToMap
import tusur.kkv.domain.utils.mapToList

class OrderRepository(private val employeeRepository: IEmployeeRepository,
                      private val customerRepository: ICustomerRepository,
                      private val dateFormatter: DateFormatter,
                      private val exceptionTypeFormatter: ExceptionTypeFormatter,
                      mapper: Function<OrderTuple, Order>,
                      diskService: DiskService<Long, OrderRaw, OrderParams>,
                      memoryStore: ReactiveStore<Long, Order>) :
        ComplexRepository<OrderTuple, OrderRaw, Order, OrderParams>(mapper,
                diskService,
                memoryStore),
        IOrderRepository {

    override fun search(id: String,
                        customerId: Long?,
                        employeeId: Long?,
                        registrationDateFrom: String?,
                        registrationDateTo: String?): Maybe<List<Order>> {
        val params =
                SearchParams(id, customerId, employeeId, registrationDateFrom, registrationDateTo)
        val selection = listOf(
                id.toLongOrNull()?.let { "$TABLE_NAME.$KEY_ID = $it" },
                customerId?.let { "$KEY_CUSTOMER_ID = $it" },
                employeeId?.let { "$KEY_EMPLOYEE_ID = $it" },
                registrationDateFrom?.let { "$KEY_REGISTRATION_DATE >= \'$it\'" },
                registrationDateTo?.let { "$KEY_REGISTRATION_DATE <= \'$it\'" }
        ).filter { it != null }.joinToString(separator = " and ")
        val columns = listOf(KEY_ID,
                KEY_REGISTRATION_DATE,
                KEY_EXECUTION_DATE,
                KEY_CUSTOMER_ID,
                KEY_EMPLOYEE_ID
        ).joinToString(separator = ", $TABLE_NAME.", prefix = " $TABLE_NAME.")

        return assertSearchParams(params)
                .andThen(diskService.search(
                        "select $columns from $TABLE_NAME " +

                        "left join ${CustomerDiskService.TABLE_NAME} " +
                        "on $TABLE_NAME.$KEY_CUSTOMER_ID = " +
                        "${CustomerDiskService.TABLE_NAME}.${CustomerDiskService.KEY_ID} " +

                        "left join ${EmployeeDiskService.TABLE_NAME} " +
                        "on $TABLE_NAME.$KEY_EMPLOYEE_ID = " +
                        "${EmployeeDiskService.TABLE_NAME}.${EmployeeDiskService.KEY_ID} " +
                        if (selection.isNotBlank()) "where $selection" else ""
                ))
                .flatMap { all -> all.toValues().filter { !it.isEmpty() } }

    }

    override fun store(customerId: Long,
                       employeeId: Long,
                       executionDate: String?,
                       registrationDate: String?): Maybe<Order> {
        val params = OrderParams(customerId, employeeId, executionDate, registrationDate)
        return assertParams(params)
                .andThen(diskService.putSingular(params))
                .flatMapMaybe { requestSingular(it) }
                .doOnSuccess { memoryStore.storeSingular(it) }
    }

    override fun replace(id: Long,
                         customerId: Long,
                         employeeId: Long,
                         executionDate: String?,
                         registrationDate: String?): Completable {
        val params = OrderParams(customerId, employeeId, executionDate, registrationDate)
        return assertParams(params)
                .andThen(diskService.replaceSingular(id, params))
                .andThen(requestSingular(id))
                .doOnSuccess { memoryStore.storeSingular(it) }
                .ignoreElement()
    }

    override fun List<OrderRaw>.toValues(): Single<List<Order>> {
        val employees = employeeRepository.requestSeveral(mapToList { employeeId }).listToMap { id }
        val customers = customerRepository.requestSeveral(mapToList { customerId }).listToMap { id }
        return employees.flatMap { e ->
            customers.flatMap { c ->
                Observable.fromIterable(this)
                        .map { OrderTuple(it, e[it.employeeId]!!, c[it.customerId]!!) }
                        .map(mapper)
                        .toList()
            }
        }
    }

    private fun assertSearchParams(params: SearchParams): Completable =
            when {
                params.registrationDateFrom.let { it != null && !dateFormatter.matchesServer(it) } ->
                    Completable.error(IllegalParamsException(REGISTRATION_DATE_FROM,
                            exceptionTypeFormatter.apply(UNSATISFIED_FORMAT)))
                params.registrationDateTo.let { it != null && !dateFormatter.matchesServer(it) } ->
                    Completable.error(IllegalParamsException(REGISTRATION_DATE_TO,
                            exceptionTypeFormatter.apply(UNSATISFIED_FORMAT)))
                else -> Completable.complete()
            }

    private fun assertParams(params: OrderParams): Completable =
            when {
                params.registrationDate != null && !dateFormatter.matchesServer(params.registrationDate) ->
                    Completable.error(IllegalParamsException(REGISTRATION_DATE,
                            exceptionTypeFormatter.apply(UNSATISFIED_FORMAT)))
                params.executionDate != null && !dateFormatter.matchesServer(params.executionDate) ->
                    Completable.error(IllegalParamsException(EXECUTION_DATE,
                            exceptionTypeFormatter.apply(UNSATISFIED_FORMAT)))
                params.customerId < 0 ->
                    Completable.error(IllegalParamsException(CUSTOMER,
                            exceptionTypeFormatter.apply(EMPTY)))
                params.employeeId < 0 ->
                    Completable.error(IllegalParamsException(EMPLOYEE,
                            exceptionTypeFormatter.apply(EMPTY)))
                else -> Completable.complete()
            }

    private data class SearchParams(val id: String,
                                    val customerId: Long?,
                                    val employeeId: Long?,
                                    val registrationDateFrom: String?,
                                    val registrationDateTo: String?)
}