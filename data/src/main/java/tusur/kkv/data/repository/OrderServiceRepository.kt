package tusur.kkv.data.repository

import io.reactivex.Completable
import io.reactivex.Maybe
import tusur.kkv.data.database.base.SimpleJoinTableDiskService
import tusur.kkv.data.repository.base.JoinRepository
import tusur.kkv.domain.model.Order
import tusur.kkv.domain.model.Service
import tusur.kkv.domain.repository.IOrderRepository
import tusur.kkv.domain.repository.IOrderServiceRepository
import tusur.kkv.domain.repository.IServiceRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class OrderServiceRepository
@Inject constructor(joinDiskService: SimpleJoinTableDiskService,
                    orderRepository: IOrderRepository,
                    serviceRepository: IServiceRepository) :
        JoinRepository<Order, Service>(joinDiskService, orderRepository, serviceRepository),
        IOrderServiceRepository {

    override fun deleteOrdersWithServices(services: List<Long>): Completable =
            deleteFirstsWithSeconds(services)

    override fun deleteServicesWithOrders(orders: List<Long>): Completable =
            deleteSecondsWithFirsts(orders)

    override fun storeOrderWithServices(orderId: Long,
                                        services: List<Long>?): Maybe<Pair<Order, List<Service>>> =
            storeFirstWithSeconds(orderId, services)

    override fun replaceOrderWithServices(orderId: Long,
                                          services: List<Long>?): Maybe<Pair<Order, List<Service>>> =
            replaceFirstWithSeconds(orderId, services)

    override fun storeServiceWithOrders(serviceId: Long,
                                        orders: List<Long>?): Maybe<Pair<Service, List<Order>>> =
            storeSecondWithFirsts(serviceId, orders)

    override fun replaceServiceWithOrders(serviceId: Long,
                                          orders: List<Long>?): Maybe<Pair<Service, List<Order>>> =
            replaceSecondWithFirsts(serviceId, orders)

    override fun requestOrdersByServiceId(serviceId: Long): Maybe<List<Order>> =
            requestFirstsBySecondId(serviceId)

    override fun requestServicesByOrderId(orderId: Long): Maybe<List<Service>> =
            requestSecondsByFirstId(orderId)
}