package tusur.kkv.data.repository

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.functions.Function
import tusur.kkv.data.database.diskservice.position.PositionParams
import tusur.kkv.data.exception.IllegalParamsException
import tusur.kkv.data.exception.typeformatter.ExceptionTypeFormatter
import tusur.kkv.data.model.PositionRaw
import tusur.kkv.data.repository.base.SimpleRepository
import tusur.kkv.data.store.DiskService
import tusur.kkv.data.store.MemoryReactiveStore
import tusur.kkv.domain.model.Position
import tusur.kkv.domain.repository.IPositionRepository
import tusur.kkv.domain.repository.IPositionRepository.ParamsExceptionId.Companion.DUTIES
import tusur.kkv.domain.repository.IPositionRepository.ParamsExceptionId.Companion.NAME
import tusur.kkv.domain.repository.IPositionRepository.ParamsExceptionId.Companion.SALARY
import tusur.kkv.domain.repository.ParamsExceptionTypeId.Companion.EMPTY
import tusur.kkv.domain.repository.ParamsExceptionTypeId.Companion.UNSATISFIED_FORMAT
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PositionRepository
@Inject constructor(private val exceptionTypeFormatter: ExceptionTypeFormatter,
                    diskStore: DiskService<Long, PositionRaw, PositionParams>,
                    memoryStore: MemoryReactiveStore<Long, Position>,
                    mapper: Function<PositionRaw, Position>) :
        SimpleRepository<PositionRaw, Position, PositionParams>(diskStore, memoryStore, mapper),
        IPositionRepository {

    override fun store(name: String, salary: Long?, duties: String): Maybe<Position> {
        val params = PositionParams(name, salary, duties)
        return assertParams(params)
                .andThen(diskService.putSingular(params))
                .flatMapMaybe { diskService.singular(it) }
                .map(mapper)
                .doOnSuccess { memoryStore.storeSingular(it) }
    }

    private fun assertParams(params: PositionParams): Completable =
            when {
                params.name.isBlank() ->
                    Completable.error(IllegalParamsException(NAME,
                            exceptionTypeFormatter.apply(EMPTY)))
                params.salary == null ->
                    Completable.error(IllegalParamsException(SALARY,
                            exceptionTypeFormatter.apply(EMPTY)))
                params.salary < 0 ->
                    Completable.error(IllegalParamsException(SALARY,
                            exceptionTypeFormatter.apply(UNSATISFIED_FORMAT)))
                params.duties.isBlank() ->
                    Completable.error(IllegalParamsException(DUTIES,
                            exceptionTypeFormatter.apply(EMPTY)))
                else -> Completable.complete()
            }

    override fun replace(id: Long, name: String, salary: Long, duties: String): Completable =
            diskService.replaceSingular(id, PositionParams(name, salary, duties))
                    .andThen(requestSingular(id))
                    .doOnSuccess { memoryStore.storeSingular(it) }
                    .ignoreElement()


}