package tusur.kkv.data.repository

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.functions.Function
import tusur.kkv.data.database.diskservice.purveyor.PurveyorParams
import tusur.kkv.data.exception.IllegalParamsException
import tusur.kkv.data.exception.typeformatter.ExceptionTypeFormatter
import tusur.kkv.data.model.PurveyorRaw
import tusur.kkv.data.repository.base.SimpleRepository
import tusur.kkv.data.store.DiskService
import tusur.kkv.data.store.ReactiveStore
import tusur.kkv.domain.model.Purveyor
import tusur.kkv.domain.repository.IPurveyorRepository
import tusur.kkv.domain.repository.IPurveyorRepository.ParamsExceptionId.Companion.ADDRESS
import tusur.kkv.domain.repository.IPurveyorRepository.ParamsExceptionId.Companion.NAME
import tusur.kkv.domain.repository.IPurveyorRepository.ParamsExceptionId.Companion.PHONE
import tusur.kkv.domain.repository.ParamsExceptionTypeId

class PurveyorRepository(diskStore: DiskService<Long, PurveyorRaw, PurveyorParams>,
                         memoryStore: ReactiveStore<Long, Purveyor>,
                         mapper: Function<PurveyorRaw, Purveyor>,
                         private val exceptionTypeFormatter: ExceptionTypeFormatter) :
        SimpleRepository<PurveyorRaw, Purveyor, PurveyorParams>(diskStore, memoryStore, mapper),
        IPurveyorRepository {

    override fun store(name: String,
                       phoneNumber: String,
                       address: String,
                       representativeName: String?): Maybe<Purveyor> {
        val params = PurveyorParams(name, phoneNumber, address, representativeName)
        return assertParams(params)
                .andThen(diskService.putSingular(params))
                .flatMapMaybe { diskService.singular(it) }
                .map(mapper)
                .doOnSuccess { memoryStore.storeSingular(it) }
    }

    override fun replace(id: Long,
                         name: String,
                         phoneNumber: String,
                         address: String,
                         representativeName: String?): Completable {
        val params = PurveyorParams(name, phoneNumber, address, representativeName)
        return assertParams(params)
                .andThen(diskService.replaceSingular(id, params))
                .andThen(requestSingular(id))
                .doOnSuccess { memoryStore.storeSingular(it) }
                .ignoreElement()
    }

    private fun assertParams(params: PurveyorParams): Completable = when {
        params.name.isBlank() ->
            Completable.error(IllegalParamsException(NAME,
                    exceptionTypeFormatter.apply(ParamsExceptionTypeId.EMPTY)))
        params.phoneNumber.isBlank() ->
            Completable.error(IllegalParamsException(PHONE,
                    exceptionTypeFormatter.apply(ParamsExceptionTypeId.EMPTY)))
        params.address.isBlank() ->
            Completable.error(IllegalParamsException(ADDRESS,
                    exceptionTypeFormatter.apply(ParamsExceptionTypeId.EMPTY)))
        else -> Completable.complete()
    }

}