package tusur.kkv.data.repository

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.functions.Function
import tusur.kkv.data.database.diskservice.order.OrderDiskService
import tusur.kkv.data.database.diskservice.orderservicejoin.OrderServiceJoinDiskService
import tusur.kkv.data.database.diskservice.service.ServiceDiskService
import tusur.kkv.data.database.diskservice.service.ServiceDiskService.Companion.KEY_ID
import tusur.kkv.data.database.diskservice.service.ServiceDiskService.Companion.KEY_NAME
import tusur.kkv.data.database.diskservice.service.ServiceDiskService.Companion.TABLE_NAME
import tusur.kkv.data.database.diskservice.service.ServiceDiskService.Companion.allKeys
import tusur.kkv.data.database.diskservice.service.ServiceParams
import tusur.kkv.data.database.mapper.BestEmployeeRawFactory
import tusur.kkv.data.database.mapper.BestServiceRawFactory
import tusur.kkv.data.database.mapper.CursorListMapper
import tusur.kkv.data.exception.IllegalParamsException
import tusur.kkv.data.exception.typeformatter.ExceptionTypeFormatter
import tusur.kkv.data.mapper.BestServiceMapper
import tusur.kkv.data.model.ServiceRaw
import tusur.kkv.data.repository.base.SimpleRepository
import tusur.kkv.data.store.DiskService
import tusur.kkv.data.store.ReactiveStore
import tusur.kkv.domain.model.BestService
import tusur.kkv.domain.model.Service
import tusur.kkv.domain.repository.IServiceRepository
import tusur.kkv.domain.repository.IServiceRepository.ParamsExceptionId.Companion.COST
import tusur.kkv.domain.repository.IServiceRepository.ParamsExceptionId.Companion.DESCRIPTION
import tusur.kkv.domain.repository.IServiceRepository.ParamsExceptionId.Companion.NAME
import tusur.kkv.domain.repository.ParamsExceptionTypeId.Companion.EMPTY
import tusur.kkv.domain.repository.ParamsExceptionTypeId.Companion.UNSATISFIED_FORMAT

class ServiceRepository(diskStore: DiskService<Long, ServiceRaw, ServiceParams>,
                        memoryStore: ReactiveStore<Long, Service>,
                        mapper: Function<ServiceRaw, Service>,
                        private val bestServiceRawFactory: BestServiceRawFactory,
                        private val bestServiceMapper: BestServiceMapper,
                        private val exceptionTypeFormatter: ExceptionTypeFormatter) :
        SimpleRepository<ServiceRaw, Service, ServiceParams>(diskStore, memoryStore, mapper),
        IServiceRepository {

    override fun best(limit: Long): Maybe<List<BestService>> {
        val columns = allKeys()
                              .joinToString(
                                      separator = ", $TABLE_NAME.",
                                      prefix = " $TABLE_NAME.") + ", " +
                      "sum (${ServiceDiskService.TABLE_NAME}.${ServiceDiskService.KEY_COST}) " +
                      "as ${BestServiceRawFactory.REVENUE}"

        return diskService.searchCursor(
                "select $columns from $TABLE_NAME " +

                "left join ${OrderServiceJoinDiskService.TABLE_NAME} " +
                "on ${OrderServiceJoinDiskService.TABLE_NAME}.${OrderServiceJoinDiskService.KEY_SERVICE_ID} = " +
                "$TABLE_NAME.$KEY_ID " +

                "left join ${OrderDiskService.TABLE_NAME} " +
                "on ${OrderDiskService.TABLE_NAME}.${OrderDiskService.KEY_ID} = " +
                "${OrderServiceJoinDiskService.TABLE_NAME}.${OrderServiceJoinDiskService.KEY_ORDER_ID} " +


                "group by $TABLE_NAME.$KEY_ID " +
                "order by ${BestEmployeeRawFactory.REVENUE} desc, " +
                "$TABLE_NAME.$KEY_NAME asc"
        )
                .flatMapMaybe(CursorListMapper(bestServiceRawFactory))
                .flatMap { list ->
                    Observable.fromIterable(list)
                            .map(bestServiceMapper)
                            .toList()
                            .filter { it.isNotEmpty() }
                }
    }

    override fun store(name: String, cost: Long, description: String?): Maybe<Service> {
        val params = ServiceParams(name, cost, description)
        return assertParams(params)
                .andThen(diskService.putSingular(params))
                .flatMapMaybe { diskService.singular(it) }
                .map(mapper)
                .doOnSuccess { memoryStore.storeSingular(it) }
    }

    override fun replace(id: Long, name: String, cost: Long, description: String?): Completable {
        val params = ServiceParams(name, cost, description)
        return assertParams(params)
                .andThen(diskService.replaceSingular(id, params))
                .andThen(requestSingular(id))
                .doOnSuccess { memoryStore.storeSingular(it) }
                .ignoreElement()
    }

    private fun assertParams(params: ServiceParams): Completable =
            when {
                params.name.isBlank() ->
                    Completable.error(IllegalParamsException(NAME,
                            exceptionTypeFormatter.apply(EMPTY)))
                params.cost < 0 ->
                    Completable.error(IllegalParamsException(COST,
                            exceptionTypeFormatter.apply(UNSATISFIED_FORMAT)))
                params.description != null && params.description.isBlank() ->
                    Completable.error(IllegalParamsException(DESCRIPTION,
                            exceptionTypeFormatter.apply(UNSATISFIED_FORMAT)))
                else -> Completable.complete()
            }


}