package tusur.kkv.data.repository.base

import com.annimon.stream.Optional
import io.reactivex.Completable
import io.reactivex.Observable
import tusur.kkv.data.store.DiskService
import tusur.kkv.data.store.ReactiveStore
import tusur.kkv.domain.repository.IBaseRepository

abstract class BaseRepository<Key, Raw, Value, Params>(protected val diskService: DiskService<Key, Raw, Params>,
                                                       protected val memoryStore: ReactiveStore<Key, Value>)
    : IBaseRepository<Key, Value> {

    override fun getAll(): Observable<Optional<List<Value>>> {
        return memoryStore.getAll()
    }

    override fun delete(id: Key): Completable {
        return diskService.deleteSingular(id)
                .andThen { memoryStore.removeSingular(id) }
    }
}