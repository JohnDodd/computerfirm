package tusur.kkv.data.repository.base

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.functions.Function
import tusur.kkv.data.store.DiskService
import tusur.kkv.data.store.ReactiveStore

abstract class ComplexRepository<Tuple, Raw, Value, Params>(protected val mapper: Function<Tuple, Value>,
                                                            diskService: DiskService<Long, Raw, Params>,
                                                            memoryStore: ReactiveStore<Long, Value>) :
        BaseRepository<Long, Raw, Value, Params>(diskService, memoryStore) {

    override fun requestSeveral(keys: List<Long>): Maybe<List<Value>> {
        return diskService.several(keys)
                .flatMap { all -> all.toValues().filter { !it.isEmpty() } }
    }

    override fun requestSingular(key: Long): Maybe<Value> {
        return diskService.singular(key)
                .flatMap { raw -> listOf(raw).toValues().filter { !it.isEmpty() } }
                .map { it.first() }
    }

    override fun fetch(): Completable {
        return diskService.all()
                .flatMap { all -> all.toValues().toMaybe() }
                .doOnSuccess { memoryStore.replaceAll(it) }
                .ignoreElement()
    }

    protected abstract fun List<Raw>.toValues(): Single<List<Value>>

}

