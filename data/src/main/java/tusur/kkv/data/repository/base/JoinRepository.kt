package tusur.kkv.data.repository.base

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.rxkotlin.Maybes
import tusur.kkv.data.database.base.SimpleJoinTableDiskService
import tusur.kkv.domain.repository.ISimpleRepository
import tusur.kkv.domain.utils.extractParam

abstract class JoinRepository<Value1, Value2>(protected val joinDiskService: SimpleJoinTableDiskService,
                                              private val firstRepository: ISimpleRepository<Value1>,
                                              private val secondRepository: ISimpleRepository<Value2>) {
    protected fun deleteSecondsWithFirsts(firsts: List<Long>): Completable =
            joinDiskService.deleteByFirst(*firsts.toTypedArray())

    protected fun deleteFirstsWithSeconds(seconds: List<Long>): Completable =
            joinDiskService.deleteBySecond(*seconds.toTypedArray())

    protected fun storeFirstWithSeconds(firstId: Long,
                                        seconds: List<Long>?): Maybe<Pair<Value1, List<Value2>>> =
            if (seconds == null)
                Maybe.empty()
            else Observable.fromIterable(seconds)
                    .flatMapSingle { joinDiskService.putSingular(firstId, it) }
                    .toList()
                    .flatMapMaybe { joinDiskService.allByFirst(firstId) }
                    .flatMap {
                        Maybes.zip(firstRepository.requestSingular(firstId),
                                secondRepository.requestSeveral(seconds))
                    }

    protected fun replaceFirstWithSeconds(firstId: Long,
                                          seconds: List<Long>?): Maybe<Pair<Value1, List<Value2>>> =
            if (seconds == null)
                Maybe.empty()
            else joinDiskService.deleteByFirst(firstId)
                    .andThen(storeFirstWithSeconds(firstId, seconds))


    protected fun storeSecondWithFirsts(secondId: Long,
                                        firsts: List<Long>?): Maybe<Pair<Value2, List<Value1>>> =
            if (firsts == null)
                Maybe.empty()
            else Observable.fromIterable(firsts)
                    .flatMapSingle { joinDiskService.putSingular(it, secondId) }
                    .toList()
                    .flatMapMaybe { joinDiskService.allBySecond(secondId) }
                    .flatMap {
                        Maybes.zip(secondRepository.requestSingular(secondId),
                                firstRepository.requestSeveral(firsts))
                    }


    protected fun replaceSecondWithFirsts(secondId: Long,
                                          firsts: List<Long>?): Maybe<Pair<Value2, List<Value1>>> =
            if (firsts == null)
                Maybe.empty()
            else joinDiskService.deleteBySecond(secondId)
                    .andThen(storeSecondWithFirsts(secondId, firsts))

    protected fun requestFirstsBySecondId(secondId: Long): Maybe<List<Value1>> =
            joinDiskService.allBySecond(secondId)
                    .extractParam { firstId }
                    .flatMap(firstRepository::requestSeveral)

    protected fun requestSecondsByFirstId(firstId: Long): Maybe<List<Value2>> =
            joinDiskService.allByFirst(firstId)
                    .extractParam { secondId }
                    .flatMap(secondRepository::requestSeveral)

}