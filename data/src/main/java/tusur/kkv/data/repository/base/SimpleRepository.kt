package tusur.kkv.data.repository.base

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.functions.Function
import tusur.kkv.data.store.DiskService
import tusur.kkv.data.store.ReactiveStore

abstract class SimpleRepository<Raw, Value, Params>(diskService: DiskService<Long, Raw, Params>,
                                                    memoryStore: ReactiveStore<Long, Value>,
                                                    protected val mapper: Function<Raw, Value>)
    : BaseRepository<Long, Raw, Value, Params>(diskService, memoryStore) {

    override fun fetch(): Completable {
        return diskService.all()
                .flatMapObservable { Observable.fromIterable(it) }
                .map(mapper)
                .toList()
                .doOnSuccess { memoryStore.replaceAll(it) }
                .ignoreElement()
    }

    override fun requestSeveral(keys: List<Long>): Maybe<List<Value>> =
            diskService.several(keys)
                    .flatMap {
                        Observable.fromIterable(it)
                                .map(mapper)
                                .toList()
                                .toMaybe()
                    }
                    .flatMap { if (it.isEmpty()) Maybe.empty() else Maybe.just(it) }

    override fun requestSingular(key: Long): Maybe<Value> = diskService.singular(key).map(mapper)
}