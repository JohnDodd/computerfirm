package tusur.kkv.data.store

import android.database.Cursor
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

interface DiskService<Key, Value, Params> {

    fun all(): Maybe<List<Value>>

    fun several(keys: List<Key>): Maybe<List<Value>>

    fun singular(key: Key): Maybe<Value>

    fun putAll(paramsList: List<Params>): Single<List<Value>>

    fun putSingular(params: Params): Single<Key>

    fun replaceSingular(key: Key, params: Params): Completable

    fun deleteSingular(key: Key): Completable

    fun clear(): Completable

    fun search(rawQuery: String): Maybe<List<Value>>

    fun searchCursor(rawQuery: String): Single<Cursor>
}