package tusur.kkv.data.store

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

interface JoinDiskService<Key1, Key2, Value> {

    fun allByFirst(key: Key1): Maybe<List<Value>>

    fun allBySecond(key: Key2): Maybe<List<Value>>

    fun putSingular(key1: Key1, key2: Key2): Single<Long>

    fun deleteSingular(key1: Key1, key2: Key2): Completable
    fun deleteByFirst(vararg keys: Key1): Completable
    fun deleteBySecond(vararg keys: Key2): Completable
}