package tusur.kkv.data.store

import com.annimon.stream.Optional
import com.annimon.stream.Optional.empty
import com.annimon.stream.Optional.of
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.observables.GroupedObservable
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import tusur.kkv.data.injection.qualifier.ForComputation
import java.util.*
import javax.inject.Inject

class MemoryReactiveStore<Key, Value>
@Inject constructor(private val cache: Store.MemoryStore<Key, Value>,
                    private val extractKeyFromModel: (Value) -> Key,
                    @ForComputation
                    private val schedulerComputation: Scheduler) : ReactiveStore<Key, Value> {

    private val allSubject = PublishSubject.create<Optional<List<Value>>>().toSerialized()

    private val subjectMap = HashMap<Key, Subject<Optional<Value>>>()

    override fun removeSingular(key: Key) {
        cache.deleteSingular(key)
        getOrCreateSubjectForKey(key).onNext(empty())
        allSubject.onNext(cache.all().map { of(it) }.blockingGet(empty()))
    }

    override fun storeSingular(value: Value) {
        val key = extractKeyFromModel(value)
        cache.putSingular(value)
        getOrCreateSubjectForKey(key).onNext(of(value))
        // One item has been added/updated, notify to all as well
        val allValues = cache.all().map { of(it) }.blockingGet(empty())
        allSubject.onNext(allValues)
    }

    override fun replace(value: Value) {
        storeSingular(value)
    }

    override fun storeAll(values: List<Value>) {
        cache.putAll(values)
        allSubject.onNext(of(values))
        // Publish in all the existing single item streams.
        // This could be improved publishing only in the items that changed. Maybe use DiffUtils?
        publishInEachKey()
    }

    override fun replaceAll(values: List<Value>) {
        cache.clear()
        storeAll(values)
    }

    override fun getSingular(key: Key): Observable<Optional<Value>> {
        return Observable.defer { getOrCreateSubjectForKey(key).startWith(getValue(key)) }
                .observeOn(schedulerComputation)
    }

    override fun getAll(): Observable<Optional<List<Value>>> {
        return Observable.defer { allSubject.startWith(getAllValues()) }
                .observeOn(schedulerComputation)
    }

    override fun getSeveral(keys: List<Key>): Observable<GroupedObservable<Key, Optional<Value>>> {
        return Observable.fromIterable(keys)
                .flatMap { key -> getOrCreateSubjectForKey(key).map { Pair(key, it) } }
                .groupBy({ it.first }, { it.second })
    }

    private fun getValue(key: Key): Optional<Value> {
        return cache.singular(key).map { of(it) }.blockingGet(empty())
    }

    private fun getAllValues(): Optional<List<Value>> {
        return cache.all().map { of(it) }.blockingGet(empty())
    }

    private fun getOrCreateSubjectForKey(key: Key): Subject<Optional<Value>> {
        synchronized(subjectMap) {
            return subjectMap[key] ?: createAndStoreNewSubjectForKey(key)
        }
    }

    private fun createAndStoreNewSubjectForKey(key: Key): Subject<Optional<Value>> {
        val processor = PublishSubject.create<Optional<Value>>().toSerialized()
        synchronized(subjectMap) {
            subjectMap.put(key, processor)
        }
        return processor
    }

    /**
     * Publishes the cached data in each independent stream only if it exists already.
     */
    private fun publishInEachKey() {
        val keySet = HashSet(subjectMap.keys)
        for (key in keySet) {
            val value = cache.singular(key).map { of(it) }.blockingGet(empty())
            publishInKey(key, value)
        }
    }

    /**
     * Publishes the cached value if there is an already existing stream for the passed key. The case where there isn't a stream for the passed key
     * means that the data for this key is not being consumed and therefore there is no need to publish.
     */
    private fun publishInKey(key: Key, model: Optional<Value>) {
        subjectMap[key]?.onNext(model)
    }

}