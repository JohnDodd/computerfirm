package tusur.kkv.data.store

import com.annimon.stream.Optional
import io.reactivex.Observable
import io.reactivex.observables.GroupedObservable

interface ReactiveStore<Key, Value> {

    fun removeSingular(key: Key)
    fun storeSingular(value: Value)
    fun replace(value: Value)
    fun storeAll(values: List<Value>)
    fun replaceAll(values: List<Value>)
    fun getSingular(key: Key): Observable<Optional<Value>>
    fun getAll(): Observable<Optional<List<Value>>>
    fun getSeveral(keys: List<Key>): Observable<GroupedObservable<Key, Optional<Value>>>
}