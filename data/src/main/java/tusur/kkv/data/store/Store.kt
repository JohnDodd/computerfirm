package tusur.kkv.data.store

import io.reactivex.Maybe

interface Store<Key, Value> {

    fun all(): Maybe<List<Value>>

    fun several(keys: List<Key>): Maybe<List<Value>>

    fun singular(key: Key): Maybe<Value>

    fun putAll(values: List<Value>)

    fun putSingular(value: Value)

    fun deleteSingular(key: Key)

    fun clear()

    interface MemoryStore<Key, Value> : Store<Key, Value>

}