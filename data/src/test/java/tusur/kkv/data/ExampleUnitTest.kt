package tusur.kkv.data

import org.junit.Test
import tusur.kkv.data.base.BaseTest

class ExampleUnitTest : BaseTest() {

    @Test
    fun `test string join`() {

        val keys = listOf(1, 2, 3, 4, 5)
        val result = keys.mapIndexed { index, key -> "$key in smth and " }
                .joinToString(separator = "")
        println(result)
    }
}