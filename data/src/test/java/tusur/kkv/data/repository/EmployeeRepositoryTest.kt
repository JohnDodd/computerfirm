package tusur.kkv.data.repository

import com.nhaarman.mockitokotlin2.*
import io.reactivex.Maybe
import io.reactivex.functions.Function
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test
import tusur.kkv.data.base.BaseTest
import tusur.kkv.data.database.diskservice.employee.EmployeeParams
import tusur.kkv.data.model.EmployeeRaw
import tusur.kkv.data.model.tuple.EmployeeTuple
import tusur.kkv.data.store.DiskService
import tusur.kkv.data.store.ReactiveStore
import tusur.kkv.domain.model.Employee
import tusur.kkv.domain.model.Position
import tusur.kkv.domain.repository.IPositionRepository

class EmployeeRepositoryTest : BaseTest() {

    lateinit var diskStore: DiskService<Long, EmployeeRaw, EmployeeParams>
    lateinit var memoryStore: ReactiveStore<Long, Employee>
    lateinit var mapper: Function<EmployeeTuple, Employee>
    lateinit var positionRepository: IPositionRepository
    lateinit var repository: EmployeeRepository

    @Before
    fun setUp() {
        val scheduler = TestScheduler()
        diskStore = mock()
        memoryStore = mock()//MemoryReactiveStore(mock(), mock(), scheduler)
        positionRepository = mock()
        mapper = mock()
        whenever(mapper.apply(any())).thenReturn(mock())

        repository = EmployeeRepository(
                positionRepository,
                mapper,
                diskStore,
                memoryStore)
    }

    @Test
    fun `fetched ok when same emloyees and positions`() {
        ArrangeBuilder()
                .withMaybeFromEmployeeDiskStore(Maybe.just(createEmployeeRaws(1)))
                .withMaybeFromPositionRepository(Maybe.just(createPositions(1)))
        verifyFetch(1, 1)
    }

    @Test
    fun `fetched ok when no emloyees and some positions`() {
        ArrangeBuilder()
                .withMaybeFromEmployeeDiskStore(Maybe.empty())
                .withMaybeFromPositionRepository(Maybe.just(createPositions(1, 2, 3)))
        verifyFetch(0, 0)
    }

    @Test
    fun `fetched ok when no positions`() {
        ArrangeBuilder()
                .withMaybeFromEmployeeDiskStore(Maybe.just(createEmployeeRaws(1)))
                .withMaybeFromPositionRepository(Maybe.empty())
        verifyFetch(1, 0)
    }

    @Test
    fun `fetched ok when 3 emloyees with 2 positions`() {
        ArrangeBuilder()
                .withMaybeFromEmployeeDiskStore(Maybe.just(createEmployeeRaws(0, 1, 2, 3, 4)))
                .withMaybeFromPositionRepository(Maybe.just(createPositions(3, 4, 5, 6, 7)))
        verifyFetch(5, 2)
    }

    private fun verifyFetch(employeesSize: Int, crossings: Int) {
        repository.fetch().test().assertComplete()
        verify(positionRepository, atMost(employeesSize)).requestSeveral(any())
        verify(memoryStore, atMost(employeesSize)).replaceAll(argThat { size == employeesSize })

    }

    private fun createEmployeeRaws(vararg positionId: Long) =
            positionId.map { createEmployeeRaw(it) }

    private fun createEmployeeRaw(positionId: Long) =
            EmployeeRaw(1, "", "", "", "", "", "", positionId)

    private fun createPosition(positionId: Long) = Position(positionId, "", 0, "")

    private fun createPositions(vararg positionIds: Long) = positionIds.map { createPosition(it) }

    private inner class ArrangeBuilder {

        fun withMaybeFromEmployeeDiskStore(maybe: Maybe<List<EmployeeRaw>>): ArrangeBuilder {
            whenever(diskStore.all()).thenReturn(maybe)
            return this
        }

        fun withMaybeFromPositionRepository(maybe: Maybe<List<Position>>) = apply {
            whenever(positionRepository.requestSeveral(any())).thenReturn(maybe)
        }


    }

}