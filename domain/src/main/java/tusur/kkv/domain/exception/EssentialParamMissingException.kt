package tusur.kkv.domain.exception

class EssentialParamMissingException(missingParam: String,
                                     rawObject: Any)
    : RuntimeException("The params: $missingParam are missing in received object: $rawObject")