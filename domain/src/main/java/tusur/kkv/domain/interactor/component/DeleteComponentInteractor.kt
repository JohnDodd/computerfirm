package tusur.kkv.domain.interactor.component

import com.annimon.stream.Optional
import io.reactivex.Completable
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.repository.IComponentRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class DeleteComponentInteractor
@Inject constructor(private val repository: IComponentRepository)
    : ReactiveInteractor.DeleteInteractor<Long> {

    override fun getSingle(params: Optional<Long>): Completable {
        val id = params.unwrapOrThrow()
        return repository.delete(id)
    }
}