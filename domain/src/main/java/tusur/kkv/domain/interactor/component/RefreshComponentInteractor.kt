package tusur.kkv.domain.interactor.component

import com.annimon.stream.Optional
import io.reactivex.Completable
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.repository.IComponentRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class RefreshComponentInteractor @Inject constructor(private val repository: IComponentRepository)
    : ReactiveInteractor.RefreshInteractor<RefreshComponentInteractor.Params> {

    override fun getRefreshSingle(params: Optional<Params>): Completable {
        val unwrapped = params.unwrapOrThrow()
        return repository.replace(
                unwrapped.id,
                unwrapped.typeId,
                unwrapped.guaranteePeriod,
                unwrapped.name,
                unwrapped.cost,
                unwrapped.purveyorId
        )
    }

    data class Params(val id: Long,
                      val typeId: Long,
                      val guaranteePeriod: Long?,
                      val name: String,
                      val cost: Long,
                      val purveyorId: Long)
}