package tusur.kkv.domain.interactor.component

import com.annimon.stream.Optional
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.model.Component
import tusur.kkv.domain.repository.IComponentRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class RequestComponentInteractor @Inject constructor(private val repository: IComponentRepository)
    : ReactiveInteractor.RequestInteractor<Long, Component> {

    override fun getSingle(params: Optional<Long>): Single<Component> {
        val id = params.unwrapOrThrow()
        return repository.requestSeveral(listOf(id)).map { it.first() }.toSingle()
    }
}