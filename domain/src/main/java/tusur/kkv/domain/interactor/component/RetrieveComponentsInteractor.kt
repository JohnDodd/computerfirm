package tusur.kkv.domain.interactor.component

import com.annimon.stream.Optional
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.model.Component
import tusur.kkv.domain.repository.IComponentRepository
import tusur.kkv.domain.utils.unwrap
import javax.inject.Inject

class RetrieveComponentsInteractor @Inject constructor(private val repository: IComponentRepository) :
        ReactiveInteractor.RetrieveInteractor<Void, List<Component>> {

    override fun getBehaviorStream(params: Optional<Void>): Observable<List<Component>> {
        return repository.getAll()
                .flatMapSingle { fetchWhenNoneAndThenComponent(it) }
                .unwrap()
    }

    private fun fetchWhenNoneAndThenComponent(components: Optional<List<Component>>): Single<Optional<List<Component>>> {
        return fetchWhenNone(components).andThen(Single.just(components))
    }

    private fun fetchWhenNone(components: Optional<List<Component>>): Completable {
        return if (components.isPresent) Completable.complete() else repository.fetch()
    }
}