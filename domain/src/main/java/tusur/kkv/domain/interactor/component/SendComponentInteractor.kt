package tusur.kkv.domain.interactor.component

import com.annimon.stream.Optional
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.repository.IComponentRepository
import tusur.kkv.domain.utils.isNotEmpty
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class SendComponentInteractor @Inject constructor(private val repository: IComponentRepository)
    : ReactiveInteractor.SendInteractor<SendComponentInteractor.Params, Boolean> {

    override fun getSingle(params: Optional<Params>): Single<Boolean> {
        val unwrapped = params.unwrapOrThrow()
        return repository.store(
                unwrapped.typeId,
                unwrapped.guaranteePeriod,
                unwrapped.name,
                unwrapped.cost,
                unwrapped.purveyorId
        )
                .isNotEmpty()
    }

    data class Params(val typeId: Long,
                      val guaranteePeriod: Long?,
                      val name: String,
                      val cost: Long,
                      val purveyorId: Long)
}
