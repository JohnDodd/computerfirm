package tusur.kkv.domain.interactor.componenttype.choose

import com.annimon.stream.Optional
import io.reactivex.Completable
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.repository.IComponentTypeRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class DeleteComponentTypesInteractor @Inject constructor(private val repository: IComponentTypeRepository)
    : ReactiveInteractor.DeleteInteractor<Long> {

    override fun getSingle(params: Optional<Long>): Completable {
        val id = params.unwrapOrThrow()
        return repository.delete(id)
    }

}