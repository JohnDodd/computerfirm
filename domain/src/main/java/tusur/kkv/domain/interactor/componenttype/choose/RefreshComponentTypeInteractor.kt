package tusur.kkv.domain.interactor.componenttype.choose

import com.annimon.stream.Optional
import io.reactivex.Completable
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.interactor.componenttype.choose.RefreshComponentTypeInteractor.Params
import tusur.kkv.domain.repository.IComponentTypeRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class RefreshComponentTypeInteractor @Inject constructor(private val repository: IComponentTypeRepository)
    : ReactiveInteractor.RefreshInteractor<Params> {

    override fun getRefreshSingle(params: Optional<Params>): Completable {
        val unwrap = params.unwrapOrThrow()
        return repository
                .replace(unwrap.id,
                        unwrap.name,
                        unwrap.specifications,
                        unwrap.manufacturer)
    }

    data class Params(val id: Long,
                      val name: String,
                      val specifications: String,
                      val manufacturer: String)
}