package tusur.kkv.domain.interactor.componenttype.choose

import com.annimon.stream.Optional
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.model.ComponentType
import tusur.kkv.domain.repository.IComponentTypeRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class RequestComponentTypeInteractor @Inject constructor(private val repository: IComponentTypeRepository)
    : ReactiveInteractor.RequestInteractor<Long, ComponentType> {

    override fun getSingle(params: Optional<Long>): Single<ComponentType> {
        val id = params.unwrapOrThrow()
        return repository.requestSeveral(listOf(id)).map { it.first() }.toSingle()
    }
}