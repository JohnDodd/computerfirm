package tusur.kkv.domain.interactor.componenttype.choose

import com.annimon.stream.Optional
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.model.ComponentType
import tusur.kkv.domain.repository.IComponentTypeRepository
import tusur.kkv.domain.utils.unwrap
import javax.inject.Inject

class RetrieveComponentTypesInteractor @Inject constructor(private val repository: IComponentTypeRepository)
    : ReactiveInteractor.RetrieveInteractor<Void, List<ComponentType>> {

    override fun getBehaviorStream(params: Optional<Void>): Observable<List<ComponentType>> =
            repository.getAll()
                    .flatMapSingle { fetchWhenNoneAndThenComponentType(it) }
                    .unwrap()

    private fun fetchWhenNoneAndThenComponentType(types: Optional<List<ComponentType>>) =
            fetchWhenNone(types).andThen(Single.just(types))

    private fun fetchWhenNone(types: Optional<List<ComponentType>>): Completable =
            if (types.isPresent) Completable.complete() else repository.fetch()

}