package tusur.kkv.domain.interactor.componenttype.choose

import com.annimon.stream.Optional
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.interactor.componenttype.choose.SendComponentTypeInteractor.Params
import tusur.kkv.domain.repository.IComponentTypeRepository
import tusur.kkv.domain.utils.isNotEmpty
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class SendComponentTypeInteractor @Inject constructor(private val repository: IComponentTypeRepository)
    : ReactiveInteractor.SendInteractor<Params, Boolean> {

    override fun getSingle(params: Optional<Params>): Single<Boolean> {
        val unwrap = params.unwrapOrThrow()
        return repository
                .store(unwrap.name,
                        unwrap.specifications,
                        unwrap.manufacturer)
                .isNotEmpty()
    }

    data class Params(val name: String, val specifications: String, val manufacturer: String)

}