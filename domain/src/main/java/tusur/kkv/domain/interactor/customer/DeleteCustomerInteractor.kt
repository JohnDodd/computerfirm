package tusur.kkv.domain.interactor.customer

import com.annimon.stream.Optional
import io.reactivex.Completable
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.repository.ICustomerRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class DeleteCustomerInteractor @Inject constructor(private val repository: ICustomerRepository)
    : ReactiveInteractor.DeleteInteractor<Long> {

    override fun getSingle(params: Optional<Long>): Completable {
        val id = params.unwrapOrThrow()
        return repository.delete(id)
    }

}