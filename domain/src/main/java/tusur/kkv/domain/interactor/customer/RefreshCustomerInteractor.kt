package tusur.kkv.domain.interactor.customer

import com.annimon.stream.Optional
import io.reactivex.Completable
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.interactor.customer.RefreshCustomerInteractor.Params
import tusur.kkv.domain.repository.ICustomerRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class RefreshCustomerInteractor @Inject constructor(private val repository: ICustomerRepository)
    : ReactiveInteractor.RefreshInteractor<Params> {

    override fun getRefreshSingle(params: Optional<Params>): Completable {
        val unwrap = params.unwrapOrThrow()
        return repository.replace(unwrap.id, unwrap.fullName, unwrap.email, unwrap.phoneNumber)
    }

    data class Params(val id: Long,
                      val fullName: String,
                      val email: String?,
                      val phoneNumber: String)

}