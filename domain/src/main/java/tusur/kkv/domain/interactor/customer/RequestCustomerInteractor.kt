package tusur.kkv.domain.interactor.customer

import com.annimon.stream.Optional
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.model.Customer
import tusur.kkv.domain.repository.ICustomerRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class RequestCustomerInteractor @Inject constructor(private val repository: ICustomerRepository)
    : ReactiveInteractor.RequestInteractor<Long, Customer> {

    override fun getSingle(params: Optional<Long>): Single<Customer> {
        val id = params.unwrapOrThrow()
        return repository.requestSeveral(listOf(id)).map { it.first() }.toSingle()
    }
}