package tusur.kkv.domain.interactor.customer

import com.annimon.stream.Optional
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.model.Customer
import tusur.kkv.domain.repository.ICustomerRepository
import tusur.kkv.domain.utils.unwrap
import javax.inject.Inject

class RetrieveCustomersInteractor @Inject constructor(private val repository: ICustomerRepository)
    : ReactiveInteractor.RetrieveInteractor<Void, List<Customer>> {

    override fun getBehaviorStream(params: Optional<Void>): Observable<List<Customer>> =
            repository.getAll()
                    .flatMapSingle { fetchWhenNoneAndThenCustomer(it) }
                    .unwrap()

    private fun fetchWhenNoneAndThenCustomer(customers: Optional<List<Customer>>) =
            fetchWhenNone(customers).andThen(Single.just(customers))

    private fun fetchWhenNone(customers: Optional<List<Customer>>): Completable =
            if (customers.isPresent) Completable.complete() else repository.fetch()

}