package tusur.kkv.domain.interactor.customer

import com.annimon.stream.Optional
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.interactor.customer.SendCustomerInteractor.Params
import tusur.kkv.domain.repository.ICustomerRepository
import tusur.kkv.domain.utils.isNotEmpty
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class SendCustomerInteractor @Inject constructor(private val repository: ICustomerRepository)
    : ReactiveInteractor.SendInteractor<Params, Boolean> {

    override fun getSingle(params: Optional<Params>): Single<Boolean> {
        val unwrap = params.unwrapOrThrow()
        return repository.store(unwrap.fullName, unwrap.email, unwrap.phoneNumber).isNotEmpty()
    }

    data class Params(val fullName: String, val email: String?, val phoneNumber: String)

}