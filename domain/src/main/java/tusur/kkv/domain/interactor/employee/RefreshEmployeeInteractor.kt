package tusur.kkv.domain.interactor.employee

import com.annimon.stream.Optional
import io.reactivex.Completable
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.interactor.employee.RefreshEmployeeInteractor.Params
import tusur.kkv.domain.model.Gender
import tusur.kkv.domain.repository.IEmployeeRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class RefreshEmployeeInteractor
@Inject constructor(private val repository: IEmployeeRepository)
    : ReactiveInteractor.RefreshInteractor<Params> {

    override fun getRefreshSingle(params: Optional<Params>): Completable {
        val unwrapped = params.unwrapOrThrow()
        return repository.replace(
                unwrapped.id,
                unwrapped.name,
                unwrapped.birthDate,
                unwrapped.gender,
                unwrapped.address,
                unwrapped.phoneNumber,
                unwrapped.passportData,
                unwrapped.positionId)

    }

    data class Params(val id: Long,
                      val name: String,
                      val birthDate: String,
                      val gender: Gender?,
                      val address: String,
                      val phoneNumber: String,
                      val passportData: String,
                      val positionId: Long)
}