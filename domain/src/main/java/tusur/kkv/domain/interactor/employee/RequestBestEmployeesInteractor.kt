package tusur.kkv.domain.interactor.employee

import com.annimon.stream.Optional
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.model.BestEmployee
import tusur.kkv.domain.repository.IEmployeeRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class RequestBestEmployeesInteractor @Inject constructor(private val repository: IEmployeeRepository)
    : ReactiveInteractor.RequestInteractor<Long, List<BestEmployee>> {

    override fun getSingle(params: Optional<Long>): Single<List<BestEmployee>> {
        val limit = params.unwrapOrThrow()
        return repository.best(limit).toSingle(listOf())
    }

}