package tusur.kkv.domain.interactor.employee

import com.annimon.stream.Optional
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.model.Employee
import tusur.kkv.domain.repository.IEmployeeRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class RequestEmployeeInteractor @Inject constructor(private val repository: IEmployeeRepository)
    : ReactiveInteractor.RequestInteractor<Long, Employee> {

    override fun getSingle(params: Optional<Long>): Single<Employee> {
        val id = params.unwrapOrThrow()
        return repository.requestSeveral(listOf(id)).map { it.first() }.toSingle()
    }
}