package tusur.kkv.domain.interactor.employee

import com.annimon.stream.Optional
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.model.Employee
import tusur.kkv.domain.repository.IEmployeeRepository
import tusur.kkv.domain.utils.unwrap
import javax.inject.Inject

class RetrieveEmployeesInteractor @Inject constructor(private val repository: IEmployeeRepository) :
        ReactiveInteractor.RetrieveInteractor<Void, List<Employee>> {

    override fun getBehaviorStream(params: Optional<Void>): Observable<List<Employee>> {
        return repository.getAll()
                .flatMapSingle { fetchWhenNoneAndThenEmployee(it) }
                .unwrap()
    }

    private fun fetchWhenNoneAndThenEmployee(employees: Optional<List<Employee>>): Single<Optional<List<Employee>>> {
        return fetchWhenNone(employees).andThen(Single.just(employees))
    }

    private fun fetchWhenNone(employees: Optional<List<Employee>>): Completable {
        return if (employees.isPresent)
            Completable.complete()
        else
            repository.fetch()
    }
}