package tusur.kkv.domain.interactor.employee

import com.annimon.stream.Optional
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.model.Gender
import tusur.kkv.domain.repository.IEmployeeRepository
import tusur.kkv.domain.utils.isNotEmpty
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class SendEmployeeInteractor
@Inject constructor(private val repository: IEmployeeRepository)
    : ReactiveInteractor.SendInteractor<SendEmployeeInteractor.Params, Boolean> {

    override fun getSingle(params: Optional<Params>): Single<Boolean> {
        val unwrapped = params.unwrapOrThrow()
        return repository.store(unwrapped.name,
                unwrapped.birthDate,
                unwrapped.gender,
                unwrapped.address,
                unwrapped.phoneNumber,
                unwrapped.passportData,
                unwrapped.positionId)
                .isNotEmpty()
    }

    data class Params(val name: String,
                      val birthDate: String,
                      val gender: Gender?,
                      val address: String,
                      val phoneNumber: String,
                      val passportData: String,
                      val positionId: Long)
}