package tusur.kkv.domain.interactor.order

import com.annimon.stream.Optional
import io.reactivex.Completable
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.repository.IOrderRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class DeleteOrderInteractor @Inject constructor(private val repository: IOrderRepository)
    : ReactiveInteractor.DeleteInteractor<Long> {

    override fun getSingle(params: Optional<Long>): Completable =
            repository.delete(params.unwrapOrThrow())
}