package tusur.kkv.domain.interactor.order

import com.annimon.stream.Optional
import io.reactivex.Completable
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.interactor.order.RefreshOrderInteractor.Params
import tusur.kkv.domain.repository.IOrderComponentRepository
import tusur.kkv.domain.repository.IOrderRepository
import tusur.kkv.domain.repository.IOrderServiceRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class RefreshOrderInteractor
@Inject constructor(private val repository: IOrderRepository,
                    private val servicesJoinRepository: IOrderServiceRepository,
                    private val componentsJoinRepository: IOrderComponentRepository)
    : ReactiveInteractor.RefreshInteractor<Params> {

    override fun getRefreshSingle(params: Optional<Params>): Completable {
        val unwrap = params.unwrapOrThrow()
        return repository.replace(
                unwrap.id,
                unwrap.customerId,
                unwrap.employeeId,
                unwrap.executionDate,
                unwrap.registrationDate
        )
                .andThen(servicesJoinRepository.replaceOrderWithServices(unwrap.id, unwrap.services)
                        .ignoreElement()
                        .andThen(componentsJoinRepository
                                .replaceOrderWithComponents(unwrap.id, unwrap.components))
                        .ignoreElement())
    }

    data class Params(val id: Long,
                      val customerId: Long,
                      val employeeId: Long,
                      val executionDate: String?,
                      val services: List<Long>?,
                      val components: List<Long>?,
                      val registrationDate: String?)
}