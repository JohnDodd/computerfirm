package tusur.kkv.domain.interactor.order

import com.annimon.stream.Optional
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.model.Order
import tusur.kkv.domain.repository.IOrderRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class RequestOrderInteractor @Inject constructor(private val repository: IOrderRepository)
    : ReactiveInteractor.RequestInteractor<Long, Order> {

    override fun getSingle(params: Optional<Long>): Single<Order> {
        val id = params.unwrapOrThrow()
        return repository.requestSeveral(listOf(id)).map { it.first() }.toSingle()
    }
}