package tusur.kkv.domain.interactor.order

import com.annimon.stream.Optional
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.interactor.order.RequestOrdersSearchInteractor.Params
import tusur.kkv.domain.model.Order
import tusur.kkv.domain.repository.IOrderRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class RequestOrdersSearchInteractor @Inject constructor(private val repository: IOrderRepository)
    : ReactiveInteractor.RequestInteractor<Params, List<Order>> {

    override fun getSingle(params: Optional<Params>): Single<List<Order>> {
        val unwrap = params.unwrapOrThrow()
        return repository.search(
                unwrap.id,
                unwrap.customerId,
                unwrap.employeeId,
                unwrap.registrationDateFrom,
                unwrap.registrationDateTo
        ).toSingle(listOf())
    }

    data class Params(
            val id: String,
            val customerId: Long?,
            val employeeId: Long?,
            val registrationDateFrom: String?,
            val registrationDateTo: String?
    )
}