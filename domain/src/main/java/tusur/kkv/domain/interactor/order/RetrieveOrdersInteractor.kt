package tusur.kkv.domain.interactor.order

import com.annimon.stream.Optional
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.model.Order
import tusur.kkv.domain.repository.IOrderRepository
import tusur.kkv.domain.utils.unwrap
import javax.inject.Inject

class RetrieveOrdersInteractor @Inject constructor(private val repository: IOrderRepository)
    : ReactiveInteractor.RetrieveInteractor<Void, List<Order>> {

    override fun getBehaviorStream(params: Optional<Void>): Observable<List<Order>> {
        return repository.getAll()
                .flatMapSingle { fetchWhenNoneAndThenOrder(it) }
                .unwrap()
    }

    private fun fetchWhenNoneAndThenOrder(drafts: Optional<List<Order>>): Single<Optional<List<Order>>> {
        return fetchWhenNone(drafts).andThen(Single.just(drafts))
    }

    private fun fetchWhenNone(purveyors: Optional<List<Order>>): Completable {
        return if (purveyors.isPresent)
            Completable.complete()
        else
            repository.fetch()
    }

}