package tusur.kkv.domain.interactor.order

import com.annimon.stream.Optional
import io.reactivex.Maybe
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.interactor.order.SendOrderInteractor.Params
import tusur.kkv.domain.repository.IOrderComponentRepository
import tusur.kkv.domain.repository.IOrderRepository
import tusur.kkv.domain.repository.IOrderServiceRepository
import tusur.kkv.domain.utils.isNotEmpty
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class SendOrderInteractor @Inject constructor(private val repository: IOrderRepository,
                                              private val servicesJoinRepository: IOrderServiceRepository,
                                              private val componentsJoinRepository: IOrderComponentRepository)
    : ReactiveInteractor.SendInteractor<Params, Boolean> {

    override fun getSingle(params: Optional<Params>): Single<Boolean> {
        val unwrap = params.unwrapOrThrow()
        return repository.store(
                unwrap.customerId,
                unwrap.employeeId,
                unwrap.executionDate,
                unwrap.registrationDate
        )
                .flatMap { order ->
                    servicesJoinRepository.storeOrderWithServices(order.id, unwrap.services)
                            .ignoreElement()
                            .andThen(componentsJoinRepository.storeOrderWithComponents(order.id,
                                    unwrap.components))
                            .ignoreElement()
                            .andThen(Maybe.just(order))
                }.isNotEmpty()
    }

    data class Params(val customerId: Long,
                      val employeeId: Long,
                      val executionDate: String?,
                      val services: List<Long>?,
                      val components: List<Long>?,
                      val registrationDate: String?)
}