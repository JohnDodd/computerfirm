package tusur.kkv.domain.interactor.ordercomponent

import com.annimon.stream.Optional
import io.reactivex.Completable
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.repository.IOrderComponentRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class DeleteOrdersWithComponents @Inject constructor(private val repository: IOrderComponentRepository)
    : ReactiveInteractor.DeleteInteractor<List<Long>> {

    override fun getSingle(params: Optional<List<Long>>): Completable {
        val components = params.unwrapOrThrow()
        return repository.deleteOrdersWithComponents(components)
    }
}