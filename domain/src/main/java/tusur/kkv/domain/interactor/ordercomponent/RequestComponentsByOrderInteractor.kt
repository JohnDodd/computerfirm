package tusur.kkv.domain.interactor.ordercomponent

import com.annimon.stream.Optional
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.model.Component
import tusur.kkv.domain.repository.IOrderComponentRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class RequestComponentsByOrderInteractor @Inject constructor(private val repository: IOrderComponentRepository)
    : ReactiveInteractor.RequestInteractor<Long, List<Component>> {

    override fun getSingle(params: Optional<Long>): Single<List<Component>> {
        val unwrap = params.unwrapOrThrow()
        return repository.requestComponentsByOrderId(unwrap).toSingle()
    }
}