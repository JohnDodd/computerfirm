package tusur.kkv.domain.interactor.ordercomponent

import com.annimon.stream.Optional
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.interactor.ordercomponent.SendComponentOrdersInteractor.Params
import tusur.kkv.domain.repository.IOrderComponentRepository
import tusur.kkv.domain.utils.isNotEmpty
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class SendComponentOrdersInteractor @Inject constructor(private val repository: IOrderComponentRepository)
    : ReactiveInteractor.SendInteractor<Params, Boolean> {

    override fun getSingle(params: Optional<Params>): Single<Boolean> {
        val (componentId, orders) = params.unwrapOrThrow()
        return repository.storeComponentWithOrders(componentId, orders).isNotEmpty()
    }

    data class Params(val componentId: Long, val orders: List<Long>)
}