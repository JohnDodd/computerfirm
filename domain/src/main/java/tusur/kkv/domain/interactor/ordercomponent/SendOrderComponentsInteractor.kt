package tusur.kkv.domain.interactor.ordercomponent

import com.annimon.stream.Optional
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.interactor.ordercomponent.SendOrderComponentsInteractor.Params
import tusur.kkv.domain.repository.IOrderComponentRepository
import tusur.kkv.domain.utils.isNotEmpty
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class SendOrderComponentsInteractor @Inject constructor(private val repository: IOrderComponentRepository)
    : ReactiveInteractor.SendInteractor<Params, Boolean> {

    override fun getSingle(params: Optional<Params>): Single<Boolean> {
        val (orderId, components) = params.unwrapOrThrow()
        return repository.storeOrderWithComponents(orderId, components).isNotEmpty()
    }

    data class Params(val orderId: Long, val components: List<Long>)
}