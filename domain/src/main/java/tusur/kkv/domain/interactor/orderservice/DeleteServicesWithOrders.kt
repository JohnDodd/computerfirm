package tusur.kkv.domain.interactor.orderservice

import com.annimon.stream.Optional
import io.reactivex.Completable
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.repository.IOrderServiceRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class DeleteServicesWithOrders @Inject constructor(private val repository: IOrderServiceRepository)
    : ReactiveInteractor.DeleteInteractor<List<Long>> {

    override fun getSingle(params: Optional<List<Long>>): Completable {
        val orders = params.unwrapOrThrow()
        return repository.deleteServicesWithOrders(orders)
    }
}