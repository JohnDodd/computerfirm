package tusur.kkv.domain.interactor.orderservice

import com.annimon.stream.Optional
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.model.Order
import tusur.kkv.domain.repository.IOrderServiceRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class RequestOrdersByServiceInteractor @Inject constructor(private val repository: IOrderServiceRepository)
    : ReactiveInteractor.RequestInteractor<Long, List<Order>> {

    override fun getSingle(params: Optional<Long>): Single<List<Order>> {
        val unwrap = params.unwrapOrThrow()
        return repository.requestOrdersByServiceId(unwrap).toSingle()
    }
}