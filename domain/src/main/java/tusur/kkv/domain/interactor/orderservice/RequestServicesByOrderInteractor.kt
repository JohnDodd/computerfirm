package tusur.kkv.domain.interactor.orderservice

import com.annimon.stream.Optional
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.model.Service
import tusur.kkv.domain.repository.IOrderServiceRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class RequestServicesByOrderInteractor @Inject constructor(private val repository: IOrderServiceRepository)
    : ReactiveInteractor.RequestInteractor<Long, List<Service>> {

    override fun getSingle(params: Optional<Long>): Single<List<Service>> {
        val unwrap = params.unwrapOrThrow()
        return repository.requestServicesByOrderId(unwrap).toSingle()
    }
}