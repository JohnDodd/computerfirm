package tusur.kkv.domain.interactor.orderservice

import com.annimon.stream.Optional
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.interactor.orderservice.SendOrderServicesInteractor.Params
import tusur.kkv.domain.repository.IOrderServiceRepository
import tusur.kkv.domain.utils.isNotEmpty
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class SendOrderServicesInteractor @Inject constructor(private val repository: IOrderServiceRepository)
    : ReactiveInteractor.SendInteractor<Params, Boolean> {

    override fun getSingle(params: Optional<Params>): Single<Boolean> {
        val (orderId, services) = params.unwrapOrThrow()
        return repository.storeOrderWithServices(orderId, services).isNotEmpty()
    }

    data class Params(val orderId: Long, val services: List<Long>)
}