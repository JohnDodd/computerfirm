package tusur.kkv.domain.interactor.orderservice

import com.annimon.stream.Optional
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.interactor.orderservice.SendServiceOrdersInteractor.Params
import tusur.kkv.domain.repository.IOrderServiceRepository
import tusur.kkv.domain.utils.isNotEmpty
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class SendServiceOrdersInteractor @Inject constructor(private val repository: IOrderServiceRepository)
    : ReactiveInteractor.SendInteractor<Params, Boolean> {

    override fun getSingle(params: Optional<Params>): Single<Boolean> {
        val (serviceId, orders) = params.unwrapOrThrow()
        return repository.storeServiceWithOrders(serviceId, orders).isNotEmpty()
    }

    data class Params(val serviceId: Long, val orders: List<Long>)
}