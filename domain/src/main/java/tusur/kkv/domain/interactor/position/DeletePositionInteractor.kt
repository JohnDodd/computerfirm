package tusur.kkv.domain.interactor.position

import com.annimon.stream.Optional
import io.reactivex.Completable
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.repository.IPositionRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class DeletePositionInteractor @Inject constructor(private val repository: IPositionRepository)
    : ReactiveInteractor.DeleteInteractor<Long> {

    override fun getSingle(params: Optional<Long>): Completable {
        val id = params.unwrapOrThrow()
        return repository.delete(id)
    }
}