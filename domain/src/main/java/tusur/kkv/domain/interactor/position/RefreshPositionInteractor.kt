package tusur.kkv.domain.interactor.position

import com.annimon.stream.Optional
import io.reactivex.Completable
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.interactor.position.RefreshPositionInteractor.Params
import tusur.kkv.domain.repository.IPositionRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class RefreshPositionInteractor
@Inject constructor(private val repository: IPositionRepository)
    : ReactiveInteractor.RefreshInteractor<Params> {

    override fun getRefreshSingle(params: Optional<Params>): Completable {
        val unwrap = params.unwrapOrThrow()
        return repository.replace(unwrap.id, unwrap.name, unwrap.salary, unwrap.duties)
    }

    data class Params(val id: Long, val name: String, val salary: Long, val duties: String)

}