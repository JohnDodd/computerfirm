package tusur.kkv.domain.interactor.position

import com.annimon.stream.Optional
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.model.Position
import tusur.kkv.domain.repository.IPositionRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class RequestPositionInteractor @Inject constructor(private val repository: IPositionRepository)
    : ReactiveInteractor.RequestInteractor<Long, Position> {

    override fun getSingle(params: Optional<Long>): Single<Position> {
        val id = params.unwrapOrThrow()
        return repository.requestSeveral(listOf(id)).map { it.first() }.toSingle()
    }
}