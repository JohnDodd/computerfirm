package tusur.kkv.domain.interactor.position

import com.annimon.stream.Optional
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.model.Position
import tusur.kkv.domain.repository.IPositionRepository
import tusur.kkv.domain.utils.unwrap
import javax.inject.Inject

class RetrievePositionInteractor
@Inject constructor(private val repository: IPositionRepository)
    : ReactiveInteractor.RetrieveInteractor<Void, List<Position>> {

    override fun getBehaviorStream(params: Optional<Void>): Observable<List<Position>> =
            repository.getAll()
                    .flatMapSingle { fetchWhenNoneAndThenPosition(it) }
                    .unwrap()

    private fun fetchWhenNoneAndThenPosition(positions: Optional<List<Position>>) =
            fetchWhenNone(positions).andThen(Single.just(positions))

    private fun fetchWhenNone(positions: Optional<List<Position>>): Completable =
            if (positions.isPresent) Completable.complete() else repository.fetch()

}