package tusur.kkv.domain.interactor.position

import com.annimon.stream.Optional
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.interactor.position.SendPositionInteractor.Params
import tusur.kkv.domain.repository.IPositionRepository
import tusur.kkv.domain.utils.isNotEmpty
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class SendPositionInteractor
@Inject constructor(private val repository: IPositionRepository)
    : ReactiveInteractor.SendInteractor<Params, Boolean> {

    override fun getSingle(params: Optional<Params>): Single<Boolean> {
        val unwrap = params.unwrapOrThrow()
        return repository.store(unwrap.name, unwrap.salary, unwrap.duties).isNotEmpty()
    }

    data class Params(val name: String, val salary: Long?, val duties: String)

}