package tusur.kkv.domain.interactor.purveyor

import com.annimon.stream.Optional
import io.reactivex.Completable
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.repository.IPurveyorRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class DeletePurveyorInteractor @Inject constructor(private val repository: IPurveyorRepository)
    : ReactiveInteractor.DeleteInteractor<Long> {

    override fun getSingle(params: Optional<Long>): Completable {
        val id = params.unwrapOrThrow()
        return repository.delete(id)
    }
}