package tusur.kkv.domain.interactor.purveyor

import com.annimon.stream.Optional
import io.reactivex.Completable
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.interactor.purveyor.RefreshPurveyorInteractor.Params
import tusur.kkv.domain.repository.IPurveyorRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class RefreshPurveyorInteractor @Inject constructor(private val repository: IPurveyorRepository)
    : ReactiveInteractor.RefreshInteractor<Params> {

    override fun getRefreshSingle(params: Optional<Params>): Completable {
        val unwrapped = params.unwrapOrThrow()
        return repository.replace(
                unwrapped.id,
                unwrapped.name,
                unwrapped.phoneNumber,
                unwrapped.address,
                unwrapped.representativeName)
    }

    data class Params(val id: Long,
                      val name: String,
                      val phoneNumber: String,
                      val address: String,
                      val representativeName: String?)
}