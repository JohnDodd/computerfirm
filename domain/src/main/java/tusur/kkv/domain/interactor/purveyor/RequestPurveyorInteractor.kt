package tusur.kkv.domain.interactor.purveyor

import com.annimon.stream.Optional
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.model.Purveyor
import tusur.kkv.domain.repository.IPurveyorRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class RequestPurveyorInteractor @Inject constructor(private val repository: IPurveyorRepository)
    : ReactiveInteractor.RequestInteractor<Long, Purveyor> {

    override fun getSingle(params: Optional<Long>): Single<Purveyor> {
        val id = params.unwrapOrThrow()
        return repository.requestSeveral(listOf(id)).map { it.first() }.toSingle()
    }
}