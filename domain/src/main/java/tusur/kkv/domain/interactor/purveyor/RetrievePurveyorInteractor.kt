package tusur.kkv.domain.interactor.purveyor

import com.annimon.stream.Optional
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.model.Purveyor
import tusur.kkv.domain.repository.IPurveyorRepository
import tusur.kkv.domain.utils.unwrap
import javax.inject.Inject

class RetrievePurveyorInteractor @Inject constructor(private val purveyorRepository: IPurveyorRepository)
    : ReactiveInteractor.RetrieveInteractor<Void, List<Purveyor>> {

    override fun getBehaviorStream(params: Optional<Void>): Observable<List<Purveyor>> {
        return purveyorRepository.getAll()
                .flatMapSingle { fetchWhenNoneAndThenPurveyor(it) }
                .unwrap()
    }

    private fun fetchWhenNoneAndThenPurveyor(drafts: Optional<List<Purveyor>>): Single<Optional<List<Purveyor>>> {
        return fetchWhenNone(drafts).andThen(Single.just(drafts))
    }

    private fun fetchWhenNone(purveyors: Optional<List<Purveyor>>): Completable {
        return if (purveyors.isPresent)
            Completable.complete()
        else
            purveyorRepository.fetch()
    }

}

