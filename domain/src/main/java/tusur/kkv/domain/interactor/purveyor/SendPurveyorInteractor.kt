package tusur.kkv.domain.interactor.purveyor

import com.annimon.stream.Optional
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.repository.IPurveyorRepository
import tusur.kkv.domain.utils.isNotEmpty
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class SendPurveyorInteractor @Inject constructor(private val purveyorRepository: IPurveyorRepository)
    : ReactiveInteractor.SendInteractor<SendPurveyorInteractor.Params, Boolean> {

    override fun getSingle(params: Optional<Params>): Single<Boolean> {
        val unwrapped = params.unwrapOrThrow()
        return purveyorRepository.store(
                unwrapped.name,
                unwrapped.phoneNumber,
                unwrapped.address,
                unwrapped.representativeName)
                .isNotEmpty()
    }


    data class Params(val name: String,
                      val phoneNumber: String,
                      val address: String,
                      val representativeName: String?)
}