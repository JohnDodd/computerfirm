package tusur.kkv.domain.interactor.service

import com.annimon.stream.Optional
import io.reactivex.Completable
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.repository.IServiceRepository
import tusur.kkv.domain.utils.unwrapOrThrow

class DeleteServiceInteractor(private val repository: IServiceRepository)
    : ReactiveInteractor.DeleteInteractor<Long> {

    override fun getSingle(params: Optional<Long>): Completable {
        val id = params.unwrapOrThrow()
        return repository.delete(id)
    }
}