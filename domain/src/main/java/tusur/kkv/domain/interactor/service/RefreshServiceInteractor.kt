package tusur.kkv.domain.interactor.service

import com.annimon.stream.Optional
import io.reactivex.Completable
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.interactor.service.RefreshServiceInteractor.Params
import tusur.kkv.domain.repository.IServiceRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class RefreshServiceInteractor @Inject constructor(private val repository: IServiceRepository)
    : ReactiveInteractor.RefreshInteractor<Params> {

    override fun getRefreshSingle(params: Optional<Params>): Completable {
        val unwrapped = params.unwrapOrThrow()
        return repository.replace(
                unwrapped.id,
                unwrapped.name,
                unwrapped.cost,
                unwrapped.description)
    }

    data class Params(val id: Long, val name: String, val cost: Long, val description: String?)
}