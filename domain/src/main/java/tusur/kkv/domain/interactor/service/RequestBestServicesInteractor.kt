package tusur.kkv.domain.interactor.service

import com.annimon.stream.Optional
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.model.BestService
import tusur.kkv.domain.repository.IServiceRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class RequestBestServicesInteractor @Inject constructor(private val repository: IServiceRepository)
    : ReactiveInteractor.RequestInteractor<Long, List<BestService>> {

    override fun getSingle(params: Optional<Long>): Single<List<BestService>> {
        val limit = params.unwrapOrThrow()
        return repository.best(limit).toSingle(listOf())
    }

}