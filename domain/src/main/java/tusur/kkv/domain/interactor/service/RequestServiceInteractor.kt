package tusur.kkv.domain.interactor.service

import com.annimon.stream.Optional
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.model.Service
import tusur.kkv.domain.repository.IServiceRepository
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class RequestServiceInteractor @Inject constructor(private val repository: IServiceRepository)
    : ReactiveInteractor.RequestInteractor<Long, Service> {

    override fun getSingle(params: Optional<Long>): Single<Service> {
        val id = params.unwrapOrThrow()
        return repository.requestSeveral(listOf(id)).map { it.first() }.toSingle()
    }
}