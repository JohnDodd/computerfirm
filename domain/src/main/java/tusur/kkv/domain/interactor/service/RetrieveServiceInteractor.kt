package tusur.kkv.domain.interactor.service

import com.annimon.stream.Optional
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.model.Service
import tusur.kkv.domain.repository.IServiceRepository
import tusur.kkv.domain.utils.unwrap
import javax.inject.Inject

class RetrieveServiceInteractor @Inject constructor(private val repository: IServiceRepository)
    : ReactiveInteractor.RetrieveInteractor<Void, List<Service>> {

    override fun getBehaviorStream(params: Optional<Void>): Observable<List<Service>> {
        return repository.getAll()
                .flatMapSingle { fetchWhenNoneAndThenService(it) }
                .unwrap()
    }

    private fun fetchWhenNoneAndThenService(services: Optional<List<Service>>): Single<Optional<List<Service>>> {
        return fetchWhenNone(services).andThen(Single.just(services))
    }

    private fun fetchWhenNone(services: Optional<List<Service>>): Completable {
        return if (services.isPresent)
            Completable.complete()
        else
            repository.fetch()
    }
}