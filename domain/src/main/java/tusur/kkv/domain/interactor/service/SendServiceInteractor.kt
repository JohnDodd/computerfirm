package tusur.kkv.domain.interactor.service

import com.annimon.stream.Optional
import io.reactivex.Single
import tusur.kkv.domain.base.ReactiveInteractor
import tusur.kkv.domain.repository.IServiceRepository
import tusur.kkv.domain.utils.isNotEmpty
import tusur.kkv.domain.utils.unwrapOrThrow
import javax.inject.Inject

class SendServiceInteractor @Inject constructor(private val repository: IServiceRepository)
    : ReactiveInteractor.SendInteractor<SendServiceInteractor.Params, Boolean> {

    override fun getSingle(params: Optional<Params>): Single<Boolean> {
        val unwrapped = params.unwrapOrThrow()
        return repository.store(
                unwrapped.name,
                unwrapped.cost,
                unwrapped.description)
                .isNotEmpty()

    }

    data class Params(val name: String, val cost: Long, val description: String?)
}