package tusur.kkv.domain.model

class BestEmployee(val id: Long,
                   val fullName: String,
                   val birthDate: String,
                   val gender: Gender,
                   val address: String,
                   val phoneNumber: String,
                   val passportData: String,
                   val revenue: Long,
                   val componentsCount: Int,
                   val servicesCount: Int)