package tusur.kkv.domain.model

data class BestService(val id: Long,
                       val name: String,
                       val cost: Long,
                       val revenue: Long)