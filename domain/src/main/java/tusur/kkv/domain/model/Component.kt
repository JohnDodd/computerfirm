package tusur.kkv.domain.model

data class Component(val id: Long,
                     val type: ComponentType,
                     val guaranteePeriod: Long?,
                     val name: String,
                     val cost: Long,
                     val purveyor: Purveyor)