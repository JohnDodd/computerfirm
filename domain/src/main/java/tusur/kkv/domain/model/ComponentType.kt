package tusur.kkv.domain.model

data class ComponentType(val id: Long,
                         val name: String,
                         val specifications: String,
                         val manufacturer: String)