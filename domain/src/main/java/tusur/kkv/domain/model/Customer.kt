package tusur.kkv.domain.model

data class Customer(val id: Long,
                    val fullName: String,
                    val email: String?,
                    val phoneNumber: String)