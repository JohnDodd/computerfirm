package tusur.kkv.domain.model

data class Employee(val id: Long,
                    val fullName: String,
                    val birthDate: String,
                    val gender: Gender,
                    val address: String,
                    val phoneNumber: String,
                    val passportData: String,
                    val position: Position)