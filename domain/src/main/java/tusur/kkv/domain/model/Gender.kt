package tusur.kkv.domain.model

enum class Gender(val value: String) {
    MALE("male"),
    FEMALE("female"),
    UNKNOWN("unknown");

    companion object {
        fun from(value: String) = values().first { it.value == value }
    }
}