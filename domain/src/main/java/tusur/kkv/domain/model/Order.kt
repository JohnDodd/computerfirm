package tusur.kkv.domain.model

data class Order(val id: Long,
                 val registrationDate: String,
                 val executionDate: String?,
                 val customer: Customer,
                 val employee: Employee)