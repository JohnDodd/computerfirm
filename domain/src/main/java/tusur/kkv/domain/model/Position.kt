package tusur.kkv.domain.model

data class Position(val id: Long,
                    val name: String,
                    val salary: Long,
                    val duties: String)