package tusur.kkv.domain.model

data class Purveyor(val id: Long,
                    val name: String,
                    val phoneNumber: String,
                    val address: String,
                    val representativeName: String?)