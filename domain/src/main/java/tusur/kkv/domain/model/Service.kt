package tusur.kkv.domain.model

data class Service(val id: Long,
                   val name: String,
                   val cost: Long,
                   val description: String?)