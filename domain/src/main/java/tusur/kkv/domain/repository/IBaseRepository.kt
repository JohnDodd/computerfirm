package tusur.kkv.domain.repository

import com.annimon.stream.Optional
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable

interface IBaseRepository<Key, Value> {

    fun getAll(): Observable<Optional<List<Value>>>

    fun fetch(): Completable

    fun delete(id: Key): Completable

    fun requestSeveral(keys: List<Key>): Maybe<List<Value>>

    fun requestSingular(key: Key): Maybe<Value>
}