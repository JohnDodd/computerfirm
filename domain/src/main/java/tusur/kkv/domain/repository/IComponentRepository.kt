package tusur.kkv.domain.repository

import io.reactivex.Completable
import io.reactivex.Maybe
import tusur.kkv.domain.model.Component

interface IComponentRepository : ISimpleRepository<Component> {
    fun replace(id: Long,
                typeId: Long,
                guaranteePeriod: Long?,
                name: String,
                cost: Long,
                purveyorId: Long): Completable

    fun store(typeId: Long,
              guaranteePeriod: Long?,
              name: String,
              cost: Long,
              purveyorId: Long): Maybe<Component>

    interface ParamsExceptionId {
        companion object {
            const val NAME = 0
            const val COST = 1
            const val PURVEYOR_ID = 2
            const val TYPE_ID = 3
        }
    }
}