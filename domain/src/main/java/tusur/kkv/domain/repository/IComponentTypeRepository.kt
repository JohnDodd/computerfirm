package tusur.kkv.domain.repository

import io.reactivex.Completable
import io.reactivex.Maybe
import tusur.kkv.domain.model.ComponentType

interface IComponentTypeRepository : ISimpleRepository<ComponentType> {
    fun store(name: String, specifications: String, manufacturer: String): Maybe<ComponentType>

    fun replace(id: Long, name: String, specifications: String, manufacturer: String): Completable

    interface ParamsExceptionId {
        companion object {
            const val NAME = 0
            const val MANUFACTURER = 1
            const val SPECIFICATIONS = 2
        }
    }
}