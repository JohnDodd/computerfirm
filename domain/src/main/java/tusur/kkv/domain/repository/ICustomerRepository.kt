package tusur.kkv.domain.repository

import io.reactivex.Completable
import io.reactivex.Maybe
import tusur.kkv.domain.model.Customer

interface ICustomerRepository : ISimpleRepository<Customer> {

    fun store(fullName: String, email: String?, phoneNumber: String): Maybe<Customer>

    fun replace(id: Long, fullName: String, email: String?, phoneNumber: String): Completable

    interface ParamsExceptionId {
        companion object {
            const val NAME = 1
            const val EMAIL = 2
            const val PHONE = 3
        }
    }

}