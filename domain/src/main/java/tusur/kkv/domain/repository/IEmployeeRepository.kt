package tusur.kkv.domain.repository

import io.reactivex.Completable
import io.reactivex.Maybe
import tusur.kkv.domain.model.BestEmployee
import tusur.kkv.domain.model.Employee
import tusur.kkv.domain.model.Gender

interface IEmployeeRepository : ISimpleRepository<Employee> {

    fun store(name: String,
              birthDate: String,
              gender: Gender?,
              address: String,
              phoneNumber: String,
              passportData: String,
              positionId: Long): Maybe<Employee>

    fun replace(id: Long,
                name: String,
                birthDate: String,
                gender: Gender?,
                address: String,
                phoneNumber: String,
                passportData: String,
                positionId: Long): Completable

    fun best(limit: Long): Maybe<List<BestEmployee>>

    interface ParamsExceptionId {
        companion object {
            const val GENDER = 0
            const val NAME = 1
            const val BIRTHDATE = 2
            const val ADDRESS = 3
            const val PHONE_NUMBER = 4
            const val PASSPORT_DATA = 5
            const val POSITION = 6
        }
    }

}