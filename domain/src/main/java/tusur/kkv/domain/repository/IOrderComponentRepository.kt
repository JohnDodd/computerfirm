package tusur.kkv.domain.repository

import io.reactivex.Completable
import io.reactivex.Maybe
import tusur.kkv.domain.model.Component
import tusur.kkv.domain.model.Order

interface IOrderComponentRepository {

    fun deleteOrdersWithComponents(components: List<Long>): Completable
    fun deleteComponentsWithOrders(orders: List<Long>): Completable

    fun storeOrderWithComponents(orderId: Long,
                                 components: List<Long>?): Maybe<Pair<Order, List<Component>>>

    fun storeComponentWithOrders(componentId: Long,
                                 orders: List<Long>?): Maybe<Pair<Component, List<Order>>>

    fun requestOrdersByComponentId(componentId: Long): Maybe<List<Order>>
    fun requestComponentsByOrderId(orderId: Long): Maybe<List<Component>>
    fun replaceComponentWithOrders(componentId: Long,
                                   orders: List<Long>?): Maybe<Pair<Component, List<Order>>>

    fun replaceOrderWithComponents(orderId: Long,
                                   components: List<Long>?): Maybe<Pair<Order, List<Component>>>
}