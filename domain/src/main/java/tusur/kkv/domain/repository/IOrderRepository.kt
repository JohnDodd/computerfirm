package tusur.kkv.domain.repository

import io.reactivex.Completable
import io.reactivex.Maybe
import tusur.kkv.domain.model.Order

interface IOrderRepository : ISimpleRepository<Order> {

    fun store(customerId: Long,
              employeeId: Long,
              executionDate: String?,
              registrationDate: String?): Maybe<Order>

    fun replace(id: Long,
                customerId: Long,
                employeeId: Long,
                executionDate: String?,
                registrationDate: String?): Completable

    fun search(id: String,
               customerId: Long?,
               employeeId: Long?,
               registrationDateFrom: String?,
               registrationDateTo: String?): Maybe<List<Order>>

    interface ParamsExceptionId {
        companion object {
            const val REGISTRATION_DATE = 0
            const val EXECUTION_DATE = 1
            const val CUSTOMER = 2
            const val EMPLOYEE = 3
            const val REGISTRATION_DATE_FROM = 4
            const val REGISTRATION_DATE_TO = 5
        }
    }

}