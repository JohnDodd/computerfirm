package tusur.kkv.domain.repository

import io.reactivex.Completable
import io.reactivex.Maybe
import tusur.kkv.domain.model.Order
import tusur.kkv.domain.model.Service

interface IOrderServiceRepository {

    fun storeOrderWithServices(orderId: Long,
                               services: List<Long>?): Maybe<Pair<Order, List<Service>>>

    fun storeServiceWithOrders(serviceId: Long,
                               orders: List<Long>?): Maybe<Pair<Service, List<Order>>>

    fun deleteOrdersWithServices(services: List<Long>): Completable

    fun deleteServicesWithOrders(orders: List<Long>): Completable

    fun requestOrdersByServiceId(serviceId: Long): Maybe<List<Order>>

    fun requestServicesByOrderId(orderId: Long): Maybe<List<Service>>
    fun replaceOrderWithServices(orderId: Long,
                                 services: List<Long>?): Maybe<Pair<Order, List<Service>>>

    fun replaceServiceWithOrders(serviceId: Long,
                                 orders: List<Long>?): Maybe<Pair<Service, List<Order>>>
}