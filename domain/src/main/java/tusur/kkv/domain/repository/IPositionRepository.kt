package tusur.kkv.domain.repository

import io.reactivex.Completable
import io.reactivex.Maybe
import tusur.kkv.domain.model.Position

interface IPositionRepository : ISimpleRepository<Position> {

    fun store(name: String, salary: Long?, duties: String): Maybe<Position>

    fun replace(id: Long, name: String, salary: Long, duties: String): Completable

    interface ParamsExceptionId {
        companion object {
            const val NAME = 1
            const val SALARY = 2
            const val DUTIES = 3
        }
    }
}