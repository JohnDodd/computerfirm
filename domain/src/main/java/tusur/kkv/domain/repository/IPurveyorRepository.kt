package tusur.kkv.domain.repository

import io.reactivex.Completable
import io.reactivex.Maybe
import tusur.kkv.domain.model.Purveyor

interface IPurveyorRepository : ISimpleRepository<Purveyor> {

    fun store(name: String,
              phoneNumber: String,
              address: String,
              representativeName: String?): Maybe<Purveyor>

    fun replace(id: Long,
                name: String,
                phoneNumber: String,
                address: String,
                representativeName: String?): Completable

    interface ParamsExceptionId {
        companion object {
            const val NAME = 0
            const val PHONE = 1
            const val ADDRESS = 2
            const val REPRESENTATIVE = 3
        }
    }

}