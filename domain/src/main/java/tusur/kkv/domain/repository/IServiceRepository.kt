package tusur.kkv.domain.repository

import io.reactivex.Completable
import io.reactivex.Maybe
import tusur.kkv.domain.model.BestService
import tusur.kkv.domain.model.Service

interface IServiceRepository : ISimpleRepository<Service> {

    fun store(name: String,
              cost: Long,
              description: String?): Maybe<Service>

    fun replace(id: Long,
                name: String,
                cost: Long,
                description: String?): Completable

    fun best(limit: Long): Maybe<List<BestService>>

    interface ParamsExceptionId {
        companion object {
            const val NAME = 1
            const val COST = 2
            const val DESCRIPTION = 3
        }
    }

}