package tusur.kkv.domain.repository

interface ParamsExceptionTypeId {
    companion object {
        const val EMPTY = 0
        const val UNSATISFIED_FORMAT = 1
    }
}