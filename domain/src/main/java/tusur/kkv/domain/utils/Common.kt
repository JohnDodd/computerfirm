package tusur.kkv.domain.utils

import com.annimon.stream.Optional

fun <T> Optional<T>.unwrapOrThrow(): T =
        if (isPresent) get() else throw IllegalArgumentException("unwrap fails")

fun <T, E> List<T>.mapToList(prop: T.() -> E?) =
        asSequence().filter { it.prop() != null }.map { it.prop()!! }.toList()

inline fun <reified T> Collection<T>.toTypedArrayNoNulls(): Array<T> =
        filter { it != null }.toTypedArray()