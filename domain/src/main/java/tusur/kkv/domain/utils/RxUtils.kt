package tusur.kkv.domain.utils

import com.annimon.stream.Optional
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single

fun <T> Single<Optional<T>>.unwrap() = filter { it.isPresent }.map { it.get() }

fun <T> Observable<Optional<T>>.unwrap() = filter { it.isPresent }.map { it.get() }

fun <T> Maybe<T>.isNotEmpty(): Single<Boolean> = isEmpty.map { !it }

fun <T, E> Maybe<List<T>>.listToMap(key: T.() -> E?) =
        flatMapObservable { Observable.fromIterable(it) }
                .toMap { it.key() }

fun <T, E> Maybe<List<T>>.extractParam(param: T.() -> E) =
        flatMap {
            Observable.fromIterable(it)
                    .map { joinRaw -> joinRaw.param() }
                    .toList()
                    .filter { list -> !list.isEmpty() }
        }

fun <T, R> Observable<List<T>>.forEachElement(f: (Observable<T>) -> Observable<R>): Observable<List<R>> =
        flatMapSingle { f(Observable.fromIterable(it)).toList() }

fun <T, R> Single<List<T>>.forEachElement(f: (Observable<T>) -> Observable<R>): Single<List<R>> =
        flatMap { f(Observable.fromIterable(it)).toList() }

fun <T, R> Observable<Array<out T>>.applyForEach(f: Observable<T>.() -> Observable<R>): Observable<List<R>> =
        flatMapSingle { f(Observable.fromArray(*it)).toList() }