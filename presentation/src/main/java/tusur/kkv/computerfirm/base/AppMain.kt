package tusur.kkv.computerfirm.base

import android.app.Application
import tusur.kkv.computerfirm.injection.component.DaggerAppComponent

class AppMain : Application() {

    val component by lazy { DaggerAppComponent.builder().app(this).build() }

    override fun onCreate() {
        super.onCreate()
        component.inject(this)
    }
}