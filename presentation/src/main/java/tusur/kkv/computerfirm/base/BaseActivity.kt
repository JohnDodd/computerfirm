package tusur.kkv.computerfirm.base

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import tusur.kkv.computerfirm.controller.CompositeController
import tusur.kkv.computerfirm.utils.logd
import javax.inject.Inject

abstract class BaseActivity<P : BasePresenter> : AppCompatActivity() {

    protected lateinit var view: View
    abstract val layoutResID: Int
    @Inject
    lateinit var presenter: P
    @Inject
    lateinit var uiRouter: UIRouter
    @Inject
    lateinit var compositeController: CompositeController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutResID)
        view = findViewById<View>(android.R.id.content)
    }

    override fun onStart() {
        super.onStart()
        presenter.onViewShown()
    }

    override fun onStop() {
        super.onStop()
        presenter.onViewHidden()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        compositeController.onCreateOptionsMenu(menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        compositeController.onPrepareOptionsMenu(menu)
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        compositeController.onOptionsItemSelected(item)
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            startActivity(Intent(Intent.ACTION_MAIN)
                    .addCategory(Intent.CATEGORY_HOME)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
            return
        }
        findFirstNonNullTag().apply {
            logd("${supportFragmentManager.backStackEntryCount}, $this")
            supportFragmentManager.popBackStack(this, 0)
        }
    }

    private fun findFirstNonNullTag(): String? =
            (supportFragmentManager.backStackEntryCount - 2 downTo 0)
                    .map { supportFragmentManager.getBackStackEntryAt(it).name }
                    .find { it?.isNotBlank() ?: false }

    private fun isBackStackContains(tag: String): Boolean {
        var result = false
        supportFragmentManager.apply {
            for (i in 0 until backStackEntryCount) {
                if (getBackStackEntryAt(i).name == tag) {
                    result = true
                    break
                }
            }
        }
        return result
    }

    protected val appMain
        get() = application as AppMain

}