package tusur.kkv.computerfirm.base

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.transition.Fade
import tusur.kkv.computerfirm.features.main.MainActivity

abstract class BaseDialogFragment<P : BasePresenter> : AppCompatDialogFragment() {

    abstract var presenter: P
    protected abstract val viewId: Int

    private val containerView by lazy {
        LayoutInflater.from(context).inflate(viewId, null)
    }

    override fun getView(): View = containerView

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(context!!)
                .setView(containerView)
                .bindDialog()
                .create().also {
                    bindView(containerView, savedInstanceState, it)
                }
    }

    abstract fun bindView(view: View,
                          savedInstanceState: Bundle?,
                          dialog: AlertDialog)

    open fun AlertDialog.Builder.bindDialog(): AlertDialog.Builder = this

    override fun onStart() {
        super.onStart()
        presenter.onViewShown()
    }

    override fun onStop() {
        super.onStop()
        presenter.onViewHidden()
    }

    open fun getSharedElements(): Array<View>? {
        return null
    }

    override fun getEnterTransition(): Any? {
        return Fade(Fade.IN).setDuration(150)
    }

    override fun getExitTransition(): Any? {
        return Fade(Fade.OUT).setDuration(150)
    }

    val appCompatActivity: AppCompatActivity
        get() {
            return context as AppCompatActivity
        }

    protected val baseActivity: MainActivity
        get() = MainActivity::class.java.cast(activity)

}
