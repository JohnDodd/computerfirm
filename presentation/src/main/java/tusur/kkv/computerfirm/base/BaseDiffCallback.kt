package tusur.kkv.computerfirm.base

import com.github.vivchar.rendererrecyclerviewadapter.DiffCallback
import javax.inject.Inject

class BaseDiffCallback @Inject constructor() : DiffCallback<BaseViewModel>() {

    override fun areItemsTheSame(oldItem: BaseViewModel, newItem: BaseViewModel) =
            oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: BaseViewModel, newItem: BaseViewModel) =
            oldItem == newItem
}