package tusur.kkv.computerfirm.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.transition.Fade
import tusur.kkv.computerfirm.features.main.MainActivity
import tusur.kkv.computerfirm.utils.inflate

abstract class BaseFragment<P : BasePresenter> : Fragment() {

    abstract var presenter: P
    protected abstract val viewId: Int

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return container?.inflate(viewId)
    }

    abstract fun bindView(view: View, savedInstanceState: Bundle?)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindView(view, savedInstanceState)
        getSharedElements()?.forEach {
            ViewCompat.setTransitionName(it, it.tag.toString())
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.onViewShown()
    }

    override fun onStop() {
        super.onStop()
        presenter.onViewHidden()
    }

    open fun getSharedElements(): Array<View>? {
        return null
    }

    override fun getEnterTransition(): Any? {
        return Fade(Fade.IN).setDuration(150)
    }

    override fun getExitTransition(): Any? {
        return Fade(Fade.OUT).setDuration(150)
    }

    val appCompatActivity: AppCompatActivity
        get() = context as AppCompatActivity

    protected val baseActivity: MainActivity
        get() = MainActivity::class.java.cast(activity)

}