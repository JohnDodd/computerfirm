package tusur.kkv.computerfirm.base

import android.os.Bundle

abstract class BaseInjectingActivity<Component, P : BasePresenter>
    : BaseActivity<P>() {

    val component by lazy { createComponent() }

    override fun onCreate(savedInstanceState: Bundle?) {
        onInject(component)
        super.onCreate(savedInstanceState)
    }

    abstract fun onInject(component: Component)

    abstract fun createComponent(): Component
}