package tusur.kkv.computerfirm.base

import android.content.Context
import javax.inject.Inject

abstract class BaseInjectingFragment<Component, P : BasePresenter> : BaseFragment<P>() {
    protected val component by lazy { createComponent() }
    @Inject
    override lateinit var presenter: P

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        component.onInject()
    }

    abstract fun Component.onInject()

    abstract fun createComponent(): Component
}