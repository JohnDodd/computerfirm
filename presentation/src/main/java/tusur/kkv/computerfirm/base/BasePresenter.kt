package tusur.kkv.computerfirm.base

import androidx.annotation.CallSuper
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import tusur.kkv.computerfirm.injection.scope.FragmentScope
import tusur.kkv.computerfirm.utils.alert

@FragmentScope
abstract class BasePresenter {
    private val compositeDisposable = CompositeDisposable()

    @CallSuper
    open fun onViewShown() {

    }

    @CallSuper
    open fun onViewHidden() {
        compositeDisposable.clear()
    }

    protected fun Completable.addSubscription(consumer: () -> Unit = {},
                                              errorConsumer: (Throwable) -> Unit = {
                                                  alert("", it)
                                              }) {
        compositeDisposable.add(this
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ consumer() }, { errorConsumer(it) }))
    }

    protected fun <T> Single<T>.addSubscription(consumer: (T) -> Unit) {
        compositeDisposable.add(this
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ consumer(it) }, { alert("", it) }))
    }

    protected fun <T> Single<T>.addSubscription(consumer: (T) -> Unit,
                                                errorConsumer: (Throwable) -> Unit) {
        compositeDisposable.add(this
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ consumer(it) }, { errorConsumer(it) }))
    }

    protected fun <T> Observable<T>.addSubscription(consumer: Consumer<T>) {
        addSubscription(consumer, Consumer { alert("", it) })
    }

    protected fun <T> Observable<T>.addSubscription(consumer: (T) -> Unit) {
        addSubscription(Consumer { consumer.invoke(it) })
    }

    protected fun <T> Observable<T>.addSubscription(consumer: (T) -> Unit,
                                                    errorConsumer: (Throwable) -> Unit) {
        addSubscription(Consumer { consumer.invoke(it) }, Consumer { errorConsumer.invoke(it) })
    }

    protected fun <T> Observable<T>.addSubscription(consumer: Consumer<T>,
                                                    errorConsumer: Consumer<Throwable>) {
        compositeDisposable.add(this
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(consumer, errorConsumer))
    }

    protected fun Disposable.addSubscription(): Disposable {
        compositeDisposable.add(this)
        return this
    }

    open fun onBindOptionsMenu() {}

}