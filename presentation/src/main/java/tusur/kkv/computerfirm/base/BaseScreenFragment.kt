package tusur.kkv.computerfirm.base

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.ActionBar
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.controller.OptionsMenuController
import javax.inject.Inject


abstract class BaseScreenFragment<Component, P : BasePresenter>
    : BaseInjectingFragment<Component, P>() {
    protected open val titleId = R.string.base_toolbar_title

    @Inject
    lateinit var optionsMenuController: OptionsMenuController

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        resetState()
        appCompatActivity.supportActionBar?.bindToolbar()
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    protected open fun resetState() {
        appCompatActivity.window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
    }

    override fun onStart() {
        super.onStart()
        bindOptionsMenu()
        optionsMenuController.invalidate()
    }

    protected open fun bindOptionsMenu() {
        optionsMenuController.clear()
        presenter.onBindOptionsMenu()
    }

    protected open fun ActionBar.bindToolbar() {
        setDisplayHomeAsUpEnabled(true)
        title = getString(titleId)
        show()
    }

    protected fun hideActionBar() {
        appCompatActivity.supportActionBar?.hide()
    }

    protected fun showActionBar() {
        appCompatActivity.supportActionBar?.show()
    }

    protected fun hideStatusBar() {
        appCompatActivity.window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
    }

    protected fun popBackStack() {
        fragmentManager?.popBackStack()
        baseActivity.getSystemService(Activity.INPUT_METHOD_SERVICE).apply {
            this as InputMethodManager
            hideSoftInputFromWindow(view?.windowToken, 0)
        }
    }

}
