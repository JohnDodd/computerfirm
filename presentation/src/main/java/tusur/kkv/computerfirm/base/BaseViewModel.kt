package tusur.kkv.computerfirm.base

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel

abstract class BaseViewModel(open val id: Long) : ViewModel