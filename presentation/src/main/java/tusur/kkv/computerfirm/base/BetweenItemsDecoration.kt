package tusur.kkv.computerfirm.base

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import tusur.kkv.computerfirm.utils.toPx

class BetweenItemsDecoration(private val spaceDp: Int = 24) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect,
                                view: View,
                                parent: RecyclerView,
                                state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)
        val adapter = parent.adapter!!
        val position = parent.getChildAdapterPosition(view)
        val isFirstPosition = position == 0
        val isLastPosition = position == adapter.itemCount - 1

        val spacePx = spaceDp.toPx()
        val halfSpacePx = spacePx / 2
        outRect.top = halfSpacePx
        outRect.bottom = halfSpacePx
        outRect.left = spacePx
        outRect.right = spacePx

        if (isFirstPosition) {
            outRect.top = spacePx
        }
        if (isLastPosition) {
            outRect.bottom = spacePx
        }
    }

}