package tusur.kkv.computerfirm.base

import androidx.annotation.MenuRes
import tusur.kkv.computerfirm.R

enum class MenuId(@MenuRes val id: Int) {
    SEARCH(R.id.action_search),
    SEARCH_SIMPLE(R.id.action_search_simple),
    BEST(R.id.action_best);

    companion object {

        fun fromId(@MenuRes from: Int): MenuId? = values().firstOrNull { it.id == from }
    }
}