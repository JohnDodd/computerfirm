package tusur.kkv.computerfirm.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.features.component.choose.ComponentChooseDialog
import tusur.kkv.computerfirm.features.component.edit.ComponentEditFragment
import tusur.kkv.computerfirm.features.component.home.ComponentHomeFragment
import tusur.kkv.computerfirm.features.componenttype.choose.ComponentTypeChooseDialog
import tusur.kkv.computerfirm.features.componenttype.edit.ComponentTypeEditFragment
import tusur.kkv.computerfirm.features.componenttype.home.ComponentTypeHomeFragment
import tusur.kkv.computerfirm.features.customer.choose.CustomerChooseDialog
import tusur.kkv.computerfirm.features.customer.edit.CustomerEditFragment
import tusur.kkv.computerfirm.features.customer.home.CustomerHomeFragment
import tusur.kkv.computerfirm.features.employee.choose.EmployeeChooseDialog
import tusur.kkv.computerfirm.features.employee.edit.EmployeeEditFragment
import tusur.kkv.computerfirm.features.employee.home.EmployeeHomeFragment
import tusur.kkv.computerfirm.features.gender.choose.GenderChooseDialog
import tusur.kkv.computerfirm.features.order.edit.OrderEditFragment
import tusur.kkv.computerfirm.features.order.home.OrderHomeFragment
import tusur.kkv.computerfirm.features.position.choose.PositionChooseDialog
import tusur.kkv.computerfirm.features.position.edit.PositionEditFragment
import tusur.kkv.computerfirm.features.position.home.PositionHomeFragment
import tusur.kkv.computerfirm.features.purveyor.choose.PurveyorChooseDialog
import tusur.kkv.computerfirm.features.purveyor.edit.PurveyorEditFragment
import tusur.kkv.computerfirm.features.purveyor.home.PurveyorHomeFragment
import tusur.kkv.computerfirm.features.service.choose.ServiceChooseDialog
import tusur.kkv.computerfirm.features.service.edit.ServiceEditFragment
import tusur.kkv.computerfirm.features.service.home.ServiceHomeFragment
import tusur.kkv.computerfirm.injection.scope.ActivityScope
import tusur.kkv.data.injection.qualifier.ForActivity
import java.util.*
import javax.inject.Inject

@ActivityScope
class UIRouter @Inject constructor(@ForActivity private val context: Context) {

    private val supportFragmentManager
        get() = (context as AppCompatActivity).supportFragmentManager

    fun openEmployeeHomePage() {
        openPage(::EmployeeHomeFragment)
    }

    fun openCustomerHomePage() {
        openPage(::CustomerHomeFragment)
    }

    fun openPositionHomePage() {
        openPage(::PositionHomeFragment)
    }

    fun openComponentHomePage() {
        openPage(::ComponentHomeFragment)
    }

    fun openServiceHomePage() {
        openPage(::ServiceHomeFragment)
    }

    fun openComponentTypeHomePage() {
        openPage(::ComponentTypeHomeFragment)
    }

    fun openPurveyorHomePage() {
        openPage(::PurveyorHomeFragment)
    }

    fun openOrderHomePage() {
        openPage(::OrderHomeFragment)
    }

    fun openChooseGenderPage(targetFragment: Fragment) {
        openDialog(::GenderChooseDialog, targetFragment)
    }

    fun openPositionChoosePage(targetFragment: Fragment) {
        openDialog(::PositionChooseDialog, targetFragment)
    }

    fun openComponentChoosePage(targetFragment: Fragment,
                                orderId: Long? = null,
                                multiSelect: Boolean) {
        openDialog({
            ComponentChooseDialog().apply {
                arguments = Bundle().apply {
                    orderId?.let { putLong(ComponentChooseDialog.ORDER_ID, it) }
                    putBoolean(ComponentChooseDialog.MULTI_SELECT, multiSelect)
                }
            }
        }, targetFragment)
    }

    fun openServiceChoosePage(targetFragment: Fragment,
                              orderId: Long? = null,
                              multiSelect: Boolean) {
        openDialog({
            ServiceChooseDialog().apply {
                arguments = Bundle().apply {
                    orderId?.let { putLong(ServiceChooseDialog.ORDER_ID, it) }
                    putBoolean(ServiceChooseDialog.MULTI_SELECT, multiSelect)
                }
            }
        }, targetFragment)
    }


    fun openPurveyorChoosePage(targetFragment: Fragment) {
        openDialog(::PurveyorChooseDialog, targetFragment)
    }

    fun openComponentTypeChoosePage(targetFragment: Fragment) {
        openDialog(::ComponentTypeChooseDialog, targetFragment)
    }

    fun openCustomerChoosePage(targetFragment: Fragment) {
        openDialog(::CustomerChooseDialog, targetFragment)
    }

    fun openEmployeeChoosePage(targetFragment: Fragment) {
        openDialog(::EmployeeChooseDialog, targetFragment)
    }

    private inline fun <reified F : BaseDialogFragment<out BasePresenter>>
            openDialog(fragment: () -> F, targetFragment: Fragment) {
        if (supportFragmentManager.findFragmentByTag(F::class.java.name) == null)
            fragment().apply { setTargetFragment(targetFragment, 0) }.show(supportFragmentManager,
                    F::class.java.name)
    }

    fun openDatePage(result: (Date) -> Unit) {
        val calendar = Calendar.getInstance()
        val dialog = DatePickerDialog.newInstance(
                { _, year, monthOfYear, dayOfMonth ->
                    calendar.set(Calendar.YEAR, year)
                    calendar.set(Calendar.MONTH, monthOfYear)
                    calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                    result(calendar.time)
                },
                calendar.get(Calendar.YEAR), // Initial year selection
                calendar.get(Calendar.MONTH), // Initial month selection
                calendar.get(Calendar.DAY_OF_MONTH) // Inital day selection
        )
        dialog.show(supportFragmentManager, dialog.javaClass.name)
    }

    fun openEmployeeEditPage(employeeId: Long = NO_ID) {
        openPage({
            EmployeeEditFragment().apply {
                arguments = Bundle().apply { putLong(EmployeeEditFragment.KEY_ID, employeeId) }
            }
        })
    }

    fun openPositionEditPage(positionId: Long = NO_ID) {
        openPage({
            PositionEditFragment().apply {
                arguments = Bundle().apply { putLong(PositionEditFragment.KEY_ID, positionId) }
            }
        })
    }

    fun openCustomerEditPage(id: Long = NO_ID) {
        openPage({
            CustomerEditFragment().apply {
                arguments = Bundle().apply { putLong(CustomerEditFragment.KEY_ID, id) }
            }
        })
    }

    fun openServiceEditPage(id: Long = NO_ID) {
        openPage({
            ServiceEditFragment().apply {
                arguments = Bundle().apply { putLong(ServiceEditFragment.KEY_ID, id) }
            }
        })
    }

    fun openOrderEditPage(id: Long = NO_ID) {
        openPage({
            OrderEditFragment().apply {
                arguments = Bundle().apply { putLong(OrderEditFragment.KEY_ID, id) }
            }
        })
    }


    fun openPurveyorEditPage(purveyorId: Long = NO_ID) {
        openPage({
            PurveyorEditFragment().apply {
                arguments = Bundle().apply { putLong(PurveyorEditFragment.KEY_ID, purveyorId) }
            }
        })
    }


    fun openComponentEditPage(componentId: Long = NO_ID) {
        openPage({
            ComponentEditFragment().apply {
                arguments = Bundle().apply { putLong(ComponentEditFragment.KEY_ID, componentId) }
            }
        })
    }

    fun openComponentTypeEditPage(componentTypeId: Long = NO_ID) {
        openPage({
            ComponentTypeEditFragment().apply {
                arguments =
                        Bundle().apply {
                            putLong(ComponentTypeEditFragment.KEY_ID,
                                    componentTypeId)
                        }
            }
        })
    }

    private inline fun <P : BasePresenter, reified T : BaseFragment<P>> openPage(fragment: () -> T,
                                                                                 addToStack: Boolean = true) {
        replaceIfNeed(R.id.main_container, fragment)
    }

    fun AppCompatActivity.removeFromStack(fragment: Fragment) {
        supportFragmentManager.inTransaction { remove(fragment) }
    }

    private inline fun <reified T : BaseFragment<BasePresenter>> Fragment.replaceIfNeed(id: Int,
                                                                                        fragmentSupplier: () -> T,
                                                                                        addToStack: Boolean = false) {
        val fragmentById = childFragmentManager.findFragmentById(id)
        if (fragmentById == null || fragmentById !is T) {
            val fragment = fragmentSupplier()
            performTransaction { replaceWithTag(id, fragment, addToStack) }
        }
    }

    private inline fun <P : BasePresenter, reified T : BaseFragment<P>> replaceIfNeed(id: Int,
                                                                                      fragment: () -> T,
                                                                                      addToStack: Boolean = true) {
        context as AppCompatActivity
        context.apply {
            val fragmentByTag = supportFragmentManager.findFragmentByTag(T::class.java.name)
            fragmentByTag?.let {
                supportFragmentManager.popBackStackImmediate(T::class.java.name, 0)
                return
            }
            val fragmentById = supportFragmentManager.findFragmentById(id)
            if (fragmentById == null || fragmentById !is T) {
                val sharedElements = fragmentById?.let {
                    it as BaseFragment<BasePresenter>
                    it.getSharedElements()
                }
                performTransaction { replaceWithTag(id, fragment(), addToStack, sharedElements) }
            }
        }
    }

    private inline fun <reified T : Fragment> AppCompatActivity.addIfNeed(id: Int, fragment: T) {
        val fragmentById = supportFragmentManager.findFragmentById(id)
        if (fragmentById == null || fragmentById !is T) {
            performTransaction { addWithTag(id, fragment) }
        }
    }

    inline fun <reified T : Fragment> FragmentTransaction.addWithTag(id: Int, fragment: T) {
        addToBackStack(T::class.java.name)
        add(id, fragment, T::class.java.name)
    }

    inline fun <P : BasePresenter, reified T : BaseFragment<P>>
            FragmentTransaction.replaceWithTag(id: Int,
                                               fragment: T,
                                               addToStack: Boolean = true,
                                               sharedElements: Array<View>? = null) {
        sharedElements?.forEach {
            ViewCompat.setTransitionName(it, it.tag.toString())
            addSharedElement(it, ViewCompat.getTransitionName(it) ?: "")
        }

        replace(id, fragment, T::class.java.name)
        if (addToStack) addToBackStack(T::class.java.name)
    }

    fun Context.inflate(layoutId: Int, root: ViewGroup? = null): View =
            LayoutInflater.from(this).inflate(layoutId, root)

    inline fun Fragment.performTransaction(body: FragmentTransaction.() -> Unit) {
        childFragmentManager.inTransaction(body)
    }

    inline fun AppCompatActivity.performTransaction(body: FragmentTransaction.() -> Unit) {
        supportFragmentManager.inTransaction(body)
    }

    inline fun FragmentManager.inTransaction(body: FragmentTransaction.() -> Unit) {
        beginTransaction().apply(body).commitAllowingStateLoss()
    }

    private fun <T> findByTag(clazz: Class<T>) =
            supportFragmentManager.findFragmentByTag(clazz.name)

}