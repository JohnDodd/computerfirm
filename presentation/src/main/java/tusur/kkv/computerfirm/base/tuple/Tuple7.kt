package tusur.kkv.computerfirm.base.tuple

data class Tuple7<T1, T2, T3, T4, T5, T6, T7>(val first: T1,
                                              val second: T2,
                                              val third: T3,
                                              val fourth: T4,
                                              val fifth: T5,
                                              val sixth: T6,
                                              val seventh: T7
)