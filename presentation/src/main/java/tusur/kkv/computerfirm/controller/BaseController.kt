package tusur.kkv.computerfirm.controller

import android.view.Menu
import android.view.MenuItem

interface BaseController {

    fun onCreateOptionsMenu(menu: Menu) {}
    fun onOptionsItemSelected(item: MenuItem) {}
    fun onPrepareOptionsMenu(menu: Menu) {}
}