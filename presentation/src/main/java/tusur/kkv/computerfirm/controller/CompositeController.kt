package tusur.kkv.computerfirm.controller

import android.view.Menu
import android.view.MenuItem
import tusur.kkv.data.injection.qualifier.ForActivity
import javax.inject.Inject

@ForActivity
class CompositeController @Inject constructor(optionsMenuController: OptionsMenuController)
    : BaseController {

    private val controllers = listOf<BaseController>(optionsMenuController)

    override fun onCreateOptionsMenu(menu: Menu) {
        controllers.forEach { it.onCreateOptionsMenu(menu) }
    }

    override fun onOptionsItemSelected(item: MenuItem) {
        controllers.forEach { it.onOptionsItemSelected(item) }
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        controllers.forEach { it.onPrepareOptionsMenu(menu) }
    }

}