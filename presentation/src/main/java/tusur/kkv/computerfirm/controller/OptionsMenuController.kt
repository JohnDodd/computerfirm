package tusur.kkv.computerfirm.controller

import android.app.Activity
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject.createDefault
import io.reactivex.subjects.PublishSubject.create
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.base.MenuId
import tusur.kkv.computerfirm.injection.scope.ActivityScope
import javax.inject.Inject

@ActivityScope
class OptionsMenuController @Inject constructor(private val activity: Activity)
    : BaseController {

    private var menu: Menu? = null
    private val menuItems = HashSet<MenuId>()

    private val onOptionsItemSelectedSubject = create<MenuId>()
    private val onSearchExpandedSubject = createDefault(false)
    private val onQueryTextChangeSubject = createDefault("")
    private val onQueryTextSubmitSubject = create<String>()

    val onMenuItemSelectedStream: Observable<MenuId> get() = onOptionsItemSelectedSubject.hide()
    val onSearchExpandedStream: Observable<Boolean> get() = onSearchExpandedSubject.hide()
    val onQueryTextChangeStream: Observable<String> get() = onQueryTextChangeSubject.hide()
    val onQueryTextSubmitStream: Observable<String> get() = onQueryTextSubmitSubject.hide()

    override fun onCreateOptionsMenu(menu: Menu) {
        activity.menuInflater.inflate(R.menu.main, menu)
        this.menu = menu
        val searchItem = menu.findItem(R.id.action_search)
        val searchView = searchItem.actionView as SearchView
        searchItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                onSearchExpandedSubject.onNext(true)
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                onSearchExpandedSubject.onNext(false)
                return true
            }
        })
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            init {
                val srcTextView =
                        searchView.findViewById<TextView>(R.id.search_src_text)
                srcTextView.setOnEditorActionListener { _, _, _ ->
                    onQueryTextSubmit(searchView.query.toString())
                    true
                }
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                onQueryTextSubmitSubject.onNext(query)
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                onQueryTextChangeSubject.onNext(newText)
                return true
            }
        })
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        for (i in 0 until menu.size()) {
            val item = menu.getItem(i)
            val fromId = MenuId.fromId(item.itemId)
            item.isVisible = if (fromId != null) menuItems.contains(fromId) else false
//            item.isVisible = fromId?.let { menuItems.contains(it) } ?: false
        }
    }

    fun clear() {
        menuItems.clear()
    }

    fun invalidate() {
        activity.invalidateOptionsMenu()
    }

    fun setMenuItems(vararg menuId: MenuId) {
        clear()
        menuItems.addAll(menuId)
    }

    override fun onOptionsItemSelected(item: MenuItem) {
        MenuId.fromId(item.itemId)?.let { onOptionsItemSelectedSubject.onNext(it) }
    }

    fun expandSearchView(expand: Boolean) {
        menu?.findItem(R.id.action_search)?.apply {
            if (expand) expandActionView()
            else collapseActionView()
        }
    }

}