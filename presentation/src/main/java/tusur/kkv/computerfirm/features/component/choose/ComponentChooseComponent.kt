package tusur.kkv.computerfirm.features.component.choose

import dagger.Subcomponent
import tusur.kkv.computerfirm.injection.scope.FragmentScope

@FragmentScope
@Subcomponent(modules = [ComponentChooseDialogModule::class])
interface ComponentChooseComponent {

    fun inject(fragment: ComponentChooseDialog)
}