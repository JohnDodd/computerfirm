package tusur.kkv.computerfirm.features.component.choose

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import tusur.kkv.computerfirm.base.NO_ID
import tusur.kkv.computerfirm.features.simpledialog.SimpleDialog

class ComponentChooseDialog : SimpleDialog<ComponentChooseComponent, ComponentChoosePresenter>() {

    override fun ComponentChooseComponent.onInject() = inject(this@ComponentChooseDialog)

    override fun bindView(view: View,
                          savedInstanceState: Bundle?,
                          dialog: AlertDialog) {
        super.bindView(view, savedInstanceState, dialog)
        val orderId = arguments?.getLong(ORDER_ID) ?: NO_ID
        val multiSelect = arguments?.getBoolean(MULTI_SELECT) ?: false
        presenter.onBindView(multiSelect, orderId)
    }

    override fun onCancel(dialog: DialogInterface?) {
        super.onCancel(dialog)
        presenter.onDismissed()
    }


    override fun createComponent() =
            baseActivity.component.createComponentChooseDialogComponent(ComponentChooseDialogModule(
                    this,
                    simpleViewPresenter))

    companion object {
        const val ORDER_ID = "orderId"
        const val MULTI_SELECT = "multiSelect"
    }
}
