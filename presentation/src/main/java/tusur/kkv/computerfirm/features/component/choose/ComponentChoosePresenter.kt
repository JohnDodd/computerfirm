package tusur.kkv.computerfirm.features.component.choose

import io.reactivex.Observable
import io.reactivex.rxkotlin.Observables
import io.reactivex.rxkotlin.withLatestFrom
import io.reactivex.subjects.BehaviorSubject.createDefault
import io.reactivex.subjects.PublishSubject.create
import tusur.kkv.computerfirm.base.NO_ID
import tusur.kkv.computerfirm.base.UIRouter
import tusur.kkv.computerfirm.features.simpledialog.AppendSimpleAddViewModel
import tusur.kkv.computerfirm.features.simpledialog.SimpleDialogPresenter
import tusur.kkv.domain.interactor.component.RetrieveComponentsInteractor
import tusur.kkv.domain.interactor.ordercomponent.RequestComponentsByOrderInteractor
import tusur.kkv.domain.model.Component
import javax.inject.Inject

class ComponentChoosePresenter
@Inject constructor(private val mapper: ComponentSimpleViewModelMapper,
                    private val retrieve: RetrieveComponentsInteractor,
                    private val resultListener: OnComponentResult,
                    private val requestByOrder: RequestComponentsByOrderInteractor,
                    view: SimpleDialogPresenter.SimpleDialogView,
                    appendMapper: AppendSimpleAddViewModel,
                    uiRouter: UIRouter) :
        SimpleDialogPresenter(view, appendMapper, uiRouter) {

    private val multiSelectSubject = createDefault(false)
    private val modelClickedSubject = create<Component>()
    private val selectedItems = mutableListOf<Component>()

    fun onBindView(multiSelect: Boolean, orderId: Long) {
        multiSelectSubject.onNext(multiSelect)
        if (orderId != NO_ID) {
            requestByOrder.getSingle(orderId).addSubscription({
                selectedItems.clear()
                selectedItems.addAll(it)
            }) {
                selectedItems.clear()
            }
        }
    }

    override fun onViewShown() {
        super.onViewShown()
        val selectedItemsStream = modelClickedSubject
                .withLatestFrom(multiSelectSubject)
                .scan(selectedItems) { list, pair ->
                    val component = pair.first
                    val multiSelect = pair.second
                    if (multiSelect && !list.remove(component)) list.add(component)
                    if (!multiSelect && !list.isEmpty()) list.clear()
                    list
                }

        Observables.combineLatest(retrieve.getBehaviorStream().startWith(listOf<Component>()),
                selectedItemsStream
        )
                .flatMapSingle { pair ->
                    val components = pair.first
                    val selected = pair.second
                    Observable.fromIterable(components)
                            .map { Pair(it, selected.contains(it)) }
                            .map(mapper)
                            .toList()

                }
                .map(appendMapper)
                .addSubscription(view::updateItems)


        modelClickedSubject.withLatestFrom(multiSelectSubject).addSubscription {
            if (!it.second) {
                resultListener.onResult(it.first)
                view.close()
            }
        }
    }

    fun onModelClicked(model: Component) {
        modelClickedSubject.onNext(model)
    }

    fun onAddClicked() {
        uiRouter.openComponentEditPage()
        view.close()
    }

    fun onLongClicked(model: Component) {
        uiRouter.openComponentEditPage(model.id)
        view.close()
    }

    fun onDismissed() {
        resultListener.onResult(*selectedItems.toTypedArray())
    }

}