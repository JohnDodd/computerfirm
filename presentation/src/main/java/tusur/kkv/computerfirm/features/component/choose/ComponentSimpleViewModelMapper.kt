package tusur.kkv.computerfirm.features.component.choose

import io.reactivex.functions.Function
import tusur.kkv.computerfirm.features.simpledialog.SimpleViewModel
import tusur.kkv.domain.model.Component
import javax.inject.Inject

class ComponentSimpleViewModelMapper
@Inject constructor() : Function<Pair<Component, Boolean>, SimpleViewModel<Component>> {
    override fun apply(pair: Pair<Component, Boolean>): SimpleViewModel<Component> {
        val component = pair.first
        val isSelected = pair.second
        return SimpleViewModel(
                component.id,
                component.name,
                component,
                isSelected
        )
    }
}