package tusur.kkv.computerfirm.features.component.choose

import tusur.kkv.domain.model.Component

interface OnComponentResult {
    fun onResult(vararg component: Component)
}