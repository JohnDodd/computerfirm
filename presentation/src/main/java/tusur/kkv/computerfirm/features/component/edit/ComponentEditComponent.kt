package tusur.kkv.computerfirm.features.component.edit

import dagger.Subcomponent
import tusur.kkv.computerfirm.injection.scope.FragmentScope

@FragmentScope
@Subcomponent(modules = [ComponentEditFragmentModule::class])
interface ComponentEditComponent {

    fun inject(fragment: ComponentEditFragment)

}