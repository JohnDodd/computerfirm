package tusur.kkv.computerfirm.features.component.edit

import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.fragment_component_edit.*
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.base.BaseScreenFragment
import tusur.kkv.computerfirm.base.NO_ID
import tusur.kkv.computerfirm.base.UIRouter
import tusur.kkv.computerfirm.features.componenttype.choose.OnComponentTypeResult
import tusur.kkv.computerfirm.features.purveyor.choose.OnPurveyorResult
import tusur.kkv.computerfirm.utils.onClick
import tusur.kkv.computerfirm.utils.showSnack
import tusur.kkv.computerfirm.utils.toLongOrElse
import tusur.kkv.computerfirm.utils.toStringOrEmpty
import tusur.kkv.domain.model.ComponentType
import tusur.kkv.domain.model.Purveyor
import javax.inject.Inject

class ComponentEditFragment : BaseScreenFragment<ComponentEditComponent, ComponentEditPresenter>(),
        OnPurveyorResult,
        OnComponentTypeResult {

    override val viewId = R.layout.fragment_component_edit

    @Inject
    lateinit var uiRouter: UIRouter

    override fun createComponent() = baseActivity.component
            .createComponentEditComponent(ComponentEditFragmentModule(this, viewPresenter))

    override fun ComponentEditComponent.onInject() = inject(this@ComponentEditFragment)

    override fun bindView(view: View, savedInstanceState: Bundle?) {
        val componentId = arguments?.getLong(KEY_ID) ?: NO_ID
        presenter.onBindView(componentId)
        purveyor_text.onClick(presenter::onPurveyorClicked)
        component_type_name_text.onClick(presenter::onComponentTypeClicked)
        saveButton.onClick(presenter::onSaveButtonClicked)
    }

    override fun onResult(result: ComponentType) {
        presenter.onComponentTypeChanged(result)
    }

    override fun onResult(purveyor: Purveyor) {
        presenter.onCustomerChanged(purveyor)
    }

    private val viewPresenter = object : ComponentEditPresenter.View {
        override val guaranteePeriod: Long?
            get() = guarantee_period_text.text.toStringOrEmpty().toLongOrNull()
        override val name: String
            get() = component_name_text.text.toStringOrEmpty()
        override val cost: Long
            get() = cost_text.text.toStringOrEmpty().toLongOrElse(-1)

        override fun updateCost(cost: String) {
            cost_text.setText(cost)
        }

        override fun updateGuaranteePeriod(guaranteePeriod: String) {
            guarantee_period_text.setText(guaranteePeriod)
        }

        override fun updateName(name: String) {
            component_name_text.setText(name)
        }

        override fun openPurveyorChoosePage() {
            uiRouter.openPurveyorChoosePage(this@ComponentEditFragment)
        }

        override fun openComponentTypeChoosePage() {
            uiRouter.openComponentTypeChoosePage(this@ComponentEditFragment)
        }

        override fun showSaveError() {
            view?.showSnack(getString(R.string.simple_error))
        }

        override fun closeView() {
            popBackStack()
        }

        override fun showNameError(message: String) {
            component_name_text.error = message
            scroll_view.smoothScrollTo(0, component_name_text.bottom)
        }

        override fun showCostError(message: String) {
            cost_text.error = message
            scroll_view.smoothScrollTo(0, cost_text.bottom)
        }

        override fun showPurveyorError(message: String) {
            purveyor_text.error = message
            scroll_view.smoothScrollTo(0, purveyor_text.bottom)
        }

        override fun showTypeError(message: String) {
            component_type_name_text.error = message
            scroll_view.smoothScrollTo(0, component_type_name_text.bottom)
        }

        override fun updateComponentType(type: String) {
            component_type_name_text.text = type
        }

        override fun updatePurveyor(purveyor: String) {
            purveyor_text.text = purveyor
        }
    }

    companion object {
        const val KEY_ID = "KEY_ID"
    }
}