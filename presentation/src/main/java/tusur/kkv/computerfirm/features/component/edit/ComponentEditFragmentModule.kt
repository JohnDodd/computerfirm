package tusur.kkv.computerfirm.features.component.edit

import dagger.Module
import tusur.kkv.computerfirm.injection.module.FragmentModule

@Module
class ComponentEditFragmentModule(fragment: ComponentEditFragment,
                                  view: ComponentEditPresenter.View)
    : FragmentModule<ComponentEditFragment, ComponentEditPresenter.View>(fragment, view) {

}