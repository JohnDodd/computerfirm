package tusur.kkv.computerfirm.features.component.edit

import com.annimon.stream.Optional
import com.annimon.stream.Optional.empty
import com.annimon.stream.Optional.of
import io.reactivex.rxkotlin.Observables
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.BehaviorSubject.createDefault
import tusur.kkv.computerfirm.base.BasePresenter
import tusur.kkv.computerfirm.base.BaseView
import tusur.kkv.computerfirm.base.NO_ID
import tusur.kkv.data.exception.IllegalParamsException
import tusur.kkv.domain.interactor.component.RefreshComponentInteractor
import tusur.kkv.domain.interactor.component.RequestComponentInteractor
import tusur.kkv.domain.interactor.component.SendComponentInteractor
import tusur.kkv.domain.model.ComponentType
import tusur.kkv.domain.model.Purveyor
import tusur.kkv.domain.repository.IComponentRepository.ParamsExceptionId.Companion.COST
import tusur.kkv.domain.repository.IComponentRepository.ParamsExceptionId.Companion.NAME
import tusur.kkv.domain.repository.IComponentRepository.ParamsExceptionId.Companion.PURVEYOR_ID
import tusur.kkv.domain.repository.IComponentRepository.ParamsExceptionId.Companion.TYPE_ID
import tusur.kkv.domain.utils.unwrap
import javax.inject.Inject

class ComponentEditPresenter
@Inject constructor(private val request: RequestComponentInteractor,
                    private val send: SendComponentInteractor,
                    private val refresh: RefreshComponentInteractor,
                    private val mapper: EditComponentViewModelMapper,
                    private val view: ComponentEditPresenter.View)
    : BasePresenter() {

    private var idSubject = createDefault(NO_ID)

    private val purveyorSubject: BehaviorSubject<Optional<Purveyor>> =
            createDefault(empty())

    private val componentTypeSubject: BehaviorSubject<Optional<ComponentType>> =
            createDefault(empty())

    override fun onViewShown() {
        super.onViewShown()
        componentTypeSubject.unwrap().map { it.name }.addSubscription(view::updateComponentType)
        purveyorSubject.unwrap().map { it.name }.addSubscription(view::updatePurveyor)
    }

    fun onBindView(id: Long) {
        idSubject.onNext(id)
        if (id != NO_ID)
            request.getSingle(id).map(mapper).addSubscription {
                view.updateCost(it.cost)
                view.updateGuaranteePeriod(it.guaranteePeriod)
                view.updateName(it.name)
                purveyorSubject.onNext(of(it.purveyor))
                componentTypeSubject.onNext(of(it.type))
            }
    }

    fun onSaveButtonClicked() {
        Observables.combineLatest(idSubject,
                componentTypeSubject.map { it.map { o -> o.id }.orElse(NO_ID) },
                purveyorSubject.map { it.map { o -> o.id }.orElse(NO_ID) },
                ::Triple)
                .firstOrError()
                .addSubscription({ tuple ->
                    val id = tuple.first
                    val typeId = tuple.second
                    val purveyorId = tuple.third
                    if (id == NO_ID) addSendSubscription(typeId, purveyorId)
                    else addRefreshSubscription(id, typeId, purveyorId)
                }) { view.showSaveError() }
    }

    private fun addSendSubscription(typeId: Long, purveyorId: Long) {
        send.getSingle(SendComponentInteractor.Params(
                typeId,
                view.guaranteePeriod,
                view.name,
                view.cost,
                purveyorId
        )).addSubscription({
            if (it) view.closeView()
            else view.showSaveError()
        }, showErrorConsumer)
    }

    private fun addRefreshSubscription(id: Long, typeId: Long, purveyorId: Long) {
        refresh.getRefreshSingle(RefreshComponentInteractor.Params(
                id,
                typeId,
                view.guaranteePeriod,
                view.name,
                view.cost,
                purveyorId
        )).addSubscription({ view.closeView() }, showErrorConsumer)
    }

    fun onPurveyorClicked() {
        view.openPurveyorChoosePage()
    }

    fun onComponentTypeClicked() {
        view.openComponentTypeChoosePage()
    }

    fun onComponentTypeChanged(componentType: ComponentType) {
        componentTypeSubject.onNext(of(componentType))
    }

    fun onCustomerChanged(purveyor: Purveyor) {
        purveyorSubject.onNext(of(purveyor))
    }

    private val showErrorConsumer: (Throwable) -> Unit = {
        if (it is IllegalParamsException)
            when (it.id) {
                NAME -> view.showNameError(it.message)
                COST -> view.showCostError(it.message)
                TYPE_ID -> view.showTypeError(it.message)
                PURVEYOR_ID -> view.showPurveyorError(it.message)
                else -> view.showSaveError()
            }
        else view.showSaveError()
    }


    interface View : BaseView {
        val guaranteePeriod: Long?
        val name: String
        val cost: Long

        fun updateCost(cost: String)
        fun updateGuaranteePeriod(guaranteePeriod: String)
        fun updateName(name: String)

        fun openPurveyorChoosePage()
        fun openComponentTypeChoosePage()

        fun showSaveError()
        fun closeView()

        fun showCostError(message: String)
        fun showPurveyorError(message: String)
        fun showTypeError(message: String)

        fun showNameError(message: String)
        fun updateComponentType(type: String)
        fun updatePurveyor(purveyor: String)


    }
}