package tusur.kkv.computerfirm.features.component.edit

import tusur.kkv.domain.model.ComponentType
import tusur.kkv.domain.model.Purveyor

data class ComponentEditViewModel(val id: Long,
                                  val type: ComponentType,
                                  val guaranteePeriod: String,
                                  val name: String,
                                  val cost: String,
                                  val purveyor: Purveyor)