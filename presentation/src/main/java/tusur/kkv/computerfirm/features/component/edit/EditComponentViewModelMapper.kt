package tusur.kkv.computerfirm.features.component.edit

import io.reactivex.functions.Function
import tusur.kkv.computerfirm.utils.toStringOrEmpty
import tusur.kkv.domain.model.Component
import javax.inject.Inject

class EditComponentViewModelMapper
@Inject constructor() : Function<Component, ComponentEditViewModel> {

    override fun apply(component: Component) = ComponentEditViewModel(
            component.id,
            component.type,
            component.guaranteePeriod.toStringOrEmpty(),
            component.name,
            component.cost.toString(),
            component.purveyor
    )


}