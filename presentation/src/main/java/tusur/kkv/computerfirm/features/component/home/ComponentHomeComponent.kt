package tusur.kkv.computerfirm.features.component.home

import dagger.Subcomponent
import tusur.kkv.computerfirm.injection.scope.FragmentScope

@FragmentScope
@Subcomponent(modules = [ComponentHomeFragmentModule::class])
interface ComponentHomeComponent {

    fun inject(fragment: ComponentHomeFragment)

}