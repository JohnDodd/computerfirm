package tusur.kkv.computerfirm.features.component.home

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import kotlinx.android.synthetic.main.fragment_employee_home.*
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.base.BaseScreenFragment
import tusur.kkv.computerfirm.utils.onClick
import javax.inject.Inject
import javax.inject.Provider

class ComponentHomeFragment : BaseScreenFragment<ComponentHomeComponent, ComponentHomePresenter>() {

    override val viewId = R.layout.fragment_component_home
    @Inject
    lateinit var adapter: RendererRecyclerViewAdapter
    @Inject
    lateinit var layoutManager: Provider<RecyclerView.LayoutManager>
    @Inject
    lateinit var decoration: Provider<RecyclerView.ItemDecoration>

    override val titleId = R.string.components

    override fun ComponentHomeComponent.onInject() = inject(this@ComponentHomeFragment)

    override fun createComponent() = baseActivity.component
            .createComponentHomeComponent(ComponentHomeFragmentModule(this, viewPresenter))

    override fun bindView(view: View, savedInstanceState: Bundle?) {
        recyclerView.adapter = adapter
        recyclerView.layoutManager = layoutManager.get()
        recyclerView.addItemDecoration(decoration.get())
        fab.onClick(presenter::onFabClicked)
    }

    private val viewPresenter = object : ComponentHomePresenter.View {
        override fun updateItems(items: List<ViewModel>) {
            adapter.setItems(items)
        }
    }
}