package tusur.kkv.computerfirm.features.component.home

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import tusur.kkv.computerfirm.base.BasePresenter
import tusur.kkv.computerfirm.base.BaseView
import tusur.kkv.computerfirm.base.UIRouter
import tusur.kkv.computerfirm.features.component.home.model.ComponentHomeViewModel
import tusur.kkv.domain.interactor.component.RetrieveComponentsInteractor
import tusur.kkv.domain.utils.forEachElement
import javax.inject.Inject

class ComponentHomePresenter
@Inject constructor(private val view: ComponentHomePresenter.View,
                    private val retrieve: RetrieveComponentsInteractor,
                    private val mapper: ComponentHomeViewModelMapper,
                    private val uiRouter: UIRouter)
    : BasePresenter() {

    override fun onViewShown() {
        super.onViewShown()
        retrieve.getBehaviorStream()
                .forEachElement { it.map(mapper) }
                .addSubscription { view.updateItems(it) }
    }

    fun onFabClicked() {
        uiRouter.openComponentEditPage()
    }

    fun onModelClicked(model: ComponentHomeViewModel) {
        uiRouter.openComponentEditPage(model.id)
    }

    interface View : BaseView {
        fun updateItems(items: List<ViewModel>)
    }


}