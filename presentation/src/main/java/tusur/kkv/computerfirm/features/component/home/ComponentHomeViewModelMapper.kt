package tusur.kkv.computerfirm.features.component.home

import io.reactivex.functions.Function
import tusur.kkv.computerfirm.features.component.home.model.ComponentHomeViewModel
import tusur.kkv.computerfirm.mapper.MoneyMapper
import tusur.kkv.domain.model.Component
import javax.inject.Inject

class ComponentHomeViewModelMapper @Inject constructor(private val moneyMapper: MoneyMapper)
    : Function<Component, ComponentHomeViewModel> {

    override fun apply(component: Component) = ComponentHomeViewModel(
            component.id,
            component.name,
            component.type.name,
            moneyMapper.apply(component.cost)
    )

}