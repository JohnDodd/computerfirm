package tusur.kkv.computerfirm.features.component.home.model

import tusur.kkv.computerfirm.base.BaseViewModel

class ComponentHomeViewModel(override val id: Long,
                             val name: String,
                             val type: String,
                             val cost: String)
    : BaseViewModel(id)