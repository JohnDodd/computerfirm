package tusur.kkv.computerfirm.features.componenttype.choose

import dagger.Subcomponent
import tusur.kkv.computerfirm.injection.scope.FragmentScope


@FragmentScope
@Subcomponent(modules = [ComponentTypeChooseDialogModule::class])
interface ComponentTypeChooseComponent {

    fun inject(fragment: ComponentTypeChooseDialog)

}