package tusur.kkv.computerfirm.features.componenttype.choose

import tusur.kkv.computerfirm.features.simpledialog.SimpleDialog

class ComponentTypeChooseDialog : SimpleDialog<ComponentTypeChooseComponent, ComponentTypeChoosePresenter>() {

    override fun ComponentTypeChooseComponent.onInject() = inject(this@ComponentTypeChooseDialog)

    override fun createComponent() =
            baseActivity.component.createComponentTypeChooseDialogComponent(
                    ComponentTypeChooseDialogModule(this, simpleViewPresenter))
}