package tusur.kkv.computerfirm.features.componenttype.choose

import tusur.kkv.computerfirm.base.UIRouter
import tusur.kkv.computerfirm.features.simpledialog.AppendSimpleAddViewModel
import tusur.kkv.computerfirm.features.simpledialog.SimpleDialogPresenter
import tusur.kkv.domain.interactor.componenttype.choose.RetrieveComponentTypesInteractor
import tusur.kkv.domain.model.ComponentType
import tusur.kkv.domain.utils.forEachElement
import javax.inject.Inject

class ComponentTypeChoosePresenter
@Inject constructor(private val mapper: ComponentTypeSimpleViewModelMapper,
                    private val retrieve: RetrieveComponentTypesInteractor,
                    private val resultListener: OnComponentTypeResult,
                    view: SimpleDialogView,
                    appendMapper: AppendSimpleAddViewModel,
                    uiRouter: UIRouter)
    : SimpleDialogPresenter(view, appendMapper, uiRouter) {

    override fun onViewShown() {
        super.onViewShown()
        retrieve.getBehaviorStream()
                .startWith(listOf<ComponentType>())
                .forEachElement { it.map(mapper) }
                .map(appendMapper)
                .addSubscription(view::updateItems)
    }


    fun onModelClicked(model: ComponentType) {
        resultListener.onResult(model)
        view.close()
    }

    fun onAddClicked() {
        uiRouter.openComponentTypeEditPage()
        view.close()
    }

    fun onLongClicked(model: ComponentType) {
        uiRouter.openComponentTypeEditPage(model.id)
        view.close()
    }

}