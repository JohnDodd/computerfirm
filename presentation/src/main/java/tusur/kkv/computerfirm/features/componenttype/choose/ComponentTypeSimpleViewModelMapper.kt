package tusur.kkv.computerfirm.features.componenttype.choose

import io.reactivex.functions.Function
import tusur.kkv.computerfirm.features.simpledialog.SimpleViewModel
import tusur.kkv.domain.model.ComponentType
import javax.inject.Inject

class ComponentTypeSimpleViewModelMapper
@Inject constructor() : Function<ComponentType, SimpleViewModel<ComponentType>> {

    override fun apply(type: ComponentType) =
            SimpleViewModel(
                    type.id,
                    type.name,
                    type
            )
}