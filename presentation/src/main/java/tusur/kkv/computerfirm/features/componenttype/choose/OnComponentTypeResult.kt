package tusur.kkv.computerfirm.features.componenttype.choose

import tusur.kkv.domain.model.ComponentType

interface OnComponentTypeResult {
    fun onResult(result: ComponentType)
}