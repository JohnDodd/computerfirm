package tusur.kkv.computerfirm.features.componenttype.edit

import dagger.Subcomponent
import tusur.kkv.computerfirm.injection.scope.FragmentScope

@FragmentScope
@Subcomponent(modules = [ComponentTypeEditFragmentModule::class])
interface ComponentTypeEditComponent {

    fun inject(fragment: ComponentTypeEditFragment)

}