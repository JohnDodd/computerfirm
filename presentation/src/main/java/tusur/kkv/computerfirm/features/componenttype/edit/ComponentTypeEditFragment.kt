package tusur.kkv.computerfirm.features.componenttype.edit

import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.fragment_component_type_edit.*
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.base.BaseScreenFragment
import tusur.kkv.computerfirm.base.NO_ID
import tusur.kkv.computerfirm.utils.onClick
import tusur.kkv.computerfirm.utils.showSnack
import tusur.kkv.computerfirm.utils.toStringOrEmpty

class ComponentTypeEditFragment : BaseScreenFragment<ComponentTypeEditComponent, ComponentTypeEditPresenter>() {

    override val viewId = R.layout.fragment_component_type_edit

    override fun createComponent() = baseActivity.component
            .createComponentTypeEditComponent(ComponentTypeEditFragmentModule(this, viewPresenter))

    override fun ComponentTypeEditComponent.onInject() = inject(this@ComponentTypeEditFragment)

    override fun bindView(view: View, savedInstanceState: Bundle?) {
        val componentTypeId = arguments?.getLong(KEY_ID) ?: NO_ID
        presenter.onBindView(componentTypeId)
        saveButton.onClick(presenter::onSaveButtonClicked)
    }

    private val viewPresenter = object : ComponentTypeEditPresenter.View {

        override val name: String
            get() = component_type_name_text.text.toStringOrEmpty()
        override val specifications: String
            get() = component_type_specification_text.text.toStringOrEmpty()
        override val manufacturer: String
            get() = component_type_manufacturer_text.text.toStringOrEmpty()

        override fun updateName(name: String) {
            component_type_name_text.setText(name)
        }

        override fun updateManufacturer(manufacturer: String) {
            component_type_manufacturer_text.setText(manufacturer)
        }

        override fun updateSpecifications(specifications: String) {
            component_type_specification_text.setText(specifications)
        }

        override fun showSaveError() {
            view?.showSnack(getString(R.string.simple_error))
        }

        override fun closeView() {
            popBackStack()
        }

        override fun showNameError(message: String) {
            component_type_name_text.error = message
            scroll_view.smoothScrollTo(0, component_type_name_text.bottom)
        }

        override fun showSpecificationsError(message: String) {
            component_type_specification_text.error = message
            scroll_view.smoothScrollTo(0, component_type_specification_text.bottom)
        }

        override fun showManufacturerError(message: String) {
            component_type_manufacturer_text.error = message
            scroll_view.smoothScrollTo(0, component_type_manufacturer_text.bottom)
        }
    }

    companion object {
        const val KEY_ID = "KEY_ID"
    }
}