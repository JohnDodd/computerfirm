package tusur.kkv.computerfirm.features.componenttype.edit

import dagger.Module
import tusur.kkv.computerfirm.injection.module.FragmentModule

@Module
class ComponentTypeEditFragmentModule(fragment: ComponentTypeEditFragment,
                                      view: ComponentTypeEditPresenter.View)
    : FragmentModule<ComponentTypeEditFragment, ComponentTypeEditPresenter.View>(fragment, view) {

}