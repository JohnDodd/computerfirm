package tusur.kkv.computerfirm.features.componenttype.edit

import io.reactivex.subjects.BehaviorSubject
import tusur.kkv.computerfirm.base.BasePresenter
import tusur.kkv.computerfirm.base.BaseView
import tusur.kkv.computerfirm.base.NO_ID
import tusur.kkv.data.exception.IllegalParamsException
import tusur.kkv.domain.interactor.componenttype.choose.RefreshComponentTypeInteractor
import tusur.kkv.domain.interactor.componenttype.choose.RequestComponentTypeInteractor
import tusur.kkv.domain.interactor.componenttype.choose.SendComponentTypeInteractor
import tusur.kkv.domain.repository.IComponentTypeRepository.ParamsExceptionId.Companion.MANUFACTURER
import tusur.kkv.domain.repository.IComponentTypeRepository.ParamsExceptionId.Companion.NAME
import tusur.kkv.domain.repository.IComponentTypeRepository.ParamsExceptionId.Companion.SPECIFICATIONS
import javax.inject.Inject

class ComponentTypeEditPresenter
@Inject constructor(private val request: RequestComponentTypeInteractor,
                    private val send: SendComponentTypeInteractor,
                    private val refresh: RefreshComponentTypeInteractor,
                    private val mapper: EditComponentTypeViewModelMapper,
                    private val view: ComponentTypeEditPresenter.View) : BasePresenter() {

    private var idSubject = BehaviorSubject.createDefault(NO_ID)

    fun onBindView(id: Long) {
        idSubject.onNext(id)
        if (id != NO_ID)
            request.getSingle(id).map(mapper).addSubscription {
                view.updateName(it.name)
                view.updateManufacturer(it.manufacturer)
                view.updateSpecifications(it.specifications)
            }
    }

    fun onSaveButtonClicked() {
        idSubject.firstOrError().addSubscription({ id ->
            if (id == NO_ID) addSendSubscription()
            else addRefreshSubscription(id)
        }) { view.showSaveError() }

    }

    private fun addRefreshSubscription(id: Long) {
        refresh.getRefreshSingle(RefreshComponentTypeInteractor.Params(
                id,
                view.name,
                view.specifications,
                view.manufacturer
        )).addSubscription({ view.closeView() }, showErrorConsumer)
    }

    private fun addSendSubscription() {
        send.getSingle(SendComponentTypeInteractor.Params(
                view.name,
                view.specifications,
                view.manufacturer
        )).addSubscription({
            if (it) view.closeView()
            else view.showSaveError()
        }, showErrorConsumer)
    }

    private val showErrorConsumer: (Throwable) -> Unit = {
        if (it is IllegalParamsException)
            when (it.id) {
                NAME -> view.showNameError(it.message)
                SPECIFICATIONS -> view.showSpecificationsError(it.message)
                MANUFACTURER -> view.showManufacturerError(it.message)
                else -> view.showSaveError()
            }
        else view.showSaveError()
    }

    interface View : BaseView {
        val name: String
        val specifications: String
        val manufacturer: String

        fun updateName(name: String)
        fun updateManufacturer(manufacturer: String)
        fun updateSpecifications(specifications: String)

        fun showSaveError()
        fun closeView()

        fun showNameError(message: String)
        fun showSpecificationsError(message: String)
        fun showManufacturerError(message: String)

    }

}