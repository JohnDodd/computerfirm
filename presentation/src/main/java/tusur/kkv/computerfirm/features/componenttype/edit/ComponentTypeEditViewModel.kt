package tusur.kkv.computerfirm.features.componenttype.edit

data class ComponentTypeEditViewModel(val id: Long,
                                      val name: String,
                                      val specifications: String,
                                      val manufacturer: String)