package tusur.kkv.computerfirm.features.componenttype.edit

import io.reactivex.functions.Function
import tusur.kkv.domain.model.ComponentType
import javax.inject.Inject

class EditComponentTypeViewModelMapper
@Inject constructor() : Function<ComponentType, ComponentTypeEditViewModel> {

    override fun apply(type: ComponentType) = ComponentTypeEditViewModel(
            type.id,
            type.name,
            type.specifications,
            type.manufacturer
    )

}