package tusur.kkv.computerfirm.features.componenttype.home

import dagger.Subcomponent
import tusur.kkv.computerfirm.injection.scope.FragmentScope

@FragmentScope
@Subcomponent(modules = [ComponentTypeHomeFragmentModule::class])
interface ComponentTypeHomeComponent {
    fun inject(fragment: ComponentTypeHomeFragment)
}