package tusur.kkv.computerfirm.features.componenttype.home

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import kotlinx.android.synthetic.main.fragment_component_type_home.*
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.base.BaseScreenFragment
import tusur.kkv.computerfirm.utils.onClick
import javax.inject.Inject
import javax.inject.Provider

class ComponentTypeHomeFragment : BaseScreenFragment<ComponentTypeHomeComponent, ComponentTypeHomePresenter>() {

    override val viewId = R.layout.fragment_component_type_home
    @Inject
    lateinit var adapter: RendererRecyclerViewAdapter
    @Inject
    lateinit var layoutManager: Provider<RecyclerView.LayoutManager>
    @Inject
    lateinit var decoration: Provider<RecyclerView.ItemDecoration>

    override val titleId = R.string.component_types

    override fun ComponentTypeHomeComponent.onInject() = inject(this@ComponentTypeHomeFragment)

    override fun createComponent() = baseActivity.component
            .createComponentTypeHomeComponentType(ComponentTypeHomeFragmentModule(this,
                    viewPresenter))

    override fun bindView(view: View, savedInstanceState: Bundle?) {
        recyclerView.adapter = adapter
        recyclerView.layoutManager = layoutManager.get()
        recyclerView.addItemDecoration(decoration.get())
        fab.onClick(presenter::onFabClicked)
    }

    private val viewPresenter = object : ComponentTypeHomePresenter.View {
        override fun updateItems(items: List<ViewModel>) {
            adapter.setItems(items)
        }
    }
}