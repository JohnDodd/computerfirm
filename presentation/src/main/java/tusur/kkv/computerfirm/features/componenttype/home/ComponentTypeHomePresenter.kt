package tusur.kkv.computerfirm.features.componenttype.home

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import tusur.kkv.computerfirm.base.BasePresenter
import tusur.kkv.computerfirm.base.BaseView
import tusur.kkv.computerfirm.base.UIRouter
import tusur.kkv.computerfirm.features.componenttype.home.model.ComponentTypeHomeViewModel
import tusur.kkv.computerfirm.features.componenttype.home.model.ComponentTypeHomeViewModelMapper
import tusur.kkv.domain.interactor.componenttype.choose.RetrieveComponentTypesInteractor
import tusur.kkv.domain.utils.forEachElement
import javax.inject.Inject

class ComponentTypeHomePresenter
@Inject constructor(private val view: ComponentTypeHomePresenter.View,
                    private val retrieve: RetrieveComponentTypesInteractor,
                    private val mapper: ComponentTypeHomeViewModelMapper,
                    private val uiRouter: UIRouter)
    : BasePresenter() {

    override fun onViewShown() {
        super.onViewShown()
        retrieve.getBehaviorStream()
                .forEachElement { it.map(mapper) }
                .addSubscription { view.updateItems(it) }
    }

    fun onFabClicked() {
        uiRouter.openComponentTypeEditPage()
    }

    fun onModelClicked(model: ComponentTypeHomeViewModel) {
        uiRouter.openComponentTypeEditPage(model.id)
    }

    interface View : BaseView {
        fun updateItems(items: List<ViewModel>)
    }


}