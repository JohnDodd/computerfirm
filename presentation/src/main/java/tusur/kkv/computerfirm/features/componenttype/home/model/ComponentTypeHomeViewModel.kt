package tusur.kkv.computerfirm.features.componenttype.home.model

import tusur.kkv.computerfirm.base.BaseViewModel

class ComponentTypeHomeViewModel(override val id: Long,
                                 val name: String) : BaseViewModel(id)