package tusur.kkv.computerfirm.features.componenttype.home.model

import io.reactivex.functions.Function
import tusur.kkv.domain.model.ComponentType
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ComponentTypeHomeViewModelMapper @Inject constructor()
    : Function<ComponentType, ComponentTypeHomeViewModel> {

    override fun apply(type: ComponentType) = ComponentTypeHomeViewModel(
            type.id,
            type.name
    )

}