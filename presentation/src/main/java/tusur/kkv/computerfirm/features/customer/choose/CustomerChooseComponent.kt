package tusur.kkv.computerfirm.features.customer.choose

import dagger.Subcomponent
import tusur.kkv.computerfirm.injection.scope.FragmentScope

@FragmentScope
@Subcomponent(modules = [CustomerChooseDialogModule::class])
interface CustomerChooseComponent {

    fun inject(fragment: CustomerChooseDialog)
}