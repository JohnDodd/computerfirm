package tusur.kkv.computerfirm.features.customer.choose

import tusur.kkv.computerfirm.features.simpledialog.SimpleDialog

class CustomerChooseDialog : SimpleDialog<CustomerChooseComponent, CustomerChoosePresenter>() {

    override fun CustomerChooseComponent.onInject() = inject(this@CustomerChooseDialog)

    override fun createComponent() =
            baseActivity.component.createCustomerChooseDialogComponent(CustomerChooseDialogModule(
                    this,
                    simpleViewPresenter))

}