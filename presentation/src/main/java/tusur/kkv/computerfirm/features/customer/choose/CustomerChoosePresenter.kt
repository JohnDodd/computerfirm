package tusur.kkv.computerfirm.features.customer.choose

import tusur.kkv.computerfirm.base.UIRouter
import tusur.kkv.computerfirm.features.simpledialog.AppendSimpleAddViewModel
import tusur.kkv.computerfirm.features.simpledialog.AppendSimpleClearViewModel
import tusur.kkv.computerfirm.features.simpledialog.SimpleDialogPresenter
import tusur.kkv.domain.interactor.customer.RetrieveCustomersInteractor
import tusur.kkv.domain.model.Customer
import tusur.kkv.domain.utils.forEachElement
import javax.inject.Inject

class CustomerChoosePresenter
@Inject constructor(private val mapper: CustomerSimpleViewModelMapper,
                    private val retrieve: RetrieveCustomersInteractor,
                    private val resultListener: OnCustomerResult,
                    private val appendClearMapper: AppendSimpleClearViewModel,
                    view: SimpleDialogPresenter.SimpleDialogView,
                    appendMapper: AppendSimpleAddViewModel,
                    uiRouter: UIRouter) :
        SimpleDialogPresenter(view, appendMapper, uiRouter) {

    override fun onViewShown() {
        super.onViewShown()
        retrieve.getBehaviorStream()
                .startWith(listOf<Customer>())
                .forEachElement { it.map(mapper) }
                .map(appendClearMapper)
                .map(appendMapper)
                .addSubscription(view::updateItems)
    }

    fun onModelClicked(model: Customer) {
        resultListener.onResult(model)
        view.close()
    }

    fun onClearClicked() {
        resultListener.onResult(null)
        view.close()
    }

    fun onAddClicked() {
        uiRouter.openCustomerEditPage()
        view.close()
    }

    fun onLongClicked(model: Customer) {
        uiRouter.openCustomerEditPage(model.id)
        view.close()
    }

}