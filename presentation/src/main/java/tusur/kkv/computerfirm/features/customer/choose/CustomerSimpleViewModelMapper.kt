package tusur.kkv.computerfirm.features.customer.choose

import io.reactivex.functions.Function
import tusur.kkv.computerfirm.features.simpledialog.SimpleViewModel
import tusur.kkv.domain.model.Customer
import javax.inject.Inject

class CustomerSimpleViewModelMapper
@Inject constructor() : Function<Customer, SimpleViewModel<Customer>> {
    override fun apply(customer: Customer) =
            SimpleViewModel(
                    customer.id,
                    customer.fullName,
                    customer
            )
}