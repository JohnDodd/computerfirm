package tusur.kkv.computerfirm.features.customer.choose

import tusur.kkv.domain.model.Customer

interface OnCustomerResult {
    fun onResult(customer: Customer?)
}