package tusur.kkv.computerfirm.features.customer.edit

import dagger.Subcomponent
import tusur.kkv.computerfirm.injection.scope.FragmentScope

@FragmentScope
@Subcomponent(modules = [CustomerEditFragmentModule::class])
interface CustomerEditComponent {

    fun inject(fragment: CustomerEditFragment)
}