package tusur.kkv.computerfirm.features.customer.edit

import android.os.Bundle
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.fragment_customer_edit.*
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.base.BaseScreenFragment
import tusur.kkv.computerfirm.base.NO_ID
import tusur.kkv.computerfirm.utils.onClick
import tusur.kkv.computerfirm.utils.showSnack
import tusur.kkv.computerfirm.utils.toStringOrEmpty

class CustomerEditFragment : BaseScreenFragment<CustomerEditComponent, CustomerEditPresenter>() {

    override val viewId = R.layout.fragment_customer_edit

    override fun CustomerEditComponent.onInject() = inject(this@CustomerEditFragment)

    override fun createComponent() = baseActivity.component
            .createEditCustomerComponent(CustomerEditFragmentModule(this, viewPresenter))

    override fun bindView(view: View, savedInstanceState: Bundle?) {
        val customerId = arguments?.getLong(KEY_ID) ?: NO_ID
        presenter.onBindView(customerId)
        saveButton.onClick(presenter::onSaveButtonClicked)
    }

    private val viewPresenter = object : CustomerEditPresenter.View {
        override val name: String
            get() = customer_name_text.text.toStringOrEmpty()
        override val email: String?
            get() = email_text.text?.let { if (it.isBlank()) null else it.toString() }
        override val phone: String
            get() = phone_text.text.toStringOrEmpty()

        override fun updateName(name: String) {
            customer_name_text.setText(name)
        }

        override fun updateEmail(email: String) {
            email_text.setText(email)
        }

        override fun updatePhone(phone: String) {
            phone_text.setText(phone)
        }

        override fun showNameError(message: String) {
            customer_name_text.showError(message)
        }

        override fun showEmailError(message: String) {
            email_text.showError(message)
        }

        override fun showPhoneError(message: String) {
            phone_text.showError(message)
        }

        override fun showSaveError() {
            view?.showSnack(getString(R.string.simple_error))
        }

        override fun closeView() {
            popBackStack()
        }

        private fun TextView.showError(message: String) {
            error = message
            scroll_view.scrollTo(0, this.bottom)
        }

    }

    companion object {
        const val KEY_ID = "KEY_ID"
    }
}
