package tusur.kkv.computerfirm.features.customer.edit

import dagger.Module
import tusur.kkv.computerfirm.injection.module.FragmentModule

@Module
class CustomerEditFragmentModule(fragment: CustomerEditFragment, view: CustomerEditPresenter.View)
    : FragmentModule<CustomerEditFragment, CustomerEditPresenter.View>(fragment, view)