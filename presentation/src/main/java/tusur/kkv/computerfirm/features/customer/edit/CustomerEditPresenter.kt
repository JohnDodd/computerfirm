package tusur.kkv.computerfirm.features.customer.edit

import io.reactivex.subjects.BehaviorSubject
import tusur.kkv.computerfirm.base.BasePresenter
import tusur.kkv.computerfirm.base.BaseView
import tusur.kkv.computerfirm.base.NO_ID
import tusur.kkv.data.exception.IllegalParamsException
import tusur.kkv.domain.interactor.customer.RefreshCustomerInteractor
import tusur.kkv.domain.interactor.customer.RequestCustomerInteractor
import tusur.kkv.domain.interactor.customer.SendCustomerInteractor
import tusur.kkv.domain.repository.ICustomerRepository.ParamsExceptionId.Companion.EMAIL
import tusur.kkv.domain.repository.ICustomerRepository.ParamsExceptionId.Companion.NAME
import tusur.kkv.domain.repository.ICustomerRepository.ParamsExceptionId.Companion.PHONE
import javax.inject.Inject

class CustomerEditPresenter
@Inject constructor(private val request: RequestCustomerInteractor,
                    private val send: SendCustomerInteractor,
                    private val refresh: RefreshCustomerInteractor,
                    private val mapper: CustomerEditViewModelMapper,
                    private val view: CustomerEditPresenter.View) : BasePresenter() {

    private var idSubject = BehaviorSubject.createDefault(NO_ID)

    fun onBindView(id: Long) {
        idSubject.onNext(id)
        if (id != NO_ID)
            request.getSingle(id).map(mapper).addSubscription {
                view.updateName(it.fullName)
                view.updateEmail(it.email)
                view.updatePhone(it.phoneNumber)
            }
    }

    fun onSaveButtonClicked() {
        idSubject.firstOrError().addSubscription({ id ->
            if (id == NO_ID) addSendSubscription()
            else addRefreshSubscription(id)
        }) { view.showSaveError() }
    }

    private fun addSendSubscription() {
        send.getSingle(SendCustomerInteractor.Params(
                view.name,
                view.email,
                view.phone
        )).addSubscription({
            if (it) view.closeView()
            else view.showSaveError()
        }, showErrorConsumer)
    }

    private fun addRefreshSubscription(id: Long) {
        refresh.getRefreshSingle(RefreshCustomerInteractor.Params(
                id,
                view.name,
                view.email,
                view.phone
        )).addSubscription({ view.closeView() }, showErrorConsumer)
    }

    private val showErrorConsumer: (Throwable) -> Unit = {
        if (it is IllegalParamsException)
            when (it.id) {
                NAME -> view.showNameError(it.message)
                EMAIL -> view.showEmailError(it.message)
                PHONE -> view.showPhoneError(it.message)
                else -> view.showSaveError()
            }
        else view.showSaveError()
    }


    interface View : BaseView {
        val name: String
        val email: String?
        val phone: String

        fun updateName(name: String)
        fun updateEmail(email: String)
        fun updatePhone(phone: String)

        fun showNameError(message: String)
        fun showEmailError(message: String)
        fun showPhoneError(message: String)

        fun showSaveError()
        fun closeView()
    }

}