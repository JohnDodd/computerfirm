package tusur.kkv.computerfirm.features.customer.edit

data class CustomerEditViewModel(val id: Long,
                                 val fullName: String,
                                 val email: String,
                                 val phoneNumber: String)