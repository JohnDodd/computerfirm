package tusur.kkv.computerfirm.features.customer.edit

import io.reactivex.functions.Function
import tusur.kkv.computerfirm.utils.toStringOrEmpty
import tusur.kkv.domain.model.Customer
import javax.inject.Inject

class CustomerEditViewModelMapper
@Inject constructor() : Function<Customer, CustomerEditViewModel> {
    override fun apply(customer: Customer) = CustomerEditViewModel(
            customer.id,
            customer.fullName,
            customer.email.toStringOrEmpty(),
            customer.phoneNumber
    )
}
