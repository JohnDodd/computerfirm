package tusur.kkv.computerfirm.features.customer.home

import dagger.Subcomponent
import tusur.kkv.computerfirm.injection.scope.FragmentScope

@FragmentScope
@Subcomponent(modules = [CustomerHomeFragmentModule::class])
interface CustomerHomeComponent {

    fun inject(fragment: CustomerHomeFragment)

}