package tusur.kkv.computerfirm.features.customer.home

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import dagger.Module
import dagger.Provides
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.base.BaseDiffCallback
import tusur.kkv.computerfirm.base.BetweenItemsDecoration
import tusur.kkv.computerfirm.features.customer.home.model.CustomerHomeViewModel
import tusur.kkv.computerfirm.injection.module.FragmentModule
import tusur.kkv.computerfirm.utils.onClick
import tusur.kkv.computerfirm.utils.register
import tusur.kkv.data.injection.qualifier.ForActivity

@Module
class CustomerHomeFragmentModule(fragment: CustomerHomeFragment,
                                 view: CustomerHomePresenter.View)
    : FragmentModule<CustomerHomeFragment, CustomerHomePresenter.View>(fragment, view) {

    @Provides
    fun provideAdapter(diffCallback: BaseDiffCallback) = RendererRecyclerViewAdapter().apply {
        register<CustomerHomeViewModel>(R.layout.item_customer) { model ->
            setText(R.id.customer_name_layout, model.name)
            setText(R.id.customer_email, model.email)
            onClick { fragment.presenter.onModelClicked(model) }
        }
        setDiffCallback(diffCallback)
    }

    @Provides
    fun provideLayoutManager(@ForActivity context: Context): RecyclerView.LayoutManager =
            LinearLayoutManager(context)

    @Provides
    fun provideDecoration(): RecyclerView.ItemDecoration = BetweenItemsDecoration()


}