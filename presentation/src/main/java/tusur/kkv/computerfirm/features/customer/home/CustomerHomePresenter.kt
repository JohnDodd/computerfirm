package tusur.kkv.computerfirm.features.customer.home

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import tusur.kkv.computerfirm.base.BasePresenter
import tusur.kkv.computerfirm.base.BaseView
import tusur.kkv.computerfirm.base.UIRouter
import tusur.kkv.computerfirm.features.customer.home.model.CustomerHomeViewModel
import tusur.kkv.computerfirm.features.customer.home.model.CustomerHomeViewModelMapper
import tusur.kkv.domain.interactor.customer.RetrieveCustomersInteractor
import tusur.kkv.domain.utils.forEachElement
import javax.inject.Inject

class CustomerHomePresenter
@Inject constructor(private val view: View,
                    private val retrieve: RetrieveCustomersInteractor,
                    private val mapper: CustomerHomeViewModelMapper,
                    private val uiRouter: UIRouter)
    : BasePresenter() {

    override fun onViewShown() {
        super.onViewShown()
        retrieve.getBehaviorStream()
                .forEachElement { it.map(mapper) }
                .addSubscription { view.updateItems(it) }
    }

    fun onFabClicked() {
        uiRouter.openCustomerEditPage()
    }

    fun onModelClicked(model: CustomerHomeViewModel) {
        uiRouter.openCustomerEditPage(model.id)
    }

    interface View : BaseView {
        fun updateItems(items: List<ViewModel>)
    }
}