package tusur.kkv.computerfirm.features.customer.home.model

import tusur.kkv.computerfirm.base.BaseViewModel

data class CustomerHomeViewModel(override val id: Long,
                                 val name: String,
                                 val email: String)
    : BaseViewModel(id)