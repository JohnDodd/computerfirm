package tusur.kkv.computerfirm.features.customer.home.model

import io.reactivex.functions.Function
import tusur.kkv.domain.model.Customer
import javax.inject.Inject

class CustomerHomeViewModelMapper @Inject constructor()
    : Function<Customer, CustomerHomeViewModel> {
    override fun apply(customer: Customer) = CustomerHomeViewModel(
            customer.id,
            customer.fullName,
            customer.email ?: customer.phoneNumber
    )
}