package tusur.kkv.computerfirm.features.employee.choose

import dagger.Subcomponent
import tusur.kkv.computerfirm.injection.scope.FragmentScope


@FragmentScope
@Subcomponent(modules = [EmployeeChooseDialogModule::class])
interface EmployeeChooseComponent {
    fun inject(fragment: EmployeeChooseDialog)
}