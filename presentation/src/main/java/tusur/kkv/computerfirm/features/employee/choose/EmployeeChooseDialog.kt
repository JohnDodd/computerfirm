package tusur.kkv.computerfirm.features.employee.choose

import tusur.kkv.computerfirm.features.simpledialog.SimpleDialog

class EmployeeChooseDialog : SimpleDialog<EmployeeChooseComponent, EmployeeChoosePresenter>() {

    override fun EmployeeChooseComponent.onInject() = inject(this@EmployeeChooseDialog)

    override fun createComponent() =
            baseActivity.component.createEmployeeChooseDialogComponent(EmployeeChooseDialogModule(
                    this,
                    simpleViewPresenter))

}