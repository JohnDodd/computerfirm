package tusur.kkv.computerfirm.features.employee.choose

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import dagger.Module
import dagger.Provides
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.base.BaseDiffCallback
import tusur.kkv.computerfirm.features.simpledialog.SimpleAddModel
import tusur.kkv.computerfirm.features.simpledialog.SimpleClearModel
import tusur.kkv.computerfirm.features.simpledialog.SimpleDialogPresenter
import tusur.kkv.computerfirm.features.simpledialog.SimpleViewModel
import tusur.kkv.computerfirm.injection.module.DialogModule
import tusur.kkv.computerfirm.utils.onClick
import tusur.kkv.computerfirm.utils.onLongClick
import tusur.kkv.computerfirm.utils.register
import tusur.kkv.data.injection.qualifier.ForActivity
import tusur.kkv.domain.model.Employee

@Module
class EmployeeChooseDialogModule(fragment: EmployeeChooseDialog,
                                 view: SimpleDialogPresenter.SimpleDialogView)
    : DialogModule<EmployeeChooseDialog, SimpleDialogPresenter.SimpleDialogView>(fragment, view) {

    @Provides
    fun provideOnResultListener(): OnEmployeeResult =
            fragment.targetFragment as OnEmployeeResult

    @Provides
    fun provideAdapter(diffCallback: BaseDiffCallback) = RendererRecyclerViewAdapter().apply {
        register<SimpleViewModel<Employee>>(R.layout.item_simple) { model ->
            setText(R.id.simple_text, model.text)
            onClick { fragment.presenter.onModelClicked(model.model) }
            onLongClick { fragment.presenter.onLongClicked(model.model) }
        }
        register<SimpleAddModel>(R.layout.item_simple_add) {
            onClick { fragment.presenter.onAddClicked() }
        }
        register<SimpleClearModel>(R.layout.item_simple_clear) {
            onClick { fragment.presenter.onClearClicked() }
        }
        setDiffCallback(diffCallback)
    }

    @Provides
    fun provideLayoutManager(@ForActivity context: Context): RecyclerView.LayoutManager =
            LinearLayoutManager(context)


}