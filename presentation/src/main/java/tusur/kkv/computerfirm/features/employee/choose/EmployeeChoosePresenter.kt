package tusur.kkv.computerfirm.features.employee.choose

import tusur.kkv.computerfirm.base.UIRouter
import tusur.kkv.computerfirm.features.simpledialog.AppendSimpleAddViewModel
import tusur.kkv.computerfirm.features.simpledialog.AppendSimpleClearViewModel
import tusur.kkv.computerfirm.features.simpledialog.SimpleDialogPresenter
import tusur.kkv.domain.interactor.employee.RetrieveEmployeesInteractor
import tusur.kkv.domain.model.Employee
import tusur.kkv.domain.utils.forEachElement
import javax.inject.Inject

class EmployeeChoosePresenter
@Inject constructor(private val mapper: EmployeeSimpleViewModelMapper,
                    private val retrieve: RetrieveEmployeesInteractor,
                    private val resultListener: OnEmployeeResult,
                    private val appendClearMapper: AppendSimpleClearViewModel,
                    view: SimpleDialogPresenter.SimpleDialogView,
                    appendMapper: AppendSimpleAddViewModel,
                    uiRouter: UIRouter) :
        SimpleDialogPresenter(view, appendMapper, uiRouter) {

    override fun onViewShown() {
        super.onViewShown()
        retrieve.getBehaviorStream()
                .startWith(listOf<Employee>())
                .forEachElement { it.map(mapper) }
                .map(appendClearMapper)
                .map(appendMapper)
                .addSubscription(view::updateItems)
    }

    fun onModelClicked(model: Employee) {
        resultListener.onResult(model)
        view.close()
    }

    fun onAddClicked() {
        uiRouter.openEmployeeEditPage()
        view.close()
    }

    fun onClearClicked() {
        resultListener.onResult(null)
        view.close()
    }

    fun onLongClicked(model: Employee) {
        uiRouter.openEmployeeEditPage(model.id)
        view.close()
    }

}