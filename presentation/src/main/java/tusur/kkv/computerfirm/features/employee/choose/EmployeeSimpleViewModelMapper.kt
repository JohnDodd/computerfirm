package tusur.kkv.computerfirm.features.employee.choose

import io.reactivex.functions.Function
import tusur.kkv.computerfirm.features.simpledialog.SimpleViewModel
import tusur.kkv.domain.model.Employee
import javax.inject.Inject

class EmployeeSimpleViewModelMapper
@Inject constructor() : Function<Employee, SimpleViewModel<Employee>> {
    override fun apply(employee: Employee) =
            SimpleViewModel(
                    employee.id,
                    employee.fullName,
                    employee
            )
}