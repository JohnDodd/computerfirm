package tusur.kkv.computerfirm.features.employee.choose

import tusur.kkv.domain.model.Employee

interface OnEmployeeResult {
    fun onResult(employee: Employee?)
}