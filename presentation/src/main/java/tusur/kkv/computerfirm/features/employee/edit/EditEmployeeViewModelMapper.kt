package tusur.kkv.computerfirm.features.employee.edit

import io.reactivex.functions.Function
import tusur.kkv.domain.model.Employee
import javax.inject.Inject

class EditEmployeeViewModelMapper
@Inject constructor() : Function<Employee, EmployeeEditViewModel> {
    override fun apply(employee: Employee) = EmployeeEditViewModel(
            employee.fullName,
            employee.birthDate,
            employee.gender,
            employee.address,
            employee.phoneNumber,
            employee.passportData,
            employee.position
    )
}