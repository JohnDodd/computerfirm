package tusur.kkv.computerfirm.features.employee.edit

import dagger.Subcomponent
import tusur.kkv.computerfirm.injection.scope.FragmentScope

@FragmentScope
@Subcomponent(modules = [EmployeeEditFragmentModule::class])
interface EmployeeEditComponent {
    fun inject(fragment: EmployeeEditFragment)
}