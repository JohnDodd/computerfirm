package tusur.kkv.computerfirm.features.employee.edit


import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.fragment_employee_edit.*
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.base.BaseScreenFragment
import tusur.kkv.computerfirm.base.NO_ID
import tusur.kkv.computerfirm.base.UIRouter
import tusur.kkv.computerfirm.features.gender.choose.GenderChoosePresenter
import tusur.kkv.computerfirm.features.position.choose.OnPositionResult
import tusur.kkv.computerfirm.utils.onClick
import tusur.kkv.computerfirm.utils.onImeActionDone
import tusur.kkv.computerfirm.utils.showSnack
import tusur.kkv.computerfirm.utils.toStringOrEmpty
import tusur.kkv.domain.model.Gender
import tusur.kkv.domain.model.Position
import javax.inject.Inject

class EmployeeEditFragment : BaseScreenFragment<EmployeeEditComponent, EmployeeEditPresenter>(),
        GenderChoosePresenter.OnResultListener,
        OnPositionResult {

    override val viewId = R.layout.fragment_employee_edit

    override fun EmployeeEditComponent.onInject() = inject(this@EmployeeEditFragment)

    @Inject
    lateinit var uiRouter: UIRouter

    override fun createComponent() = baseActivity.component
            .createEditEmployeeFragmentComponent(EmployeeEditFragmentModule(this, viewPresenter))

    override fun bindView(view: View, savedInstanceState: Bundle?) {
        val employeeId = arguments?.getLong(KEY_ID) ?: NO_ID
        presenter.onBindView(employeeId)
        saveButton.onClick(presenter::onSaveButtonClicked)
        simple_text.onClick(presenter::onGenderClicked)
        birthdate_text.onClick(presenter::onDateClicked)
        position_text.onClick(presenter::onPositionClicked)
        position_text.onImeActionDone(presenter::onSaveButtonClicked)
    }

    override fun onResult(gender: Gender) {
        presenter.onGenderChanged(gender)
    }

    override fun onResult(model: Position) {
        presenter.onPositionChanged(model)
    }

    private val viewPresenter = object : EmployeeEditPresenter.View {
        override val name: String
            get() = employee_name_text.text.toStringOrEmpty()

        override val address: String
            get() = address_text.text.toStringOrEmpty()
        override val phoneNumber: String
            get() = phone_text.text.toStringOrEmpty()
        override val passportData: String
            get() = passport_text.text.toStringOrEmpty()

        override fun updateName(name: String) {
            employee_name_text.setText(name)
        }

        override fun updateGender(genderName: String) {
            simple_text.text = genderName
        }

        override fun updateAddress(address: String) {
            address_text.setText(address)
        }

        override fun updatePhone(phone: String) {
            phone_text.setText(phone)
        }

        override fun updatePassport(passport: String) {
            passport_text.setText(passport)
        }

        override fun updatePosition(position: String) {
            position_text.text = position
        }

        override fun updateDate(text: String) {
            birthdate_text.text = text
        }

        override fun closeView() {
            popBackStack()
        }

        override fun showSaveError() {
            view?.showSnack(getString(R.string.simple_error))
        }

        override fun showNameError(message: String) {
            employee_name_text.error = message
            scroll_view.smoothScrollTo(0, employee_name_text.top)
        }

        override fun showAddressError(message: String) {
            address_text.error = message
            scroll_view.smoothScrollTo(0, address_text.top)
        }

        override fun showPhoneError(message: String) {
            phone_text.error = message
            scroll_view.smoothScrollTo(0, phone_text.top)
        }

        override fun showPassportError(message: String) {
            passport_text.error = message
            scroll_view.smoothScrollTo(0, passport_text.top)
        }

        override fun showBirthDateError(message: String) {
            birthdate_text.error = message
            scroll_view.smoothScrollTo(0, birthdate_text.top)
        }

        override fun showPositionError(message: String) {
            position_text.error = message
            scroll_view.smoothScrollTo(0, position_text.top)
        }

        override fun showGenderError(message: String) {
            simple_text.error = message
            scroll_view.smoothScrollTo(0, simple_text.top)
        }

        override fun openGenderPage() {
            uiRouter.openChooseGenderPage(this@EmployeeEditFragment)
        }

        override fun openPositionPage() {
            uiRouter.openPositionChoosePage(this@EmployeeEditFragment)
        }

    }

    companion object {
        const val KEY_ID = "KEY_ID"
    }
}
