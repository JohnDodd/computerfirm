package tusur.kkv.computerfirm.features.employee.edit

import dagger.Module
import tusur.kkv.computerfirm.injection.module.FragmentModule

@Module
class EmployeeEditFragmentModule(fragment: EmployeeEditFragment,
                                 view: EmployeeEditPresenter.View)
    : FragmentModule<EmployeeEditFragment, EmployeeEditPresenter.View>(fragment, view)