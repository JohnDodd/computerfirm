package tusur.kkv.computerfirm.features.employee.edit

import com.annimon.stream.Optional
import com.annimon.stream.Optional.empty
import com.annimon.stream.Optional.of
import io.reactivex.rxkotlin.Observables
import io.reactivex.subjects.BehaviorSubject.createDefault
import tusur.kkv.computerfirm.base.BasePresenter
import tusur.kkv.computerfirm.base.BaseView
import tusur.kkv.computerfirm.base.NO_ID
import tusur.kkv.computerfirm.base.UIRouter
import tusur.kkv.computerfirm.base.tuple.Tuple4
import tusur.kkv.computerfirm.mapper.GenderMapper
import tusur.kkv.data.database.base.DateFormatter
import tusur.kkv.data.exception.IllegalParamsException
import tusur.kkv.domain.interactor.employee.RefreshEmployeeInteractor
import tusur.kkv.domain.interactor.employee.RequestEmployeeInteractor
import tusur.kkv.domain.interactor.employee.SendEmployeeInteractor
import tusur.kkv.domain.model.Gender
import tusur.kkv.domain.model.Position
import tusur.kkv.domain.repository.IEmployeeRepository.ParamsExceptionId.Companion.ADDRESS
import tusur.kkv.domain.repository.IEmployeeRepository.ParamsExceptionId.Companion.BIRTHDATE
import tusur.kkv.domain.repository.IEmployeeRepository.ParamsExceptionId.Companion.GENDER
import tusur.kkv.domain.repository.IEmployeeRepository.ParamsExceptionId.Companion.NAME
import tusur.kkv.domain.repository.IEmployeeRepository.ParamsExceptionId.Companion.PASSPORT_DATA
import tusur.kkv.domain.repository.IEmployeeRepository.ParamsExceptionId.Companion.PHONE_NUMBER
import tusur.kkv.domain.repository.IEmployeeRepository.ParamsExceptionId.Companion.POSITION
import tusur.kkv.domain.utils.unwrap
import javax.inject.Inject

class EmployeeEditPresenter
@Inject constructor(private val request: RequestEmployeeInteractor,
                    private val send: SendEmployeeInteractor,
                    private val refresh: RefreshEmployeeInteractor,
                    private val mapper: EditEmployeeViewModelMapper,
                    private val genderMapper: GenderMapper,
                    private val dateFormatter: DateFormatter,
                    private val uiRouter: UIRouter,
                    private val view: View)
    : BasePresenter() {

    private var genderSubject = createDefault(empty<Gender>())
    private var birthDateSubject = createDefault("")
    private var positionSubject = createDefault(empty<Position>())
    private var idSubject = createDefault(-1L)

    fun onBindView(id: Long) {
        idSubject.onNext(id)
        if (id != NO_ID)
            request.getSingle(id).map(mapper).addSubscription {
                genderSubject.onNext(of(it.gender))
                positionSubject.onNext(Optional.of(it.position))
                birthDateSubject.onNext(it.birthDate)
                view.updateName(it.name)
                view.updateAddress(it.address)
                view.updatePhone(it.phone)
                view.updatePassport(it.passport)
            }
    }

    override fun onViewShown() {
        super.onViewShown()
        genderSubject.unwrap().map(genderMapper).addSubscription(view::updateGender)
        birthDateSubject.addSubscription(view::updateDate)
        positionSubject.unwrap().map { it.name }.addSubscription(view::updatePosition)
    }

    fun onGenderClicked() {
        view.openGenderPage()
    }

    fun onDateClicked() {
        uiRouter.openDatePage {
            val formatted = dateFormatter.formatToViewDate(it)
            birthDateSubject.onNext(formatted)
            view.updateDate(formatted)
        }
    }

    fun onPositionClicked() {
        view.openPositionPage()
    }

    fun onSaveButtonClicked() {
        Observables.combineLatest(
                idSubject,
                birthDateSubject,
                genderSubject,
                positionSubject.map { it.map { o -> o.id }.orElse(NO_ID) },
                ::Tuple4
        ).firstOrError().addSubscription({ tuple ->
            val employeeId = tuple.first
            val birthDate = tuple.second
            val gender: Gender? = tuple.third.orElse(null)
            val positionId = tuple.fourth
            if (employeeId == NO_ID) {
                addSendSubscription(birthDate, gender, positionId)
            } else {
                addRefreshSubscription(employeeId, birthDate, gender, positionId)
            }
        }) { view.showSaveError() }
    }

    private fun addSendSubscription(birthDate: String,
                                    gender: Gender?,
                                    positionId: Long) {
        send.getSingle(SendEmployeeInteractor.Params(
                view.name,
                birthDate,
                gender,
                view.address,
                view.phoneNumber,
                view.passportData,
                positionId
        ))
                .addSubscription({
                    if (it) view.closeView()
                    else view.showSaveError()
                },
                        showErrorConsumer)
    }

    private fun addRefreshSubscription(id: Long,
                                       birthDate: String,
                                       gender: Gender?,
                                       positionId: Long) {
        refresh.getRefreshSingle(RefreshEmployeeInteractor.Params(
                id,
                view.name,
                birthDate,
                gender,
                view.address,
                view.phoneNumber,
                view.passportData,
                positionId
        )).addSubscription({ view.closeView() }, showErrorConsumer)
    }

    fun onGenderChanged(gender: Gender) {
        genderSubject.onNext(of(gender))
    }

    fun onPositionChanged(position: Position) {
        positionSubject.onNext(Optional.of(position))
    }

    private val showErrorConsumer: (Throwable) -> Unit = {
        if (it is IllegalParamsException)
            when (it.id) {
                GENDER -> view.showGenderError(it.message)
                NAME -> view.showNameError(it.message)
                BIRTHDATE -> view.showBirthDateError(it.message)
                ADDRESS -> view.showAddressError(it.message)
                PHONE_NUMBER -> view.showPhoneError(it.message)
                PASSPORT_DATA -> view.showPassportError(it.message)
                POSITION -> view.showPositionError(it.message)
                else -> view.showSaveError()
            }
        else view.showSaveError()
    }

    interface View : BaseView {
        val name: String
        val address: String
        val phoneNumber: String
        val passportData: String

        fun updateName(name: String)
        fun updateGender(genderName: String)
        fun updateAddress(address: String)
        fun updatePhone(phone: String)
        fun updatePassport(passport: String)
        fun updatePosition(position: String)
        fun updateDate(text: String)
        fun closeView()
        fun showSaveError()

        fun showNameError(message: String)
        fun showAddressError(message: String)
        fun showPhoneError(message: String)
        fun showPassportError(message: String)
        fun showBirthDateError(message: String)
        fun showPositionError(message: String)
        fun showGenderError(message: String)

        fun openGenderPage()
        fun openPositionPage()
    }
}