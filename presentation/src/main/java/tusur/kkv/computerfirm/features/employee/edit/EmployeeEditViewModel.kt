package tusur.kkv.computerfirm.features.employee.edit

import tusur.kkv.domain.model.Gender
import tusur.kkv.domain.model.Position

data class EmployeeEditViewModel(val name: String,
                                 val birthDate: String,
                                 val gender: Gender,
                                 val address: String,
                                 val phone: String,
                                 val passport: String,
                                 val position: Position)