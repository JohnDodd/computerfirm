package tusur.kkv.computerfirm.features.employee.home

import dagger.Subcomponent
import tusur.kkv.computerfirm.injection.scope.FragmentScope

@FragmentScope
@Subcomponent(modules = [EmployeeHomeFragmentModule::class])
interface EmployeeHomeComponent {

    fun inject(fragment: EmployeeHomeFragment)

}