package tusur.kkv.computerfirm.features.employee.home

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import dagger.Module
import dagger.Provides
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.base.BaseDiffCallback
import tusur.kkv.computerfirm.base.BetweenItemsDecoration
import tusur.kkv.computerfirm.features.employee.home.model.EmployeeBestHomeViewModel
import tusur.kkv.computerfirm.features.employee.home.model.EmployeeHomeViewModel
import tusur.kkv.computerfirm.injection.module.FragmentModule
import tusur.kkv.computerfirm.utils.onClick
import tusur.kkv.computerfirm.utils.register
import tusur.kkv.data.injection.qualifier.ForActivity

@Module
class EmployeeHomeFragmentModule(fragment: EmployeeHomeFragment,
                                 view: EmployeeHomePresenter.View)
    : FragmentModule<EmployeeHomeFragment, EmployeeHomePresenter.View>(fragment, view) {

    @Provides
    fun provideAdapter(diffCallback: BaseDiffCallback) = RendererRecyclerViewAdapter().apply {
        register<EmployeeHomeViewModel>(R.layout.item_employee) { model ->
            setText(R.id.employee_name_layout, model.name)
            setText(R.id.employee_short_info, model.shortInfo)
            onClick { fragment.presenter.onModelClicked(model) }
        }
        register<EmployeeBestHomeViewModel>(R.layout.item_best_employee) { model ->
            setText(R.id.best_employee_name, model.name)
            setText(R.id.best_employee_revenue, model.revenue)
            setText(R.id.best_employee_components, model.componentsCount)
            setText(R.id.best_employee_services, model.servicesCount)
            onClick { fragment.presenter.onModelClicked(model) }
        }
        setDiffCallback(diffCallback)
    }

    @Provides
    fun provideLayoutManager(@ForActivity context: Context): RecyclerView.LayoutManager =
            LinearLayoutManager(context)

    @Provides
    fun provideDecoration(): RecyclerView.ItemDecoration = BetweenItemsDecoration()


}