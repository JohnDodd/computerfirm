package tusur.kkv.computerfirm.features.employee.home

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import tusur.kkv.computerfirm.base.BasePresenter
import tusur.kkv.computerfirm.base.BaseView
import tusur.kkv.computerfirm.base.MenuId
import tusur.kkv.computerfirm.base.MenuId.BEST
import tusur.kkv.computerfirm.base.UIRouter
import tusur.kkv.computerfirm.controller.OptionsMenuController
import tusur.kkv.computerfirm.features.employee.home.model.EmployeeBestHomeViewModel
import tusur.kkv.computerfirm.features.employee.home.model.EmployeeBestHomeViewModelMapper
import tusur.kkv.computerfirm.features.employee.home.model.EmployeeHomeViewModel
import tusur.kkv.computerfirm.features.employee.home.model.EmployeeHomeViewModelMapper
import tusur.kkv.domain.interactor.employee.RequestBestEmployeesInteractor
import tusur.kkv.domain.interactor.employee.RetrieveEmployeesInteractor
import tusur.kkv.domain.utils.forEachElement
import javax.inject.Inject

class EmployeeHomePresenter
@Inject constructor(private val view: View,
                    private val retrieve: RetrieveEmployeesInteractor,
                    private val requestBest: RequestBestEmployeesInteractor,
                    private val mapper: EmployeeHomeViewModelMapper,
                    private val bestMapper: EmployeeBestHomeViewModelMapper,
                    private val optionsMenuController: OptionsMenuController,
                    private val uiRouter: UIRouter)
    : BasePresenter() {

    override fun onViewShown() {
        super.onViewShown()
        retrieve.getBehaviorStream()
                .forEachElement { it.map(mapper) }
                .addSubscription { view.updateItems(it) }
        optionsMenuController.onMenuItemSelectedStream.addSubscription(::onMenuItemSelected)
    }

    private fun onMenuItemSelected(menuId: MenuId) {
        when (menuId) {
            BEST -> requestBest.getSingle(5)
                    .forEachElement { it.map(bestMapper) }
                    .addSubscription { view.updateItems(it) }
        }
    }

    fun onFabClicked() {
        uiRouter.openEmployeeEditPage()
    }

    fun onModelClicked(model: EmployeeHomeViewModel) {
        uiRouter.openEmployeeEditPage(model.id)
    }

    fun onModelClicked(model: EmployeeBestHomeViewModel) {
        uiRouter.openEmployeeEditPage(model.id)
    }

    override fun onBindOptionsMenu() {
        super.onBindOptionsMenu()
        optionsMenuController.setMenuItems(BEST)
    }

    interface View : BaseView {
        fun updateItems(items: List<ViewModel>)
    }
}