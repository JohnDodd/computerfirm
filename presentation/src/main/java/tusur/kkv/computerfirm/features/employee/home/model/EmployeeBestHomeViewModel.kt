package tusur.kkv.computerfirm.features.employee.home.model

import tusur.kkv.computerfirm.base.BaseViewModel

class EmployeeBestHomeViewModel(override val id: Long,
                                val name: String,
                                val revenue: String,
                                val componentsCount: String,
                                val servicesCount: String) : BaseViewModel(id)