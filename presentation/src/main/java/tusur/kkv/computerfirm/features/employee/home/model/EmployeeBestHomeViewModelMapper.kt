package tusur.kkv.computerfirm.features.employee.home.model

import android.content.Context
import io.reactivex.functions.Function
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.mapper.MoneyMapper
import tusur.kkv.data.injection.qualifier.ForApplication
import tusur.kkv.domain.model.BestEmployee
import javax.inject.Inject

class EmployeeBestHomeViewModelMapper @Inject constructor(@ForApplication
                                                          private val context: Context,
                                                          private val moneyMapper: MoneyMapper)
    : Function<BestEmployee, EmployeeBestHomeViewModel> {

    override fun apply(best: BestEmployee) = EmployeeBestHomeViewModel(
            best.id,
            best.fullName,
            context.getString(R.string.best_employee_revenue, moneyMapper.apply(best.revenue)),
            context.getString(R.string.best_employee_components_count, best.componentsCount),
            context.getString(R.string.best_employee_services_count, best.servicesCount)
    )

}