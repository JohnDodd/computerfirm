package tusur.kkv.computerfirm.features.employee.home.model

import tusur.kkv.computerfirm.base.BaseViewModel

data class EmployeeHomeViewModel(override val id: Long,
                                 val name: String,
                                 val shortInfo: String)
    : BaseViewModel(id)