package tusur.kkv.computerfirm.features.employee.home.model

import android.content.Context
import io.reactivex.functions.Function
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.mapper.GenderMapper
import tusur.kkv.data.injection.qualifier.ForApplication
import tusur.kkv.domain.model.Employee
import tusur.kkv.domain.model.Gender
import tusur.kkv.domain.model.Position
import javax.inject.Inject

class EmployeeHomeViewModelMapper @Inject constructor(@ForApplication private val context: Context,
                                                      private val genderMapper: GenderMapper)
    : Function<Employee, EmployeeHomeViewModel> {

    override fun apply(employee: Employee) = EmployeeHomeViewModel(
            employee.id,
            employee.fullName,
            mapShortInfo(employee.gender, employee.position)
    )

    private fun mapShortInfo(gender: Gender, position: Position?) =
            listOf(genderMapper.apply(gender), position?.name ?: "")
                    .asSequence()
                    .filter { it.isNotBlank() }
                    .joinToString(separator = context.getString(R.string.join_separator))

}