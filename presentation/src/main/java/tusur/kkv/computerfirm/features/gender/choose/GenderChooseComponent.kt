package tusur.kkv.computerfirm.features.gender.choose

import dagger.Subcomponent
import tusur.kkv.computerfirm.injection.scope.FragmentScope

@FragmentScope
@Subcomponent(modules = [GenderChooseDialogModule::class])
interface GenderChooseComponent {

    fun inject(fragment: GenderChooseDialog)
}