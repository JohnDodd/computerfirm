package tusur.kkv.computerfirm.features.gender.choose

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import kotlinx.android.synthetic.main.fragment_employee_home.*
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.base.BaseInjectingDialogFragment
import javax.inject.Inject
import javax.inject.Provider

class GenderChooseDialog : BaseInjectingDialogFragment<GenderChooseComponent, GenderChoosePresenter>() {

    override fun GenderChooseComponent.onInject() = inject(this@GenderChooseDialog)

    override val viewId = R.layout.dialog_simple

    @Inject
    lateinit var adapter: RendererRecyclerViewAdapter
    @Inject
    lateinit var layoutManager: Provider<RecyclerView.LayoutManager>

    override fun bindView(view: View,
                          savedInstanceState: Bundle?,
                          dialog: AlertDialog) {
        recyclerView.adapter = adapter
        recyclerView.layoutManager = layoutManager.get()
    }

    override fun createComponent() =
            baseActivity.component
                    .createGenderDialogComponent(GenderChooseDialogModule(
                            this,
                            viewPresenter))

    private val viewPresenter = object : GenderChoosePresenter.View {
        override fun close() {
            dismiss()
        }

        override fun updateItems(items: List<ViewModel>) {
            adapter.setItems(items)
        }
    }
}