package tusur.kkv.computerfirm.features.gender.choose

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import dagger.Module
import dagger.Provides
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.base.BaseDiffCallback
import tusur.kkv.computerfirm.features.gender.choose.model.GenderViewModel
import tusur.kkv.computerfirm.injection.module.DialogModule
import tusur.kkv.computerfirm.utils.onClick
import tusur.kkv.computerfirm.utils.register
import tusur.kkv.data.injection.qualifier.ForActivity

@Module
class GenderChooseDialogModule(fragment: GenderChooseDialog,
                               view: GenderChoosePresenter.View)
    : DialogModule<GenderChooseDialog, GenderChoosePresenter.View>(fragment, view) {

    @Provides
    fun provideOnResultListener(): GenderChoosePresenter.OnResultListener =
            fragment.targetFragment as GenderChoosePresenter.OnResultListener

    @Provides
    fun provideAdapter(diffCallback: BaseDiffCallback) = RendererRecyclerViewAdapter().apply {
        register<GenderViewModel>(R.layout.item_simple) { model ->
            setText(R.id.simple_text, model.text)
            onClick { fragment.presenter.onGenderClicked(model) }
        }
        setDiffCallback(diffCallback)
    }

    @Provides
    fun provideLayoutManager(@ForActivity context: Context): RecyclerView.LayoutManager =
            LinearLayoutManager(context)

}