package tusur.kkv.computerfirm.features.gender.choose

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import io.reactivex.Observable
import tusur.kkv.computerfirm.base.BasePresenter
import tusur.kkv.computerfirm.base.BaseView
import tusur.kkv.computerfirm.features.gender.choose.model.GenderViewModel
import tusur.kkv.computerfirm.features.gender.choose.model.GenderViewModelMapper
import tusur.kkv.domain.model.Gender
import javax.inject.Inject

class GenderChoosePresenter
@Inject constructor(private val resultListener: OnResultListener,
                    private val mapper: GenderViewModelMapper,
                    private val view: View)
    : BasePresenter() {

    override fun onViewShown() {
        super.onViewShown()
        Observable.just(Gender.MALE, Gender.FEMALE, Gender.UNKNOWN)
                .map(mapper)
                .toList()
                .addSubscription(view::updateItems)
    }

    fun onGenderClicked(model: GenderViewModel) {
        resultListener.onResult(model.gender)
        view.close()
    }

    interface View : BaseView {
        fun close()
        fun updateItems(items: List<ViewModel>)
    }

    interface OnResultListener {
        fun onResult(gender: Gender)
    }
}