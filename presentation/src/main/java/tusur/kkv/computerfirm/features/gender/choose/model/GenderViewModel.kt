package tusur.kkv.computerfirm.features.gender.choose.model

import tusur.kkv.computerfirm.base.BaseViewModel
import tusur.kkv.domain.model.Gender

data class GenderViewModel(override val id: Long,
                           val text: String,
                           val gender: Gender) : BaseViewModel(id)