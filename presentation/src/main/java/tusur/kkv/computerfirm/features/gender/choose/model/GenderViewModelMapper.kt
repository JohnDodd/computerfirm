package tusur.kkv.computerfirm.features.gender.choose.model

import io.reactivex.functions.Function
import tusur.kkv.computerfirm.mapper.GenderMapper
import tusur.kkv.domain.model.Gender
import javax.inject.Inject

class GenderViewModelMapper
@Inject constructor(private val genderTextMapper: GenderMapper) : Function<Gender, GenderViewModel> {

    override fun apply(gender: Gender) = when (gender) {
        Gender.MALE -> GenderViewModel(0,
                genderTextMapper.apply(gender),
                gender)
        Gender.FEMALE -> GenderViewModel(1,
                genderTextMapper.apply(gender),
                gender)
        Gender.UNKNOWN -> GenderViewModel(2,
                genderTextMapper.apply(gender),
                gender)
    }


}