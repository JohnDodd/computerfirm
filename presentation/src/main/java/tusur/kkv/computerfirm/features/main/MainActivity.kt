package tusur.kkv.computerfirm.features.main

import android.os.Bundle
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import com.google.android.material.navigation.NavigationView.OnNavigationItemSelectedListener
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.base.BaseInjectingActivity
import tusur.kkv.computerfirm.injection.component.MainActivityComponent
import tusur.kkv.computerfirm.injection.module.ActivityModule

class MainActivity : BaseInjectingActivity<MainActivityComponent, MainPresenter>() {

    override val layoutResID = R.layout.activity_main

    override fun onInject(component: MainActivityComponent) {
        component.inject(this)
    }

    override fun createComponent(): MainActivityComponent =
            appMain.component.createMainActivityComponent(ActivityModule(this))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this,
                navigationDrawer,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close)
        navigationDrawer.addDrawerListener(toggle)
        toggle.syncState()

        navigationView.setNavigationItemSelectedListener(navigationListener)

        if (supportFragmentManager.findFragmentById(R.id.main_container) == null)
            uiRouter.openEmployeeHomePage()
    }

    override fun onBackPressed() {
        if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
            navigationDrawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    private val navigationListener = OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.nav_employees -> uiRouter.openEmployeeHomePage()
            R.id.nav_positions -> uiRouter.openPositionHomePage()
            R.id.nav_component -> uiRouter.openComponentHomePage()
            R.id.nav_services -> uiRouter.openServiceHomePage()
            R.id.nav_orders -> uiRouter.openOrderHomePage()
            R.id.nav_customers -> uiRouter.openCustomerHomePage()
            R.id.nav_component_types -> uiRouter.openComponentTypeHomePage()
            R.id.nav_purveyors -> uiRouter.openPurveyorHomePage()
        }

        navigationDrawer.closeDrawer(GravityCompat.START)
        true
    }

}