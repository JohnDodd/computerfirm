package tusur.kkv.computerfirm.features.order.edit

import dagger.Subcomponent
import tusur.kkv.computerfirm.injection.scope.FragmentScope


@FragmentScope
@Subcomponent(modules = [OrderEditFragmentModule::class])
interface OrderEditComponent {

    fun inject(fragment: OrderEditFragment)

}