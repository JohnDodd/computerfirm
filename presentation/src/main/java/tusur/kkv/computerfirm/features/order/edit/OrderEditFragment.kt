package tusur.kkv.computerfirm.features.order.edit

import android.os.Bundle
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.fragment_order_edit.*
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.base.BaseScreenFragment
import tusur.kkv.computerfirm.base.NO_ID
import tusur.kkv.computerfirm.base.UIRouter
import tusur.kkv.computerfirm.features.component.choose.OnComponentResult
import tusur.kkv.computerfirm.features.customer.choose.OnCustomerResult
import tusur.kkv.computerfirm.features.employee.choose.OnEmployeeResult
import tusur.kkv.computerfirm.features.service.choose.OnServiceResult
import tusur.kkv.computerfirm.utils.onClick
import tusur.kkv.computerfirm.utils.showSnack
import tusur.kkv.domain.model.Component
import tusur.kkv.domain.model.Customer
import tusur.kkv.domain.model.Employee
import tusur.kkv.domain.model.Service
import javax.inject.Inject

class OrderEditFragment : BaseScreenFragment<OrderEditComponent, OrderEditPresenter>(),
        OnCustomerResult,
        OnEmployeeResult,
        OnServiceResult,
        OnComponentResult {

    override val viewId = R.layout.fragment_order_edit

    override fun OrderEditComponent.onInject() = inject(this@OrderEditFragment)

    @Inject
    lateinit var uiRouter: UIRouter

    override fun createComponent() = baseActivity.component
            .createEditOrderFragmentComponent(OrderEditFragmentModule(this, viewPresenter))

    override fun bindView(view: View, savedInstanceState: Bundle?) {
        val orderId = arguments?.getLong(KEY_ID) ?: NO_ID
        presenter.onBindView(orderId)
        saveButton.onClick(presenter::onSaveButtonClicked)
        execution_date_text.onClick(presenter::onExecutionDateClicked)
        registration_date_text.onClick(presenter::onRegistrationDateClicked)
        customer_text.onClick(presenter::onCustomerClicked)
        employee_text.onClick(presenter::onEmployeeClicked)
        components_text.onClick(presenter::onComponentsClicked)
        services_text.onClick(presenter::onServicesClicked)
    }

    override fun onResult(customer: Customer?) {
        presenter.onCustomerChanged(customer)
    }

    override fun onResult(employee: Employee?) {
        presenter.onEmployeeChanged(employee)
    }

    override fun onResult(vararg service: Service) {
        presenter.onServicesChanged(service)
    }

    override fun onResult(vararg component: Component) {
        presenter.onComponentsChanged(component)
    }


    private val viewPresenter = object : OrderEditPresenter.View {
        override fun showRegistrationDateError(message: String) {
            registration_date_text.showError(message)
        }

        override fun updateComponents(text: String) {
            components_text.text = text
        }

        override fun updateServices(text: String) {
            services_text.text = text
        }

        override fun updateExecutionDate(text: String) {
            execution_date_text.text = text
        }

        override fun updateRegistrationDate(text: String) {
            registration_date_text.text = text
        }

        override fun updateCustomer(text: String) {
            customer_text.text = text
        }

        override fun updateEmployee(text: String) {
            employee_text.text = text
        }

        override fun showExecutionDateError(message: String) {
            execution_date_text.showError(message)
        }

        override fun showCustomerError(message: String) {
            customer_text.showError(message)
        }

        override fun showEmployeeError(message: String) {
            employee_text.showError(message)
        }

        override fun openCustomerPage() {
            uiRouter.openCustomerChoosePage(this@OrderEditFragment)
        }

        override fun openEmployeePage() {
            uiRouter.openEmployeeChoosePage(this@OrderEditFragment)
        }

        override fun openComponentsPage(orderId: Long) {
            uiRouter.openComponentChoosePage(this@OrderEditFragment, orderId, true)
        }

        override fun openServicesPage(orderId: Long) {
            uiRouter.openServiceChoosePage(this@OrderEditFragment, orderId, true)
        }

        override fun closeView() {
            popBackStack()
        }

        override fun showSaveError() {
            view?.showSnack(getString(R.string.simple_error))
        }

        private fun TextView.showError(message: String) {
            error = message
            scroll_view.scrollTo(0, this.bottom)
        }

    }

    companion object {
        const val KEY_ID = "KEY_ID"
    }
}
