package tusur.kkv.computerfirm.features.order.edit

import dagger.Module
import tusur.kkv.computerfirm.injection.module.FragmentModule

@Module
class OrderEditFragmentModule(fragment: OrderEditFragment,
                              view: OrderEditPresenter.View)
    : FragmentModule<OrderEditFragment, OrderEditPresenter.View>(fragment, view) 