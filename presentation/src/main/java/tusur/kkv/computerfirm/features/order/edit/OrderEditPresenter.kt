package tusur.kkv.computerfirm.features.order.edit

import com.annimon.stream.Optional.*
import io.reactivex.rxkotlin.Observables
import io.reactivex.subjects.BehaviorSubject.createDefault
import tusur.kkv.computerfirm.base.BasePresenter
import tusur.kkv.computerfirm.base.BaseView
import tusur.kkv.computerfirm.base.NO_ID
import tusur.kkv.computerfirm.base.UIRouter
import tusur.kkv.computerfirm.base.tuple.Tuple7
import tusur.kkv.data.database.base.DateFormatter
import tusur.kkv.data.exception.IllegalParamsException
import tusur.kkv.domain.interactor.order.RefreshOrderInteractor
import tusur.kkv.domain.interactor.order.RequestOrderInteractor
import tusur.kkv.domain.interactor.order.SendOrderInteractor
import tusur.kkv.domain.interactor.ordercomponent.RequestComponentsByOrderInteractor
import tusur.kkv.domain.interactor.orderservice.RequestServicesByOrderInteractor
import tusur.kkv.domain.model.Component
import tusur.kkv.domain.model.Customer
import tusur.kkv.domain.model.Employee
import tusur.kkv.domain.model.Service
import tusur.kkv.domain.repository.IOrderRepository.ParamsExceptionId.Companion.CUSTOMER
import tusur.kkv.domain.repository.IOrderRepository.ParamsExceptionId.Companion.EMPLOYEE
import tusur.kkv.domain.repository.IOrderRepository.ParamsExceptionId.Companion.EXECUTION_DATE
import tusur.kkv.domain.repository.IOrderRepository.ParamsExceptionId.Companion.REGISTRATION_DATE
import tusur.kkv.domain.utils.applyForEach
import tusur.kkv.domain.utils.unwrap
import javax.inject.Inject

class OrderEditPresenter
@Inject constructor(private val request: RequestOrderInteractor,
                    private val send: SendOrderInteractor,
                    private val refresh: RefreshOrderInteractor,
                    private val requestServices: RequestServicesByOrderInteractor,
                    private val requestComponents: RequestComponentsByOrderInteractor,
                    private val mapper: EditOrderViewModelMapper,
                    private val dateFormatter: DateFormatter,
                    private val uiRouter: UIRouter,
                    private val view: OrderEditPresenter.View)
    : BasePresenter() {

    private val idSubject = createDefault(NO_ID)
    private val executionDateSubject = createDefault(empty<String>())
    private val registrationDateSubject = createDefault(empty<String>())
    private val customerSubject = createDefault(empty<Customer>())
    private val employeeSubject = createDefault(empty<Employee>())
    private val servicesSubject = createDefault(empty<Array<out Service>>())
    private val componentsSubject = createDefault(empty<Array<out Component>>())

    fun onBindView(id: Long) {
        idSubject.onNext(id)
        if (id != NO_ID)
            request.getSingle(id).map(mapper).addSubscription {
                if (it.executionDate.isNotBlank())
                    executionDateSubject.onNext(of(it.executionDate))
                if (it.registrationDate.isNotBlank())
                    registrationDateSubject.onNext(of(it.registrationDate))
                customerSubject.onNext(of(it.customer))
                employeeSubject.onNext(of(it.employee))
            }
        requestServices.getSingle(id)
                .addSubscription({ servicesSubject.onNext(of(it.toTypedArray())) }) {}
        requestComponents.getSingle(id)
                .addSubscription({ componentsSubject.onNext(of(it.toTypedArray())) }) {}
    }

    override fun onViewShown() {
        super.onViewShown()
        executionDateSubject.unwrap().addSubscription(view::updateExecutionDate)
        registrationDateSubject.unwrap().addSubscription(view::updateRegistrationDate)
        customerSubject
                .map { o -> o.map { it.fullName }.orElse("") }
                .addSubscription(view::updateCustomer)
        employeeSubject
                .map { o -> o.map { it.fullName }.orElse("") }
                .addSubscription(view::updateEmployee)
        componentsSubject.unwrap()
                .applyForEach { map(Component::name) }
                .map { it.joinToString(",\n") }
                .addSubscription(view::updateComponents)

        servicesSubject.unwrap()
                .applyForEach { map(Service::name) }
                .map { it.joinToString(",\n") }
                .addSubscription(view::updateServices)
    }

    fun onExecutionDateClicked() {
        uiRouter.openDatePage {
            val formatted = dateFormatter.formatToViewDate(it)
            executionDateSubject.onNext(of(formatted))
        }
    }

    fun onRegistrationDateClicked() {
        uiRouter.openDatePage {
            val formatted = dateFormatter.formatToViewDate(it)
            registrationDateSubject.onNext(of(formatted))
        }
    }


    fun onCustomerClicked() {
        view.openCustomerPage()
    }

    fun onEmployeeClicked() {
        view.openEmployeePage()
    }

    fun onComponentsClicked() {
        view.openComponentsPage(idSubject.value!!)
    }

    fun onServicesClicked() {
        view.openServicesPage(idSubject.value!!)
    }

    fun onSaveButtonClicked() {
        Observables.combineLatest(
                idSubject,
                executionDateSubject.map { it.map(dateFormatter::parseFromViewDate) },
                customerSubject.map { it.map { o -> o.id }.orElse(NO_ID) },
                employeeSubject.map { it.map { o -> o.id }.orElse(NO_ID) },
                servicesSubject.map { it.map { o -> o.map { s -> s.id } } },
                componentsSubject.map { it.map { o -> o.map { c -> c.id } } },
                registrationDateSubject.map { it.map(dateFormatter::parseFromViewDate) },
                ::Tuple7
        ).firstOrError().addSubscription { tuple ->
            val id = tuple.first
            val registrationDate = tuple.seventh.orElse(null)
            val executionDate = tuple.second.orElse(null)
            val customerId = tuple.third
            val employeeId = tuple.fourth
            val services: List<Long>? = tuple.fifth.orElse(null)
            val components: List<Long>? = tuple.sixth.orElse(null)
            if (id == NO_ID)
                addSendSubscription(registrationDate,
                        executionDate,
                        customerId,
                        employeeId,
                        services,
                        components)
            else
                addRefreshSubscription(id,
                        executionDate,
                        customerId,
                        employeeId,
                        services,
                        components,
                        registrationDate
                )
        }
    }

    private fun addRefreshSubscription(id: Long,
                                       executionDate: String?,
                                       customerId: Long,
                                       employeeId: Long,
                                       services: List<Long>?,
                                       components: List<Long>?,
                                       registrationDate: String?) {
        refresh.getRefreshSingle(RefreshOrderInteractor.Params(
                id,
                customerId,
                employeeId,
                executionDate,
                services,
                components,
                registrationDate
        )).addSubscription({ view.closeView() }, showErrorConsumer)
    }

    private fun addSendSubscription(registrationDate: String?,
                                    executionDate: String?,
                                    customerId: Long,
                                    employeeId: Long,
                                    services: List<Long>?,
                                    components: List<Long>?) {
        send.getSingle(SendOrderInteractor.Params(
                customerId,
                employeeId,
                executionDate,
                services,
                components,
                registrationDate
        )).addSubscription({
            if (it) view.closeView()
            else view.showSaveError()
        }, showErrorConsumer)
    }

    private val showErrorConsumer: (Throwable) -> Unit = {
        if (it is IllegalParamsException)
            when (it.id) {
                REGISTRATION_DATE -> view.showRegistrationDateError(it.message)
                EXECUTION_DATE -> view.showExecutionDateError(it.message)
                CUSTOMER -> view.showCustomerError(it.message)
                EMPLOYEE -> view.showEmployeeError(it.message)
                else -> view.showSaveError()
            }
        else view.showSaveError()
    }

    fun onCustomerChanged(customer: Customer?) {
        customerSubject.onNext(ofNullable(customer))
    }

    fun onEmployeeChanged(employee: Employee?) {
        employeeSubject.onNext(ofNullable(employee))
    }

    fun onServicesChanged(service: Array<out Service>) {
        servicesSubject.onNext(of(service))
    }

    fun onComponentsChanged(component: Array<out Component>) {
        componentsSubject.onNext(of(component))
    }

    interface View : BaseView {

        fun updateExecutionDate(text: String)
        fun updateRegistrationDate(text: String)
        fun updateCustomer(text: String)
        fun updateEmployee(text: String)
        fun updateComponents(text: String)
        fun updateServices(text: String)

        fun showRegistrationDateError(message: String)
        fun showExecutionDateError(message: String)
        fun showCustomerError(message: String)
        fun showEmployeeError(message: String)

        fun openCustomerPage()
        fun openEmployeePage()
        fun openComponentsPage(orderId: Long)
        fun openServicesPage(orderId: Long)

        fun closeView()
        fun showSaveError()
    }

}