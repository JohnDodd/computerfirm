package tusur.kkv.computerfirm.features.order.edit

import tusur.kkv.domain.model.Customer
import tusur.kkv.domain.model.Employee

data class OrderEditViewModel(val id: Long,
                              val registrationDate: String,
                              val executionDate: String,
                              val customer: Customer,
                              val employee: Employee)