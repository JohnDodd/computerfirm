package tusur.kkv.computerfirm.features.order.edit

import io.reactivex.functions.Function
import tusur.kkv.computerfirm.utils.toStringOrEmpty
import tusur.kkv.data.database.base.DateFormatter
import tusur.kkv.domain.model.Order
import javax.inject.Inject


class EditOrderViewModelMapper
@Inject constructor(private val dateFormatter: DateFormatter) : Function<Order, OrderEditViewModel> {
    override fun apply(order: Order) = OrderEditViewModel(
            order.id,
            dateFormatter.parseFromServerDate(order.registrationDate).toStringOrEmpty(),
            dateFormatter.parseFromServerDate(order.executionDate).toStringOrEmpty(),
            order.customer,
            order.employee
    )
}