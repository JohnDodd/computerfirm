package tusur.kkv.computerfirm.features.order.home

import dagger.Subcomponent
import tusur.kkv.computerfirm.injection.scope.FragmentScope

@FragmentScope
@Subcomponent(modules = [OrderHomeFragmentModule::class])
interface OrderHomeComponent {
    fun inject(fragment: OrderHomeFragment)
}