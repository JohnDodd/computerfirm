package tusur.kkv.computerfirm.features.order.home

import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import kotlinx.android.synthetic.main.fragment_order_home.*
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.base.BaseScreenFragment
import tusur.kkv.computerfirm.base.MenuId.SEARCH_SIMPLE
import tusur.kkv.computerfirm.base.UIRouter
import tusur.kkv.computerfirm.features.customer.choose.OnCustomerResult
import tusur.kkv.computerfirm.features.employee.choose.OnEmployeeResult
import tusur.kkv.computerfirm.utils.onClick
import tusur.kkv.computerfirm.utils.showSnack
import tusur.kkv.domain.model.Customer
import tusur.kkv.domain.model.Employee
import javax.inject.Inject
import javax.inject.Provider

class OrderHomeFragment : BaseScreenFragment<OrderHomeComponent, OrderHomePresenter>(),
        OnCustomerResult,
        OnEmployeeResult {

    override val viewId = R.layout.fragment_order_home

    @Inject
    lateinit var adapter: RendererRecyclerViewAdapter
    @Inject
    lateinit var layoutManager: Provider<RecyclerView.LayoutManager>
    @Inject
    lateinit var decoration: Provider<RecyclerView.ItemDecoration>
    @Inject
    lateinit var itemTouchHelper: ItemTouchHelper
    @Inject
    lateinit var uiRouter: UIRouter

    override val titleId = R.string.orders

    override fun OrderHomeComponent.onInject() = inject(this@OrderHomeFragment)

    override fun createComponent() = baseActivity.component
            .createOrdersComponent(OrderHomeFragmentModule(
                    this,
                    viewPresenter))

    override fun bindView(view: View, savedInstanceState: Bundle?) {
        recyclerView.adapter = adapter
        recyclerView.layoutManager = layoutManager.get()
        recyclerView.addItemDecoration(decoration.get())
        itemTouchHelper.attachToRecyclerView(recyclerView)
        fab.onClick(presenter::onFabClicked)
        registration_date_from_text.onClick(presenter::onRegistrationDateFromClicked)
        registration_date_to_text.onClick(presenter::onRegistrationDateToClicked)
        customer_text.onClick(presenter::onCustomerClicked)
        employee_text.onClick(presenter::onEmployeeClicked)
        fab_search.onClick(presenter::onSearchFabClicked)
    }


    override fun onResult(customer: Customer?) {
        presenter.onSearchCustomerChanged(customer)
    }

    override fun onResult(employee: Employee?) {
        presenter.onSearchEmployeeChanged(employee)
    }

    private val viewPresenter = object : OrderHomePresenter.View {
        override fun openCustomerPage() {
            uiRouter.openCustomerChoosePage(this@OrderHomeFragment)
        }

        override fun openEmployeePage() {
            uiRouter.openEmployeeChoosePage(this@OrderHomeFragment)
        }

        override fun updateCustomer(text: String) {
            customer_text.text = text
        }

        override fun updateEmployee(text: String) {
            employee_text.text = text
        }

        override fun updateRegistrationDateFromText(text: String) {
            registration_date_from_text.text = text
        }

        override fun updateRegistrationDateToText(text: String) {
            registration_date_to_text.text = text
        }

        override fun showSearchView(show: Boolean) {
            order_search.visibility = if (show) VISIBLE else GONE
            recyclerView.visibility = if (show) GONE else VISIBLE
            if (show) fab.hide() else fab.show()
            if (!show) fab_search.hide() else fab_search.show()
            if (show) optionsMenuController.clear()
            else optionsMenuController.setMenuItems(SEARCH_SIMPLE)
            optionsMenuController.invalidate()
        }

        override fun updateItems(items: List<ViewModel>) {
            adapter.setItems(items)
        }

        override fun showRegistrationsDateFromError(message: String) {
            registration_date_from_text.error = message
        }

        override fun showRegistrationsDateToError(message: String) {
            registration_date_to_text.error = message
        }

        override fun showError() {
            view?.showSnack(getString(R.string.simple_error))
        }

    }
}