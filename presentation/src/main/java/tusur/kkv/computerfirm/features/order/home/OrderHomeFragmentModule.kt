package tusur.kkv.computerfirm.features.order.home

import android.content.Context
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import dagger.Module
import dagger.Provides
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.base.BaseDiffCallback
import tusur.kkv.computerfirm.base.BetweenItemsDecoration
import tusur.kkv.computerfirm.features.order.home.model.OrderHomeViewModel
import tusur.kkv.computerfirm.injection.module.FragmentModule
import tusur.kkv.computerfirm.utils.onClick
import tusur.kkv.computerfirm.utils.register
import tusur.kkv.computerfirm.view.SwipeToDeleteCallback
import tusur.kkv.data.injection.qualifier.ForActivity

@Module
class OrderHomeFragmentModule(fragment: OrderHomeFragment, view: OrderHomePresenter.View)
    : FragmentModule<OrderHomeFragment, OrderHomePresenter.View>(fragment, view) {

    @Provides
    fun provideAdapter(diffCallback: BaseDiffCallback) = RendererRecyclerViewAdapter().apply {
        register<OrderHomeViewModel>(R.layout.item_order_home) { model ->
            setText(R.id.position_id_text, model.idText)
            setText(R.id.position_registration_date_text, model.registrationDate)
            onClick { fragment.presenter.onModelClicked(model) }
        }
        setDiffCallback(diffCallback)
    }

    @Provides
    fun provideLayoutManager(@ForActivity context: Context): RecyclerView.LayoutManager =
            LinearLayoutManager(context)

    @Provides
    fun provideDecoration(): RecyclerView.ItemDecoration = BetweenItemsDecoration()

    @Provides
    fun provideItemTouchHelper(@ForActivity context: Context): ItemTouchHelper =
            ItemTouchHelper(object : SwipeToDeleteCallback(context) {
                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                    fragment.presenter.onSwiped(viewHolder.adapterPosition)
                }
            })

}