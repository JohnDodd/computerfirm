package tusur.kkv.computerfirm.features.order.home

import com.annimon.stream.Optional
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import io.reactivex.Observable
import io.reactivex.rxkotlin.withLatestFrom
import io.reactivex.subjects.BehaviorSubject.createDefault
import io.reactivex.subjects.PublishSubject
import tusur.kkv.computerfirm.base.BasePresenter
import tusur.kkv.computerfirm.base.BaseView
import tusur.kkv.computerfirm.base.MenuId
import tusur.kkv.computerfirm.base.MenuId.SEARCH_SIMPLE
import tusur.kkv.computerfirm.base.UIRouter
import tusur.kkv.computerfirm.base.tuple.Tuple5
import tusur.kkv.computerfirm.controller.OptionsMenuController
import tusur.kkv.computerfirm.features.order.home.model.OrderHomeViewModel
import tusur.kkv.computerfirm.features.order.home.model.OrderHomeViewModelMapper
import tusur.kkv.data.database.base.DateFormatter
import tusur.kkv.data.exception.IllegalParamsException
import tusur.kkv.domain.interactor.order.DeleteOrderInteractor
import tusur.kkv.domain.interactor.order.RequestOrdersSearchInteractor
import tusur.kkv.domain.interactor.order.RetrieveOrdersInteractor
import tusur.kkv.domain.model.Customer
import tusur.kkv.domain.model.Employee
import tusur.kkv.domain.repository.IOrderRepository.ParamsExceptionId.Companion.REGISTRATION_DATE_FROM
import tusur.kkv.domain.repository.IOrderRepository.ParamsExceptionId.Companion.REGISTRATION_DATE_TO
import tusur.kkv.domain.utils.forEachElement
import tusur.kkv.domain.utils.unwrap
import javax.inject.Inject

class OrderHomePresenter
@Inject constructor(private val view: View,
                    private val retrieve: RetrieveOrdersInteractor,
                    private val search: RequestOrdersSearchInteractor,
                    private val mapper: OrderHomeViewModelMapper,
                    private val delete: DeleteOrderInteractor,
                    private val dateFormatter: DateFormatter,
                    private val optionsMenuController: OptionsMenuController,
                    private val uiRouter: UIRouter) : BasePresenter() {

    val items = mutableListOf<ViewModel>()
    private val registrationDateFromSubject = createDefault(Optional.empty<String>())
    private val registrationDateToSubject = createDefault(Optional.empty<String>())
    private val searchCustomerSubject = createDefault(Optional.empty<Customer>())
    private val searchEmployeeSubject = createDefault(Optional.empty<Employee>())
    private val onSearchFabClickedSubject = PublishSubject.create<Boolean>()

    override fun onViewShown() {
        super.onViewShown()
        retrieve.getBehaviorStream()
                .forEachElement { it.map(mapper) }
                .map { it.reversed() }
                .scan(items) { accumulator, models ->
                    accumulator.clear()
                    accumulator.addAll(models)
                    accumulator
                }
                .addSubscription(view::updateItems)

        registrationDateFromSubject.unwrap().addSubscription(view::updateRegistrationDateFromText)
        registrationDateToSubject.unwrap().addSubscription(view::updateRegistrationDateToText)
        optionsMenuController.onMenuItemSelectedStream.addSubscription { onMenuItemClicked(it) }

        onSearchFabClickedSubject
                .withLatestFrom(
                        registrationDateFromSubject,
                        registrationDateToSubject,
                        searchCustomerSubject,
                        searchEmployeeSubject
                ) { t1, t2, t3, t4, t5 -> Tuple5(t1, t2, t3, t4, t5) }
                .flatMapSingle { tuple ->
                    val from = tuple.second.map(dateFormatter::parseFromViewDate).orElse(null)
                    val to = tuple.third.map(dateFormatter::parseFromViewDate).orElse(null)
                    val customerId = tuple.fourth.map { it.id }.orElse(null)
                    val employeeId = tuple.fifth.map { it.id }.orElse(null)
                    search.getSingle(
                            RequestOrdersSearchInteractor.Params(
                                    "",
                                    customerId,
                                    employeeId,
                                    from,
                                    to))
                            .flatMap { list -> Observable.fromIterable(list).map(mapper).toList() }
                }.addSubscription({
                    view.showSearchView(false)
                    view.updateItems(it)
                }, showErrorConsumer)

        searchCustomerSubject
                .map { o -> o.map { it.fullName }.orElse("") }
                .addSubscription(view::updateCustomer)
        searchEmployeeSubject
                .map { o -> o.map { it.fullName }.orElse("") }
                .addSubscription(view::updateEmployee)
    }

    private fun onMenuItemClicked(menuId: MenuId) {
        when (menuId) {
            SEARCH_SIMPLE -> view.showSearchView(true)
        }
    }

    fun onRegistrationDateFromClicked() {
        uiRouter.openDatePage {
            val formatted = dateFormatter.formatToViewDate(it)
            registrationDateFromSubject.onNext(Optional.of(formatted))
        }
    }

    fun onRegistrationDateToClicked() {
        uiRouter.openDatePage {
            val formatted = dateFormatter.formatToViewDate(it)
            registrationDateToSubject.onNext(Optional.of(formatted))
        }
    }

    fun onCustomerClicked() {
        view.openCustomerPage()
    }

    fun onEmployeeClicked() {
        view.openEmployeePage()
    }

    fun onSearchCustomerChanged(customer: Customer?) {
        searchCustomerSubject.onNext(Optional.ofNullable(customer))
    }

    fun onSearchEmployeeChanged(employee: Employee?) {
        searchEmployeeSubject.onNext(Optional.ofNullable(employee))
    }

    fun onFabClicked() {
        uiRouter.openOrderEditPage()
    }

    fun onModelClicked(model: OrderHomeViewModel) {
        uiRouter.openOrderEditPage(model.id)
    }

    fun onSwiped(position: Int) {
        val order = items[position] as OrderHomeViewModel
        delete.getSingle(order.id).addSubscription()
    }

    override fun onBindOptionsMenu() {
        optionsMenuController.setMenuItems(SEARCH_SIMPLE)
    }

    fun onSearchFabClicked() {
        onSearchFabClickedSubject.onNext(true)
    }

    private val showErrorConsumer: (Throwable) -> Unit = {
        if (it is IllegalParamsException)
            when (it.id) {
                REGISTRATION_DATE_FROM -> view.showRegistrationsDateFromError(it.message)
                REGISTRATION_DATE_TO -> view.showRegistrationsDateToError(it.message)
                else -> view.showError()
            }
        else view.showError()
    }


    interface View : BaseView {
        fun updateItems(items: List<ViewModel>)
        fun updateRegistrationDateFromText(text: String)
        fun updateRegistrationDateToText(text: String)
        fun showSearchView(show: Boolean)
        fun updateCustomer(text: String)
        fun updateEmployee(text: String)
        fun openCustomerPage()
        fun openEmployeePage()
        fun showRegistrationsDateFromError(message: String)
        fun showRegistrationsDateToError(message: String)
        fun showError()
    }
}