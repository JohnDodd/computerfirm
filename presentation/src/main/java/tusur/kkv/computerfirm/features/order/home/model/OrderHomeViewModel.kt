package tusur.kkv.computerfirm.features.order.home.model

import tusur.kkv.computerfirm.base.BaseViewModel

data class OrderHomeViewModel(override val id: Long,
                              val idText: String,
                              val registrationDate: String)
    : BaseViewModel(id)