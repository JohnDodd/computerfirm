package tusur.kkv.computerfirm.features.order.home.model

import android.content.Context
import io.reactivex.functions.Function
import tusur.kkv.computerfirm.R
import tusur.kkv.data.database.base.DateFormatter
import tusur.kkv.data.injection.qualifier.ForApplication
import tusur.kkv.domain.model.Order
import javax.inject.Inject

class OrderHomeViewModelMapper
@Inject constructor(private val dateFormatter: DateFormatter,
                    @ForApplication
                    private val context: Context) : Function<Order, OrderHomeViewModel> {
    override fun apply(order: Order) = OrderHomeViewModel(
            order.id,
            context.getString(R.string.orders_id, order.id),
            dateFormatter.parseFromServerDate(order.registrationDate)!!
    )

}