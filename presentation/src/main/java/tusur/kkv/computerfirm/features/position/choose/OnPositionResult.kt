package tusur.kkv.computerfirm.features.position.choose

import tusur.kkv.domain.model.Position

interface OnPositionResult {

    fun onResult(result: Position)
}