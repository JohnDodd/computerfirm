package tusur.kkv.computerfirm.features.position.choose

import dagger.Subcomponent
import tusur.kkv.computerfirm.injection.scope.FragmentScope

@FragmentScope
@Subcomponent(modules = [PositionChooseDialogModule::class])
interface PositionChooseComponent {

    fun inject(fragment: PositionChooseDialog)
}