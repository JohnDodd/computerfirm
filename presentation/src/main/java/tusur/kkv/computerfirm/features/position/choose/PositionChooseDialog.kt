package tusur.kkv.computerfirm.features.position.choose

import tusur.kkv.computerfirm.features.simpledialog.SimpleDialog

class PositionChooseDialog : SimpleDialog<PositionChooseComponent, PositionChoosePresenter>() {

    override fun PositionChooseComponent.onInject() = inject(this@PositionChooseDialog)

    override fun createComponent() =
            baseActivity.component.createPositionChooseDialogComponent(PositionChooseDialogModule(
                    this,
                    simpleViewPresenter))

}