package tusur.kkv.computerfirm.features.position.choose

import tusur.kkv.computerfirm.base.UIRouter
import tusur.kkv.computerfirm.features.simpledialog.AppendSimpleAddViewModel
import tusur.kkv.computerfirm.features.simpledialog.SimpleDialogPresenter
import tusur.kkv.domain.interactor.position.RetrievePositionInteractor
import tusur.kkv.domain.model.Position
import tusur.kkv.domain.utils.forEachElement
import javax.inject.Inject

class PositionChoosePresenter
@Inject constructor(private val mapper: PositionSimpleViewModelMapper,
                    private val retrieve: RetrievePositionInteractor,
                    private val resultListener: OnPositionResult,
                    view: SimpleDialogPresenter.SimpleDialogView,
                    appendMapper: AppendSimpleAddViewModel,
                    uiRouter: UIRouter) :
        SimpleDialogPresenter(view, appendMapper, uiRouter) {

    override fun onViewShown() {
        super.onViewShown()
        retrieve.getBehaviorStream()
                .startWith(listOf<Position>())
                .forEachElement { it.map(mapper) }
                .map(appendMapper)
                .addSubscription(view::updateItems)
    }

    fun onModelClicked(model: Position) {
        resultListener.onResult(model)
        view.close()
    }

    fun onAddClicked() {
        uiRouter.openPositionEditPage()
        view.close()
    }

    fun onLongClicked(model: Position) {
        uiRouter.openPositionEditPage(model.id)
        view.close()
    }

}