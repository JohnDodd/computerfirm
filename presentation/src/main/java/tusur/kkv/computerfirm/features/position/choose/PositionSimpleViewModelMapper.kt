package tusur.kkv.computerfirm.features.position.choose

import io.reactivex.functions.Function
import tusur.kkv.computerfirm.features.simpledialog.SimpleViewModel
import tusur.kkv.domain.model.Position
import javax.inject.Inject

class PositionSimpleViewModelMapper
@Inject constructor() : Function<Position, SimpleViewModel<Position>> {
    override fun apply(position: Position) =
            SimpleViewModel(
                    position.id,
                    position.name,
                    position
            )
}