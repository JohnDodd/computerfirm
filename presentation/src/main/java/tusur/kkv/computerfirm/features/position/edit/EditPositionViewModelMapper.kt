package tusur.kkv.computerfirm.features.position.edit

import io.reactivex.functions.Function
import tusur.kkv.domain.model.Position
import javax.inject.Inject

class EditPositionViewModelMapper
@Inject constructor() : Function<Position, PositionEditViewModel> {

    override fun apply(position: Position) = PositionEditViewModel(
            position.id,
            position.name,
            position.salary.toString(),
            position.duties
    )
}