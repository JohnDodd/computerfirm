package tusur.kkv.computerfirm.features.position.edit

import dagger.Subcomponent
import tusur.kkv.computerfirm.injection.scope.FragmentScope

@FragmentScope
@Subcomponent(modules = [PositionEditFragmentModule::class])
interface PositionEditComponent {

    fun inject(fragment: PositionEditFragment)
}