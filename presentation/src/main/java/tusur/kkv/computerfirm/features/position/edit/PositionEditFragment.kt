package tusur.kkv.computerfirm.features.position.edit


import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.fragment_position_edit.*
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.base.BaseScreenFragment
import tusur.kkv.computerfirm.base.NO_ID
import tusur.kkv.computerfirm.utils.onClick
import tusur.kkv.computerfirm.utils.showSnack
import tusur.kkv.computerfirm.utils.toStringOrEmpty

class PositionEditFragment : BaseScreenFragment<PositionEditComponent, PositionEditPresenter>() {

    override val viewId = R.layout.fragment_position_edit

    override fun PositionEditComponent.onInject() = inject(this@PositionEditFragment)

    override fun createComponent() = baseActivity.component
            .createEditPositionComponent(PositionEditFragmentModule(this, viewPresenter))

    override fun bindView(view: View, savedInstanceState: Bundle?) {
        val positionId = arguments?.getLong(KEY_ID) ?: NO_ID
        presenter.onBindView(positionId)
        saveButton.onClick(presenter::onSaveButtonClicked)
    }

    private val viewPresenter = object : PositionEditPresenter.View {

        override val name
            get() = position_name_text.text.toStringOrEmpty()
        override val salary
            get() = salary_text.text.toStringOrEmpty()
        override val duties
            get() = duties_text.text.toStringOrEmpty()

        override fun updateName(name: String) {
            position_name_text.setText(name)
        }

        override fun updateSalary(salary: String) {
            salary_text.setText(salary)
        }

        override fun updateDuties(duties: String) {
            duties_text.setText(duties)
        }

        override fun showSaveError() {
            view?.showSnack(getString(R.string.simple_error))
        }

        override fun showNameError(message: String) {
            position_name_text.error = message
            scroll_view.smoothScrollTo(0, position_name_text.top)
        }

        override fun showSalaryError(message: String) {
            salary_text.error = message
            scroll_view.smoothScrollTo(0, salary_text.top)
        }

        override fun showDutiesError(message: String) {
            duties_text.error = message
            scroll_view.smoothScrollTo(0, duties_text.top)
        }

        override fun closeView() {
            popBackStack()
        }
    }

    companion object {
        const val KEY_ID = "KEY_ID"
    }
}
