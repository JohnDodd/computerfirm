package tusur.kkv.computerfirm.features.position.edit

import dagger.Module
import tusur.kkv.computerfirm.injection.module.FragmentModule

@Module
class PositionEditFragmentModule(fragment: PositionEditFragment, view: PositionEditPresenter.View)
    : FragmentModule<PositionEditFragment, PositionEditPresenter.View>(fragment, view)