package tusur.kkv.computerfirm.features.position.edit

import io.reactivex.subjects.BehaviorSubject
import tusur.kkv.computerfirm.base.BasePresenter
import tusur.kkv.computerfirm.base.BaseView
import tusur.kkv.computerfirm.base.NO_ID
import tusur.kkv.computerfirm.utils.toLongOrElse
import tusur.kkv.data.exception.IllegalParamsException
import tusur.kkv.domain.interactor.position.RefreshPositionInteractor
import tusur.kkv.domain.interactor.position.RequestPositionInteractor
import tusur.kkv.domain.interactor.position.SendPositionInteractor
import tusur.kkv.domain.repository.IPositionRepository.ParamsExceptionId.Companion.DUTIES
import tusur.kkv.domain.repository.IPositionRepository.ParamsExceptionId.Companion.NAME
import tusur.kkv.domain.repository.IPositionRepository.ParamsExceptionId.Companion.SALARY
import javax.inject.Inject

class PositionEditPresenter
@Inject constructor(private val request: RequestPositionInteractor,
                    private val send: SendPositionInteractor,
                    private val refresh: RefreshPositionInteractor,
                    private val mapper: EditPositionViewModelMapper,
                    private val view: PositionEditPresenter.View) : BasePresenter() {

    private var idSubject = BehaviorSubject.createDefault(NO_ID)

    fun onBindView(id: Long) {
        idSubject.onNext(id)
        if (id != NO_ID)
            request.getSingle(id).map(mapper).addSubscription {
                view.updateName(it.name)
                view.updateSalary(it.salary)
                view.updateDuties(it.duties)
            }
    }

    fun onSaveButtonClicked() {
        idSubject.firstOrError().addSubscription({ id ->
            if (id == NO_ID) addSendSubscription()
            else addRefreshSubscription(id)
        }) { view.showSaveError() }
    }


    private fun addSendSubscription() {
        send.getSingle(SendPositionInteractor.Params(
                view.name,
                view.salary.toLongOrNull(),
                view.duties
        )).addSubscription({
            if (it) view.closeView()
            else view.showSaveError()
        },
                showErrorConsumer)
    }

    private fun addRefreshSubscription(id: Long) {
        refresh.getRefreshSingle(RefreshPositionInteractor.Params(
                id,
                view.name,
                view.salary.toLongOrElse(-1),
                view.duties
        )).addSubscription({ view.closeView() }, showErrorConsumer)
    }

    private val showErrorConsumer: (Throwable) -> Unit = {
        if (it is IllegalParamsException)
            when (it.id) {
                NAME -> view.showNameError(it.message)
                SALARY -> view.showSalaryError(it.message)
                DUTIES -> view.showDutiesError(it.message)
                else -> view.showSaveError()
            }
        else view.showSaveError()
    }

    interface View : BaseView {
        val name: String
        val salary: String
        val duties: String

        fun updateName(name: String)
        fun updateSalary(salary: String)
        fun updateDuties(duties: String)
        fun showSaveError()

        fun showNameError(message: String)
        fun showSalaryError(message: String)
        fun showDutiesError(message: String)

        fun closeView()
    }
}