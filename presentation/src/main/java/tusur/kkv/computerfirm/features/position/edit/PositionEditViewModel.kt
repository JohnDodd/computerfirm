package tusur.kkv.computerfirm.features.position.edit

data class PositionEditViewModel(val id: Long,
                                 val name: String,
                                 val salary: String,
                                 val duties: String)