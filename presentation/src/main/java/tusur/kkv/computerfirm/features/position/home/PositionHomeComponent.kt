package tusur.kkv.computerfirm.features.position.home

import dagger.Subcomponent
import tusur.kkv.computerfirm.injection.scope.FragmentScope


@FragmentScope
@Subcomponent(modules = [PositionHomeFragmentModule::class])
interface PositionHomeComponent {

    fun inject(fragment: PositionHomeFragment)
}