package tusur.kkv.computerfirm.features.position.home

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import kotlinx.android.synthetic.main.fragment_employee_home.*
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.base.BaseScreenFragment
import tusur.kkv.computerfirm.utils.onClick
import javax.inject.Inject
import javax.inject.Provider

class PositionHomeFragment : BaseScreenFragment<PositionHomeComponent, PositionHomePresenter>() {

    override val viewId = R.layout.fragment_position_home

    @Inject
    lateinit var adapter: RendererRecyclerViewAdapter
    @Inject
    lateinit var layoutManager: Provider<RecyclerView.LayoutManager>
    @Inject
    lateinit var decoration: Provider<RecyclerView.ItemDecoration>

    override val titleId = R.string.positions

    override fun PositionHomeComponent.onInject() = inject(this@PositionHomeFragment)

    override fun createComponent() = baseActivity.component
            .createPositionsComponent(PositionHomeFragmentModule(
                    this,
                    viewPresenter))

    override fun bindView(view: View, savedInstanceState: Bundle?) {
        recyclerView.adapter = adapter
        recyclerView.layoutManager = layoutManager.get()
        recyclerView.addItemDecoration(decoration.get())
        fab.onClick(presenter::onFabClicked)
    }

    private val viewPresenter = object : PositionHomePresenter.View {
        override fun updateItems(items: List<ViewModel>) {
            adapter.setItems(items)
        }
    }
}