package tusur.kkv.computerfirm.features.position.home

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import dagger.Module
import dagger.Provides
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.base.BaseDiffCallback
import tusur.kkv.computerfirm.base.BetweenItemsDecoration
import tusur.kkv.computerfirm.features.position.home.model.HomePositionViewModel
import tusur.kkv.computerfirm.injection.module.FragmentModule
import tusur.kkv.computerfirm.utils.onClick
import tusur.kkv.computerfirm.utils.register
import tusur.kkv.data.injection.qualifier.ForActivity

@Module
class PositionHomeFragmentModule(fragment: PositionHomeFragment, view: PositionHomePresenter.View)
    : FragmentModule<PositionHomeFragment, PositionHomePresenter.View>(fragment, view) {

    @Provides
    fun provideAdapter(diffCallback: BaseDiffCallback) = RendererRecyclerViewAdapter().apply {
        register<HomePositionViewModel>(R.layout.item_position) { model ->
            setText(R.id.position_name_text, model.name)
            setText(R.id.position_salary_text, model.salary)
            onClick { fragment.presenter.onModelClicked(model) }
        }
        setDiffCallback(diffCallback)
    }

    @Provides
    fun provideLayoutManager(@ForActivity context: Context): RecyclerView.LayoutManager =
            LinearLayoutManager(context)

    @Provides
    fun provideDecoration(): RecyclerView.ItemDecoration = BetweenItemsDecoration()

}