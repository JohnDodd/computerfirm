package tusur.kkv.computerfirm.features.position.home

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import tusur.kkv.computerfirm.base.BasePresenter
import tusur.kkv.computerfirm.base.BaseView
import tusur.kkv.computerfirm.base.UIRouter
import tusur.kkv.computerfirm.features.position.home.model.HomePositionViewModel
import tusur.kkv.computerfirm.features.position.home.model.HomePositionViewModelMapper
import tusur.kkv.domain.interactor.position.RetrievePositionInteractor
import tusur.kkv.domain.utils.forEachElement
import javax.inject.Inject

class PositionHomePresenter
@Inject constructor(private val view: View,
                    private val retrievePositions: RetrievePositionInteractor,
                    private val mapper: HomePositionViewModelMapper,
                    private val uiRouter: UIRouter) : BasePresenter() {

    override fun onViewShown() {
        super.onViewShown()
        retrievePositions.getBehaviorStream()
                .forEachElement { it.map(mapper) }
                .addSubscription { view.updateItems(it) }
    }

    fun onFabClicked() {
        uiRouter.openPositionEditPage()
    }

    fun onModelClicked(model: HomePositionViewModel) {
        uiRouter.openPositionEditPage(model.id)
    }

    interface View : BaseView {
        fun updateItems(items: List<ViewModel>)
    }
}