package tusur.kkv.computerfirm.features.position.home.model

import tusur.kkv.computerfirm.base.BaseViewModel

data class HomePositionViewModel(override val id: Long,
                                 val name: String,
                                 val salary: String) : BaseViewModel(id)