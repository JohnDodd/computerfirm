package tusur.kkv.computerfirm.features.position.home.model

import io.reactivex.functions.Function
import tusur.kkv.computerfirm.mapper.MoneyMapper
import tusur.kkv.domain.model.Position
import javax.inject.Inject

class HomePositionViewModelMapper
@Inject constructor(private val moneyMapper: MoneyMapper) : Function<Position, HomePositionViewModel> {
    override fun apply(position: Position) =
            HomePositionViewModel(
                    position.id,
                    position.name,
                    moneyMapper.apply(position.salary)
            )
}