package tusur.kkv.computerfirm.features.purveyor.choose

import tusur.kkv.domain.model.Purveyor

interface OnPurveyorResult {

    fun onResult(purveyor: Purveyor)
}