package tusur.kkv.computerfirm.features.purveyor.choose

import dagger.Subcomponent
import tusur.kkv.computerfirm.injection.scope.FragmentScope

@FragmentScope
@Subcomponent(modules = [PurveyorChooseDialogModule::class])
interface PurveyorChooseComponent {

    fun inject(fragment: PurveyorChooseDialog)

}