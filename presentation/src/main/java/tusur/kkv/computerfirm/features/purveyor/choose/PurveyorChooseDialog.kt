package tusur.kkv.computerfirm.features.purveyor.choose

import tusur.kkv.computerfirm.features.simpledialog.SimpleDialog

class PurveyorChooseDialog : SimpleDialog<PurveyorChooseComponent, PurveyorChoosePresenter>() {

    override fun PurveyorChooseComponent.onInject() = inject(this@PurveyorChooseDialog)

    override fun createComponent() =
            baseActivity.component.createPurveyorChooseDialogComponent(PurveyorChooseDialogModule(
                    this,
                    simpleViewPresenter))
}