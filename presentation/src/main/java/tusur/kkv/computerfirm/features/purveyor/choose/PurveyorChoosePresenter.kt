package tusur.kkv.computerfirm.features.purveyor.choose

import tusur.kkv.computerfirm.base.UIRouter
import tusur.kkv.computerfirm.features.simpledialog.AppendSimpleAddViewModel
import tusur.kkv.computerfirm.features.simpledialog.SimpleDialogPresenter
import tusur.kkv.domain.interactor.purveyor.RetrievePurveyorInteractor
import tusur.kkv.domain.model.Purveyor
import tusur.kkv.domain.utils.forEachElement
import javax.inject.Inject

class PurveyorChoosePresenter
@Inject constructor(private val mapper: PurveyorSimpleViewModelMapper,
                    private val retrieve: RetrievePurveyorInteractor,
                    private val resultListener: OnPurveyorResult,
                    view: SimpleDialogPresenter.SimpleDialogView,
                    appendMapper: AppendSimpleAddViewModel,
                    uiRouter: UIRouter) :
        SimpleDialogPresenter(view, appendMapper, uiRouter) {

    override fun onViewShown() {
        super.onViewShown()
        retrieve.getBehaviorStream()
                .startWith(listOf<Purveyor>())
                .forEachElement { it.map(mapper) }
                .map(appendMapper)
                .addSubscription(view::updateItems)
    }

    fun onModelClicked(model: Purveyor) {
        resultListener.onResult(model)
        view.close()
    }

    fun onAddClicked() {
        uiRouter.openPurveyorEditPage()
        view.close()
    }

    fun onLongClicked(model: Purveyor) {
        uiRouter.openPurveyorEditPage(model.id)
        view.close()
    }

}