package tusur.kkv.computerfirm.features.purveyor.choose

import io.reactivex.functions.Function
import tusur.kkv.computerfirm.features.simpledialog.SimpleViewModel
import tusur.kkv.domain.model.Purveyor
import javax.inject.Inject

class PurveyorSimpleViewModelMapper
@Inject constructor() : Function<Purveyor, SimpleViewModel<Purveyor>> {

    override fun apply(purveyor: Purveyor) =
            SimpleViewModel(
                    purveyor.id,
                    purveyor.name,
                    purveyor
            )


}