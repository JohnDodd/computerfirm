package tusur.kkv.computerfirm.features.purveyor.edit

import dagger.Subcomponent
import tusur.kkv.computerfirm.injection.scope.FragmentScope


@FragmentScope
@Subcomponent(modules = [PurveyorEditFragmentModule::class])
interface PurveyorEditComponent {

    fun inject(fragment: PurveyorEditFragment)
}