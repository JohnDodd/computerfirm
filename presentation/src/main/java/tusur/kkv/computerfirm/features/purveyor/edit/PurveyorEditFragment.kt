package tusur.kkv.computerfirm.features.purveyor.edit

import android.os.Bundle
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.fragment_purveyor_edit.*
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.base.BaseScreenFragment
import tusur.kkv.computerfirm.base.NO_ID
import tusur.kkv.computerfirm.utils.onClick
import tusur.kkv.computerfirm.utils.showSnack
import tusur.kkv.computerfirm.utils.toStringOrEmpty

class PurveyorEditFragment : BaseScreenFragment<PurveyorEditComponent, PurveyorEditPresenter>() {

    override val viewId = R.layout.fragment_purveyor_edit

    override fun PurveyorEditComponent.onInject() = inject(this@PurveyorEditFragment)

    override fun createComponent() = baseActivity.component
            .createEditPurveyorComponent(PurveyorEditFragmentModule(this, viewPresenter))

    override fun bindView(view: View, savedInstanceState: Bundle?) {
        val purveyorId = arguments?.getLong(KEY_ID) ?: NO_ID
        presenter.onBindView(purveyorId)
        saveButton.onClick(presenter::onSaveButtonClicked)
    }

    private val viewPresenter = object : PurveyorEditPresenter.View {
        override val name: String
            get() = purveyor_name_text.text.toStringOrEmpty()
        override val phoneNumber: String
            get() = phone_text.text.toStringOrEmpty()
        override val address: String
            get() = address_text.text.toStringOrEmpty()
        override val representativeName: String
            get() = representative_text.text.toStringOrEmpty()

        override fun updateName(name: String) {
            purveyor_name_text.setText(name)
        }

        override fun updatePhoneNumber(phoneNumber: String) {
            phone_text.setText(phoneNumber)
        }

        override fun updateAddress(address: String) {
            address_text.setText(address)
        }

        override fun updateRepresentativeName(representativeName: String) {
            representative_text.setText(representativeName)
        }

        override fun showNameError(message: String) {
            purveyor_name_text.showError(message)
        }

        override fun showPhoneNumberError(message: String) {
            phone_text.showError(message)
        }

        override fun showAddressError(message: String) {
            address_text.showError(message)
        }

        override fun showRepresentativeNameError(message: String) {
            representative_text.showError(message)
        }

        override fun showSaveError() {
            view?.showSnack(getString(R.string.simple_error))
        }

        override fun closeView() {
            popBackStack()
        }

        private fun TextView.showError(message: String) {
            error = message
            scroll_view.scrollTo(0, this.bottom)
        }
    }

    companion object {
        const val KEY_ID = "KEY_ID"
    }
}