package tusur.kkv.computerfirm.features.purveyor.edit

import dagger.Module
import tusur.kkv.computerfirm.injection.module.FragmentModule

@Module
class PurveyorEditFragmentModule(fragment: PurveyorEditFragment, view: PurveyorEditPresenter.View)
    : FragmentModule<PurveyorEditFragment, PurveyorEditPresenter.View>(fragment, view)