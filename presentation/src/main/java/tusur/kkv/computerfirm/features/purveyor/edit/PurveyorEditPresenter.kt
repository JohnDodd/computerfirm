package tusur.kkv.computerfirm.features.purveyor.edit

import io.reactivex.subjects.BehaviorSubject
import tusur.kkv.computerfirm.base.BasePresenter
import tusur.kkv.computerfirm.base.BaseView
import tusur.kkv.computerfirm.base.NO_ID
import tusur.kkv.data.exception.IllegalParamsException
import tusur.kkv.domain.interactor.purveyor.RefreshPurveyorInteractor
import tusur.kkv.domain.interactor.purveyor.RequestPurveyorInteractor
import tusur.kkv.domain.interactor.purveyor.SendPurveyorInteractor
import tusur.kkv.domain.repository.IPurveyorRepository.ParamsExceptionId.Companion.ADDRESS
import tusur.kkv.domain.repository.IPurveyorRepository.ParamsExceptionId.Companion.NAME
import tusur.kkv.domain.repository.IPurveyorRepository.ParamsExceptionId.Companion.PHONE
import tusur.kkv.domain.repository.IPurveyorRepository.ParamsExceptionId.Companion.REPRESENTATIVE
import javax.inject.Inject

class PurveyorEditPresenter
@Inject constructor(private val request: RequestPurveyorInteractor,
                    private val send: SendPurveyorInteractor,
                    private val refresh: RefreshPurveyorInteractor,
                    private val mapper: PurveyorEditViewModelMapper,
                    private val view: PurveyorEditPresenter.View) : BasePresenter() {

    private var idSubject = BehaviorSubject.createDefault(NO_ID)

    fun onBindView(id: Long) {
        idSubject.onNext(id)
        if (id != NO_ID)
            request.getSingle(id).map(mapper).addSubscription {
                view.updateName(it.name)
                view.updatePhoneNumber(it.phoneNumber)
                view.updateAddress(it.address)
                view.updateRepresentativeName(it.representativeName)
            }
    }


    fun onSaveButtonClicked() {
        idSubject.firstOrError().addSubscription({ id ->
            if (id == NO_ID) addSendSubscription()
            else addRefreshSubscription(id)
        }) { view.showSaveError() }
    }

    private fun addSendSubscription() {
        send.getSingle(SendPurveyorInteractor.Params(
                view.name,
                view.phoneNumber,
                view.address,
                view.representativeName
        )).addSubscription({
            if (it) view.closeView()
            else view.showSaveError()
        }, showErrorConsumer)
    }

    private fun addRefreshSubscription(id: Long) {
        refresh.getRefreshSingle(RefreshPurveyorInteractor.Params(
                id,
                view.name,
                view.phoneNumber,
                view.address,
                view.representativeName
        )).addSubscription({ view.closeView() }, showErrorConsumer)
    }

    private val showErrorConsumer: (Throwable) -> Unit = {
        if (it is IllegalParamsException)
            when (it.id) {
                NAME -> view.showNameError(it.message)
                PHONE -> view.showPhoneNumberError(it.message)
                ADDRESS -> view.showAddressError(it.message)
                REPRESENTATIVE -> view.showRepresentativeNameError(it.message)
                else -> view.showSaveError()
            }
        else view.showSaveError()
    }


    interface View : BaseView {
        val name: String
        val phoneNumber: String
        val address: String
        val representativeName: String

        fun updateName(name: String)
        fun updatePhoneNumber(phoneNumber: String)
        fun updateAddress(address: String)
        fun updateRepresentativeName(representativeName: String)


        fun showNameError(message: String)
        fun showPhoneNumberError(message: String)
        fun showAddressError(message: String)
        fun showRepresentativeNameError(message: String)

        fun showSaveError()
        fun closeView()

    }
}