package tusur.kkv.computerfirm.features.purveyor.edit

data class PurveyorEditViewModel(val id: Long,
                                 val name: String,
                                 val phoneNumber: String,
                                 val address: String,
                                 val representativeName: String)