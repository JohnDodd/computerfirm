package tusur.kkv.computerfirm.features.purveyor.edit

import io.reactivex.functions.Function
import tusur.kkv.computerfirm.utils.toStringOrEmpty
import tusur.kkv.domain.model.Purveyor
import javax.inject.Inject

class PurveyorEditViewModelMapper
@Inject constructor() : Function<Purveyor, PurveyorEditViewModel> {
    override fun apply(purveyor: Purveyor) = PurveyorEditViewModel(
            purveyor.id,
            purveyor.name,
            purveyor.phoneNumber,
            purveyor.address,
            purveyor.representativeName.toStringOrEmpty()
    )

}