package tusur.kkv.computerfirm.features.purveyor.home

import dagger.Subcomponent
import tusur.kkv.computerfirm.injection.scope.FragmentScope


@FragmentScope
@Subcomponent(modules = [PurveyorHomeFragmentModule::class])
interface PurveyorHomeComponent {
    fun inject(fragment: PurveyorHomeFragment)
}