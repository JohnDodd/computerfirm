package tusur.kkv.computerfirm.features.purveyor.home

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import kotlinx.android.synthetic.main.fragment_purveyor_home.*
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.base.BaseScreenFragment
import tusur.kkv.computerfirm.utils.onClick
import javax.inject.Inject
import javax.inject.Provider

class PurveyorHomeFragment : BaseScreenFragment<PurveyorHomeComponent, PurveyorHomePresenter>() {

    override val viewId = R.layout.fragment_purveyor_home
    @Inject
    lateinit var adapter: RendererRecyclerViewAdapter
    @Inject
    lateinit var layoutManager: Provider<RecyclerView.LayoutManager>
    @Inject
    lateinit var decoration: Provider<RecyclerView.ItemDecoration>

    override val titleId = R.string.purveyors

    override fun PurveyorHomeComponent.onInject() = inject(this@PurveyorHomeFragment)

    override fun createComponent() = baseActivity.component
            .createPurveyorHomePurveyor(PurveyorHomeFragmentModule(this, viewPresenter))

    override fun bindView(view: View, savedInstanceState: Bundle?) {
        recyclerView.adapter = adapter
        recyclerView.layoutManager = layoutManager.get()
        recyclerView.addItemDecoration(decoration.get())
        fab.onClick(presenter::onFabClicked)
    }

    private val viewPresenter = object : PurveyorHomePresenter.View {
        override fun updateItems(items: List<ViewModel>) {
            adapter.setItems(items)
        }
    }
}