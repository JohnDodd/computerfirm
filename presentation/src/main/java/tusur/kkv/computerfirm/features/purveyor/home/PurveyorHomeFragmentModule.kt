package tusur.kkv.computerfirm.features.purveyor.home

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import dagger.Module
import dagger.Provides
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.base.BaseDiffCallback
import tusur.kkv.computerfirm.base.BetweenItemsDecoration
import tusur.kkv.computerfirm.features.purveyor.home.model.PurveyorHomeViewModel
import tusur.kkv.computerfirm.injection.module.FragmentModule
import tusur.kkv.computerfirm.utils.onClick
import tusur.kkv.computerfirm.utils.register
import tusur.kkv.data.injection.qualifier.ForActivity

@Module
class PurveyorHomeFragmentModule(fragment: PurveyorHomeFragment,
                                 view: PurveyorHomePresenter.View)
    : FragmentModule<PurveyorHomeFragment, PurveyorHomePresenter.View>(fragment, view) {

    @Provides
    fun provideAdapter(diffCallback: BaseDiffCallback) = RendererRecyclerViewAdapter().apply {
        register<PurveyorHomeViewModel>(R.layout.item_purveyor_home) { model ->
            setText(R.id.purveyor_name_text, model.name)
            setText(R.id.purveyor_phone_text, model.phone)
            onClick { fragment.presenter.onModelClicked(model) }
        }
        setDiffCallback(diffCallback)
    }

    @Provides
    fun provideLayoutManager(@ForActivity context: Context): RecyclerView.LayoutManager =
            LinearLayoutManager(context)

    @Provides
    fun provideDecoration(): RecyclerView.ItemDecoration = BetweenItemsDecoration()

}