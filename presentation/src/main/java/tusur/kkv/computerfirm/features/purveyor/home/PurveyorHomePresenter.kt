package tusur.kkv.computerfirm.features.purveyor.home

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import tusur.kkv.computerfirm.base.BasePresenter
import tusur.kkv.computerfirm.base.BaseView
import tusur.kkv.computerfirm.base.UIRouter
import tusur.kkv.computerfirm.features.purveyor.home.model.PurveyorHomeViewModel
import tusur.kkv.computerfirm.features.purveyor.home.model.PurveyorHomeViewModelMapper
import tusur.kkv.domain.interactor.purveyor.RetrievePurveyorInteractor
import tusur.kkv.domain.utils.forEachElement
import javax.inject.Inject

class PurveyorHomePresenter
@Inject constructor(private val view: PurveyorHomePresenter.View,
                    private val retrieve: RetrievePurveyorInteractor,
                    private val mapper: PurveyorHomeViewModelMapper,
                    private val uiRouter: UIRouter)
    : BasePresenter() {

    override fun onViewShown() {
        super.onViewShown()
        retrieve.getBehaviorStream()
                .forEachElement { it.map(mapper) }
                .addSubscription { view.updateItems(it) }
    }

    fun onFabClicked() {
        uiRouter.openPurveyorEditPage()
    }

    fun onModelClicked(model: PurveyorHomeViewModel) {
        uiRouter.openPurveyorEditPage(model.id)
    }

    interface View : BaseView {
        fun updateItems(items: List<ViewModel>)
    }


}