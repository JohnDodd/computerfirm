package tusur.kkv.computerfirm.features.purveyor.home.model

import tusur.kkv.computerfirm.base.BaseViewModel

class PurveyorHomeViewModel(override val id: Long,
                            val name: String,
                            val phone: String) : BaseViewModel(id)