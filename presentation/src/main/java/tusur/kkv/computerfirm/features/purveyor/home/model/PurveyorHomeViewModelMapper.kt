package tusur.kkv.computerfirm.features.purveyor.home.model

import io.reactivex.functions.Function
import tusur.kkv.domain.model.Purveyor
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PurveyorHomeViewModelMapper @Inject constructor()
    : Function<Purveyor, PurveyorHomeViewModel> {

    override fun apply(purveyor: Purveyor) = PurveyorHomeViewModel(
            purveyor.id,
            purveyor.name,
            purveyor.phoneNumber
    )

}