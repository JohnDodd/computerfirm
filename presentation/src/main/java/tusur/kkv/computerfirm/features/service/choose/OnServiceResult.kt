package tusur.kkv.computerfirm.features.service.choose

import tusur.kkv.domain.model.Service

interface OnServiceResult {
    fun onResult(vararg service: Service)
}