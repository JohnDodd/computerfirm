package tusur.kkv.computerfirm.features.service.choose

import dagger.Subcomponent
import tusur.kkv.computerfirm.injection.scope.FragmentScope

@FragmentScope
@Subcomponent(modules = [ServiceChooseDialogModule::class])
interface ServiceChooseComponent {

    fun inject(fragment: ServiceChooseDialog)
}