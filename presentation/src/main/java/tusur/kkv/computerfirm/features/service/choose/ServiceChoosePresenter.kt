package tusur.kkv.computerfirm.features.service.choose

import io.reactivex.Observable
import io.reactivex.rxkotlin.Observables
import io.reactivex.rxkotlin.withLatestFrom
import io.reactivex.subjects.BehaviorSubject.createDefault
import io.reactivex.subjects.PublishSubject.create
import tusur.kkv.computerfirm.base.NO_ID
import tusur.kkv.computerfirm.base.UIRouter
import tusur.kkv.computerfirm.features.simpledialog.AppendSimpleAddViewModel
import tusur.kkv.computerfirm.features.simpledialog.SimpleDialogPresenter
import tusur.kkv.domain.interactor.orderservice.RequestServicesByOrderInteractor
import tusur.kkv.domain.interactor.service.RetrieveServiceInteractor
import tusur.kkv.domain.model.Service
import javax.inject.Inject

class ServiceChoosePresenter
@Inject constructor(private val mapper: ServiceSimpleViewModelMapper,
                    private val retrieve: RetrieveServiceInteractor,
                    private val resultListener: OnServiceResult,
                    private val requestByOrder: RequestServicesByOrderInteractor,
                    view: SimpleDialogPresenter.SimpleDialogView,
                    appendMapper: AppendSimpleAddViewModel,
                    uiRouter: UIRouter) :
        SimpleDialogPresenter(view, appendMapper, uiRouter) {

    private val multiSelectSubject = createDefault(false)
    private val modelClickedSubject = create<Service>()
    private val selectedItems = mutableListOf<Service>()

    fun onBindView(multiSelect: Boolean, orderId: Long) {
        multiSelectSubject.onNext(multiSelect)
        if (orderId != NO_ID) {
            requestByOrder.getSingle(orderId).addSubscription({
                selectedItems.clear()
                selectedItems.addAll(it)
            }) {
                selectedItems.clear()
            }
        }
    }

    override fun onViewShown() {
        super.onViewShown()
        val selectedItemsStream = modelClickedSubject
                .withLatestFrom(multiSelectSubject)
                .scan(selectedItems) { list, pair ->
                    val service = pair.first
                    val multiSelect = pair.second
                    if (multiSelect && !list.remove(service)) list.add(service)
                    if (!multiSelect && !list.isEmpty()) list.clear()
                    list
                }

        Observables.combineLatest(retrieve.getBehaviorStream().startWith(listOf<Service>()),
                selectedItemsStream
        )
                .flatMapSingle { pair ->
                    val services = pair.first
                    val selected = pair.second
                    Observable.fromIterable(services)
                            .map { Pair(it, selected.contains(it)) }
                            .map(mapper)
                            .toList()

                }
                .map(appendMapper)
                .addSubscription(view::updateItems)


        modelClickedSubject.withLatestFrom(multiSelectSubject).addSubscription {
            if (!it.second) {
                resultListener.onResult(it.first)
                view.close()
            }
        }
    }

    fun onModelClicked(model: Service) {
        modelClickedSubject.onNext(model)
    }

    fun onAddClicked() {
        uiRouter.openServiceEditPage()
        view.close()
    }

    fun onLongClicked(model: Service) {
        uiRouter.openServiceEditPage(model.id)
        view.close()
    }

    fun onDismissed() {
        resultListener.onResult(*selectedItems.toTypedArray())
    }

}