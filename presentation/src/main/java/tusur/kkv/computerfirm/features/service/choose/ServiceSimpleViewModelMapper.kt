package tusur.kkv.computerfirm.features.service.choose

import io.reactivex.functions.Function
import tusur.kkv.computerfirm.features.simpledialog.SimpleViewModel
import tusur.kkv.domain.model.Service
import javax.inject.Inject

class ServiceSimpleViewModelMapper
@Inject constructor() : Function<Pair<Service, Boolean>, SimpleViewModel<Service>> {
    override fun apply(pair: Pair<Service, Boolean>): SimpleViewModel<Service> {
        val service = pair.first
        val isSelected = pair.second
        return SimpleViewModel(
                service.id,
                service.name,
                service,
                isSelected
        )
    }
}