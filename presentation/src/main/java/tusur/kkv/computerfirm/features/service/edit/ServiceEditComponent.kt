package tusur.kkv.computerfirm.features.service.edit

import dagger.Subcomponent
import tusur.kkv.computerfirm.injection.scope.FragmentScope


@FragmentScope
@Subcomponent(modules = [ServiceEditFragmentModule::class])
interface ServiceEditComponent {

    fun inject(fragment: ServiceEditFragment)

}