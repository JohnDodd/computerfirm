package tusur.kkv.computerfirm.features.service.edit

import android.os.Bundle
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.fragment_service_edit.*
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.base.BaseScreenFragment
import tusur.kkv.computerfirm.base.NO_ID
import tusur.kkv.computerfirm.utils.onClick
import tusur.kkv.computerfirm.utils.showSnack
import tusur.kkv.computerfirm.utils.toLongOrElse
import tusur.kkv.computerfirm.utils.toStringOrEmpty

class ServiceEditFragment : BaseScreenFragment<ServiceEditComponent, ServiceEditPresenter>() {

    override val viewId = R.layout.fragment_service_edit

    override fun ServiceEditComponent.onInject() = inject(this@ServiceEditFragment)

    override fun createComponent() = baseActivity.component
            .createEditServiceComponent(ServiceEditFragmentModule(this, viewPresenter))

    override fun bindView(view: View, savedInstanceState: Bundle?) {
        val serviceId = arguments?.getLong(KEY_ID) ?: NO_ID
        presenter.onBindView(serviceId)
        saveButton.onClick(presenter::onSaveButtonClicked)
    }

    private val viewPresenter = object : ServiceEditPresenter.View {
        override val name: String
            get() = service_name_text.text.toStringOrEmpty()
        override val cost: Long
            get() = cost_text.text.toStringOrEmpty().toLongOrElse(NO_ID)
        override val description: String?
            get() = description_text.text?.let { if (it.isBlank()) null else it.toString() }

        override fun updateName(name: String) {
            service_name_text.setText(name)
        }

        override fun updateCost(cost: String) {
            cost_text.setText(cost)
        }

        override fun updateDescription(description: String) {
            description_text.setText(description)
        }

        override fun showNameError(message: String) {
            service_name_text.showError(message)
        }

        override fun showCostError(message: String) {
            cost_text.showError(message)
        }

        override fun showDescriptionError(message: String) {
            description_text.showError(message)
        }

        override fun showSaveError() {
            view?.showSnack(getString(R.string.simple_error))
        }

        override fun closeView() {
            popBackStack()
        }

        private fun TextView.showError(message: String) {
            error = message
            scroll_view.scrollTo(0, this.bottom)
        }

    }

    companion object {
        const val KEY_ID = "KEY_ID"
    }
}
