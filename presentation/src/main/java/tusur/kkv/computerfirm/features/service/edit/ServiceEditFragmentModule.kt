package tusur.kkv.computerfirm.features.service.edit

import dagger.Module
import tusur.kkv.computerfirm.injection.module.FragmentModule

@Module
class ServiceEditFragmentModule(fragment: ServiceEditFragment, view: ServiceEditPresenter.View)
    : FragmentModule<ServiceEditFragment, ServiceEditPresenter.View>(fragment, view)