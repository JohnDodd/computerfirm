package tusur.kkv.computerfirm.features.service.edit

import io.reactivex.subjects.BehaviorSubject
import tusur.kkv.computerfirm.base.BasePresenter
import tusur.kkv.computerfirm.base.BaseView
import tusur.kkv.computerfirm.base.NO_ID
import tusur.kkv.data.exception.IllegalParamsException
import tusur.kkv.domain.interactor.service.RefreshServiceInteractor
import tusur.kkv.domain.interactor.service.RequestServiceInteractor
import tusur.kkv.domain.interactor.service.SendServiceInteractor
import tusur.kkv.domain.repository.IServiceRepository.ParamsExceptionId.Companion.COST
import tusur.kkv.domain.repository.IServiceRepository.ParamsExceptionId.Companion.DESCRIPTION
import tusur.kkv.domain.repository.IServiceRepository.ParamsExceptionId.Companion.NAME
import javax.inject.Inject

class ServiceEditPresenter
@Inject constructor(private val request: RequestServiceInteractor,
                    private val send: SendServiceInteractor,
                    private val refresh: RefreshServiceInteractor,
                    private val mapper: ServiceEditViewModelMapper,
                    private val view: ServiceEditPresenter.View) : BasePresenter() {
    private var idSubject = BehaviorSubject.createDefault(NO_ID)

    fun onBindView(id: Long) {
        idSubject.onNext(id)
        if (id != NO_ID)
            request.getSingle(id).map(mapper).addSubscription {
                view.updateName(it.name)
                view.updateCost(it.cost)
                view.updateDescription(it.description)
            }
    }

    fun onSaveButtonClicked() {
        idSubject.firstOrError().addSubscription({ id ->
            if (id == NO_ID) addSendSubscription()
            else addRefreshSubscription(id)
        }) { view.showSaveError() }
    }

    private fun addSendSubscription() {
        send.getSingle(SendServiceInteractor.Params(
                view.name,
                view.cost,
                view.description
        )).addSubscription({
            if (it) view.closeView()
            else view.showSaveError()
        }, showErrorConsumer)
    }

    private fun addRefreshSubscription(id: Long) {
        refresh.getRefreshSingle(RefreshServiceInteractor.Params(
                id,
                view.name,
                view.cost,
                view.description
        )).addSubscription({ view.closeView() }, showErrorConsumer)
    }

    private val showErrorConsumer: (Throwable) -> Unit = {
        if (it is IllegalParamsException)
            when (it.id) {
                NAME -> view.showNameError(it.message)
                COST -> view.showCostError(it.message)
                DESCRIPTION -> view.showDescriptionError(it.message)
                else -> view.showSaveError()
            }
        else view.showSaveError()
    }

    interface View : BaseView {
        val name: String
        val cost: Long
        val description: String?

        fun updateName(name: String)
        fun updateCost(cost: String)
        fun updateDescription(description: String)

        fun showNameError(message: String)
        fun showCostError(message: String)
        fun showDescriptionError(message: String)

        fun showSaveError()
        fun closeView()
    }

}