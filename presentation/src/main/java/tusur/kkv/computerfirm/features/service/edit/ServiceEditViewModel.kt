package tusur.kkv.computerfirm.features.service.edit

data class ServiceEditViewModel(val id: Long,
                                val name: String,
                                val cost: String,
                                val description: String)