package tusur.kkv.computerfirm.features.service.edit

import io.reactivex.functions.Function
import tusur.kkv.computerfirm.utils.toStringOrEmpty
import tusur.kkv.domain.model.Service
import javax.inject.Inject

class ServiceEditViewModelMapper
@Inject constructor() : Function<Service, ServiceEditViewModel> {
    override fun apply(service: Service) = ServiceEditViewModel(
            service.id,
            service.name,
            service.cost.toString(),
            service.description.toStringOrEmpty()
    )
}