package tusur.kkv.computerfirm.features.service.home

import dagger.Subcomponent
import tusur.kkv.computerfirm.injection.scope.FragmentScope

@FragmentScope
@Subcomponent(modules = [ServiceHomeFragmentModule::class])
interface ServiceHomeComponent {

    fun inject(fragment: ServiceHomeFragment)

}