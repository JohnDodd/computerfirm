package tusur.kkv.computerfirm.features.service.home

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import kotlinx.android.synthetic.main.fragment_service_home.*
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.base.BaseScreenFragment
import tusur.kkv.computerfirm.utils.onClick
import javax.inject.Inject
import javax.inject.Provider

class ServiceHomeFragment : BaseScreenFragment<ServiceHomeComponent, ServiceHomePresenter>() {

    override val viewId = R.layout.fragment_service_home

    @Inject
    lateinit var adapter: RendererRecyclerViewAdapter
    @Inject
    lateinit var layoutManager: Provider<RecyclerView.LayoutManager>
    @Inject
    lateinit var decoration: Provider<RecyclerView.ItemDecoration>

    override val titleId = R.string.services

    override fun ServiceHomeComponent.onInject() = inject(this@ServiceHomeFragment)

    override fun createComponent() = baseActivity.component
            .createServicesComponent(ServiceHomeFragmentModule(this, viewPresenter))

    override fun bindView(view: View, savedInstanceState: Bundle?) {
        recyclerView.adapter = adapter
        recyclerView.layoutManager = layoutManager.get()
        recyclerView.addItemDecoration(decoration.get())
        fab.onClick(presenter::onFabClicked)
    }

    private val viewPresenter = object : ServiceHomePresenter.View {
        override fun updateItems(items: List<ViewModel>) {
            adapter.setItems(items)
        }
    }

}
