package tusur.kkv.computerfirm.features.service.home

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import dagger.Module
import dagger.Provides
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.base.BaseDiffCallback
import tusur.kkv.computerfirm.base.BetweenItemsDecoration
import tusur.kkv.computerfirm.features.service.home.model.ServiceBestHomeViewModel
import tusur.kkv.computerfirm.features.service.home.model.ServiceHomeViewModel
import tusur.kkv.computerfirm.injection.module.FragmentModule
import tusur.kkv.computerfirm.utils.onClick
import tusur.kkv.computerfirm.utils.register
import tusur.kkv.data.injection.qualifier.ForActivity

@Module
class ServiceHomeFragmentModule(fragment: ServiceHomeFragment,
                                view: ServiceHomePresenter.View)
    : FragmentModule<ServiceHomeFragment, ServiceHomePresenter.View>(fragment, view) {

    @Provides
    fun provideAdapter(diffCallback: BaseDiffCallback) = RendererRecyclerViewAdapter().apply {
        register<ServiceHomeViewModel>(R.layout.item_service_home) { model ->
            setText(R.id.service_name_text, model.name)
            setText(R.id.service_cost_text, model.cost)
            onClick { fragment.presenter.onModelClicked(model) }
        }

        register<ServiceBestHomeViewModel>(R.layout.item_best_service) { model ->
            setText(R.id.best_service_name, model.name)
            setText(R.id.best_service_cost, model.cost)
            setText(R.id.best_service_revenue, model.revenue)
            onClick { fragment.presenter.onModelClicked(model) }
        }
        setDiffCallback(diffCallback)
    }

    @Provides
    fun provideLayoutManager(@ForActivity context: Context): RecyclerView.LayoutManager =
            LinearLayoutManager(context)

    @Provides
    fun provideDecoration(): RecyclerView.ItemDecoration = BetweenItemsDecoration()


}