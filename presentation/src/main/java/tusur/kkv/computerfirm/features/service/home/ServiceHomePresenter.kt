package tusur.kkv.computerfirm.features.service.home

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import tusur.kkv.computerfirm.base.BasePresenter
import tusur.kkv.computerfirm.base.BaseView
import tusur.kkv.computerfirm.base.MenuId
import tusur.kkv.computerfirm.base.UIRouter
import tusur.kkv.computerfirm.controller.OptionsMenuController
import tusur.kkv.computerfirm.features.service.home.model.ServiceBestHomeViewModel
import tusur.kkv.computerfirm.features.service.home.model.ServiceBestHomeViewModelMapper
import tusur.kkv.computerfirm.features.service.home.model.ServiceHomeViewModel
import tusur.kkv.computerfirm.features.service.home.model.ServiceHomeViewModelMapper
import tusur.kkv.domain.interactor.service.RequestBestServicesInteractor
import tusur.kkv.domain.interactor.service.RetrieveServiceInteractor
import tusur.kkv.domain.utils.forEachElement
import javax.inject.Inject

class ServiceHomePresenter
@Inject constructor(private val view: View,
                    private val retrieve: RetrieveServiceInteractor,
                    private val requestBest: RequestBestServicesInteractor,
                    private val mapper: ServiceHomeViewModelMapper,
                    private val bestMapper: ServiceBestHomeViewModelMapper,
                    private val optionsMenuController: OptionsMenuController,
                    private val uiRouter: UIRouter)
    : BasePresenter() {

    override fun onViewShown() {
        super.onViewShown()
        retrieve.getBehaviorStream()
                .forEachElement { it.map(mapper) }
                .addSubscription { view.updateItems(it) }
        optionsMenuController.onMenuItemSelectedStream.addSubscription(::onMenuItemSelected)
    }

    private fun onMenuItemSelected(menuId: MenuId) {
        when (menuId) {
            MenuId.BEST -> requestBest.getSingle(5)
                    .forEachElement { it.map(bestMapper) }
                    .addSubscription { view.updateItems(it) }
        }
    }

    fun onFabClicked() {
        uiRouter.openServiceEditPage()
    }

    fun onModelClicked(model: ServiceHomeViewModel) {
        uiRouter.openServiceEditPage(model.id)
    }

    fun onModelClicked(model: ServiceBestHomeViewModel) {
        uiRouter.openServiceEditPage(model.id)
    }

    override fun onBindOptionsMenu() {
        super.onBindOptionsMenu()
        optionsMenuController.setMenuItems(MenuId.BEST)
    }

    interface View : BaseView {
        fun updateItems(items: List<ViewModel>)
    }
}