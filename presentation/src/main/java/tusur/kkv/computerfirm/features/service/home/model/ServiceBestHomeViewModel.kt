package tusur.kkv.computerfirm.features.service.home.model

import tusur.kkv.computerfirm.base.BaseViewModel

class ServiceBestHomeViewModel(override val id: Long,
                               val name: String,
                               val cost: String,
                               val revenue: String)
    : BaseViewModel(id)