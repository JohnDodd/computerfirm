package tusur.kkv.computerfirm.features.service.home.model

import android.content.Context
import io.reactivex.functions.Function
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.mapper.MoneyMapper
import tusur.kkv.data.injection.qualifier.ForApplication
import tusur.kkv.domain.model.BestService
import javax.inject.Inject

class ServiceBestHomeViewModelMapper @Inject constructor(@ForApplication
                                                         private val context: Context,
                                                         private val moneyMapper: MoneyMapper)
    : Function<BestService, ServiceBestHomeViewModel> {

    override fun apply(best: BestService) = ServiceBestHomeViewModel(
            best.id,
            best.name,
            moneyMapper.apply(best.cost),
            context.getString(R.string.best_service_revenue, moneyMapper.apply(best.revenue))
    )

}