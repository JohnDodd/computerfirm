package tusur.kkv.computerfirm.features.service.home.model

import tusur.kkv.computerfirm.base.BaseViewModel

data class ServiceHomeViewModel(override val id: Long,
                                val name: String,
                                val cost: String)
    : BaseViewModel(id)