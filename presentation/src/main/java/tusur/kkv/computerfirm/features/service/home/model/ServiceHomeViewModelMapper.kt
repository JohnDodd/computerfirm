package tusur.kkv.computerfirm.features.service.home.model

import io.reactivex.functions.Function
import tusur.kkv.computerfirm.mapper.MoneyMapper
import tusur.kkv.domain.model.Service
import javax.inject.Inject

class ServiceHomeViewModelMapper @Inject constructor(private val moneyMapper: MoneyMapper)
    : Function<Service, ServiceHomeViewModel> {

    override fun apply(service: Service) = ServiceHomeViewModel(
            service.id,
            service.name,
            moneyMapper.apply(service.cost)
    )
}