package tusur.kkv.computerfirm.features.simpledialog

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import io.reactivex.functions.Function
import javax.inject.Inject

class AppendSimpleAddViewModel
@Inject constructor() : Function<List<ViewModel>, List<ViewModel>> {

    override fun apply(models: List<ViewModel>) = mutableListOf<ViewModel>().apply {
        addAll(models)
        add(SimpleAddModel())
    }

}