package tusur.kkv.computerfirm.features.simpledialog

import tusur.kkv.computerfirm.base.BaseViewModel

data class SimpleAddModel(override val id: Long = 0) : BaseViewModel(id)