package tusur.kkv.computerfirm.features.simpledialog

import tusur.kkv.computerfirm.base.BaseViewModel

data class SimpleClearModel(override val id: Long = 0) : BaseViewModel(id)