package tusur.kkv.computerfirm.features.simpledialog

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import kotlinx.android.synthetic.main.fragment_employee_home.*
import tusur.kkv.computerfirm.R
import tusur.kkv.computerfirm.base.BaseInjectingDialogFragment
import tusur.kkv.computerfirm.base.BasePresenter
import javax.inject.Inject
import javax.inject.Provider

abstract class SimpleDialog<Component, Presenter : BasePresenter> : BaseInjectingDialogFragment<Component, Presenter>() {

    @Inject
    lateinit var adapter: RendererRecyclerViewAdapter
    @Inject
    lateinit var layoutManager: Provider<RecyclerView.LayoutManager>

    override val viewId = R.layout.dialog_simple

    override fun bindView(view: View,
                          savedInstanceState: Bundle?,
                          dialog: AlertDialog) {
        recyclerView.adapter = adapter
        recyclerView.layoutManager = layoutManager.get()
    }

    protected val simpleViewPresenter = object : SimpleDialogPresenter.SimpleDialogView {
        override fun close() {
            dismiss()
        }

        override fun updateItems(items: List<ViewModel>) {
            adapter.setItems(items)
        }
    }

}