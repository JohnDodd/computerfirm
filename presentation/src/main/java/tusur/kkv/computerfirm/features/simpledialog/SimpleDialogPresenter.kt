package tusur.kkv.computerfirm.features.simpledialog

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import tusur.kkv.computerfirm.base.BasePresenter
import tusur.kkv.computerfirm.base.BaseView
import tusur.kkv.computerfirm.base.UIRouter

abstract class SimpleDialogPresenter(protected val view: SimpleDialogView,
                                     protected val appendMapper: AppendSimpleAddViewModel,
                                     protected val uiRouter: UIRouter) : BasePresenter() {


    interface SimpleDialogView : BaseView {
        fun close()
        fun updateItems(items: List<ViewModel>)
    }
}