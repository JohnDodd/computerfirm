package tusur.kkv.computerfirm.features.simpledialog

import tusur.kkv.computerfirm.base.BaseViewModel

data class SimpleViewModel<M>(override val id: Long,
                              val text: String,
                              val model: M,
                              val selected: Boolean = false) : BaseViewModel(id)