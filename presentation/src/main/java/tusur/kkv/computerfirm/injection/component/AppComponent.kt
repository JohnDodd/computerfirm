package tusur.kkv.computerfirm.injection.component

import dagger.BindsInstance
import dagger.Component
import tusur.kkv.computerfirm.base.AppMain
import tusur.kkv.computerfirm.injection.module.ActivityModule
import tusur.kkv.computerfirm.injection.module.AppModule
import tusur.kkv.computerfirm.injection.module.DataModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, DataModule::class])
interface AppComponent {

    fun inject(app: AppMain)

    fun createMainActivityComponent(activityModule: ActivityModule): MainActivityComponent

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun app(app: AppMain): Builder

        fun build(): AppComponent
    }

}