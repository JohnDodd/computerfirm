package tusur.kkv.computerfirm.injection.component

import dagger.Subcomponent
import tusur.kkv.computerfirm.features.component.choose.ComponentChooseComponent
import tusur.kkv.computerfirm.features.component.choose.ComponentChooseDialogModule
import tusur.kkv.computerfirm.features.component.edit.ComponentEditComponent
import tusur.kkv.computerfirm.features.component.edit.ComponentEditFragmentModule
import tusur.kkv.computerfirm.features.component.home.ComponentHomeComponent
import tusur.kkv.computerfirm.features.component.home.ComponentHomeFragmentModule
import tusur.kkv.computerfirm.features.componenttype.choose.ComponentTypeChooseComponent
import tusur.kkv.computerfirm.features.componenttype.choose.ComponentTypeChooseDialogModule
import tusur.kkv.computerfirm.features.componenttype.edit.ComponentTypeEditComponent
import tusur.kkv.computerfirm.features.componenttype.edit.ComponentTypeEditFragmentModule
import tusur.kkv.computerfirm.features.componenttype.home.ComponentTypeHomeComponent
import tusur.kkv.computerfirm.features.componenttype.home.ComponentTypeHomeFragmentModule
import tusur.kkv.computerfirm.features.customer.choose.CustomerChooseComponent
import tusur.kkv.computerfirm.features.customer.choose.CustomerChooseDialogModule
import tusur.kkv.computerfirm.features.customer.edit.CustomerEditComponent
import tusur.kkv.computerfirm.features.customer.edit.CustomerEditFragmentModule
import tusur.kkv.computerfirm.features.customer.home.CustomerHomeComponent
import tusur.kkv.computerfirm.features.customer.home.CustomerHomeFragmentModule
import tusur.kkv.computerfirm.features.employee.choose.EmployeeChooseComponent
import tusur.kkv.computerfirm.features.employee.choose.EmployeeChooseDialogModule
import tusur.kkv.computerfirm.features.employee.edit.EmployeeEditComponent
import tusur.kkv.computerfirm.features.employee.edit.EmployeeEditFragmentModule
import tusur.kkv.computerfirm.features.employee.home.EmployeeHomeComponent
import tusur.kkv.computerfirm.features.employee.home.EmployeeHomeFragmentModule
import tusur.kkv.computerfirm.features.gender.choose.GenderChooseComponent
import tusur.kkv.computerfirm.features.gender.choose.GenderChooseDialogModule
import tusur.kkv.computerfirm.features.main.MainActivity
import tusur.kkv.computerfirm.features.order.edit.OrderEditComponent
import tusur.kkv.computerfirm.features.order.edit.OrderEditFragmentModule
import tusur.kkv.computerfirm.features.order.home.OrderHomeComponent
import tusur.kkv.computerfirm.features.order.home.OrderHomeFragmentModule
import tusur.kkv.computerfirm.features.position.choose.PositionChooseComponent
import tusur.kkv.computerfirm.features.position.choose.PositionChooseDialogModule
import tusur.kkv.computerfirm.features.position.edit.PositionEditComponent
import tusur.kkv.computerfirm.features.position.edit.PositionEditFragmentModule
import tusur.kkv.computerfirm.features.position.home.PositionHomeComponent
import tusur.kkv.computerfirm.features.position.home.PositionHomeFragmentModule
import tusur.kkv.computerfirm.features.purveyor.choose.PurveyorChooseComponent
import tusur.kkv.computerfirm.features.purveyor.choose.PurveyorChooseDialogModule
import tusur.kkv.computerfirm.features.purveyor.edit.PurveyorEditComponent
import tusur.kkv.computerfirm.features.purveyor.edit.PurveyorEditFragmentModule
import tusur.kkv.computerfirm.features.purveyor.home.PurveyorHomeComponent
import tusur.kkv.computerfirm.features.purveyor.home.PurveyorHomeFragmentModule
import tusur.kkv.computerfirm.features.service.choose.ServiceChooseComponent
import tusur.kkv.computerfirm.features.service.choose.ServiceChooseDialogModule
import tusur.kkv.computerfirm.features.service.edit.ServiceEditComponent
import tusur.kkv.computerfirm.features.service.edit.ServiceEditFragmentModule
import tusur.kkv.computerfirm.features.service.home.ServiceHomeComponent
import tusur.kkv.computerfirm.features.service.home.ServiceHomeFragmentModule
import tusur.kkv.computerfirm.injection.module.ActivityModule
import tusur.kkv.computerfirm.injection.scope.ActivityScope

@ActivityScope
@Subcomponent(modules = [ActivityModule::class])
interface MainActivityComponent {

    fun inject(activity: MainActivity)

    fun createEmployeesComponent(fragmentModule: EmployeeHomeFragmentModule): EmployeeHomeComponent

    fun createComponentHomeComponent(fragmentModule: ComponentHomeFragmentModule): ComponentHomeComponent

    fun createEditEmployeeFragmentComponent(fragmentModule: EmployeeEditFragmentModule): EmployeeEditComponent

    fun createEditPositionComponent(fragmentModule: PositionEditFragmentModule): PositionEditComponent

    fun createGenderDialogComponent(fragmentModule: GenderChooseDialogModule): GenderChooseComponent

    fun createPositionsComponent(fragmentModule: PositionHomeFragmentModule): PositionHomeComponent

    fun createPositionChooseDialogComponent(fragmentModule: PositionChooseDialogModule): PositionChooseComponent

    fun createComponentEditComponent(fragmentModule: ComponentEditFragmentModule): ComponentEditComponent

    fun createComponentTypeChooseDialogComponent(fragmentModule: ComponentTypeChooseDialogModule): ComponentTypeChooseComponent

    fun createComponentTypeEditComponent(fragmentModule: ComponentTypeEditFragmentModule): ComponentTypeEditComponent

    fun createPurveyorChooseDialogComponent(fragmentModule: PurveyorChooseDialogModule): PurveyorChooseComponent

    fun createEditPurveyorComponent(fragmentModule: PurveyorEditFragmentModule): PurveyorEditComponent

    fun createCustomerChooseDialogComponent(dialogModule: CustomerChooseDialogModule): CustomerChooseComponent

    fun createEditCustomerComponent(fragmentModule: CustomerEditFragmentModule): CustomerEditComponent

    fun createEditServiceComponent(fragmentModule: ServiceEditFragmentModule): ServiceEditComponent

    fun createServiceChooseDialogComponent(dialogModule: ServiceChooseDialogModule): ServiceChooseComponent

    fun createEditOrderFragmentComponent(fragmentModule: OrderEditFragmentModule): OrderEditComponent

    fun createEmployeeChooseDialogComponent(dialogModule: EmployeeChooseDialogModule): EmployeeChooseComponent

    fun createOrdersComponent(fragmentModule: OrderHomeFragmentModule): OrderHomeComponent

    fun createComponentChooseDialogComponent(dialogModule: ComponentChooseDialogModule): ComponentChooseComponent

    fun createCustomersComponent(fragmentModule: CustomerHomeFragmentModule): CustomerHomeComponent

    fun createServicesComponent(fragmentModule: ServiceHomeFragmentModule): ServiceHomeComponent

    fun createPurveyorHomePurveyor(fragmentModule: PurveyorHomeFragmentModule): PurveyorHomeComponent

    fun createComponentTypeHomeComponentType(fragmentModule: ComponentTypeHomeFragmentModule): ComponentTypeHomeComponent
}
