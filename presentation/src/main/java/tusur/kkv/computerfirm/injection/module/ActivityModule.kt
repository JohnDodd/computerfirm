package tusur.kkv.computerfirm.injection.module

import android.app.Activity
import android.content.Context
import dagger.Module
import dagger.Provides
import tusur.kkv.data.injection.qualifier.ForActivity

@Module
class ActivityModule(private val activity: Activity) {

    @Provides
    fun provideActivity(): Activity = activity

    @Provides
    @ForActivity
    fun provideContext(): Context = activity

//    @Provides
//    fun provideOptionsMenuController(activity: Activity) = OptionsMenuController(activity)

//    @Provides
//    fun provideUiRouter(@ForActivity context: Context) = UIRouter(context)


}