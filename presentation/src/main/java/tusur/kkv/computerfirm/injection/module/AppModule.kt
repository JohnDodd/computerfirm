package tusur.kkv.computerfirm.injection.module

import dagger.Module
import dagger.Provides
import tusur.kkv.computerfirm.base.AppMain
import tusur.kkv.data.injection.qualifier.ForApplication
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    @ForApplication
    fun provideAppContext(app: AppMain) = app.applicationContext
}