package tusur.kkv.computerfirm.injection.module

import com.annimon.stream.Optional
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import tusur.kkv.data.base.TimestampProvider
import tusur.kkv.data.cache.Cache
import tusur.kkv.data.database.diskservice.component.ComponentDiskService
import tusur.kkv.data.database.mapper.ComponentRawFactory
import tusur.kkv.data.database.mapper.RawFactory
import tusur.kkv.data.exception.typeformatter.ExceptionTypeFormatter
import tusur.kkv.data.injection.qualifier.ForComputation
import tusur.kkv.data.mapper.ComponentMapper
import tusur.kkv.data.model.ComponentRaw
import tusur.kkv.data.repository.ComponentRepository
import tusur.kkv.data.store.MemoryReactiveStore
import tusur.kkv.data.store.ReactiveStore
import tusur.kkv.data.store.Store
import tusur.kkv.domain.model.Component
import tusur.kkv.domain.repository.IComponentRepository
import tusur.kkv.domain.repository.IComponentTypeRepository
import tusur.kkv.domain.repository.IPurveyorRepository
import javax.inject.Singleton

@Module
class ComponentDataModule {

    @Provides
    @Singleton
    fun provideRepository(componentTypeRepository: IComponentTypeRepository,
                          purveyorRepository: IPurveyorRepository,
                          exceptionTypeFormatter: ExceptionTypeFormatter,
                          mapper: ComponentMapper,
                          diskService: ComponentDiskService,
                          memoryStore: ReactiveStore<Long, Component>): IComponentRepository =
            ComponentRepository(componentTypeRepository,
                    purveyorRepository,
                    exceptionTypeFormatter,
                    mapper,
                    diskService,
                    memoryStore)

    @Provides
    @Singleton
    fun provideRawFactory(): RawFactory<ComponentRaw> = ComponentRawFactory()


    @Provides
    @Singleton
    fun provideMemoryReactiveStore(cache: Store.MemoryStore<Long, Component>,
                                   @ForComputation
                                   schedulerComputation: Scheduler): ReactiveStore<Long, Component> =
            MemoryReactiveStore(cache, { it.id }, schedulerComputation)


    @Provides
    @Singleton
    fun provideMemoryStore(timestampProvider: TimestampProvider,
                           @ForComputation
                           schedulerComputation: Scheduler): Store.MemoryStore<Long, Component> =
            Cache(timestampProvider, { it.id }, Optional.empty(), schedulerComputation)


}