package tusur.kkv.computerfirm.injection.module

import com.annimon.stream.Optional
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import tusur.kkv.data.base.TimestampProvider
import tusur.kkv.data.cache.Cache
import tusur.kkv.data.database.diskservice.componenttype.ComponentTypeDiskService
import tusur.kkv.data.database.mapper.ComponentTypeRawFactory
import tusur.kkv.data.database.mapper.RawFactory
import tusur.kkv.data.exception.typeformatter.ExceptionTypeFormatter
import tusur.kkv.data.injection.qualifier.ForComputation
import tusur.kkv.data.mapper.ComponentTypeMapper
import tusur.kkv.data.model.ComponentTypeRaw
import tusur.kkv.data.repository.ComponentTypeRepository
import tusur.kkv.data.store.MemoryReactiveStore
import tusur.kkv.data.store.ReactiveStore
import tusur.kkv.data.store.Store
import tusur.kkv.domain.model.ComponentType
import tusur.kkv.domain.repository.IComponentTypeRepository
import javax.inject.Singleton

@Module
class ComponentTypeDataModule {

    @Provides
    @Singleton
    fun provideRepository(diskService: ComponentTypeDiskService,
                          memoryStore: ReactiveStore<Long, ComponentType>,
                          mapper: ComponentTypeMapper,
                          exceptionTypeFormatter: ExceptionTypeFormatter): IComponentTypeRepository =
            ComponentTypeRepository(diskService, memoryStore, mapper, exceptionTypeFormatter)

    @Provides
    @Singleton
    fun provideRawFactory(): RawFactory<ComponentTypeRaw> = ComponentTypeRawFactory()

    @Provides
    @Singleton
    fun provideMemoryReactiveStore(cache: Store.MemoryStore<Long, ComponentType>,
                                   @ForComputation
                                   schedulerComputation: Scheduler): ReactiveStore<Long, ComponentType> =
            MemoryReactiveStore(cache, { it.id }, schedulerComputation)


    @Provides
    @Singleton
    fun provideMemoryStore(timestampProvider: TimestampProvider,
                           @ForComputation
                           schedulerComputation: Scheduler): Store.MemoryStore<Long, ComponentType> =
            Cache(timestampProvider, { it.id }, Optional.empty(), schedulerComputation)


}