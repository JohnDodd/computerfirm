package tusur.kkv.computerfirm.injection.module

import com.annimon.stream.Optional
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import tusur.kkv.data.base.TimestampProvider
import tusur.kkv.data.cache.Cache
import tusur.kkv.data.database.diskservice.customer.CustomerDiskService
import tusur.kkv.data.database.mapper.CustomerRawFactory
import tusur.kkv.data.database.mapper.RawFactory
import tusur.kkv.data.exception.typeformatter.ExceptionTypeFormatter
import tusur.kkv.data.injection.qualifier.ForComputation
import tusur.kkv.data.mapper.CustomerMapper
import tusur.kkv.data.model.CustomerRaw
import tusur.kkv.data.repository.CustomerRepository
import tusur.kkv.data.store.MemoryReactiveStore
import tusur.kkv.data.store.ReactiveStore
import tusur.kkv.data.store.Store
import tusur.kkv.domain.model.Customer
import tusur.kkv.domain.repository.ICustomerRepository
import javax.inject.Singleton

@Module
class CustomerDataModule {

    @Provides
    @Singleton
    fun provideRepository(diskService: CustomerDiskService,
                          memoryStore: ReactiveStore<Long, Customer>,
                          mapper: CustomerMapper,
                          exceptionTypeFormatter: ExceptionTypeFormatter): ICustomerRepository =
            CustomerRepository(diskService, memoryStore, mapper, exceptionTypeFormatter)


    @Provides
    @Singleton
    fun provideRawFactory(): RawFactory<CustomerRaw> = CustomerRawFactory()


    @Provides
    @Singleton
    fun provideMemoryReactiveStore(cache: Store.MemoryStore<Long, Customer>,
                                   @ForComputation
                                   schedulerComputation: Scheduler): ReactiveStore<Long, Customer> =
            MemoryReactiveStore(cache, { it.id }, schedulerComputation)


    @Provides
    @Singleton
    fun provideMemoryStore(timestampProvider: TimestampProvider,
                           @ForComputation
                           schedulerComputation: Scheduler): Store.MemoryStore<Long, Customer> =
            Cache(timestampProvider, { it.id }, Optional.empty(), schedulerComputation)

}