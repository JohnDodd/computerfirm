package tusur.kkv.computerfirm.injection.module

import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import tusur.kkv.data.base.TimestampProvider
import tusur.kkv.data.database.base.DatabaseGateway
import tusur.kkv.data.database.base.IQueryBuilder
import tusur.kkv.data.database.base.Queryable
import tusur.kkv.data.database.base.Tables
import tusur.kkv.data.database.diskservice.component.ComponentDiskService
import tusur.kkv.data.database.diskservice.componenttype.ComponentTypeDiskService
import tusur.kkv.data.database.diskservice.customer.CustomerDiskService
import tusur.kkv.data.database.diskservice.employee.EmployeeDiskService
import tusur.kkv.data.database.diskservice.order.OrderDiskService
import tusur.kkv.data.database.diskservice.ordercomponentjoin.OrderComponentJoinDiskService
import tusur.kkv.data.database.diskservice.orderservicejoin.OrderServiceJoinDiskService
import tusur.kkv.data.database.diskservice.position.PositionDiskService
import tusur.kkv.data.database.diskservice.purveyor.PurveyorDiskService
import tusur.kkv.data.database.diskservice.service.ServiceDiskService
import tusur.kkv.data.injection.qualifier.ForComputation
import javax.inject.Singleton

@Module(includes = [
    EmployeeDataModule::class,
    PositionDataModule::class,
    ComponentDataModule::class,
    ComponentTypeDataModule::class,
    PurveyorDataModule::class,
    CustomerDataModule::class,
    ServiceDataModule::class,
    OrderDataModule::class,
    JoinDataModule::class
])
class DataModule {

    @Provides
    fun provideTimestampProvider(): TimestampProvider = object : TimestampProvider {
        override fun currentTimeMillis() = System.currentTimeMillis()
    }

    @Provides
    fun provideTables(employeeDiskService: EmployeeDiskService,
                      positionDiskService: PositionDiskService,
                      componentDiskService: ComponentDiskService,
                      componentTypeDiskService: ComponentTypeDiskService,
                      purveyorDiskService: PurveyorDiskService,
                      serviceDiskService: ServiceDiskService,
                      orderServiceJoinDiskService: OrderServiceJoinDiskService,
                      orderComponentJoinDiskService: OrderComponentJoinDiskService,
                      orderDiskService: OrderDiskService,
                      customerDiskService: CustomerDiskService): Tables =
            Tables(listOf(employeeDiskService,
                    positionDiskService,
                    purveyorDiskService,
                    componentTypeDiskService,
                    componentDiskService,
                    serviceDiskService,
                    orderServiceJoinDiskService,
                    orderComponentJoinDiskService,
                    orderDiskService,
                    customerDiskService
            ))

    @Provides
    fun provideQueryable(databaseGateway: DatabaseGateway): Queryable =
            object : Queryable {
                override fun create(tableName: String): IQueryBuilder =
                        DatabaseGateway.QueryBuilder(tableName, databaseGateway)
            }

    @Provides
    @Singleton
    @ForComputation
    fun provideComputationScheduler(): Scheduler = Schedulers.computation()

}