package tusur.kkv.computerfirm.injection.module

import dagger.Module
import dagger.Provides
import tusur.kkv.computerfirm.base.BaseDialogFragment
import tusur.kkv.computerfirm.base.BasePresenter
import tusur.kkv.computerfirm.base.BaseView

@Module
abstract class DialogModule<F : BaseDialogFragment<out BasePresenter>, V : BaseView>(protected val fragment: F,
                                                                                     private val view: V) {
    @Provides
    fun provideFragment() = fragment

    @Provides
    fun provideView(): V = view
}
