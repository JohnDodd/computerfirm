package tusur.kkv.computerfirm.injection.module

import com.annimon.stream.Optional
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import tusur.kkv.data.base.TimestampProvider
import tusur.kkv.data.cache.Cache
import tusur.kkv.data.database.base.DateFormatter
import tusur.kkv.data.database.diskservice.employee.EmployeeDiskService
import tusur.kkv.data.database.mapper.BestEmployeeRawFactory
import tusur.kkv.data.database.mapper.EmployeeRawFactory
import tusur.kkv.data.database.mapper.RawFactory
import tusur.kkv.data.exception.typeformatter.ExceptionTypeFormatter
import tusur.kkv.data.injection.qualifier.ForComputation
import tusur.kkv.data.mapper.BestEmployeeMapper
import tusur.kkv.data.mapper.EmployeeMapper
import tusur.kkv.data.model.EmployeeRaw
import tusur.kkv.data.repository.EmployeeRepository
import tusur.kkv.data.store.MemoryReactiveStore
import tusur.kkv.data.store.ReactiveStore
import tusur.kkv.data.store.Store.MemoryStore
import tusur.kkv.domain.model.Employee
import tusur.kkv.domain.repository.IEmployeeRepository
import tusur.kkv.domain.repository.IPositionRepository
import javax.inject.Singleton

@Module
class EmployeeDataModule {

    @Provides
    @Singleton
    fun provideEmployeeRepository(positionRepository: IPositionRepository,
                                  dateFormatter: DateFormatter,
                                  typeFormatter: ExceptionTypeFormatter,
                                  mapper: EmployeeMapper,
                                  diskService: EmployeeDiskService,
                                  bestEmployeeRawFactory: BestEmployeeRawFactory,
                                  bestEmployeeMapper: BestEmployeeMapper,
                                  memoryStore: ReactiveStore<Long, Employee>): IEmployeeRepository =
            EmployeeRepository(positionRepository,
                    dateFormatter,
                    typeFormatter,
                    bestEmployeeRawFactory,
                    bestEmployeeMapper,
                    mapper,
                    diskService,
                    memoryStore)


    @Provides
    @Singleton
    fun provideRawFactory(): RawFactory<EmployeeRaw> = EmployeeRawFactory()

    @Provides
    @Singleton
    fun provideMemoryReactiveStore(cache: MemoryStore<Long, Employee>,
                                   @ForComputation
                                   schedulerComputation: Scheduler): ReactiveStore<Long, Employee> =
            MemoryReactiveStore(cache, { it.id }, schedulerComputation)


    @Provides
    @Singleton
    fun provideMemoryStore(timestampProvider: TimestampProvider,
                           @ForComputation
                           schedulerComputation: Scheduler): MemoryStore<Long, Employee> =
            Cache(timestampProvider, { it.id }, Optional.empty(), schedulerComputation)


}