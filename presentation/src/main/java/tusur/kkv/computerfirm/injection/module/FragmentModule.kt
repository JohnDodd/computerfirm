package tusur.kkv.computerfirm.injection.module

import dagger.Module
import dagger.Provides
import tusur.kkv.computerfirm.base.BaseFragment
import tusur.kkv.computerfirm.base.BasePresenter
import tusur.kkv.computerfirm.base.BaseView

@Module
abstract class FragmentModule<F : BaseFragment<out BasePresenter>, V : BaseView>(protected val fragment: F,
                                                                                 private val view: V) {

    @Provides
    fun provideFragment() = fragment

    @Provides
    fun provideView(): V = view
}