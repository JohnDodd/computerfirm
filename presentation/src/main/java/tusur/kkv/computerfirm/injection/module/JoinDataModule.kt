package tusur.kkv.computerfirm.injection.module

import dagger.Module
import dagger.Provides
import tusur.kkv.data.database.diskservice.ordercomponentjoin.OrderComponentJoinDiskService
import tusur.kkv.data.database.diskservice.orderservicejoin.OrderServiceJoinDiskService
import tusur.kkv.data.repository.OrderComponentRepository
import tusur.kkv.data.repository.OrderServiceRepository
import tusur.kkv.domain.repository.*
import javax.inject.Singleton

@Module
class JoinDataModule {

    @Provides
    @Singleton
    fun provideOrderServiceRepository(joinDiskService: OrderServiceJoinDiskService,
                                      firstRepository: IOrderRepository,
                                      secondRepository: IServiceRepository): IOrderServiceRepository =
            OrderServiceRepository(joinDiskService, firstRepository, secondRepository)

    @Provides
    @Singleton
    fun provideOrderComponentRepository(joinDiskService: OrderComponentJoinDiskService,
                                        firstRepository: IOrderRepository,
                                        secondRepository: IComponentRepository): IOrderComponentRepository =
            OrderComponentRepository(joinDiskService, firstRepository, secondRepository)

}