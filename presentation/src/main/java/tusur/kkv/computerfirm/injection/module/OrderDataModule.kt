package tusur.kkv.computerfirm.injection.module

import com.annimon.stream.Optional
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import tusur.kkv.data.base.TimestampProvider
import tusur.kkv.data.cache.Cache
import tusur.kkv.data.database.base.DateFormatter
import tusur.kkv.data.database.diskservice.order.OrderDiskService
import tusur.kkv.data.database.mapper.OrderRawFactory
import tusur.kkv.data.database.mapper.RawFactory
import tusur.kkv.data.exception.typeformatter.ExceptionTypeFormatter
import tusur.kkv.data.injection.qualifier.ForComputation
import tusur.kkv.data.mapper.OrderMapper
import tusur.kkv.data.model.OrderRaw
import tusur.kkv.data.repository.OrderRepository
import tusur.kkv.data.store.MemoryReactiveStore
import tusur.kkv.data.store.ReactiveStore
import tusur.kkv.data.store.Store
import tusur.kkv.domain.model.Order
import tusur.kkv.domain.repository.ICustomerRepository
import tusur.kkv.domain.repository.IEmployeeRepository
import tusur.kkv.domain.repository.IOrderRepository
import javax.inject.Singleton

@Module
class OrderDataModule {

    @Provides
    @Singleton
    fun provideRepository(diskService: OrderDiskService,
                          memoryStore: ReactiveStore<Long, Order>,
                          mapper: OrderMapper,
                          employeeRepository: IEmployeeRepository,
                          customerRepository: ICustomerRepository,
                          dateFormatter: DateFormatter,
                          exceptionTypeFormatter: ExceptionTypeFormatter): IOrderRepository =
            OrderRepository(
                    employeeRepository,
                    customerRepository,
                    dateFormatter,
                    exceptionTypeFormatter,
                    mapper,
                    diskService,
                    memoryStore
            )


    @Provides
    @Singleton
    fun provideRawFactory(): RawFactory<OrderRaw> = OrderRawFactory()


    @Provides
    @Singleton
    fun provideMemoryReactiveStore(cache: Store.MemoryStore<Long, Order>,
                                   @ForComputation
                                   schedulerComputation: Scheduler): ReactiveStore<Long, Order> =
            MemoryReactiveStore(cache, { it.id }, schedulerComputation)


    @Provides
    @Singleton
    fun provideMemoryStore(timestampProvider: TimestampProvider,
                           @ForComputation
                           schedulerComputation: Scheduler): Store.MemoryStore<Long, Order> =
            Cache(timestampProvider, { it.id }, Optional.empty(), schedulerComputation)

} 