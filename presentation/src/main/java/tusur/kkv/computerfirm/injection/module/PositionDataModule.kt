package tusur.kkv.computerfirm.injection.module

import com.annimon.stream.Optional
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import tusur.kkv.data.base.TimestampProvider
import tusur.kkv.data.cache.Cache
import tusur.kkv.data.database.diskservice.position.PositionDiskService
import tusur.kkv.data.database.mapper.PositionRawFactory
import tusur.kkv.data.database.mapper.RawFactory
import tusur.kkv.data.exception.typeformatter.ExceptionTypeFormatter
import tusur.kkv.data.injection.qualifier.ForComputation
import tusur.kkv.data.mapper.PositionMapper
import tusur.kkv.data.model.PositionRaw
import tusur.kkv.data.repository.PositionRepository
import tusur.kkv.data.store.MemoryReactiveStore
import tusur.kkv.data.store.Store
import tusur.kkv.data.store.Store.MemoryStore
import tusur.kkv.domain.model.Position
import tusur.kkv.domain.repository.IPositionRepository
import javax.inject.Singleton

@Module
class PositionDataModule {

    @Provides
    @Singleton
    fun providePositionRepository(exceptionTypeFormatter: ExceptionTypeFormatter,
                                  diskStore: PositionDiskService,
                                  memoryStore: MemoryReactiveStore<Long, Position>,
                                  mapper: PositionMapper): IPositionRepository =
            PositionRepository(exceptionTypeFormatter, diskStore, memoryStore, mapper)

    @Provides
    @Singleton
    fun provideRawFactory(): RawFactory<PositionRaw> = PositionRawFactory()

    @Provides
    @Singleton
    fun provideMemoryStore(timestampProvider: TimestampProvider,
                           @ForComputation
                           schedulerComputation: Scheduler): MemoryStore<Long, Position> =
            Cache(timestampProvider, { it.id }, Optional.empty(), schedulerComputation)

    @Provides
    @Singleton
    fun provideMemoryReactiveStore(cache: Store.MemoryStore<Long, Position>,
                                   @ForComputation
                                   schedulerComputation: Scheduler): MemoryReactiveStore<Long, Position> =
            MemoryReactiveStore(cache, { it.id }, schedulerComputation)
}