package tusur.kkv.computerfirm.injection.module

import com.annimon.stream.Optional
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import tusur.kkv.data.base.TimestampProvider
import tusur.kkv.data.cache.Cache
import tusur.kkv.data.database.diskservice.purveyor.PurveyorDiskService
import tusur.kkv.data.database.mapper.PurveyorRawFactory
import tusur.kkv.data.database.mapper.RawFactory
import tusur.kkv.data.exception.typeformatter.ExceptionTypeFormatter
import tusur.kkv.data.injection.qualifier.ForComputation
import tusur.kkv.data.mapper.PurveyorMapper
import tusur.kkv.data.model.PurveyorRaw
import tusur.kkv.data.repository.PurveyorRepository
import tusur.kkv.data.store.MemoryReactiveStore
import tusur.kkv.data.store.ReactiveStore
import tusur.kkv.data.store.Store
import tusur.kkv.domain.model.Purveyor
import tusur.kkv.domain.repository.IPurveyorRepository
import javax.inject.Singleton

@Module
class PurveyorDataModule {

    @Provides
    @Singleton
    fun provideRepository(diskService: PurveyorDiskService,
                          memoryStore: ReactiveStore<Long, Purveyor>,
                          mapper: PurveyorMapper,
                          exceptionTypeFormatter: ExceptionTypeFormatter): IPurveyorRepository =
            PurveyorRepository(diskService, memoryStore, mapper, exceptionTypeFormatter)

    @Provides
    @Singleton
    fun provideRawFactory(): RawFactory<PurveyorRaw> = PurveyorRawFactory()

    @Provides
    @Singleton
    fun provideMemoryReactiveStore(cache: Store.MemoryStore<Long, Purveyor>,
                                   @ForComputation
                                   schedulerComputation: Scheduler): ReactiveStore<Long, Purveyor> =
            MemoryReactiveStore(cache, { it.id }, schedulerComputation)


    @Provides
    @Singleton
    fun provideMemoryStore(timestampProvider: TimestampProvider,
                           @ForComputation
                           schedulerComputation: Scheduler): Store.MemoryStore<Long, Purveyor> =
            Cache(timestampProvider, { it.id }, Optional.empty(), schedulerComputation)


}