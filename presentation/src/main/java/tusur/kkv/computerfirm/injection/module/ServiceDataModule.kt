package tusur.kkv.computerfirm.injection.module

import com.annimon.stream.Optional
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import tusur.kkv.data.base.TimestampProvider
import tusur.kkv.data.cache.Cache
import tusur.kkv.data.database.diskservice.service.ServiceDiskService
import tusur.kkv.data.database.mapper.BestServiceRawFactory
import tusur.kkv.data.database.mapper.RawFactory
import tusur.kkv.data.database.mapper.ServiceRawFactory
import tusur.kkv.data.exception.typeformatter.ExceptionTypeFormatter
import tusur.kkv.data.injection.qualifier.ForComputation
import tusur.kkv.data.mapper.BestServiceMapper
import tusur.kkv.data.mapper.ServiceMapper
import tusur.kkv.data.model.ServiceRaw
import tusur.kkv.data.repository.ServiceRepository
import tusur.kkv.data.store.MemoryReactiveStore
import tusur.kkv.data.store.ReactiveStore
import tusur.kkv.data.store.Store
import tusur.kkv.domain.model.Service
import tusur.kkv.domain.repository.IServiceRepository
import javax.inject.Singleton

@Module
class ServiceDataModule {

    @Provides
    @Singleton
    fun provideRepository(diskService: ServiceDiskService,
                          memoryStore: ReactiveStore<Long, Service>,
                          mapper: ServiceMapper,
                          bestServiceRawFactory: BestServiceRawFactory,
                          bestServiceMapper: BestServiceMapper,
                          exceptionTypeFormatter: ExceptionTypeFormatter): IServiceRepository =
            ServiceRepository(diskService,
                    memoryStore,
                    mapper,
                    bestServiceRawFactory,
                    bestServiceMapper,
                    exceptionTypeFormatter)


    @Provides
    @Singleton
    fun provideRawFactory(): RawFactory<ServiceRaw> = ServiceRawFactory()


    @Provides
    @Singleton
    fun provideMemoryReactiveStore(cache: Store.MemoryStore<Long, Service>,
                                   @ForComputation
                                   schedulerComputation: Scheduler): ReactiveStore<Long, Service> =
            MemoryReactiveStore(cache, { it.id }, schedulerComputation)


    @Provides
    @Singleton
    fun provideMemoryStore(timestampProvider: TimestampProvider,
                           @ForComputation
                           schedulerComputation: Scheduler): Store.MemoryStore<Long, Service> =
            Cache(timestampProvider, { it.id }, Optional.empty(), schedulerComputation)

}