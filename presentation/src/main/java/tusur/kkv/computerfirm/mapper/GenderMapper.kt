package tusur.kkv.computerfirm.mapper

import android.content.Context
import io.reactivex.functions.Function
import tusur.kkv.computerfirm.R
import tusur.kkv.data.injection.qualifier.ForApplication
import tusur.kkv.domain.model.Gender
import javax.inject.Inject

class GenderMapper @Inject constructor(@ForApplication private val context: Context)
    : Function<Gender, String> {

    override fun apply(gender: Gender): String =
            when (gender) {
                Gender.MALE -> context.getString(R.string.gender_male)
                Gender.FEMALE -> context.getString(R.string.gender_female)
                Gender.UNKNOWN -> context.getString(R.string.gender_unknown)
            }
}