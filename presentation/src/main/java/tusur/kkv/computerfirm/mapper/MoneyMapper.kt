package tusur.kkv.computerfirm.mapper

import android.content.Context
import io.reactivex.functions.Function
import tusur.kkv.computerfirm.R
import tusur.kkv.data.injection.qualifier.ForApplication
import javax.inject.Inject

class MoneyMapper
@Inject constructor(@ForApplication private val context: Context) : Function<Long, String> {

    override fun apply(salary: Long): String = context.getString(R.string.salary_rubs, salary)

}