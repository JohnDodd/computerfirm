package tusur.kkv.computerfirm.utils

import android.content.res.Resources
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewFinder

fun <T> T.logd(message: Any? = ""): T {
    Throwable().stackTrace[1].apply {
        Log.d(className.substringAfterLast("."), "$methodName:$lineNumber: $message")
    }
    return this
}

fun <T : Any> T.logd(message: String = "", stackDeep: Int): T {
    Throwable().stackTrace.apply {
        val builder = StringBuilder()
        val stackDeepCoerced = stackDeep.coerceIn(1, size)
        for ((index, stackTraceElement) in withIndex()) {
            if (index == 0) {
                continue
            }
            if (index == 1) {
                builder.append("${stackTraceElement.methodName}:${stackTraceElement.lineNumber}: $message")
                continue
            }
            if (index > stackDeepCoerced)
                break
            stackTraceElement.apply {
                builder.append("\n${className.substringAfterLast(".")}:$methodName:$lineNumber")
            }
        }
        Log.d(this@logd.javaClass.simpleName,
                builder.apply { if (message.isNotBlank()) append(" - $message") }.toString())
    }
    return this
}

fun <T> T.loge(message: Any = "", error: Throwable): T {
    Throwable().stackTrace[1].apply {
        Log.e(className.substringAfterLast("."), "$methodName:$lineNumber: $message", error)
    }
    return this
}

fun <T> T.alert(message: Any = "", error: Throwable): T {
    loge(message, error)
    throw error
    return this
}

fun Int.toDp() = (this / Resources.getSystem().displayMetrics.density).toInt()

fun Int.toPx() = (this * Resources.getSystem().displayMetrics.density).toInt()

fun String.toLongOrElse(default: Long) = try {
    toLong()
} catch (e: NumberFormatException) {
    default
}

inline fun <reified VM : ViewModel> RendererRecyclerViewAdapter.register(layoutId: Int,
                                                                         crossinline creator: ViewFinder.(VM) -> Unit) {
    registerRenderer(ViewBinder<VM>(layoutId, VM::class.java)
    { model, finder, _ -> return@ViewBinder finder.creator(model) })
}

fun View.onClick(f: () -> Unit) = setOnClickListener { f() }

fun ViewFinder.onClick(f: () -> Unit) = setOnClickListener { f() }

fun ViewFinder.onLongClick(f: () -> Unit) = getRootView<View>().setOnLongClickListener {
    f()
    true
}

fun TextView.onImeActionDone(f: () -> Unit) = setOnEditorActionListener { v, actionId, event ->
    if (actionId == EditorInfo.IME_ACTION_DONE) {
        f()
        return@setOnEditorActionListener true
    }
    false
}

fun Any?.toStringOrEmpty() = this?.toString() ?: ""