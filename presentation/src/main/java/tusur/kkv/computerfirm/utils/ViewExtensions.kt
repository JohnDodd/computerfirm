package tusur.kkv.computerfirm.utils

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.snackbar.Snackbar.LENGTH_SHORT

fun View.showSnack(message: String, length: Int = LENGTH_SHORT) {
    Snackbar.make(this, message, length).show()
}

fun ViewGroup.inflate(layoutId: Int): View =
        LayoutInflater.from(context).inflate(layoutId, this, false)